*在Anolis OS发布8.8后，龙芯平台的用户可以用过yum仓库直接升级到最新版本*

1. 降级libffi
执行yum downgrade libffi以降低其的版本，因为8.4该软件包未能实现同源异构所以libffi在8.4中的版本要高于8.8中的版本。

2. 升级系统
运行yum update升级系统。如果系统上已经安装了gvfs-afc，那么需要额外增加参数--allowerasing，即运行yum update --allowerasing。因为gvfs-afc在8.8中已经不再需要。
