# 龙蜥社区产品文档

<!-- toc -->
## Anolis OS 龙蜥操作系统

### 产品策略
+ 策略文档 | [生命周期及更新策略](/anolis/policy/life-cycle.md)
+ 认证测试 | [软硬件兼容性测试](/anolis/policy/compatibility-cert.md) | [衍生版认证](/anolis/policy/derivative-cert.md)

### 发行声明
版本 | 发行声明链接 | 软件包发布清单
-----|--------------|---------------
Anolis OS 23.0 | [Beta 发行声明](/anolis/rnotes/anolis-23.0-beta.md) | [软件包清单](/anolis/rnotes/anolis-23.0-beta-pkglist.md)
Anolis OS 23.0 | [GA 发行声明](/anolis/rnotes/anolis-23.0-ga.md) | [软件包清单](/anolis/rnotes/anolis-23.0-ga-pkglist.md)
Anolis OS 8.8  | [QU1 发行声明](/anolis/rnotes/anolis-8.8-qu1.md)   <br/>  [GA 发行声明](/anolis/rnotes/anolis-8.8.md) | [软件包清单](/anolis/rnotes/anolis-8.8-pkglist.md)
Anolis OS 8.6  | [QU1 发行声明](/anolis/rnotes/anolis-8.6-qu1.md) <br/>  [GA 发行声明](/anolis/rnotes/anolis-8.6.md) | [软件包清单](/anolis/rnotes/anolis-8.6-pkglist.md)
Anolis OS 8.4  | [GA 发行声明](/anolis/rnotes/anolis-8.4.md) | [软件包清单](/anolis/rnotes/anolis-8.4-pkglist.md)
Anolis OS 8.2  | [QU2 发行声明](/anolis/rnotes/anolis-8.2-qu2.md) <br/> [QU1 发行声明](/anolis/rnotes/anolis-8.2-qu1.md) <br/> [GA 发行声明](/anolis/rnotes/anolis-8.2.md) | [软件包清单](/anolis/rnotes/anolis-8.2-pkglist.md)

### 手册
* 用户手册
    * [安装 Anolis OS](/anolis/manual/installation.md)
* [管理员手册](/anolis/admin_guide/README.md)

### 其他
* 技术文档
    * [龙蜥八大技术方向](https://anolis.gitee.io/anolis_features)
* [知识库](/anolis/kbase/README.md)
* [FAQ](/anolis/faq.md)

## ANCK 云内核
+ [内核生命周期及更新策略](/anck/kernel-life-cycle.md)
+ 最新版本发行声明
   + ANCK-5.10-013 | [发行声明](/anck/rnotes/anck-5.10-013.md)
   + ANCK-4.19-027 | [发行声明](/anck/rnotes/anck-4.19-027.md)

<!-- endtoc -->
