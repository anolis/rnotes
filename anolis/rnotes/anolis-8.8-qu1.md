Anolis OS 8.8 qu1 LoongArch64 发行声明
=====================

<!-- toc -->
## 1. 引言
龙蜥操作系统 Anolis OS 8.8 是 OpenAnolis 龙蜥社区发行的开源 Linux 发行版，支持多计算架构，提供稳定、高性能、安全、可靠的操作系统支持。
在 OpenAnolis 社区评估认为有必要的前提下，Anolis OS 会推送优选更新(Quality Updates)版本，该版本是基于特定小版本发布的增量更新版本。在优选更新版本中，通常会更新下列内容：
+ ANCK 的最新版本；
+ 包含最新的安全漏洞修复(ANSA)的软件包；
+ OpenAnolis 社区推荐的其他必要的缺陷修复和功能增强。
优选更新版本的发布不定期，但通常不会频繁发布。优选更新版本发布后，一般推荐所有用户下载并升级到该版本，同一个小版本的上一次发布则自动标记为过期版本。

本次发布的是 Anolis OS 8.8 第一个优选更新版本(QU1),主要对龙芯架构内核、虚拟化kvm等特性升级，本文档提供了该版本的交付物清单与获取版本的方式，并介绍了该版本中的新特性、功能改进和缺陷修复等发布详情，以及介绍了该版本的已知问题和其他发布详情信息。

## 2. 交付物清单
{% hint style='tip' %}
该版本发布的所有交付物清单及下载链接，可以在[社区网站](https://openanolis.cn/download)中找到详细信息。
{% endhint %}

### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-8.8-loongarch64-dvd.iso | loongarch64 架构的基础安装 ISO, 约 7.6 GB
AnolisOS-8.8-loongarch64-minimal.iso | loongarch64 架构的精简安装 ISO, 约 1.7 GB
AnolisOS-8.8-loongarch64-boot.iso | loongarch64 架构的网络安装 ISO, 约 797 MB

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
AnolisOS-8.8-loongarch64.qcow2	| loongarch64 架构 QEMU 虚拟机镜像（qcow2 格式）

{% hint style='info' %}
镜像缺省 sudo 用户为 `anuser`，对应登录密码是 `anolisos`.
{% endhint %}

### 2.3 容器镜像
名称 | 描述
-----|-----
AnolisOS-8.8-loongarch64-docker.tar  | loongarch64 架构本地容器镜像


### 2.4 软件 YUM 仓库
名称 | 描述
-----|-----
BaseOS      | BaseOS 软件包源，该源目的是提供安装基础的所有核心包。
AppStream   | AppStream 软件包源，该源提供额外的多场景，多用途的用户态程序，数据库等。
DDE         | DDE 软件包源，提供 DDE 桌面环境以及相应的组件。

## 3. 发布详情
### 3.1 概述
#### 3.1.1 亮点
- **内核**：Anolis OS 8.8 qu1 LoongArch64架构现支持 4.19.190-7.6版本，ISO 与 repo 均可以获取可用内核包。
- **更完善地支持 loongarch64 架构平台。**
    + 本次loongarch64架构内核代码开源,代码可在社区仓库下载获取。
    + ISO 和 repo 中均增加了官方的 loongarch64 包获取途径。

### 3.2 L0 层软件（内核层）
#### 3.2.1 LoongArch64 Kernel 4.19.190-7.6

**发行版默认4.19.190。**  可以在系统内执行下列命令查看对应的内核版本信息：
```bash
$ uname -r
4.19.190-7.6.an8.loongarch64
```

+ 新增3D5000 CPU支持
+ 新增2k0500 BMC驱动支持
+ 新增KASLR功能支持
+ 新增kgdb功能支持
+ 新增内核与bootloader接口规范4.0版本支持
+ 新增统一kdump生产内核与捕获内核二进制支持
+ 新增沐创网卡驱动支持


### 3.3 L1 层（核心层）软件
#### 3.3.1 qemu/kvm
+ 虚拟机支持双桥iommu
+ 虚拟机支持pv-spinlock
+ 虚拟机支持lbt 二进制翻译功能



## 4. 已知问题
+ [Bug 3571](https://bugzilla.openanolis.cn/show_bug.cgi?id=3571) - gvfs-afc 依赖问题。 
        运行 `yum update` 升级系统。如果系统上已经安装了 gvfs-afc ，那么需要额外增加参数`--allowerasing`，即运行 `yum update --allowerasing`。因为 gvfs-afc 在 Anolis OS 8.8 中已经不再提供。影响范围：全平台。该问题将在发布后修复。
+ [Bug 3571](https://bugzilla.openanolis.cn/show_bug.cgi?id=3571) - 降级libffi问题。 
        执行yum downgrade libffi以降低其的版本，因为8.4该软件包未能实现同源异构所以libffi在8.4中的版本要高于8.8中的版本。
+ [Issue](https://gitee.com/src-anolis-sig/loongarch-kernel/issues/I7JIAU?from=project-issue) - minimal iso未集成perf。 
        龙芯4.19内核源码来源于linux kernel上游社区，perf依赖关系也来源于上游社区，依赖python2（module类包）,由于嵌入代码内部，本次minimal iso不集成perf。

## 5. 特别声明
Anolis OS 8 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第 2 版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 6. 致谢
感谢龙蜥社区对 Anolis OS 8.8 qu1 LoongArch64版本的大力支持。

## 7. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
<!-- endtoc -->
