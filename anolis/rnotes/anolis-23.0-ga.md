Anolis OS 23.0 社区版 (GA) 发行声明
=====================================

## 1. 引言
龙蜥操作系统 Anolis OS 23 是 OpenAnolis 龙蜥社区基于操作系统分层分类理论，面向上游原生社区独立选型，全面支持智算的首款国产操作系统。

当前 Anolis OS 23 最新的版本号为 Anolis OS 23.0 社区版（GA).

## 2. 交付物清单
### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-23.0-x86_64-dvd.iso  | x86\_64 架构的基础安装 ISO, 约 15 GB 
AnolisOS-23.0-x86_64-boot.iso | x86\_64 架构的网络安装 ISO，约 0.6 GB 
AnolisOS-23.0-aarch64-dvd.iso  | aarch64 架构的基础安装 ISO，约 15 GB 
AnolisOS-23.0-aarch64-boot.iso | aarch64 架构的网络安装 ISO，约 0.6 GB 

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
AnolisOS-23.0-x86\_64.vhd   | x86\_64 架构 QEMU 虚拟机镜像(vhd 格式)
AnolisOS-23.0-x86\_64.qcow2 | x86\_64  架构 QEMU 虚拟机镜像(qcow2 格式)
AnolisOS-23.0-aarch64.vhd   | aarch64 架构虚拟机镜像(vhd 格式)
AnolisOS-23.0-aarch64.qcow2 | aarch64 架构虚拟机镜像(qcow2 格式)

镜像缺省 sudo 用户为 anuser，对应登录密码是 anolisos.

### 2.3 容器镜像
名称 | 描述
-----|-----
registry.openanolis.cn/openanolis/anolisos:23 | baseos 容器镜像 
registry.openanolis.cn/openanolis/anolisos:23-busybox | 精简镜像 busybox，大小仅 2.8M，最精简镜像
registry.openanolis.cn/openanolis/anolisos:23-micro | 精简镜像 micro，仅支持 microdnf，大小 29M
registry.openanolis.cn/openanolis/anolisos:23-minimal | 精简镜像 minimal，仅支持 dnf，大小 39M
anolis-23-GA-aarch64-SiliconFastOS.tar.gz | aarch64 架构本地 SiliconFastOS 镜像 

### 2.4 软件 YUM 仓库
名称 | 描述| 仓库状态 
-----|-----|-----
[os](https://mirrors.openanolis.cn/anolis/23/os/)    | 版本正式发布的 ISO 交付件，所包括的所有的包。该仓库与 ISO 交付件包含的软件包完全一致。 | 默认开启 
[updates](https://mirrors.openanolis.cn/anolis/23/updates/) | 版本正式发布后，更新的软件包的存放仓库。该仓库会持续更新，直到该版本生命周期结束。 | 默认开启 
[kernel-6](https://mirrors.openanolis.cn/anolis/23/kernel-6/)      | Kernel-6 软件包源，提供社区滚动内核（6.x）以及对应的组件。 | 不默认开启，需要使用 enablerepo=kernel-6 
[epao](https://mirrors.openanolis.cn/epao/23/) | EAPO（Extras Package For Anolis））软件包仓库提供社区孵化类软件，比如：AI 组件等。 | 不默认开启，需要安装 anolis-epao-release 后使用 

## 3. 发布详情
### 3.1 概述
#### 3.1.1 亮点
- **双内核**：Anolis OS 23 现支持 5.10/6.1 两种内核安装，5.10 为默认内核，内核 6.1 系列为 tech-preview 版本，支持滚动升级
- **智算能力支持：**
  - 提供 rpm 格式的 AI 组件，支持用户使用 yum install 安装 AI 相关组件。
  - 提供 tensorflow2，支持深度学习框架能力
  - 提供 pytorch，支持神经网络框架能力

- **核心组件自维护**：gcc 12.2.1、binutils 2.39 、llvm 16.0.1、clang 16.0.1、perl 5.36.0 、python 3.10 等，已不再支持 python2。
- **重点组件自研替换：**
  - 提供性能调优组件 keentuned，已默认安装，并实现部分功能替代 tuned
  - 提供 dragonwell 8/11/17 三个版本的 java 组件，替代开源 openjdk 系列
  - 提供 noslate-anode 替代 nodejs 
- **重点 sig 特性，自研发：**
  - 提供系统运维组件 sysak、sysom、ssar
  - 提供开源版 polardb 数据库
  - 提供 alibaba-cloud-compiler  基于 Clang/LLVM 开发的面向大规模云业务场景的 C/C++ 编译器
  - 提供 tone-cli 组件，用于执行测试用例的自动化测试框架
- **提供广泛开源软件：**
  - 提供 intel SPR 用户态工具
  - 提供 intel 机密计算工具
  - 提供云上应用软件 nginx、httpd、mysql、mariadb、redis、php、nodejs、memcached、tomcat
  - 提供云原生组件，containerd、kata-containers、kubernetes、nydus-rs、nydus-snapshotter、nerdctl

#### 3.1.2 发行版整体支持
Anolis OS 23.0 GA 版本默认提供下列镜像介质：
+ ISO 镜像；
+ vhd 和 qcow2 格式的虚拟机镜像，适用于 QEMU/KVM 虚拟化平台；
+ 线上 baseos 容器镜像，适用于运行不同引擎的容器场景。
+ 额外提供三种线上精简容器镜像，适用于不用容器场景

#### 3.1.3 平台支持
+ **体系结构支持**: Anolis OS 23.0 GA 版本支持 x86\_64 架构、aarch64 架构。
+ **微架构支持**: Anolis OS 23.0 GA 版本分别支持 x86\_64-v2 及 armv8-a 微架构，并提供对 armv9-a 的支持能力。
+ **CPU 芯片支持**: Anolis OS 23.0 GA 版本提供对主流 CPU 芯片厂商的支持能力（仅涉及 5.10 内核），详细支持情况如下：

厂商   | 架构    | CPU 型号
-------|---------|---------
Intel  | x86\_64 | Intel(R) Xeon(R) CPU E5-2678 v3 @ 2.50GHz、Intel(R) Xeon(R) Gold 6330N CPU @ 2.20GHz、Intel(R) Xeon(R) Gold 6330 CPU @ 2.00GHz 
AMD    | x86\_64 | AMD EPYC 7402 24-Core Processor、AMD EPYC 9224 24-Core Processor 
海光   | x86\_64 | Hygon C86 7280 32-core Processor 
飞腾   | aarch64 | Phytium S2500/64、FT-2000+/64 
兆芯   | aarch64 | ZHAOXIN KaiSheng KH-37800D@2.7GHz 
平头哥 | aarch64 | YT710
鲲鹏   | aarch64 | Kunpeng 920-4826、 

+ **虚拟化平台支持**: Anolis OS 23 GA 提供对如下虚拟化平台的支持能力：
    + QEMU/KVM
+ **桌面平台支持**：Anolis OS 23 GA 提供了如下桌面的支持能力：
    + gtk4 的支持
    + 最新的 gnome-gui 系列（44）软件


### 3.2 L0 层软件

#### 3.2.1 Kernel 5.10
+ 提供完善的 XFS 文件系统支持能力，虚拟机镜像根文件系统默认采用 XFS 文件系统；
+ 全面支持Intel第四代至强可扩展处理器(code name: Sapphire Rapids) 内核特性， 包括AMX，DSA，IAA，SIOV，电源管理，PCIe Gen5, Perf/PMU, CXL1.1等
+ 内核 CVE 修复。 修复了 CVE-2022-32250, CVE-2022-34918 等重要的 CVE 漏洞。
+ 支持用户态 /dev/ioasid
+ SWIOTLB 机制性能优化
+ virtio-net 打开 napi.tx 优化 TCP Small Queue 性能
+ 支持 AST2600 PCIe 2D VGA Driver
+ 支持 FT2500 处理器
+ 支持动态开启 Group identity 特性
+ arm64 平台默认内核启动 cmdline 调整
+ 添加 Compact Numa Aware（CNA）spinlock 功能支持
+ 丰富 arm64 的 perf mem 和 perf c2c 功能
+ fsck.xfs 支持日志恢复
+ hugetext 自适应按需大页
+ 支持 SGX 动态内存管理
+ 使能 wireguard 模块

#### 3.2.2 Kernel 6.1
+ 基于Linux 6.1.27版本进行开发
+ 使能对Trust Domain Extensions(TDX) guest内核支持，此特性可使内核在tdx guest 中启动。TDX可在虚拟机 (VM) 层面实现更高的机密性，从而更好地保护数据隐私，提高数据控制力。在基于英特尔® TDX 的机密虚拟机中，客户机操作系统和虚拟机应用被隔离开来，无法被云端主机、系统管理程序和平台的其他虚拟机访问。

### 3.3 L1 层软件
#### 3.3.1 核心工具

- **Binutils。** 采用 2.39 版本，支持更可靠的二进制程序的创建和管理工具。[贡献团队：发布小组 SIG]
- **Util-linux。** 采用 2.38.1 版本，提供较新的 Linux 运行必须的各种系统程序。[贡献团队：发布小组 SIG]
- **GCC。** 采用 12.2.1版本，提供更好的 C 和 C++的支持，同时持续改进  RISC-V 和 LoongArch CPU 架构的支持。[贡献团队：发布小组 SIG]
- **Gdb。** 采用 12.1 版本，提供最新版本的调试能力。[贡献团队：发布小组 SIG]
- **Glibc。** 采用 2.36 版本，提供一些新的 API 匹配新版本的 Linux 内核公开的功能，并优化现有功能，同时正在引入 LoongArch CPU 的支持。[贡献团队：发布小组 SIG]
- **Rpm。** 采用 4.18.0 版本，提供更安全的 Linux 软件包管理器和一种新的交互式 shell 工具。[贡献团队：发布小组 SIG]
- **Dnf。** 采用 4.14.0 版本，提供了新一代 RPM 发型版软件包管理器。[贡献团队：发布小组 SIG]

#### 3.3.2 核心库
+ **OpenSSL。** 采用 3.0.7 版本，提供更多的新协议、新算法（包括开启国密SM2 和 SM4 ）及其他各方面的优化支持。[贡献团队：发布小组 SIG]

#### 3.3.3 核心服务

- **dbus。** 采用 1.14.6 版本，提供更全面的系统守护进程，是目前最新的稳定版本。[贡献团队：发布小组 SIG]
- **System-rpm-config。** 采用 23 版本，增加自动对二进制和命令行分别提供 abi 和 api 的能力，直接获取变更信息；同时提供 zst 格式的  man 手册，实现压缩方式最优化。[贡献团队：发布小组 SIG]
- **systemd。** 采用 252.4 版本，默认使能 cgroup v2，提供了更可靠的系统和服务管理器，同时增加新的组件 “systemd-sysupdate” 实现按照配置文件自动更新系统环境，但仍在测试阶段，欢迎使用。[贡献团队：发布小组 SIG]

### 3.4 L2 层软件
#### 3.4.1 系统工具

- **Llvm。** 采用 16.0.1 版本，提供更快的 LLD 链接、默认为 Clang 的 C++17 和 稳定的 LoongArch 的支持。[贡献团队：发布小组 SIG]
- **clang。** 采用 16.0.1 版本，提供 C++ 20 的特性支持，并将 C++/ObjeC++ 的标准变更为 gnu++17 和 gnu++14。[贡献团队：发布小组 SIG]
- **Coreutils。** 采用 9.3 版本，提供最新版基础 Linux 命令行工具。[贡献团队：发布小组 SIG]
- **shadow-utils。** 采用 4.13 版本，提供最新的管理账户和密码文件的应用程序。[贡献团队：发布小组 SIG]
- **zstd。** 采用 1.5.5 版本，新版本优化了压缩速度，提高了压缩比率，并修复了一些问题。[贡献团队：发布小组 SIG]
- 提供较新的语言系列支持。包括 **python3.10** 、**golang 1.20.4**、**rust 1.69.0** 和 **perl 5.36.0**。[贡献团队：发布小组 SIG]

#### 3.4.2 系统库

- **OpenSSH。** 采用到 9.0 p1 版本，提供更大型的安全隧道功能、多种认证方法和更安全的 SSH 协议。[贡献团队：发布小组 SIG]

- **crypto-policies。** 采用到 20221215 版本，在保持兼容性的条件下，提供更新更安全的加密策略。[贡献团队：发布小组 SIG]
- 提供 **ebpf** 能力。包括 **libbpf 1.1.0**、**bcc 0.26.0** 和 **bpftrace** **0.16.0**。[贡献团队：发布小组 SIG]

#### 3.4.3 系统服务

- **firewalld。** 采用 1.2.5 版本，提供更稳定的防火墙守护进程。[贡献团队：发布小组 SIG]
- **iptables。** 采用 1.8.9 版本，提供最新的命令行程序，用于配置 Linux 内核防火墙的 IP 数据包过滤规则。[贡献团队：发布小组 SIG]
- **sssd。** 采用 2.8.2 版本，提供更安全的守护进程来管理远程目录和身份验证机制。[贡献团队：发布小组 SIG]
- **rsyslog。** 采用  8.2212.0 版本，提供最新、更安全的 syslog 的守护进程，能较大程度提高系统性能。[贡献团队：发布小组 SIG]

### 3.5 L3 层软件
#### 3.5.1 应用工具

- **集成热补丁工具。** Anolis OS 23 集成 kpatch、kpatch-build，实现内核补丁管理，可在不重新启动的情况下修复内核问题。[贡献团队：系统运维 SIG]
- **新增软件 buildkit。** buildkit 是一个通过转换源代码，实现高效、易懂和可重复构建的工具包。[贡献团队：云原生 SIG]
- **新增软件 livepatch-mgr。** livepatch-mgr 用来实现 CVE 自动修复能力，保障系统安全。[贡献团队：系统运维 SIG]
- **新增软件 ras-tool。** ras-tools 通过 APEI EINJ 接口在 X86 和 Arm 平台上注入和测试 RAS 能力。[贡献团队：ARM 架构 SIG]
- **新增软件 data-profile-tools。** 用于动态 numa access 和冷热检测工具。[贡献团队：Cloud Kernel SIG]
- **新增软件 ancert。** 提供硬件兼容性测试套件，验证系统与 Anolis OS 的兼容性。[贡献团队：硬件兼容性 SIG]
- **新增软件 tone-cli。** 提供执行测试用例的自动化测试框架。[贡献团队：T-one SIG]

#### 3.5.2 应用库

+ **集成 Intel QAT 驱动。** Anolis OS 23 集成了 Intel QAT 驱动，包括 `intel-QAT20-L.0.9.4-00004.9.an23` 及 `kmod-intel-QAT20-L.0.9.4-00004.10.an23`，以提供对 Intel QAT 加速卡硬件驱动的支持，从而支持加解密和压缩功能的卸载支持。[贡献团队：Intel Arch SIG]
+ **集成 Intel DLB 驱动。** 包括 `kmod-intel_dlb2-7.7.0-2.an23` 及 `libdlb-7.7.0-1.an23`，以提供对 Intel DLB加速卡硬件驱动的支持，从而支持由硬件提供的负载均衡能力。[贡献团队：Intel Arch SIG]
+ **增强 rdma-core 特性。** `rdma-core-44.0-3.an23` 版本增加了 erdma 的支持。[贡献团队：高性能网络技术 SIG]
+ **新增软件 libxudp。** libxudp 是基于 XDP Socket（AF_XDP）实现的 bypass 内核的用户态的高性能 UDP 收发软件库。[贡献团队：高性能网络技术 SIG]
+ **新增软件 libdlb。** libdlb 是在 x86 平台上通过提供跨 CPU 内核的负载均衡、优先级的调度来实现内核的高效通信。 [贡献团队：Intel Arch SIG]
+ **新增软件 alibaba-cloud-compiler。** alibaba-cloud-compiler  基于 Clang/LLVM 开发的面向大规模云业务场景的 C/C++ 编译器。 [贡献团队：编译器 SIG]
+ **集成 Intel SPR 特性。** [贡献团队：Intel Arch SIG]
  + accel-config 提供 3.5.1 版本
  + Gtest 提供 1.13.0 版本
  + Dml 提供 dml-0.1.9~beta 版本
  + qatengine 提供 0.6.19 版本
  + intel-ipp-crypto-mb 提供 1.0.6 版本
  + intel-ipsec-mb 提供 1.3.0 版本
  + qatlib 提供 23.02.0 版本
  + qatzip 提供 1.0.9 版本
  + qpl 提供 0.2.0 版本
  + asynch_mode_nginx 提供 0.5.0 版本，实现异步 Nginx 能力。
+ **集成 Intel 机密特性。** [贡献团队：Intel Arch SIG]
  + he-toolkit 提供 2.0.1 版本
  + seal 提供 3.7.2 版本
  + helib 提供 2.2.1 版本
  + palisade 提供 1.11.6 版本
  + hexl 提供 1.2.3 版本
  + ntl 提供11.5.1 版本
  + cpu_features 提供 0.7.0 版本
  + microsoft-gsl 提供 3.1.0 版本
  + zstd 提供 1.5.5 版本
  + gmp 提供 6.2.1 版本

#### 3.5.3 应用服务

- **新增软件 scheduler-group-identity**。在 x86 架构下实现内核的 Group Identity 功能的调度。[贡献团队：Cloud Kernel SIG]

- **集成系统运维服务**。[贡献团队：系统运维 SIG]
  - sysak 提供 2.2.0 版本，提供了一些系统分析套件。 [贡献团队：系统运维 SIG]
  - sysom 提供 2.0 版本，提供了一个自动化运维平台，包括：主机管理、配置部署、监控告警、异常诊断和安全审计等功能。[贡献团队：系统运维 SIG]
  - ssar 提供 1.0.4 版本，提供更全面的系统日志记录功能。[贡献团队：系统运维 SIG]

### 3.6 其他层
#### 3.6.1 编程语言
+ **dragonwell** 。Anolis OS 23 集成了 1.8.0、11、17 三个版本的 dragonwell 组件分别替代对应版本的 openjdk，并采用 1.8.0 版本作为默认 java 使能。[贡献团队：Java语言与虚拟机 SIG]
+ **noslate-anode** 。采用 16.19.1 版本，是基于原生 nodejs 开发的更快速、可扩展的网络应用程序。[贡献团队：nodejs/web assembly 兴趣小组 SIG]
+ **golang**。采用 1.20.4 版本，在语言、命令和工具链、运行编译、核心库方面都有较高的优化，并支持 RISC-V 架构。[贡献团队：发布小组 SIG]

### 3.7 场景化组件

#### 3.7.1 AI 场景

- **tensorflow** 。 采用 2.12.0 版本，提供深度学习的框架能力。[贡献团队：AI SIG]
- **pytorch**。采用 2.0.1 版本，提供神经网络能力。[贡献团队：AI  SIG]

#### 3.7.2 云原生场景

- **containerd。** 采用 1.6.20 版本，最新版本的容器运行时，作为 Linux 和 Windows 的守护进程使用，可以管理其主机系统的完整容器生命周期：图像传输和存储、容器执行和监督、低级存储和网络附件等。[贡献团队：发布小组 SIG]
- **kata-containers**。采用 3.0.0 版本，致力于构建轻量级虚拟机（VM）的标准实现。[贡献团队：云原生 SIG]
- **kubernetes。** 采用 1.23.3 版本，用于自动部署、扩展和管理容器化应用程序。[贡献团队：发布小组 SIG]
- **cri-rm。** 采用 0.7.2 版本，在 x86 架构下的一种 CRI 资源管理器，能根据各个节点的工作负载调整资源策略。[贡献团队：云原生 SIG]
- **nydus-rs。** 采用2.1.4 版本，提供 Dragonfly 图像服务，实现容器图像的快速、安全和轻松访问。[贡献团队：云原生 SIG]
- **nydus-snapshotter。** 采用 0.5.1 版本，实现容器里重复数据的删除和延迟加载功能。[贡献团队：云原生 SIG]
- **nerdctl。** 采用 1.1.0 版本，是 containerd 的 cli 工具， 同时兼容 docker 。[贡献团队：云原生 SIG]

#### 3.7.3 数据库场景

- **polardb。** 采用 11.9.20.0 版本，是一款基于 PostgreSQL 的开源数据库系统。[贡献团队：发布小组 SIG]
- **mysql。** 采用 8.0.31 版本，提供 mysql 的客户端程序和共享库。[贡献团队：发布小组 SIG]
- **mariadb。** 采用 10.6.8 版本，提供关系型数据库管理系统。[贡献团队：发布小组 SIG]
- **redis。** 采用 7.0.11 版本，提供键值型数据库管理系统。[贡献团队：发布小组 SIG]
- **memcached。** 采用 1.6.18 版本，提供高性能分布式内存对象缓存系统，用于减轻数据库的负载来加速动态 web 程序。[贡献团队：发布小组 SIG]

#### 3.7.4 桌面场景

- **GNOME**
  - gnome 桌面环境采用 44 版本。[贡献团队：发布小组 SIG]
  - 提供常规的桌面能力。[贡献团队：发布小组 SIG]

## 4. 已知问题
+ [Bug 5374](https://bugzilla.openanolis.cn/show_bug.cgi?id=5374) - ISO镜像默认没有 crashkernel 值，需要手动配置并重启后再使用 kdump 服务。
+ [Bug 5391](https://bugzilla.openanolis.cn/show_bug.cgi?id=5391) - 在海光和飞腾服务器上进行稳定性测试，编译并运行 2023 版 ltpstres运行 2 小时左右后中断
+ [Bug 5565](https://bugzilla.openanolis.cn/show_bug.cgi?id=5565) - 倚天服务器在BMC KVM界面远程控制U盘安装操作系统，KVM界面重新加载后会出现报错，无法正常安装系统

## 5. 特别声明
Anolis OS 23 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第2版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 6. 致谢
特别鸣谢 [@fundawang](https://gitee.com/fundawang) 对于 Anolis OS 23 的大量贡献。

## 7. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
