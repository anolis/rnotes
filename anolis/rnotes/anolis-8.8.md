Anolis OS 8.8 发行声明
=====================

<!-- toc -->
## 1. 引言
龙蜥操作系统 Anolis OS 8 是 OpenAnolis 龙蜥社区发行的开源 Linux 发行版，支持多计算架构，提供稳定、高性能、安全、可靠的操作系统支持。本文档是最新的 Anolis OS 8.8 版本的发行声明，提供了该版本的交付物清单与获取版本的方式，并介绍了该版本中的新特性、功能改进和缺陷修复等发布详情，以及介绍了该版本的已知问题和其他发布详情信息。

Anolis OS 8.8 是 Anolis OS 8 发布的第四个小版本。

## 2. 交付物清单
{% hint style='tip' %}
该版本发布的所有交付物清单及下载链接，可以在[社区网站](https://openanolis.cn/download)中找到详细信息。
{% endhint %}

### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-8.8-x86\_64-dvd.iso | x86\_64 架构的基础安装 ISO, 约 15 GB
AnolisOS-8.8-x86\_64-minimal.iso | x86\_64 架构的精简安装 ISO, 约 2.4 GB
AnolisOS-8.8-x86\_64-boot.iso | x86\_64 架构的网络安装 ISO, 约 942 MB
AnolisOS-8.8-aarch64-dvd.iso | aarch64 架构的基础安装 ISO, 约 12 GB
AnolisOS-8.8-aarch64-minimal.iso | aarch64 架构的精简安装 ISO, 约 2.1 GB
AnolisOS-8.8-aarch64-boot.iso | aarch64 架构的网络安装 ISO, 约 886 MB
AnolisOS-8.8-loongarch64-dvd.iso | loongarch64 架构的基础安装 ISO, 约 7.5 GB
AnolisOS-8.8-loongarch64-minimal.iso | loongarch64 架构的精简安装 ISO, 约 1.7 GB
AnolisOS-8.8-loongarch64-boot.iso | loongarch64 架构的网络安装 ISO, 约 793 MB

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
AnolisOS-8.8-x86\_64-ANCK.qcow2 | x86\_64 架构 QEMU 虚拟机镜像（qcow2 格式, 5.10 内核）
AnolisOS-8.8-x86\_64-RHCK.qcow2 | x86\_64 架构 QEMU 虚拟机镜像（qcow2 格式, 4.18 内核）
AnolisOS-8.8-aarch64-ANCK.qcow2	| aarch64 架构 QEMU 虚拟机镜像（qcow2 格式, 5.10 内核）
AnolisOS-8.8-aarch64-RHCK.qcow2	| aarch64 架构 QEMU 虚拟机镜像（qcow2 格式, 4.18 内核）
AnolisOS-8.8-loongarch64.qcow2	| loongarch64 架构 QEMU 虚拟机镜像（qcow2 格式）

{% hint style='info' %}
镜像缺省 sudo 用户为 `anuser`，对应登录密码是 `anolisos`.
{% endhint %}

### 2.3 软件 YUM 仓库
名称 | 描述
-----|-----
BaseOS      | BaseOS 软件包源，该源目的是提供安装基础的所有核心包。
AppStream   | AppStream 软件包源，该源提供额外的多场景，多用途的用户态程序，数据库等。
Plus        | Plus 软件包源，提供社区滚动内核以及相应的组件。
DDE         | DDE 软件包源，提供 DDE 桌面环境以及相应的组件。
kernel-5.10 | 5.10 内核源，提供 5.10 内核包以及相应的组件。

## 3. 发布详情
### 3.1 概述
#### 3.1.1 亮点
- **内核**：Anolis OS 8.8 现支持 4.18/4.19/5.10 三种内核安装，ISO 与 repo 均可以获取可用内核包。
- **桌面环境**：Anolis OS 8.8 正式支持 Deepin Desktop Envionment（DDE），您可以从 ISO 或 repo 中获取该应用。
- **新平台**：Anolis OS 8.8 正式支持 Intel 第四代至强可扩展处理器 (code name: Sapphire Rapids)。
- **新平台**：Anolis OS 8.8 支持 AMD 第四代霄龙处理器 (code name: Genoa)。
- **新架构**：Anolis OS 8.8 正式支持 loongarch64 架构，loongarch64 所用源码与其他架构相同。ISO 和 repo 中均增加了官方的 loongarch64 包获取途径。
- **系统库升级**：openssl 增加国密 SM2 算法支持。

#### 3.1.2 发行版整体支持
- Anolis OS 8.8 ANCK qcow2 镜像内核变更为 5.10.134-13.
- anaconda 增加对龙芯的支持。
- anaconda 在不支持 4.18 的内核平台上将隐藏对应内核安装选项。
- anaconda 优化了版本信息的相关说明，完善了特定场景下反馈信息的表述。

#### 3.1.3 平台支持
- **全面支持Intel第四代至强可扩展处理器(code name: Sapphire Rapids)。**
    + 新增对Sapphire Rapids指令集的支持，包括AMX、AVX2 VNNI等。
    + 新增对DSA、IAA加速器的支持。
    + 新增对Scalable I/O virtualization技术的支持。
    + 在传统特性的基础上，更新了对Sapphire Rapids在电源管理、Perf/PMU等方面的支持。
    + 增强了RAS MCA Recovery的功能。
- **更完善地支持 loongarch64 架构平台。**
    + 本次发布首次实现了大部分包的同源异构，所用代码各架构一致。
    + ISO 和 repo 中均增加了官方的 loongarch64 包获取途径。
    + 增加了 loongarch 架构对 Deepin Desktop Envionment（DDE）组件的支持。
- **支持 AMD 第四代霄龙处理器 (code name: Genoa)。**
    + 新增安全虚拟化（SEV）支持。
    + 新增PTDMA 内存拷贝加速器支持。
    + 在传统特性基础上，增强了freq、温度传感器、内存错误检测/纠正、PMU采样/监控能力。

### 3.2 L0 层软件（内核层）
#### 3.2.1 ANCK-5.10

**发行版默认内核切换到 ANCK-5.10。** 从 Anolis OS 8.8 起，默认内核版本从 ANCK-4.19 开始切换到 ANCK-5.10。对于全新安装的操作系统，无论是通过 ISO 镜像安装，还是启动 Anolis OS 8.8 虚拟机镜像，默认的内核版本是 5.10 版本，Anolis OS 8.8 默认搭载的内核版本是 `5.10.134-13.an8`, 可以在系统内执行下列命令查看对应的内核版本信息：
```bash
$ uname -r
5.10.134-13.an8.x86_64
```
{% hint style='info' %}
注意，如果是从 Anolis OS 8.8 以前的镜像版本升级而来，默认内核版本不会自动升级到 5.10 内核。如果需要手动升级到 5.10 内核、回滚到 4.19 内核以及其他操作，请参阅[ANCK 内核切换说明](ANCK-kernel-switch.md)一文。
{% endhint %}

+ 全面支持Intel第四代至强可扩展处理器(code name: Sapphire Rapids) 内核特性， 包括AMX，DSA，IAA，SIOV，电源管理，PCIe Gen5, Perf/PMU, CXL1.1等
+ 支持AMD第四代霄龙处理器(code name: Genoa) 内核特性， 包括安全虚拟化增强（SEV）、CXL1.1、PTDMA、PMC、pstate等
+ 内核 CVE 修复。 修复了 CVE-2022-32250, CVE-2022-34918 等重要的 CVE 漏洞。
+ 支持用户态 /dev/ioasid
+ SWIOTLB 机制性能优化
+ virtio-net 打开 napi.tx 优化 TCP Small Queue 性能
+ 支持 AST2600 PCIe 2D VGA Driver
+ 支持 FT2500 处理器
+ 支持动态开启 Group identity 特性
+ arm64 平台默认内核启动 cmdline 调整
+ 添加 Compact Numa Aware（CNA）spinlock 功能支持
+ 丰富 arm64 的 perf mem 和 perf c2c 功能
+ fsck.xfs 支持日志恢复
+ hugetext 自适应按需大页
+ 支持 SGX 动态内存管理
+ 使能 wireguard 模块

{% hint style='tip' %}
更完整的内核特性支持情况，请参阅 [ANCK-5.10 013 版本发布声明](anck/rnotes/anck-5.10-013.md)。
{% endhint %}

#### 3.2.2 ANCK-4.19

**发行版默认内核将不再是 4.19 内核。** 从 Anolis OS 8.8 起，默认内核版本从 ANCK-4.19 开始切换到 ANCK-5.10 ，不过您依然可以继续从 ISO 或源中获取 4.19 版本内核软件包，该版本内核变动如下：

+ 版本更新至 4.19.91-27
+ 重要内核缺陷及安全漏洞（CVE）修复
+ 在 `namespace_unlock` 中使用 `synchronize_rcu_expedited` 加速 rcu 宽限期，使并发启动 100 个 busybox 容器的速度提升 19%
+ 调整 Trusted Platform Module 驱动的缓冲区大小，避免上下文切换时因内存不足报错
+ 默认使能 mq-deadline io 调度器
+ 提升 NVMe、megaraid\_sas 和 mpt3sas 三个驱动的稳定性
+ 全面支持 Aero 系列 raid 卡
+ 修复了飞腾处理器 SMMU 的硬件缺陷导致的问题
+ 支持以下阿里云自研技术：
    - 支持动态开启 Group Identity 特性
    - 支持稀疏文件映射使用系统零页，减少启动虚拟机时的内存消耗

{% hint style='tip' %}
更完整的内核特性支持情况，请参阅 [ANCK-4.19 027 版本发布声明](anck/rnotes/anck-4.19-027.md)。
{% endhint %}

### 3.3 L1 层（核心层）软件
#### 3.3.1 核心库
+ **openssl 1.1.1 支持国密 SM2 完整签名验证能力。** 在 openssl-1.1.1-0.2 以前的版本中，OpenSSL 不支持 SM2 完整签名验签能力；从 openssl-1.1.1-0.2 版本开始，加入了这一能力的支持。从这个版本开始，Anolis OS 8 的 SM2 国密算法提供了更完整的签名验签能力。为了支持 SM2 的签名验签能力，新增了三个新的 API：`ASN1_item_verify_ctx`，`X509_verify_ctx`，`X509_REQ_verify_ctx`，开发者通过他们可以调用完整的国密签名验签能力。同时终端用户也可以通过命令行工具 openssl 来调用 SM2 的完整能力。[贡献团队：商密软件栈 SIG]

### 3.4 L2 层（系统层）软件
#### 3.4.1 系统工具
+ **iptables 兼容 legacy 模式。**
+ crash 版本更新。crash 版本升级至 8.0.1 [贡献团队：发布小组 SIG]
+ branding 优化。进一步优化了 anolis-logos 及其他 Anolis OS 品牌标识（branding）内容。[贡献团队：发布小组 SIG]
+ **bison 版本更新。** bison 版本升级至 3.7.4

#### 3.4.2 系统库
+ **glib2 版本更新。** glib2 升级至 2.68.4 版本，修复了一些问题。[贡献团队：发布小 SIG]
+ **libtirpc 更新。** libtirpc 升级到 libtirpc-1.3.2-1.0.1.an8 版本，新版本 libtirpc 可以提升基准测试工具 lmbench 的性能表现。[贡献团队：发布小组 SIG]
+ **Intel QAT 驱动更新。** Anolis OS 8.8 集成了 Intel QAT 驱动，包括 `intel-QAT20-L.0.9.4-00004.8.an8` 及 `kmod-intel-QAT20-L.0.9.4-00004.8.an8`，以提供对 Intel QAT 加速卡硬件驱动的支持，从而支持加解密和压缩功能的卸载支持。[贡献团队：Intel Arch SIG]
+ **集成Intel DLB驱动。** 包括 kmod-intel_dlb2-7.7.0-2.an8及 libdlb-7.7.0-1.an8，以提供对 Intel DLB加速卡硬件驱动的支持，从而支持由硬件提供的负载均衡能力。[贡献团队：Intel Arch SIG]
+ **rdma-core 更新。** rdma-core 升级到 rdma-core-37.2-1.0.3.an8 版本，新版本 rdma-core 增加了 erdma 的支持。[贡献团队：高性能网络技术 SIG]
+ **新增软件包 libxudp。** 新增软件包 libxudp 。libxudp 是基于 XDP Socket（AF_XDP）实现的 bypass 内核的用户态的高性能 UDP 收发软件库。[贡献团队：高性能网络技术 SIG]
+ **rasdeamon 更新。** rasdeamon 升级到 rasdaemon-0.6.4-6.0.1.an8 版本，新版本 rasdeamon 支持 PFA 功能。[贡献团队：ARM 架构 SIG]
+ **zstd 更新。** zstd 版本由 1.4.4 升级至 1.5.1。新版本优化了压缩速度，提高了压缩比率，并修复了一些问题。[贡献团队：发布小组 SIG]
+ **Intel SPR 特性更新。** [贡献团队：Intel Arch SIG]
    - accel-config 升级至 accel-config-3.4.6.4-1.an8
    - gtest 升级至 gtest-1.12.1-1.an8
    - 新增 dml-0.1.9~beta-1.an8
    - qatengine 升级至 qatengine-0.6.16-3.an8
    - intel-ipp-crypto-mb 升级至 intel-ipp-crypto-mb-1.0.5-1.an8
    - intel-ipsec-mb 升级至 intel-ipsec-mb-1.3.0-1.an8
    - qatlib 升级至 qatlib-22.07.0-1.an8
    - qatzip 升级至 qatzip-1.0.9-1.an8
    - 新增 qpl-0.2.0-1.an8

### 3.5 L3 层（应用层）软件
#### 3.5.1 应用工具
+ 新增软件包 ancert。 ancert 服务于龙蜥各个发行版的硬件兼容性验证，同时通过社区SIG组维护验证框架和验证用例。各个硬件厂商可以通过下载硬件兼容性测试套件ancert，运行相关硬件验证。获取方式：开启 Experimental 仓库。 [贡献团队：硬件兼容性 SIG]
+ 新增软件包 ssar。 ssar（SRE System Activity Reporter）是一款在龙蜥社区孵化的 Linux 单机可观测工具。针对常见的 linux load 指标异常，ssar 工具还提供了比较完整的 load 指标体系，有助于找出 load 异常的问题根源。获取方式：开启 Experimental 仓库。 [贡献团队：系统运维 SIG]
+ 新增软件包 sysom。 一个集主机管理、配置部署、监控报警、异常诊断、安全审计等一系列功能的自动化运维平台。获取方式：开启 Experimental 仓库。 [贡献团队：系统运维 SIG]
+ 新增软件包 sysak。 SysAK（System Analyse Kit）是阿里云操作系统提供的一个全方位的系统运维工具集，可以覆盖系统的日常监控、线上问题诊断和系统故障修复等常见运维场景。获取方式：开启 Experimental 仓库。 [贡献团队：系统运维 SIG]
+ 软件包升级 texlive。 texlive 包版本升级至 20200406 版本。[贡献团队：发布小组 SIG]

#### 3.5.2 应用库
+ **gmp 版本更新。** gmp 版本由 6.1.2 升级至 6.2.0。该版本优化了在 AMD 平台的执行速度，增加了一些新特性，并修复了一些问题。[贡献团队： 发布小组 SIG]

### 3.6 其他层
#### 3.6.1 编程语言与工具链
+ **dragonwell。** Dragonwell 8 升级到 java-1.8.0-alibaba-dragonwell-1.8.0.352-2.an8 版本 ，提供了替代默认 openjdk 的能力。获取方式：开启 Plus 仓库。[贡献团队：Java语言与虚拟机 SIG]
+ **golang。** golang 增加 1.18 版本支持。 golang 扩展了arm64 和 ppc64 的编译支持，增加了一些新特性及 bug 修复。[贡献团队：发布小组 SIG]

### 3.7 场景化组件
#### 3.7.1 云原生场景
+ **云原生组件集成** [贡献团队：云原生 SIG]
    - nydus-rs 升级至 nydus-rs-2.1.1-1.an8
    - nerdctl 升级至 nerdctl-1.1.0-1.an8
    - kata-containers 升级至 kata-containers-3.0.0-2.an8
    - 新增软件包 buildkit 版本 0.10.5~rc.1-1.an8
    - 新增软件包 docker-compose-plugin 版本 2.10.2-1.an8

#### 3.7.2 桌面场景
+ **Deepin Desktop Enviornment（DDE）**
    - 系统基本安装 ISO 镜像添加 DDE 桌面组件。Anolis OS 8.8 将 DDE 桌面组件规划到系统基本安装组件选择中，以保证用户方便安装使用，减少配置操作，提升用户友好性。[贡献团队：发布小组 SIG]
    - 龙芯平台添加 DDE 桌面组件。Anolis OS 8.8 的 DDE 桌面组件在龙芯架构上完成了适配。保证了龙芯架构平台用户的 DDE 桌面环境的体验和使用。[贡献团队：DDE SIG]
    - 修复龙芯平台 DDE 桌面组件窗口管理器显示不正常问题。 优化龙芯平台 DDE 桌面组件的窗口管理器在使用过程中的卡顿现象。[贡献团队：DDE SIG]
    - 软件包安装源添加 DDE 桌面组件。Anolis OS 8.8 将 DDE 桌面作为独立的软件包安装源添加到版本发布的软件 YUM 仓库中。[贡献团队：DDE SIG、发布小组 SIG]
    - DDE 桌面组件编译环境升级适配。Anolis OS 8.8 主线开发环境部分开发软件升级，可能会导致兼容性问题。DDE 桌面组件及其相关包均已适配更新。[贡献团队：DDE SIG]

+ **GNOME**
    - gnome 桌面环境升级至 40 版本

## 4. 已知问题
+ [Bug 3571](https://bugzilla.openanolis.cn/show_bug.cgi?id=3571) - gvfs-afc 依赖问题。 \
        运行 `yum update` 升级系统。如果系统上已经安装了 gvfs-afc ，那么需要额外增加参数`--allowerasing`，即运行 `yum update --allowerasing`。因为 gvfs-afc 在 Anolis OS 8.8 中已经不再提供。影响范围：全平台。该问题将在发布后修复。
+ [Bug 3969](https://bugzilla.openanolis.cn/show_bug.cgi?id=3969) - brasero 依赖问题。 \
        运行 `yum update` 升级系统。如果系统上已经安装了 brasero ，那么该包会因依赖缺失无法正常安装。影响范围：全平台。该问题将在发布后修复。

## 5. 特别声明
Anolis OS 8 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第 2 版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 6. 致谢
感谢统信软件、龙芯中科、浪潮信息、中科曙光、万里红、中科方徳、红旗软件等（排名不分先后）各 OSV 方对 Anolis OS 8.8 版本的大力支持。

## 7. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
<!-- endtoc -->
