Anolis OS 23.1 社区版 (GA) 发行声明
=====================================

## 1. 引言
龙蜥操作系统 Anolis OS 23 是 OpenAnolis 龙蜥社区基于操作系统分层分类理论，面向上游原生社区独立选型，拉齐软硬件兼容性软件包的首款国产操作系统。Anolis OS 23.1 是 Anolis OS 23 系列操作系统的第二个正式版本。  
当前 Anolis OS 23 最新的版本号为 Anolis OS 23.1（GA)。

## 2. 交付物清单
### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-23.1-x86_64-dvd.iso  | x86\_64 架构的基础安装 ISO, 约 15 GB 
AnolisOS-23.1-x86_64-boot.iso | x86\_64 架构的网络安装 ISO，约 0.6 GB 
AnolisOS-23.1-aarch64-dvd.iso  | aarch64 架构的基础安装 ISO，约 15 GB 
AnolisOS-23.1-aarch64-boot.iso | aarch64 架构的网络安装 ISO，约 0.6 GB 
AnolisOS-23.1-loongarch64-dvd.iso | loongarch64 架构的基础安装 ISO，约 12 GB
AnolisOS-23.1-loongarch64-boot.iso | loongarch64 架构的网络安装 ISO，约 0.6 GB

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
AnolisOS-23.1-x86\_64.qcow2 | x86\_64  架构 QEMU 虚拟机镜像(qcow2 格式)
AnolisOS-23.1-aarch64.qcow2 | aarch64 架构虚拟机镜像(qcow2 格式)
AnolisOS-23.1-loongarch64.qcow2 | loongarch64 架构虚拟机镜像(qcow2 格式)

镜像缺省 sudo 用户为 anuser，对应登录密码是 anolisos

### 2.3 容器镜像
名称 | 描述
-----|-----
registry.openanolis.cn/openanolis/anolisos:23.1 | baseos 容器镜像 
lcr.loongnix.cn/openanolis/anolisos:23.1 | loongarch64 架构容器镜像
registry.openanolis.cn/openanolis/anolisos:23-busybox | 精简镜像 busybox，大小仅 2.8M，最精简镜像

### 2.4 软件 YUM 仓库
名称 | 描述| 仓库状态 
-----|-----|-----
[os](https://mirrors.openanolis.cn/anolis/23.1/os/)    | 版本正式发布的 ISO 交付件，所包括的所有的包。该仓库与 ISO 交付件包含的软件包完全一致。 | 默认开启 
[updates](https://mirrors.openanolis.cn/anolis/23.1/updates/) | 版本正式发布后，更新的软件包的存放仓库。该仓库会持续更新，直到该版本生命周期结束。 | 默认开启 
[kernel-5.10](https://mirrors.openanolis.cn/anolis/23.1/kernel-5.10/)      | Kernel-5.10 软件包源，提供社区内核（5.10.134-x）以及对应的组件。 | 默认开启
[epao](https://mirrors.openanolis.cn/epao/23/) | EAPO（Extras Package For Anolis）软件包仓库提供社区孵化类软件，比如：AI 组件等。 | 不默认开启，需要安装 anolis-epao-release 后使用 

## 3. 发布详情
### 3.1 概述
#### 3.1.1 亮点
- **内核演进**：Anolis OS 23.1 现提供 5.10/6.6 两种内核可供使用，6.6 为默认内核，ISO 中默认只携带 6.6 版本内核.  
- **核心组件变动**：
  - 默认 python 编译器升级为 3.11.6
  - glibc 升级至 2.38

### 3.2  L0 层软件 Kernel 6.6
- 该版本内核基于上游 Linux 6.6.25 版本，提供新特性如下：
    - 提供 userns 安全增强的 sysctl 接口
    - 支持 fscache/cachefiles 的按需加载模式下的故障恢复特性
    - 支持受控的 fuse 挂载点跨 mount namespace 传播的能力
    - fuse 支持声明不支持 file handle 能力
    - fuse 支持请求重发
    - 支持 vring_force_dma_api 启动参数
    - cgroup v1 支持 iocost 特性
    - cgroup v1 支持 writeback IO 限流特性
    - 支持 enable_context_readahead 接口
    - 对 ext4 和 jbd2 特性进行增强
    - 支持 IO hang 检测
    - TCMU 读写性能优化
    - tcm loop 设备支持 2M 大 IO
    - 对上游 KFENCE 内核内存污染检测功能的增强
    - CPU burst 特性增强
    - 支持 CMN PMU 的 perf metric 能力
    - 支持调度统计增强与容器资源视图增强
    - 在 arm64 平台支持线性区动态拆分并解决 kfence 动态使能和内存 RAS 的问题
- 该版本在平台支持层面的变动如下：
    - 新增海光四号新平台支持，包括 CPU 使能，安全特性，密码学加速器国密支持等
    - 新增飞腾平台 S5000C 支持
    - 支持龙芯 3A5000、3A6000、3C5000、3D5000 、3C6000等多个基于 loongarch 架构设计的 CPU
    - 支持 GNR/SRF 平台
    - 新增兆芯平台支持，包括 CPU 使能，新指令集，以及安全算法驱动等
- 该版本在驱动支持层面的变动如下：
    - 支持芯启源多款网卡
    - 完善高版本 txgbe、ngbe 驱动支持，进行功能增强与修复 Bug
    - 支持 dwc_rootport PMU 驱动
    - 支持澜起 Mont-TSSE 驱动
    - 优化 QAT intree 驱动支持
- 更详细的内核变动详见 《ANCK 6.6.25 发行声明》
### 3.3 L1 层软件
#### 3.3.1 核心工具
- gcc 升级至 12.3.0：在原本 12.2.1 的版本已经支持了 C++17/C++20，部分支持 C++23 和实现支持 loongarch64 架构的基础上，新增了对基于 znver4 核心的 AMD CPU 支持，并且修复了大量已知问题。 
- binutils 升级至 2.41: 新增 loongarch64 架构指令集： LSX 、LASX、LVZ 、LBT ，并优化了链接器；新增了对 Intel FRED、LKGS、AMX-COMPLEX 指令集的支持；新增 aarch64 架构 SME2 的支持。
- systemd 升级至 255 : 该版本优化了服务管理器，新增了数个可配置参数。同时对 TPM2 、磁盘加密等功能进行了优化，增强了认证功能。
- grub2 升级至 2.12：该版本开始集成 systemd 的相关 boot loader 接口，并且新增了对 loongarch 的支持。同时，新版本 grub2 也对 SDL2 库新增了支持。
- util-linux 升级至 2.39.1：常用工具如 fdisk、dmesg、lsof 等均有更新，支持了内核中的新的文件描述符，新增了 blkpr、pipesz、waitpid 等新命令和工具。
#### 3.3.2 核心库
- glibc 升级至 2.38：该版本新增了多线程相关的性能优化项，同时在 aarch64 架构上新增了对向量数学库 libmvec 的构建支持，并且新增了部分功能和宏以更好地支持 C2X 标准。
### 3.4 L2 层软件 
#### 3.4.1 关键工具
- ethtool 升级至 6.6 版本：该版本支持了 CMIS 模块的接收接口支持，并兼容旧版本内核的头文件。
- crash 升级至 8.0.4 版本：支持 kernel 6.6 的 core dump 文件分析，修复了部分已知问题。
- python3 升级至 3.11.6： 该版本优化了异常显示机制以及新增了异常类以及支持异常的通配。同时对 toml 和 asyncio 新增了支持。该版本同样在性能上有着较大的提升，相较于 3.10 版本，该版本有 10%-60% 的性能提升。
- sssd 升级至 2.9.4：该版本新增对新加密功能的支持，新增了对本地ldap server 的 api 支持。
- openssh 升级至 9.3p2：支持非活跃频道超时设置、RSA 长度控制等功能，新增 scp/sftp 可用命令选项，修复大量 bug 和安全漏洞。
- NetworkManager 升级至 1.44.2：在 dhcpv6、ipv6、vlan、dns 等层面进行了功能优化，支持 nmtui 对 wifi、wwan 网络进行禁用的能力。
- xfsprogs 升级至 6.6.0 ：匹配 kernel 版本升级，修复了 xfs_db、xfs_repair 等工具的大量已知问题。
- LLVM 工具链升级至 17 版本：llvm、clang、lldb、libomp 等相关工具均升级至 17 版本。
#### 3.4.2 关键库
- glib2 升级至 2.78.3 版本：修复了大量已知问题，修复了特定场景的崩溃问题。
- libbpf 升级至 1.2.2：该版本新增了用户态接口和功能的调用方式，对日志功能进行的优化。同时在 BPF 功能层面支持的龙芯架构，丰富了 API 调用。
### 3.5 L3 层软件
#### 3.5.1 python 相关组件
因 python3 采用 3.11 的新版本，同时考虑到相关组件的版本稳定性以及安全修复的需求，故采用如下版本：
- python-setuptools 升级至 68.0.0 版本
- python-pyudev 升级至 0.24.1 版本
- python-markupsafe 升级至 2.1.3 版本
- python-markdown 升级至 3.5.1 版本
- pygobject3 升级至 3.46.0 版本
#### 3.5.2 常规库
从版本更迭及安全修复的角度，有如下软件包的版本升级：
- libtevent 升级至 0.16.0 版本
- libsolv 升级至 0.7.24 版本
- krb5 升级至 1.21.2 版本
- gettext 升级至 0.22.0 版本
- fuse3 升级至 3.16.2 版本
- libmodulemd 升级至 2.15.0 版本
- libbpf 升级至 1.2.2 版本
#### 3.5.3 常规工具
从版本更迭及安全修复的角度，有如下软件包的版本升级：
- dracut 升级至 59 版本
- docker 升级到 24 版本
- sg3_utils 升级至 1.48 版本
- polkit 升级至 123 版本
- pkgconf 升级至 1.9.5 版本
- pcsc-lite 升级至 2.0.0 版本
#### 3.5.4 生态相关
- tbb 升级至 2021.11.0 版本：优化了 NUMA 相关的库，修复了 cmake 相关的问题。
- runc 升级至 1.1.12 版本：新增了 cgroup v2 中对内存的数个优化参数，修复了严重安全漏洞 CVE-2024-21626。
- ima-evm-utils 升级至 1.5 版本：该版本优化了文件签名，修改了 TPM 认证逻辑和调用接口。
- samba 升级至 4.19.5：该版本优化了 Kerberos 相关认证功能，支持了 samba AD TLS 认证，新增了工具和命令。
- bind 升级至 9.18.21： 该版本默认使用 SipHash-2-4 认证算法，以便于后续替换原有 dnf cookie 中所使用的  AES 算法；优化了告警信息，并修复了部分已知问题。
- rust 语言组件升级至 1.77.0：该版本优化了部分接口的逻辑，修复了部分问题和 cve。
### 3.6 场景化组件
#### 3.6.1 云原生场景
- buildkit 版本升级至 0.13.2：该版本升级了网络部分的功能配置，并修复了大量已知问题和 cve 安全漏洞。
#### 3.6.2 运维和调优场景
- keentune 系列智能调优组件升级至 2.3.1 版本：
    - bench，target和brain引入subprocess机制，提供中断调优任务功能；
    - 新增 feature 级别参数设置模块，新增设置脚本用于 xps/rps、affinity、gzip、glub、x264_265；
    - 更新 memcached.conf, mysql.conf, nginx.conf, pgsql.conf, redis.conf 等 profile内容；
#### 3.6.3 虚拟化相关
- qemu 升级至 8.2.0 ：在龙芯架构支持层面新增了 LASX、PRELDX 接口调用，支持 LSX/LASX 特性开启或关闭，支持 la132 龙芯 cpu。在 x86/arm/riscv 等架构也有不同程度的优化，同时在 xen、vitio 等虚拟化功能层面也有一定的更新。Anolis OS 23.1 还增加了对海光 CSV2/3 等迁移的适配支持。
- libvirt 升级至 9.10.0 ：虚拟化组件支持 pipewire audio 声音设备，修复了快照、磁盘设备的已知问题。Anolis OS 23.1 还增加了对海光 CSV 的适配。
- virtiofsd 引入 1.10.1 版本：该包在 qemu 8 版本中被从 qemu 中剥离，成为独立组件并使用 rust 进行重构。Anolis OS 23.1 引入了该组件的 rust 版本并在 23.1 中替代原有的 qemu-virtiofs 组件。
#### 3.6.4 桌面环境相关
- DDE 桌面环境正式支持龙芯架构：DDE 桌面组件在 Anolis OS 23.1 支持了包含龙芯架构在内的全部三种架构，

## 4. 已知问题
+ [Bug 7887](https://bugzilla.openanolis.cn/show_bug.cgi?id=7887) - 龙芯架构 jansi 目前存在已知问题，maven 也受到影响
+ [Bug 8903](https://bugzilla.openanolis.cn/show_bug.cgi?id=8903) - 内核降级回 5.10.134 后重启会进入到紧急模式

## 5. 安全修复
以下仅展示已修复的重要 CVE
CVE 编号 | 严重等级
-----|-----
CVE-2023-7101 | Important
CVE-2023-50255 | Important
CVE-2024-24821 | Important
CVE-2024-31210 | Important
CVE-2023-41038 | Important
CVE-2024-24680 | Important
CVE-2024-27351 | Important
CVE-2023-50980 | Important
CVE-2023-6879 | Important
CVE-2023-45235 | Important
CVE-2023-45230 | Important
CVE-2023-45234 | Important
CVE-2023-50447 | Important
CVE-2024-23652 | Important
CVE-2024-23653 | Important
CVE-2024-23213 | Important
CVE-2024-24577 | Important
CVE-2023-6597 | Important
CVE-2023-3966 | Important
CVE-2023-42950 | Important
CVE-2024-28085 | Important
CVE-2024-23672 | Important
CVE-2024-24549 | Important
CVE-2024-27983 | Important
CVE-2023-45288 | Important
CVE-2024-31082 | Important
CVE-2024-31081 | Important
CVE-2024-31080 | Important
CVE-2024-31083 | Important
CVE-2024-32487 | Important
CVE-2024-2961 | Important
CVE-2024-32462 | Important
CVE-2024-1135 | Important
CVE-2023-7008 | Moderate
CVE-2024-22365 | Moderate
CVE-2023-52426 | Moderate
CVE-2024-1048 | Moderate
CVE-2024-25112 | Moderate
CVE-2023-5680 | Moderate
CVE-2024-26462 | Moderate
CVE-2024-2466 | Moderate
CVE-2023-3758 | Moderate


## 6. 特别声明
Anolis OS 23 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第2版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 7. 致谢
感谢统信软件、龙芯中科、浪潮信息、中科方德、中兴通讯、海光信息、红旗软件、曙光信息、Intel、兆芯、飞腾、阿里云等（排名不分先后）各理事单位对 Anolis OS 23.1 版本研发过程中的大力支持，各理事单位在多个技术专业领域中贡献了大量的特性更新和问题修复代码，这有效地保证了版本的开发进度和质量，为版本的成功发布奠定了坚实的基础。

### 7.1 研发贡献
领域 | 功能支持贡献方
-----|-----
架构支撑 | 龙芯中科、兆芯、海光信息、飞腾
图形 | 统信软件
虚拟化 | 海光信息、Intel
硬件驱动 | 飞腾、Intel、龙芯中科、浪潮信息
基础软件包开发及维护 | 中兴通讯、浪潮信息、中科曙光、中科方德、红旗软件、龙芯中科、统信软件、阿里云
重要 CVE 修复 | 浪潮信息、中科曙光、中科方德、红旗软件、统信软件、阿里云

### 7.2 测试贡献
理事 | 测试分工
-----|-----
海光信息 | 负责海光芯片平台内核、硬件兼容性测试
兆芯 | 负责兆芯芯片平台内核、硬件兼容性测试
龙芯中科 | 负责龙芯架构全量测试
统信软件 | 负责各机型安装启动，桌面测试
浪潮信息 | 负责软件兼容性方面测试
中兴通讯 | 负责中兴主流机型内核，硬件兼容性测试
中科曙光 | 负责曙光主流机型安装启动测试、通用安装启动测试
阿里云 | 负责通用架构机型的全量测试

## 8. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
 
