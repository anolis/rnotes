Anolis OS 8.6 发行声明
=====================

<!-- toc -->
## 1. 引言
龙蜥操作系统 Anolis OS 8 是 OpenAnolis 龙蜥社区发行的开源 Linux 发行版，支持多计算架构，提供稳定、高性能、安全、可靠的操作系统支持。

Anolis OS 8.6 是 Anolis OS 8 发布的的第三个小版本。

## 2. 交付物清单
{% hint style='tip' %}**提示**

该版本发布的所有交付物清单及下载链接，可以在[社区网站](https://openanolis.cn/download)中找到详细信息。
{% endhint %}

### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-8.6-x86\_64-dvd.iso     | x86\_64 架构的完整安装 ISO，约 10GB
AnolisOS-8.6-x86\_64-minimal.iso | x86\_64 架构的最小安装 ISO，约 2.0GB
AnolisOS-8.6-x86\_64-boot.iso    | x86\_64 架构的网络安装 ISO，约 0.9GB
AnolisOS-8.6-aarch64-dvd.iso     | aarch64 架构的完整安装 ISO，约 10GB
AnolisOS-8.6-aarch64-minimal.iso | aarch64 架构的最小安装 ISO，约 2.0GB
AnolisOS-8.6-aarch64-boot.iso    | aarch64 架构的网络安装 ISO，约 0.9GB
AnolisOS-8.6-src-dvd.iso         | source 包 ISO，约18GB

{% hint style='tip' %}**提示**

完整的镜像安装说明可以参看[链接](/anolis/manual/installation.md)。
{% endhint %}

### 2.2 虚拟机镜像
| **名称 ** | **描述** |
| --- | --- |
| AnolisOS-8.6-x86\_64-ANCK.qcow2 | x86\_64 架构虚拟机镜像搭配 ANCK 内核 |
| AnolisOS-8.6-x86\_64-RHCK.qcow2 | x86\_64 架构虚拟机镜像搭配 RHCK 内核<sup>1</sup> |
| AnolisOS-8.6-aarch64-ANCK.qcow2 | aarch64 架构虚拟机镜像搭配 ANCK 内核 |
| AnolisOS-8.6-aarch64-RHCK.qcow2 | aarch64 架构虚拟机镜像搭配 RHCK 内核 |
| anolisos-disk-minimal-an8-Rawhide-sda.raw.xz | riscv64 架构虚拟机试用镜像 |

{% hint %}**说明**

<sup>1</sup>RHCK 内核兼容 RHEL 8.6 的内核，当前版本是 `kernel-4.18.0-372.9.1.an8`.
{% endhint %}

{% hint %}**说明**

镜像缺省 sudo 用户为 `anuser`，对应登录密码是 `anolisos`.
{% endhint %}

### 2.3 容器镜像
| **名称 ** | **描述** |
| --- | --- |
| AnolisOS-8.6-x86\_64-docker.tar | x86\_64 架构本地容器镜像 |
| AnolisOS-8.6-aarch64-docker.tar | aarch64 架构本地容器镜像 |
| docker pull openanolis/anolisos:8.6-x86\_64 | x86\_64 架构容器镜像 |
| docker pull openanolis/anolisos:8.6-aarch64 | aarch64 架构容器镜像 |

### 2.4 下载地址
- [社区网站](https://mirrors.openanolis.cn/anolis/8.6/isos/)
- [阿里云镜像](https://mirrors.aliyun.com/anolis/8.6/)
- [Docker 仓库](https://hub.docker.com/r/openanolis/anolisos/tags)
- [RISCV 仓库与镜像](https://mirrors.openanolis.cn/alt/risc-v/)

### 2.5 软件 YUM 仓库
#### 2.5.1 Distro REPO
| **名称** | **描述** |
| --- | --- |
| BaseOS | BaseOS 软件包源，该源目的是提供安装基础的所有核心包。 |
| AppStream | AppStream 软件包源，该源提供额外的多场景，多用途的用户态程序，数据库等。该部分引入了额外的 RPM Module 形态。 |
| PowerTools | PowerTools 软件包源， 该源提供开发者需要的额外包。 |
| Extras | 第三方仓库源 |

#### 2.5.2 SIG REPO
| **名称** | **描述** |
| --- | --- |
| HighAvailability | 高可用软件包源，提供高可用功能所需的软件包。 |
| Plus | Plus 软件包源，该源提供 OpenAnolis SIG 组专门研发包，如 ANCK 内核，Dragonwell8 JDK 等。 |
| DDE | DDE 桌面主包以及依赖包 |
| ShangMi | 全栈国密相关的软件包 |

## 3. 变更日志 (ChangeLog)
### 3.1 内核
1. Linux Kernel 4.19 LTS 版本升级到 4.19.91-26 版本，特性更新如下：
   - 提供更丰富的新型 RAID 驱动支撑；
   - 龙蜥自研混部资源隔离特性 Group Identity 支持
   - 龙蜥自研多租户容器网络 QoS 隔离特性增强
   - 龙蜥自研 Slab 内存安全回收特性支持
   - 龙蜥自研 UKFEF 特性支持
   - Megaraid\_sas设备驱动更新，并支持 Aero 系列 Controllers
   - virtio-net 支持 XDP Socket
   - 基于 EROFS 实现 RAFS v6 格式支持
   - livepatch 增加 static key 支持
   - SMR zoned 设备支持
   - ext4 delalloc buffer写性能优化
   - Kunit 测试框架支持
   - ARM 架构支持 kdump crashkernel 分配 4G 以上内存
   - ARM 架构 CMN PMU 特性支持
   - perf c2c 功能支持

### 3.2 软件包
1. 更新 Dragonwell 8 到 1.8.0.332 版本，更新 Dragonwell 11 到 11.0.15.11.9 版本，和系统更无缝集成，无需指定 JAVA\_HOME 即可运行，需开启 Plus 仓库安装；
2. 更新智能调优平台 Keentune 到 1.2.1 版本；
3. 来自高性能存储 SIG 的 nydus-rs 及 erofs-utils 工具结束孵化，正式进入 Plus 仓库；
4. 用户态软件包增加平头哥平台的补丁；
5. 安装过程增加对国产平台的动态内核选择适配；
6. 用户态软件包集成下游发行版回馈软件包集成 100+；
7. 用户态软件包增加 LoongArch64 平台的补丁；
8. 升级 Perl 5.32 到 AppStream
9. 升级 PHP 8.0 到 AppStream
10. 升级 container-tools 4.0 到 AppStream
11. 升级 AppStream 的 LLVM 工具集
12. 升级 AppStream 中的 Rust 工具集
13. 升级 AppStream 中的 Go 工具集
14. 新增 bind 9.16
15. 新增 dotnet6.0

### 3.3 镜像
1. 发布镜像丰富化，提供网络启动 ISO 和最小化 ISO；
2. 新增 RISC-V 试用预览版镜像；
3. DVD 集成了 Plus 仓库；

## 4. 硬件支撑
### 4.1 支持架构
x86\_64 ,  aarch64,  riscv64（试用）

### 4.2 Cloud Kernel 平台兼容性
Cloud Kernel 内核已验证支持的服务器如下，后续将逐步增加对其他服务器的支持，也欢迎广大合作伙伴/开发者参与贡献和验证。

| **名称** | **架构** | **CPU** |
| --- | --- | --- |
| 海光 | x86\_64 | Hygon C86 7185 32-core Process |
| 飞腾 | aarch64 | Phytium FT2000+/64 ，Phytium S2500/64 |
| 兆芯 | x86\_64 | Zhaoxin KH-37800D |
| 鲲鹏 | aarch64 | Kunpeng-920 |

## 5. 声明
Anolis OS 8 不提供任何形式的书面保障和承诺。
使用过程请参照发行版各软件包许可证。

## 6. 致谢
忠心感谢参与和协助 OpenAnolis 龙蜥社区的所有成员，尤其是[产品发布兴趣小组](https://openanolis.cn/sig/SIG-Distro)和[测试兴趣小组](https://openanolis.cn/sig/QA)是你们的辛勤付出，以及对开源的热爱才保障版本顺利发布，也为龙蜥操作系统 Anolis OS 8 更好地发展提供无限空间！

额外感谢社区用户 zhangzhizhong@supcon.con 对 8.6 版本发布阶段的试用测试！

## 7. 反馈
- [Bug 跟踪链接](https://bugzilla.openanolis.cn/)
- [邮件列表链接](http://lists.openanolis.cn/)
- 龙蜥OpenAnolis社区交流群号 33311793
- 龙蜥OpenAnolis社区交流2群号 13600003427
<!-- endtoc -->
