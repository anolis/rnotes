Anolis OS 23.1 软件包发布清单
============================

<!-- toc -->
龙蜥操作系统 (Anolis OS) 23 是 OpenAnolis 龙蜥社区发行的开源 Linux 操作系统发行版，支持多计算架构，具备稳定、高性能、安全、可靠等特点。本文提供了 Anolis OS 23.1 的软件包清单。

修订历史：

日期          | 版本 | 修订内容
--------------|------|---------
2024年5月23日 | V1.0 | 初稿

## 1. os
### 1.1 os 软件包清单
os 软件包库提供一套核心的底层操作系统的功能，作为所有应用安装的基础。

下表列出了 Anolis OS 23.1 os 软件包库中的所有软件包及其许可协议。

软件包 | 许可协议 | 功能简述
-------|----------|---------
abattis-cantarell-fonts | OFL | Humanist sans serif font 
abi-compliance-checker | GPLv2+ or LGPLv2+ | An ABI Compliance Checker 
abi-dumper | GPLv2 | Tool to dump ABI of an ELF object containing DWARF debug info 
abseil-cpp | Apache-2.0 AND LicenseRef-Fedora-Public-Domain | C++ Common Libraries 
accel-config | GPLv2 and LGPLv2+ and MIT and CC0 | Configure accelerator subsystem devices 
accountsservice | GPLv3+ | D-Bus interfaces for querying and manipulating user account information 
acl | GPLv2+ | Access control list utilities 
acpica-tools | GPLv2 | ACPICA tools for the development and debug of ACPI tables 
acpid | GPLv2+ | ACPI Event Daemon 
adcli | LGPLv2+ | Active Directory enrollment 
adobe-mappings-cmap | BSD | CMap resources for Adobe's character collections 
adobe-mappings-pdf | BSD | PDF mapping resources from Adobe 
adobe-source-code-pro-fonts | OFL | A set of mono-spaced OpenType fonts designed for coding environments 
adwaita-icon-theme | LGPLv3+ or CC-BY-SA | Adwaita icon theme 
aide | GPLv2+ | Intrusion detection environment 
alsa-lib | LGPLv2+ | The Advanced Linux Sound Architecture (ALSA) library 
alsa-utils | GPLv2+ | Advanced Linux Sound Architecture (ALSA) utilities 
amtk | LGPL-3.0-or-later | Actions, Menus and Toolbars Kit for GTK+ applications 
anaconda | GPLv2+ and MIT | Graphical system installer 
annobin | GPLv3+ | Annotate and examine compiled binary files 
anolis-logos | Licensed only for approved usage, see COPYING for details. | Anolis OS related icons and pictures 
anolis-rawhide-release | MulanPSLv2 | Extra Packages for Anolis OS 23 repository configuration 
anolis-release | MulanPSLv2 | Anolis OS 23 release file 
ant | Apache-2.0 | Java build tool 
aom | BSD-3-Clause | Royalty-free next-generation video format 
aopalliance | Public Domain | Java/J2EE AOP standards 
apache-commons-beanutils | ASL 2.0 | Java utility methods for accessing and modifying the properties of arbitrary JavaBeans 
apache-commons-cli | ASL 2.0 | Command Line Interface Library for Java 
apache-commons-codec | ASL 2.0 | Implementations of common encoders and decoders 
apache-commons-collections | ASL 2.0 | Provides new interfaces, implementations and utilities for Java Collections 
apache-commons-collections4 | ASL 2.0 | Extension of the Java Collections Framework 
apache-commons-compress | ASL 2.0 | Java API for working with compressed files and archivers 
apache-commons-exec | ASL 2.0 | Java library to reliably execute external processes from within the JVM 
apache-commons-io | ASL 2.0 | Utilities to assist with developing IO functionality 
apache-commons-jxpath | ASL 2.0 | Simple XPath interpreter 
apache-commons-lang3 | ASL 2.0 | Provides a host of helper utilities for the java.lang API 
apache-commons-logging | ASL 2.0 | Apache Commons Logging 
apache-ivy | ASL 2.0 | Java-based dependency manager 
apache-resource-bundles | ASL 2.0 | Apache Resource Bundles 
apiguardian | ASL 2.0 | API Guardian Java annotation 
appstream | GPLv2+ and LGPLv2+ | Utilities to generate, maintain and access the AppStream database 
apr | ASL 2.0 and BSD with advertising and ISC and BSD | Apache Portable Runtime library 
apr-util | ASL 2.0 | Apache Portable Runtime Utility library 
aqute-bnd | ASL 2.0 or EPL-2.0 | BND Tool 
argon2 | Public Domain or ASL 2.0 | The password-hashing tools 
args4j | MIT | Java command line arguments parser 
arm-trusted-firmware | BSD | ARM Trusted Firmware 
asciidoc | GPL+ and GPLv2+ | Text based document generation 
asio | Boost | A cross-platform C++ library for network programming 
aspell | LGPLv2+ and LGPLv2 and GPLv2+ and BSD | Spell checker 
aspell-en | MIT and BSD | English dictionaries for Aspell 
assertj-core | ASL 2.0 | Library of assertions similar to fest-assert 
at | GPLv3+ and GPLv2+ and ISC and MIT and Public Domain | Job spooling tools 
atf | BSD | Automated Testing Framework 
atinject | ASL 2.0 | Dependency injection specification for Java (JSR-330) 
atkmm | LGPLv2+ | C++ interface for the ATK library 
atlas | BSD | Automatically Tuned Linear Algebra Software 
atmel-firmware | Redistributable, no modification permitted | Firmware for Atmel at76c50x wireless network chips 
at-spi2-core | LGPLv2+ | Protocol definitions and daemon for D-Bus at-spi 
attr | GPLv2+ | Utilities for managing filesystem extended attributes 
audit | GPL-2.0-or-later AND LGPL-2.0-or-later | User space tools for kernel auditing 
augeas | LGPLv2+ | A library for changing configuration files 
authselect | GPLv3+ | Configures authentication and identity sources from supported profiles 
autoconf | GPLv2+ and GFDL | A GNU tool used to create autoconfiguration files 
autoconf-archive | GPLv3+ with exceptions | The Autoconf Macro Archive 
autogen | GPLv3+ | Automated text file generator 
automake | GPLv2+ and GFDL and Public Domain and MIT | A program which creates Makefiles from template files. 
autotrace | GPLv2+ and LGPLv2+ | Utility for converting bitmaps to vector graphics 
avahi | LGPLv2+ | Local network service discovery 
babel | BSD-3-Clause | Tools for internationalizing Python applications 
babeltrace | MIT and GPLv2 | A trace manipulation toolkit 
basesystem | Public Domain | The skeleton package which defines a simple "anolis os" system 
bash | GPLv3+ | The GNU Bourne Again shell 
bash-completion | GPLv2+ | Programmable completion functions for bash 
bc | GPLv2+ | GNU's bc (a numeric processing language) and dc (a calculator) 
bcc | ASL 2.0 | BPF Compiler Collection (BCC) 
bdftopcf | MIT | Font compiler for the X server and font server 
beakerlib | GPLv2 | A shell-level integration testing library 
beust-jcommander | ASL 2.0 | Java framework for parsing command line parameters 
biber | (GPL+ or Artistic 2.0) and Artistic 2.0 | Command-line bibliographic manager, BibTeX replacement 
bind | MPLv2.0 | The Berkeley Internet Name Domain (BIND) DNS (Domain Name System) server 
binutils | GPL-3.0-or-later AND (GPL-3.0-or-later WITH Bison-exception-2.2) AND (LGPL-2.0-or-later WITH GCC-exception-2.0) AND BSD-3-Clause AND GFDL-1.3-or-later AND GPL-2.0-or-later LGPL-2.1-or-later AND LGPL-2.0-or-later | A GNU collection of binary utilities 
biosdevname | GPLv2 | Udev helper for naming devices per BIOS names 
bison | GPLv3+ | A GNU general-purpose parser generator 
bitmap-fonts | GPLv2 and MIT and Lucida | Selected set of bitmap fonts 
blis | BSD | BLAS-like Library Instantiation Software Framework 
bluez | GPLv2+ | Bluetooth utilities 
bolt | LGPLv2+ | Thunderbolt device manager 
boost | Boost and MIT and Python | The free peer-reviewed portable C++ source libraries 
bouncycastle | MIT | Bouncy Castle Cryptography APIs for Java 
bpftrace | Apache-2.0 | High-level tracing language for Linux eBPF 
bridge-utils | GPLv2+ | Utilities for configuring the linux ethernet bridge 
brltty | LGPLv2+ | Braille display driver for Linux/Unix 
brotli | MIT | Lossless compression algorithm 
btrfs-progs | GPLv2 | An userspace programs named btrfs 
bubblewrap | LGPLv2+ | Core execution tool for unprivileged containers 
buildkit | Apache-2.0 | A toolkit for converting source code to build artifacts in an efficient, expressive and repeatable manner. 
busybox | GPLv2 | Statically linked binary providing simplified versions of system commands 
byacc | Public Domain | Berkeley Yacc, a parser generator 
byaccj | Public Domain | Parser Generator with Java Extension 
byte-buddy | ASL 2.0 | Runtime code generation for the Java virtual machine 
bzip2 | BSD | File compression utility 
ca-certificates | Public Domain | The Mozilla CA root certificate bundle 
cairo | LGPLv2 or MPLv1.1 | A 2D graphics library 
cairomm | LGPL-2.0-or-later | C++ API for the cairo graphics library 
capstone | BSD | A lightweight multi-platform, multi-architecture disassembly framework 
c-ares | MIT | A library that performs asynchronous DNS operations 
cdi-api | ASL 2.0 | CDI API 
cdparanoia | GPLv2 and GPLv2+ and LGPLv2 | Compact Disc Digital Audio (CDDA) extraction tool (or ripper) 
cdrkit | GPLv2 | A collection of CD/DVD utilities 
cepces | GPLv3+ | Certificate Enrollment through CEP/CES 
ceph | (LGPLv2+ or LGPLv3) and CC-BY-SA-3.0 and GPLv2 and Boost and BSD and MIT | User space components of the Ceph file system 
cereal | BSD | A header-only C++11 serialization library 
certmonger | GPL-3.0-or-later | Certificate status monitor and PKI enrollment client 
cglib | ASL 2.0 and BSD | Code Generation Library for Java 
chafa | LGPLv3+ | Image-to-text converter for terminal 
check | LGPLv2+ | A unit test framework for C 
checkpolicy | GPLv2 | SELinux policy compiler 
chkconfig | GPLv2 | A system tool for maintaining the /etc/rc*.d hierarchy 
chrome-gnome-shell | GPLv3+ | Support for managing GNOME Shell Extensions through web browsers 
chrony | GPLv2 | An NTP client/server 
chrpath | GPL+ | Modify rpath of compiled programs 
cifs-utils | GPL-3.0-only | Utilities for mounting and managing CIFS mounts 
clang | Apache-2.0 WITH LLVM-exception OR NCSA | A C language family front-end for LLVM 
clevis | GPLv3+ | Automated decryption framework 
clisp | GPL-2.0-or-later AND (GPL-2.0-or-later OR GFDL-1.2-or-later) AND LGPL-2.1-or-later AND HPND AND X11 | ANSI Common Lisp implementation 
cloud-init | Dual-licesed GPLv3 or Apache 2.0 | Cloud instance init scripts 
cloud-utils-growpart | GPLv3 | Script for growing a partition 
clutter | LGPLv2+ | Open Source software library for creating rich graphical user interfaces 
clutter-gtk | LGPLv2+ | A basic GTK clutter widget 
cmake | BSD and MIT and zlib | Cross-platform make system 
cmocka | ASL 2.0 | An elegant unit testing framework for C with support for mock objects 
cockpit | LGPL-2.1-or-later | Web Console for Linux servers 
cogl | LGPLv2+ | A library for using 3D graphics hardware to draw pretty pictures 
colord | GPLv2+ and LGPLv2+ | Color daemon 
colord-gtk | LGPLv2+ | GTK+ 3 support library for colord 
color-filesystem | Public Domain | Color filesystem layout 
compiler-rt | Apache-2.0 WITH LLVM-exception OR NCSA OR MIT | LLVM "compiler-rt" runtime libraries 
composer | MIT | Dependency Manager for PHP 
conntrack-tools | GPLv2 | Netfilter's connection tracking userspace tools 
console-setup | GPLv2+ and MIT and Public Domain | Tools for configuring the console using X Window System key maps 
containerd | ASL 2.0 | Open and reliable container runtime 
containernetworking-plugins | ASL 2.0 | Libraries for writing CNI plugin 
container-selinux | GPLv2 | SELinux policies for container runtimes 
copy-jdk-configs | BSD | JDKs configuration files copier 
coreutils | GPLv3+ | A set of basic GNU tools commonly used in shell scripts 
cpio | GPLv3+ | A GNU archiving program 
cppunit | LGPL-2.1-or-later | C++ unit testing framework 
cracklib | LGPL-2.1-or-later | A password-checking library 
crash | GPLv3 | Kernel analysis utility for live systems, netdump, diskdump, kdump, LKCD or mcore dumpfiles 
createrepo_c | GPLv2+ | Creates a common metadata repository 
cri-rm | ASL 2.0 | CRI Resource Manager, a CRI proxy with various in-node workload placement policies 
cri-tools | ASL 2.0 | CLI and validation tools for Container Runtime Interface 
criu | GPLv2 | Tool for Checkpoint/Restore in User-space 
cronie | MIT and BSD and ISC and GPLv2+ | Cron daemon for executing programs at set times 
crontabs | Public Domain and GPLv2 | Root crontab files used to schedule the execution of programs 
crypto-policies | LGPLv2.1+ | System-wide crypto policies 
cryptsetup | GPLv2+ and LGPLv2+ | Setup program for dm-crypt Based Encrypted Block Devices 
cscope | BSD and GPLv2+ | C source code tree search and browse tool 
csnappy | BSD | Snappy compression library ported to C 
ctags | GPLv2+ | A C programming language indexing and/or cross-reference tool 
CUnit | LGPL-2.0-or-later | Unit testing framework for C 
cups | ASL 2.0 | CUPS printing system 
cups-filters | GPLv2 and GPLv2+ and GPLv3 and GPLv3+ and LGPLv2+ and MIT and BSD with advertising | OpenPrinting CUPS filters and backends 
cups-pk-helper | GPLv2+ | A helper that makes system-config-printer use PolicyKit 
curl | MIT | A utility for getting files from remote servers (FTP, HTTP, and others) 
cvs | BSD and GPL+ and GPLv2+ and LGPLv2+ and zlib and Public Domain | Concurrent Versions System 
cyrus-sasl | BSD with advertising | The Cyrus SASL library 
Cython | ASL 2.0 | Language for writing Python extension modules 
dash | BSD and GPLv2+ and Public Domain and Copyright only | Small and fast POSIX-compliant shell 
datefudge | GPLv2+ | Fake the system date 
dav1d | BSD | AV1 cross-platform Decoder 
dblatex | GPLv2+ and GPLv2 and LPPL and DMIT and Public Domain | DocBook to LaTeX/ConTeXt Publishing 
dbus | (GPLv2+ or AFL) and GPLv2+ | D-BUS message bus 
dbus-broker | ASL 2.0 | Linux D-Bus Message Broker 
dbus-c++ | LGPLv2+ | Native C++ bindings for D-Bus 
dbus-glib | AFL and GPLv2+ | GLib bindings for D-Bus 
dbus-python | MIT | D-Bus Python Bindings 
dbxtool | GPLv2 | Secure Boot DBX updater 
dconf | LGPLv2+ and GPLv2+ and GPLv3+ | A configuration system 
debugedit | GPLv3+ and GPLv2+ and LGPLv2+ | Tools for debuginfo creation 
dejagnu | GPLv3+ | A front end for testing other programs 
dejavu-fonts | Bitstream Vera and Public Domain | The DejaVu font families 
desktop-file-utils | GPLv2+ | Utilities for manipulating .desktop files 
devhelp | GPLv3+ | API documentation browser 
device-mapper-persistent-data | GPLv3+ | Device-mapper Persistent Data Tools 
devscripts | GPL-2.0-or-later | Scripts for Debian Package maintainers 
dhcp | ISC and MPLv2.0 | Dynamic host configuration protocol software 
diffstat | MIT | A utility which provides statistics based on the output of diff 
diffutils | GPLv3+ | GNU collection of diff utilities 
ding-libs | LGPLv3+ | "DING is not GNU" helper libraries for SSSD and FreeIPA 
distribution-gpg-keys | CC0 | GPG keys of various Linux distributions 
djvulibre | GPLv2+ | DjVu viewers, encoders, and utilities 
dkms | GPLv2+ | Dynamic Kernel Module Support Framework 
dmidecode | GPLv2+ | Tool to analyse BIOS DMI data 
dml | MIT | Intel® Data Mover Library (Intel® DML) 
dmraid | GPLv2+ | Device-mapper RAID tool and library 
dnf | GPL-2.0-or-later AND GPL-1.0-only | Package manager 
dnf-plugins-core | GPLv2+ | Core Plugins for DNF 
dnsmasq | GPL-2.0-only or GPL-3.0-only | A lightweight DHCP/caching DNS server 
docbook2X | MIT | Convert docbook into man and Texinfo 
docbook5-style-xsl | MIT and MPLv1.1 | Norman Walsh's XSL stylesheets for DocBook 5.X 
docbook-dtds | Copyright only | SGML and XML document type definitions for DocBook 
docbook-style-dsssl | DMIT | Norman Walsh's modular stylesheets for DocBook 
docbook-style-xsl | DMIT | Norman Walsh's XSL stylesheets for DocBook XML 
docbook-utils | GPLv2+ | Shell scripts for managing DocBook documents 
docker | ASL 2.0 | The open-source application container engine 
dos2unix | BSD-3-Clause | Text file format converters 
dosfstools | GPLv3+ | Utilities to create and check MS-DOS FAT filesystems 
double-conversion | BSD | Library providing binary-decimal and decimal-binary routines for IEEE doubles 
doxygen | GPL+ | A documentation system for C/C++ 
dpkg | GPLv2 and GPLv2+ and LGPLv2+ and Public Domain and BSD | Package maintenance system for Debian Linux 
dracut | GPLv2+ and LGPLv2+ and GPLv2 | Initramfs generator using udev 
drpm | LGPLv2+ and BSD | A library for making, reading and applying deltarpm packages 
dtc | GPLv2+ | Device Tree Compiler 
duktape | MIT | Embeddable Javascript engine 
dump | BSD | Programs for backing up and restoring ext2/ext3/ext4 filesystems 
dwarves | GPLv2 | Debugging Information Manipulation Tools (pahole & friends) 
dwz | GPLv2+ and GPLv3+ | DWARF optimization and duplicate removal tool 
dyninst | LGPLv2+ | An API for Run-time Code Generation 
e2fsprogs | GPLv2 | Utilities for managing ext2, ext3, and ext4 file systems 
easymock | ASL 2.0 | Easy mock objects 
ecj | EPL-2.0 | Eclipse Compiler for Java 
ed | GPLv2 and GFDL | The GNU line editor 
edk2 | BSD-2-Clause-Patent and OpenSSL and MIT | UEFI firmware for 64-bit virtual machines 
efibootmgr | GPLv2+ | EFI Boot Manager 
efi-rpm-macros | GPLv3+ | Common RPM Macros for building EFI-related packages 
efivar | LGPL-2.1-only | Tools to manage UEFI variables 
eglexternalplatform | MIT | EGL External Platform Interface headers 
egl-wayland | MIT | Wayland EGL External Platform library 
eigen3 | MPLv2.0 and LGPLv2+ and BSD and Minpack | A lightweight C++ template library for vector and matrix math 
elfutils | GPLv3+ and (GPLv2+ or LGPLv3+) and GFDL | A collection of utilities and DSOs to handle ELF files and DWARF data 
elinks | GPLv2 | A text-mode Web browser 
emacs | GPL-3.0-or-later AND CC0-1.0 | GNU Emacs text editor 
emacs-auctex | GPL-3.0-or-later AND FSFAP AND GFDL-1.3-no-invariants-or-later | Enhanced TeX modes for Emacs 
enchant2 | LGPLv2+ | An Enchanting Spell Checking Library 
environment-modules | GPLv2+ | Provides dynamic modification of a user's environment 
eog | GPLv2+ and GFDL | Eye of GNOME image viewer 
esmtp | GPL+ and GPLv2+ | User configurable send-only Mail Transfer Agent 
ethtool | GPLv2 | Settings tool for Ethernet NICs 
evince | GPLv2+ and GPLv3+ and LGPLv2+ and MIT and Afmparse | Document viewer 
evolution-data-server | LGPL-2.0-or-later | Backend data server for Evolution 
exec-maven-plugin | ASL 2.0 | Exec Maven Plugin 
execstack | GPLv2+ | Utility to set/clear/query executable stack bit 
exempi | BSD | Library for easy parsing of XMP metadata 
exiv2 | GPLv2+ | Exif and Iptc metadata manipulation library 
expat | MIT | An XML parser library 
expect | Public Domain | A program-script interaction and testing utility 
fakeroot | GPLv3+ and LGPLv2+ and (GPL+ or Artistic) | tool for simulating superuser privileges 
fcoe-utils | GPLv2 | Fibre Channel over Ethernet utilities 
fdk-aac-free | FDK-AAC | Third-Party Modified Version of the Fraunhofer FDK AAC Codec Library for Android 
fdupes | MIT | Finds duplicate files in a given set of directories 
felix-utils | ASL 2.0 | Utility classes for OSGi 
fftw | GPLv2+ | A Fast Fourier Transform library 
file | BSD | Utility for determining file types 
file-roller | GPLv2+ | Tool for viewing and creating archives 
filesystem | Public Domain | The basic directory layout for a Linux system 
findutils | GPLv3+ | The GNU versions of find utilities (find and xargs) 
fio | GPLv2 | Multithreaded IO generation tool 
firefox | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Firefox Web browser 
firewalld | GPLv2+ | A firewall daemon with D-Bus interface providing a dynamic firewall 
fish | GPLv2 and BSD and ISC and LGPLv2+ and MIT | A smart and user-friendly command line shell 
flac | BSD and GPLv2+ and GFDL | An encoder/decoder for the Free Lossless Audio Codec 
flashrom | GPLv2 | Simple program for reading/writing flash chips content 
flatpak | LGPL-2.1-or-later | Application deployment framework for desktop apps 
flex | BSD and LGPLv2+ | A tool for generating scanners (text pattern recognizers) 
flexiblas | GPLv3 with exceptions and LGPLv2+ and BSD | A BLAS/LAPACK wrapper library with runtime exchangeable backends 
fltk | LGPL-2.0-or-later with exceptions | C++ user interface toolkit 
fmt | BSD | a formatting library providing a fast and safe alternative to C stdio and C++ iostreams. 
fontawesome4-fonts | OFL-1.1-RFN | Iconic font set 
fontawesome-fonts | MIT | Support files for the FontAwesome fonts 
fontconfig | MIT and Public Domain and UCD | Font configuration and customization library 
fontforge | GPLv3+ | Outline and bitmap font editor 
fonts-rpm-macros | GPLv3+ | Build-stage rpm automation for fonts packages 
fonttools | MIT | Tools to manipulate font files 
fonttosfnt | MIT | Tool to wrap bdf or pcf bitmap fonts in an sfnt wrapper 
fpaste | GPLv3+ | A simple tool for pasting info onto the Fedora community paste server 
fprintd | GPLv2+ | D-Bus service for Fingerprint reader access 
freeglut | MIT | A freely licensed alternative to the GLUT library 
freetype | (FTL or GPLv2+) and BSD and MIT and Public Domain and zlib with acknowledgement | A free and portable font rendering engine 
fribidi | LGPLv2+ and UCD | Library implementing the Unicode Bidirectional Algorithm 
fscryptctl | Apache-2.0 license | Small C tool for Linux filesystem encryption 
fstrm | MIT | A C implementation of the Frame Streams data transport protocol. 
ftp | BSD with advertising | The standard UNIX FTP (File Transfer Protocol) client 
fuse | GPL+ | File System in Userspace (FUSE) v2 utilities 
fuse3 | GPL+ | File System in Userspace (FUSE) v3 utilities 
fuse-overlayfs | GPL-3.0-or-later | FUSE overlay+shiftfs implementation for rootless containers 
fusesource-pom | ASL 2.0 | Parent POM for FuseSource Maven projects 
future | MIT | Easy, clean, reliable Python 2/3 compatibility 
fwupd | LGPLv2+ | Firmware update daemon 
galera | GPLv2 | Synchronous multi-master wsrep provider (replication engine) 
gawk | GPLv3+ and GPLv2+ and LGPLv2+ and BSD | The GNU version of the AWK text processing utility 
gc | BSD | Garbage collector for C and C++ 
gcab | LGPLv2+ | Cabinet file library and tool 
gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | Various compilers (C, C++, Objective-C, ...) 
gcr3 | LGPLv2+ | A library for bits of crypto UI and parsing 
gcr | LGPLv2+ | A library for bits of crypto UI and parsing 
gd | MIT | A graphics library for quick creation of PNG or JPEG images 
gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | GNU Project debugger 
gdbm | GPLv3+ | A GNU set of database routines which use extensible hashing 
gdisk | GPLv2 | An fdisk-like partitioning tool for GPT disks 
gdk-pixbuf2 | LGPLv2+ | An image loading library 
gdm | GPLv2+ | The GNOME Display Manager 
gedit | GPLv2+ and GFDL | Text editor for the GNOME desktop 
geoclue2 | GPLv2+ | Geolocation service 
geocode-glib | LGPLv2+ | Geocoding helper library 
geolite2 | CC-BY-SA | Free IP geolocation databases 
gettext | GPLv3+ and LGPLv2+ and GFDL | Gettext tools and documentation for developers and translators 
gflags | BSD | Library for commandline flag processing 
ghostscript | AGPLv3+ | Interpreter for PostScript language & PDF 
gi-docgen | (Apache-2.0 OR GPL-3.0-or-later) AND MIT AND CC0-1.0 | Documentation tool for GObject-based libraries 
giflib | MIT | A library and utilities for processing GIFs 
git | GPLv2 | Distributed version control system 
git-lfs | MIT | Git extension for versioning large files 
gjs | MIT and (MPLv1.1 or GPLv2+ or LGPLv2+) | Javascript Bindings for GNOME 
glade | GPLv2+ and LGPLv2+ | User Interface Designer for GTK+ 
glib2 | LGPLv2+ | A library of handy utility functions 
glibc | LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL | The GNU libc libraries 
glibmm24 | LGPLv2+ | C++ interface for the GLib library 
glib-networking | LGPLv2+ | Networking support for GLib 
gl-manpages | MIT and Open Publication | OpenGL manpages 
glslang | BSD and GPLv3+ and ASL 2.0 | OpenGL and OpenGL ES shader front end and validator 
glusterfs | GPL-2.0-only OR LGPL-3.0-or-later | Distributed File System 
glyphicons-halflings-fonts | MIT | Precisely prepared monochromatic icons and symbols 
gmp | LGPLv3+ or GPLv2+ | A GNU multiple precision arithmetic library 
gnome-autoar | LGPLv2+ | Archive library 
gnome-backgrounds | GPLv2 | Desktop backgrounds packaged with the GNOME desktop 
gnome-bluetooth | GPLv2+ | Bluetooth graphical utilities 
gnome-boxes | LGPL-2.0-or-later | A simple GNOME 3 application to access remote or virtual systems 
gnome-calculator | GPL-3.0-or-later | A desktop calculator 
gnome-calendar | GPL-3.0-or-later | Simple and beautiful calendar application designed to fit GNOME 3 
gnome-characters | BSD and GPLv2+ | Character map application for GNOME 
gnome-common | GPLv2+ | Useful things common to building GNOME packages from scratch 
gnome-control-center | GPLv2+ and CC-BY-SA | Utilities to configure the GNOME desktop 
gnome-desktop3 | GPLv2+ and LGPLv2+ | Library with common API for various GNOME modules 
gnome-disk-utility | GPL-2.0-or-later | Disks 
gnome-font-viewer | GPL-2.0-or-later | Utility for previewing fonts for GNOME 
gnome-keyring | GPLv2+ and LGPLv2+ | Framework for managing passwords and other secrets 
gnome-kiosk | GPLv2+ | Window management and application launching for GNOME 
gnome-logs | GPLv3+ | Log viewer for the systemd journal 
gnome-menus | LGPLv2+ | A menu system for the GNOME project 
gnome-online-accounts | LGPLv2+ | Single sign-on framework for GNOME 
gnome-remote-desktop | GPL-2.0-or-later | GNOME Remote Desktop screen share service 
gnome-session | GPL-2.0-or-later | GNOME session manager 
gnome-settings-daemon | GPLv2+ | The daemon sharing settings from GNOME to GTK+/KDE applications 
gnome-shell | GPLv2+ | Window management and application launching for GNOME 
gnome-shell-extensions | GPLv2+ | Modify and extend GNOME Shell functionality and behavior 
gnome-software | GPL-2.0-or-later | A software center for GNOME 
gnome-system-monitor | GPL-2.0-or-later | Process and resource monitor 
gnome-terminal | GPL-3.0-or-later AND GFDL-1.3 | Terminal emulator for GNOME 
gnome-themes-extra | LGPLv2+ | GNOME Extra Themes 
gnome-user-docs | CC-BY-SA-3.0 | GNOME User Documentation 
gnu-efi | BSD | Development Libraries and headers for EFI 
gnupg2 | GPLv3+ | Utility for secure communication and data storage 
gnutls | GPLv3+ and LGPLv2+ | A TLS protocol implementation 
go-bindata | MIT | A small utility which generates Go code from any file 
gobject-introspection | GPLv2+ and LGPLv2+ and MIT | Introspection system for GObject-based libraries 
go-compilers | GPLv3+ | Go language compilers for various architectures 
golang | BSD and Public Domain | The Go Programming Language 
golang-github-cpuguy83-md2man | MIT | Converts markdown into roff (man pages) 
golang-github-prometheus | ASL 2.0 | Prometheus monitoring system and time series database 
golang-github-prometheus-node-exporter | ASL 2.0 and MIT | Exporter for machine metrics 
golist | BSD | A tool to analyse the properties of a Go (Golang) codebase 
google-benchmark | Apache-2.0 | A microbenchmark support library 
google-droid-fonts | ASL 2.0 | A set of general-purpose font families released by Google as part of Android 
google-guice | ASL 2.0 | Lightweight dependency injection framework for Java 5 and above 
google-noto-cjk-fonts | OFL | Google Noto Sans CJK Fonts 
google-noto-emoji-fonts | OFL-1.1 AND Apache-2.0 | Google Noto Emoji font 
google-noto-fonts | OFL | Hinted and Non Hinted OpenType fonts for Unicode scripts 
google-roboto-slab-fonts | ASL 2.0 | Google Roboto Slab fonts 
go-rpm-macros | GPLv3+ | Build-stage rpm automation for Go packages 
gperf | GPLv3+ | A perfect hash function generator 
gperftools | BSD | Very fast malloc and performance analysis tools 
gpgme | LGPLv2+ and MIT | GnuPG Made Easy - high level crypto API 
gpm | GPLv2 and OFSFDL | A mouse server for the Linux console 
graphene | MIT | Thin layer of types for graphic libraries 
graphite2 | (LGPLv2+ or GPLv2+ or MPLv1.1) and (Netscape or GPLv2+ or LGPLv2+) | Font rendering capabilities for complex non-Roman writing systems 
graphviz | EPL-1.0 | Graph Visualization Tools 
grep | GPLv3+ | Pattern matching utilities 
groff | GPLv3+ and GFDL and BSD and MIT | A document formatting system 
grub2 | GPLv3+ | Bootloader with support for Linux, Multiboot and more 
grubby | GPLv2+ | Command line tool for updating bootloader configs 
gsettings-desktop-schemas | LGPLv2+ | A collection of GSettings schemas 
gsl | GPLv3+ | The GNU Scientific Library for numerical analysis 
gsm | MIT | Shared libraries for GSM speech compressor 
gsound | LGPLv2 | Small gobject library for playing system sounds 
gspell | LGPLv2+ | Spell-checking library for GTK+ 
gssdp | LGPLv2+ | Resource discovery and announcement over SSDP 
gssproxy | MIT | Daemon for managing gss-api requests 
gstreamer1 | LGPLv2+ | GStreamer streaming media framework runtime 
gstreamer1-plugins-bad-free | LGPLv2+ and LGPLv2 | GStreamer streaming media framework "bad" plugins 
gstreamer1-plugins-base | LGPLv2+ | GStreamer streaming media framework base plugins 
gstreamer1-plugins-good | LGPLv2+ | GStreamer plugins with good code and licensing 
gtest | MIT | Intel® Data Mover Library (Intel® DML) 
gtk2 | LGPLv2+ | GTK+ graphical user interface library 
gtk3 | LGPLv2+ | GTK+ graphical user interface library 
gtk4 | LGPLv2+ | GTK graphical user interface library 
gtk-doc | GPLv2+ and GFDL | API documentation generation tool for GTK+ and GNOME 
gtkglext | GPL-2.0-or-later OR LGPL-2.0-or-later | OpenGL Extension to GTK 
gtkmm24 | LGPLv2+ | C++ interface for GTK2 (a GUI library for X) 
gtkmm30 | LGPLv2+ | C++ interface for the GTK+ library 
gtksourceview4 | LGPLv2+ | Source code editing widget 
gtksourceview5 | LGPL-2.1-or-later | Source code editing widget 
gtk-vnc | LGPLv2+ | A GTK widget for VNC clients 
gts | LGPLv2+ | GNU Triangulated Surface Library 
guava | ASL 2.0 and CC0 | Google Core Libraries for Java 
guile | LGPLv3+ | A GNU implementation of Scheme for application extensibility 
gumbo-parser | ASL 2.0 | A HTML5 parser 
gupnp | LGPLv2+ | A framework for creating UPnP devices & control points 
gupnp-dlna | LGPLv2+ | A collection of helpers for building UPnP AV applications 
gupnp-igd | LGPLv2+ | Library to handle UPnP IGD port mapping 
gv | GPLv3+ | A X front-end for the Ghostscript PostScript(TM) interpreter 
gvfs | GPLv3 and LGPLv2+ and BSD and MPLv2.0 | Backends for the gio framework in GLib 
gzip | GPLv3+ and GFDL | GNU data compression program 
hamcrest | BSD | Library of matchers for building test expressions 
haproxy | GPLv2+ | HAProxy reverse proxy for high availability environments 
harfbuzz | MIT | Text shaping library 
hatch | MIT | A modern project, package, and virtual env manager 
hdparm | BSD | A utility for displaying and/or setting hard disk parameters 
help2man | GPL-3.0-or-later | Create simple man pages from --help output 
hexedit | GPLv2+ | A hexadecimal file viewer and editor 
hfsplus-tools | APSL 2.0 | Tools to create/check Apple HFS+ filesystems 
hicolor-icon-theme | GPLv2+ | Basic requirement for icon themes 
hidapi | GPLv3 or BSD | Library for communicating with USB and Bluetooth HID devices 
highlight | GPLv3 | Universal source code to formatted text converter 
highway | Apache-2.0 | Efficient and performance-portable SIMD 
hiredis | BSD | Minimalistic C client library for Redis 
hivex | LGPLv2 | Read and write Windows Registry binary hive files 
hostname | GPLv2+ | Utility to set/show the host name or domain name 
httpcomponents-client | ASL 2.0 | HTTP agent implementation based on httpcomponents HttpCore 
httpcomponents-core | ASL 2.0 | Set of low level Java HTTP transport components for HTTP services 
httpd | ASL 2.0 | Apache HTTP Server 
http-parser | MIT | HTTP request/response parser for C 
hunspell | LGPLv2+ or GPLv2+ or MPLv1.1 | A spell checker and morphological analyzer library 
hunspell-en | LGPLv2+ and LGPLv2 and BSD | English hunspell dictionaries 
hwdata | GPLv2+ | Hardware identification and configuration data 
hwloc | BSD | Portable Hardware Locality - portable abstraction of hierarchical architectures 
hyphen | GPLv2 or LGPLv2+ or MPLv1.1 | A text hyphenation library 
ibus | LGPLv2+ | Intelligent Input Bus for Linux OS 
ibus-libpinyin | GPLv3+ | Intelligent Pinyin engine based on libpinyin for IBus 
ibus-m17n | GPL-2.0-or-later | The M17N engine for IBus platform 
icoutils | GPLv3+ | Utility for extracting and converting Microsoft icon and cursor files 
icu | MIT and UCD and Public Domain | International Components for Unicode 
iftop | GPLv2+ | Command line tool that displays bandwidth usage on an interface 
iio-sensor-proxy | GPLv3+ | IIO accelerometer sensor to input device proxy 
ima-evm-utils | GPLv2 | IMA/EVM support utilities 
ImageMagick | ImageMagick | An X application for displaying and manipulating images 
imath | BSD | Library of 2D and 3D vector, matrix, and math operations for computer graphics 
imlib2 | BSD | Image loading, saving, rendering, and manipulation library 
infinipath-psm | GPLv2 or BSD | Intel Performance Scaled Messaging (PSM) Libraries 
inih | BSD | Simple INI file parser library 
iniparser | MIT | C library for parsing "INI-style" files 
iniparser | MIT | C library for parsing "INI-style" files 
initial-setup | GPLv2+ | Initial system configuration utility 
initscripts | GPLv2 | basic legacy sysv init scripts used during a boot of the system 
inkscape | GPL-2.0-or-later AND CC-BY-3.0 | Vector-based drawing program using SVG 
intel-ipp-crypto-mb | Apache-2.0 | Intel(R) IPP Cryptography multi-buffer library 
intel-ipsec-mb | BSD | IPsec cryptography library optimized for Intel Architecture 
intel-QAT20 | BSD and (BSD or GPLv2) | QAT package(Library and sample application) 
intltool | GPLv2 with exceptions | Utility for internationalizing various kinds of data files 
iotop | GPLv2+ | Top like utility for I/O 
ipcalc | GPLv2+ | IP network address calculator 
iperf3 | BSD | Measurement tool for TCP/UDP bandwidth performance 
ipmitool | BSD-3-Clause-No-Nuclear-Warranty | Utility for IPMI control 
iproute | GPLv2+ and Public Domain | Advanced IP routing and network device configuration tools 
iprutils | CPL | Utilities for the IBM Power Linux RAID adapters 
ipset | GPLv2 | Manage Linux IP sets 
iptables | GPLv2 and Artistic Licence 2.0 and ISC | Tools for managing Linux kernel packet filtering capabilities 
iputils | BSD and GPLv2+ | Network monitoring tools including ping 
ipvsadm | GPLv2+ | Utility to administer the Linux Virtual Server 
ipxe | GPLv2 with additional permissions and BSD | A network boot loader 
ipython | (BSD and MIT and Python) and GPLv2+ | An enhanced interactive Python shell 
irqbalance | GPLv2 | IRQ balancing daemon 
iscsi-initiator-utils | GPLv2+ | iSCSI daemon and utility programs 
isns-utils | LGPLv2+ | The iSNS daemon and utility programs 
iso-codes | LGPLv2+ | ISO code lists and translations 
isomd5sum | GPLv2+ | Utilities for working with md5sum implanted in ISO images 
itstool | GPLv3+ | ITS-based XML translation tool 
iw | ISC | A nl80211 based wireless configuration tool 
jakarta-activation | BSD | Jakarta Activation Specification and Implementation 
jakarta-annotations | EPL-2.0 or GPLv2 with exceptions | Jakarta Annotations 
jakarta-mail | EPL-2.0 or GPLv2 with exceptions | Jakarta Mail API 
jakarta-servlet | (EPL-2.0 or GPLv2 with exceptions) and ASL 2.0 | Server-side API for handling HTTP requests and responses 
jansi | ASL 2.0 | Generate and interpret ANSI escape sequences in Java 
jansson | MIT | C library for encoding, decoding and manipulating JSON data 
jasper | JasPer | Implementation of the JPEG-2000 standard, Part 1 
java-11-alibaba-dragonwell | GPL-v2 | Alibaba Dragonwell11 JDK 
java-11-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 11 Runtime Environment 
java-17-alibaba-dragonwell | GPL-v2 | Alibaba Dragonwell17 JDK 
java-17-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 17 Runtime Environment 
java-1.8.0-alibaba-dragonwell | GPL-v2 | Alibaba Dragonwell 1.8.0 JDK 
java-1.8.0-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib | OpenJDK 8 Runtime Environment 
javapackages-bootstrap | ASL 2.0 and ASL 1.1 and (ASL 2.0 or EPL-2.0) and (EPL-2.0 or GPLv2 with exceptions) and MIT and BSD with advertising and BSD and EPL-1.0 and EPL-2.0 and CDDL-1.0 and xpp and CC0 and Public Domain | A means of bootstrapping Java Packages Tools 
javapackages-tools | BSD | Macros and scripts for Java packaging support 
jbig2dec | AGPLv3+ | A decoder implementation of the JBIG2 image compression format 
jbigkit | GPLv2+ | JBIG1 lossless image compression tools 
jdependency | ASL 2.0 | Class dependency analysis library for Java 
jdom | Saxpath | Java alternative to DOM and SAX 
jdom2 | Saxpath | Java manipulation of XML made easy 
jemalloc | BSD | General-purpose scalable concurrent malloc implementation 
jfsutils | GPLv2+ | Utilities for managing the JFS filesystem 
jitterentropy | BSD or GPLv2 | Library implementing the jitter entropy source 
jose | ASL 2.0 | Tools for JSON Object Signing and Encryption (JOSE) 
jpegxl | BSD-3-Clause AND Apache-2.0 AND Zlib | JPEG XL image format reference implementation 
jq | MIT and ASL 2.0 and CC-BY and GPLv3 | Command-line JSON processor 
js-jquery | MIT | JavaScript DOM manipulation, event handling, and AJAX library 
json-c | MIT | JSON implementation in C 
jsoncpp | Public Domain or MIT | JSON library implemented in C++ 
json-glib | LGPLv2+ | Library for JavaScript Object Notation format 
jsr-305 | BSD and CC-BY | Correctness annotations for Java code 
Judy | LGPL-2.0-or-later | General purpose dynamic array 
junit | EPL-1.0 | Java regression test package 
junit5 | EPL-2.0 | Java regression testing framework 
jupyterlab | Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND (CC-BY-4.0 AND OFL-1.1 AND MIT) AND ISC AND MIT AND Unlicense | JupyterLab computational environment 
jurand | Apache-2.0 | A tool for manipulating Java symbols 
kata-containers | Apache-2.0 | Kata Containers version 3.x repository 
kbd | GPLv2+ | Tools for configuring the console (keyboard, virtual terminals, etc.) 
kdump-anaconda-addon | GPLv2 | Kdump configuration anaconda addon 
keenopt | MulanPSLv2 | Auto-Tuning algorithm library of KeenTune 
keentune-bench | MulanPSLv2 | Benchmark script running models for KeenTune 
keentune-brain | MulanPSLv2 | Auto-Tuning algorithm module of KeenTune 
keentuned | MulanPSLv2 | KeenTune tuning tools 
keentune-target | MulanPSLv2 | Parameters setting, reading and backup models for KeenTune 
keentune-ui | MulanPSLv2 | KeenTune web UI 
keepalived | GPLv2+ | High Availability monitor built upon LVS, VRRP and service pollers 
kernel | GPLv2 and Redistributable, no modification permitted | The Linux kernel, based on version 6.6.25, heavily modified with backports 
kexec-tools | GPLv2 | The kexec/kdump userspace component 
keybinder3 | MIT | A library for registering global keyboard shortcuts 
keyutils | GPL-2.0-or-later AND LGPL-2.1-or-later | Linux Key Management Utilities 
kmod | GPLv2+ | Linux kernel module management utilities 
kpatch | GPLv2 | Dynamic kernel patch manager 
kpatch-build | GPLv2 | kpatch - live kernel patching 
krb5 | MIT | The Kerberos network authentication system 
ksh | EPL-2.0 | The Original ATT Korn Shell 
kubernetes | ASL 2.0 | Open Source Production-Grade Container Scheduling And Management Platform 
kyotocabinet | GPLv3 | A straightforward implementation of DBM 
kyua | BSD | Testing framework for infrastructure software 
lame | GPLv2+ | Free MP3 audio compressor 
langpacks | GPLv2+ | Langpacks meta-package 
langtable | GPL-3.0-or-later | Guessing reasonable defaults for locale, keyboard layout, territory, and language. 
lapack | BSD | Numerical linear algebra package libraries 
lasi | LGPLv2+ | C++ library for creating Postscript documents 
latex2html | GPL-2.0-or-later | Converts LaTeX documents to HTML 
lato-fonts | OFL | A sanserif typeface family 
lcms2 | MIT | Color Management Engine 
lcov | GPL-2.0-or-later | LTP GCOV extension code coverage tool 
ldns | BSD-3-Clause | Low-level DNS(SEC) library with API 
leptonica | Leptonica | C library for efficient image processing and image analysis operations 
less | GPLv3+ or BSD | A text file browser similar to more, but better 
leveldb | BSD | A fast and lightweight key/value database library by Google 
libabigail | ASL 2.0 | Set of ABI analysis tools 
libadwaita | LGPLv2+ | Building blocks for modern GNOME applications 
libaio | LGPLv2+ | Linux-native asynchronous I/O access library 
libao | GPLv2+ | Cross Platform Audio Output Library 
libappindicator | LGPLv2 and LGPLv3 | Application indicators library 
libappstream-glib | LGPLv2+ | Library for AppStream metadata 
libarchive | BSD 2-Clause License AND BSD 2-clause NetBSD License BSD 2-Clause License | A library for handling streaming archive formats 
libassuan | LGPLv2+ and GPLv3+ | GnuPG IPC library 
libasyncns | LGPLv2+ | Asynchronous Name Service Library 
libatasmart | LGPLv2+ | ATA S.M.A.R.T. Reading and Parsing Library 
libatomic_ops | GPLv2 and MIT | Atomic memory update operations 
libavc1394 | GPLv2+ and LGPLv2+ | Audio/Video Control library for IEEE-1394 devices 
libavif | BSD-2-Clause | Library for encoding and decoding .avif files 
libblockdev | LGPLv2+ | A library for low-level manipulation with block devices 
libbluray | LGPLv2+ | Library to access Blu-Ray disks for video playback 
libbpf | LGPLv2 or BSD | Libbpf library 
libbsd | Beerware AND BSD-2-Clause AND BSD-3-Clause AND BSD-4-Clause AND BSD-5-clause AND Expat AND ISC AND Public-Domain | Library providing BSD-compatible functions for portability 
libburn | GPLv2+ | Library for reading, mastering and writing optical discs 
libbytesize | LGPLv2+ | A library for working with sizes in bytes 
libcacard | LGPLv2+ | CAC (Common Access Card) library 
libcanberra | LGPLv2+ | Portable Sound Event Library 
libcap | BSD or GPLv2 | Library for getting and setting POSIX.1e capabilities 
libcap-ng | LGPLv2+ | Alternate posix capabilities library 
libcbor | MIT | libcbor is a C library for parsing and generating CBOR 
libcdio | GPLv3+ | CD-ROM input and control library 
libcdio-paranoia | GPLv3+ | CD paranoia on top of libcdio 
libcgroup | LGPLv2+ | iLibrary to control and monitor control groups 
libcloudproviders | LGPLv3+ | Library for integration of cloud storage providers 
libcomps | GPLv2+ | Comps XML file manipulation library 
libconfig | LGPLv2+ | C/C++ configuration file library 
libconfuse | ISC | A configuration file parser library 
libcxx | Apache-2.0 WITH LLVM-exception OR MIT OR NCSA | C++ standard library targeting C++11 
libdaemon | LGPLv2+ | Library for writing UNIX daemons 
libdatrie | LGPLv2+ | Datrie is an implementation of double-array structure for representing trie. 
libdazzle | GPLv3+ | Experimental new features for GTK+ and GLib 
libdb | BSD and LGPLv2 and Sleepycat | The Berkeley DB database library for C 
libdbi | LGPLv2+ | Database Independent Abstraction Layer for C 
libdbusmenu | LGPLv3 or LGPLv2 and GPLv3 | Library for passing menus over DBus 
libdecor | MIT | Wayland client side decoration library 
libdmx | MIT | X.Org X11 DMX runtime library 
libdnf | LGPL-2.1-or-later | Library providing simplified C and Python API to libsolv 
libdrm | MIT | Direct Rendering Manager runtime library 
libdv | LGPLv2+ | Software decoder for DV format video 
libdvdnav | GPLv2+ | A library for reading DVD video discs based on Ogle code 
libdvdread | GPLv2+ | A library for reading DVD video discs based on Ogle code 
libdwarf | LGPL-2.1-only AND BSD-2-Clause-FreeBSD | Library to access the DWARF Debugging file format 
libecb | BSD or GPLv2+ | Compiler built-ins 
libeconf | MIT | a highly flexible and configurable library to parse and manage key=value configuration files 
libedit | BSD | The NetBSD Editline library 
libei | MIT | Library for Emulated Input 
libell | LGPLv2+ | Embedded Linux library 
libepoxy | MIT | epoxy runtime library 
liberation-fonts | OFL | Fonts to replace commonly used Microsoft Windows fonts 
libesmtp | LGPLv2+ | SMTP client library 
libestr | LGPLv2+ | String handling essentials library 
libev | BSD or GPLv2+ | High-performance event loop/event model with lots of features 
libevdev | MIT | Kernel Evdev Device Wrapper Library 
libevent | BSD and ISC | Abstract asynchronous event notification library 
libexif | LGPLv2+ | Library for extracting extra information from image files 
libfabric | BSD or GPLv2 | Open Fabric Interfaces 
libfastjson | MIT | A JSON implementation in C 
libffado | GPLv2 or GPLv3 | Free firewire audio driver library 
libffi | MIT | A portable foreign function interface library 
libfido2 | BSD | FIDO2 library 
libfontenc | MIT | X.Org X11 libfontenc runtime library 
libfprint | LGPLv2+ | Toolkit for fingerprint scanner 
libftdi | LGPLv2 | Library to program and control the FTDI USB controller 
libgcrypt | LGPLv2+ | A general-purpose cryptography library 
libgdata | LGPLv2+ | Library for the GData protocol 
libgdl | LGPLv2+ | GNOME docking library 
libgee | LGPLv2+ | GObject collection library 
libgexiv2 | GPLv2+ | Gexiv2 is a GObject-based wrapper around the Exiv2 library 
libgit2 | GPLv2 with exceptions | C implementation of the Git core methods as a library with a solid API 
libglade2 | LGPLv2+ | The libglade library for loading user interfaces 
libglib-testing | LGPLv2+ | GLib-based test library and harness 
libglvnd | MIT | The GL Vendor-Neutral Dispatch library 
libgnomekbd | LGPLv2+ | A keyboard configuration library 
libgnome-keyring | GPLv2+ and LGPLv2+ | Framework for managing passwords and other secrets 
libgovirt | LGPLv2+ | A GObject library for interacting with oVirt REST API 
libgpg-error | LGPLv2+ | Library for error values used by GnuPG components 
libgphoto2 | GPLv2+ and GPLv2 | Library for accessing digital cameras 
libgsf | LGPLv2 | GNOME Structured File library 
libgtop2 | GPLv2+ | LibGTop library (version 2) 
libgudev | LGPLv2+ | GObject-based wrapper library for libudev 
libguestfs | LGPLv2+ | Access and modify virtual machine disk images 
libgusb | LGPLv2+ | GUsb is a GObject wrapper for libusb1 
libgweather | GPLv2+ | A library for weather information 
libgxps | LGPLv2+ | GObject based library for handling and rendering XPS documents 
libhandy | LGPLv2+ | Building blocks for modern adaptive GNOME apps 
libical | LGPLv2 or MPLv2.0 | An implementation of iCalendar protocols and data formats 
libICE | MIT | X.Org X11 ICE runtime library 
libid3tag | GPLv2+ | ID3 tag manipulation library 
libidn2 | (GPLv2+ or LGPLv3+) and GPLv3+ | Library to support IDNA2008 internationalized domain names 
libiec61883 | LGPLv2+ | Streaming library for IEEE1394 
libieee1284 | GPLv2+ | A library for interfacing IEEE 1284-compatible devices 
libijs | AGPLv3+ | IJS Raster Image Transport Protocol Library 
libimagequant | GPLv3+ and MIT | Palette quantization library 
libimobiledevice | LGPLv2+ | Library for connecting to mobile devices 
libindicator | GPLv3 | Shared functions for Ayatana indicators 
libinput | MIT | Input device library 
libipt | BSD | Intel Processor Trace Decoder Library 
libiptcdata | LGPLv2+ | IPTC tag library 
libiscsi | LGPLv2+ | iSCSI client library 
libisoburn | GPLv2+ | Library to enable creation and expansion of ISO-9660 filesystems 
libisofs | GPLv2+ and LGPLv2+ | Library to create ISO 9660 disk images 
libjaylink | GPLv2+ | Library for SEGGER J-Link and compatible devices 
libjcat | LGPL-2.1-or-later | Library for reading Jcat files 
libjpeg-turbo | IJG | A MMX/SSE2/SIMD accelerated library for manipulating JPEG image files 
libkcapi | BSD or GPLv2 | User space interface to the Linux Kernel Crypto API 
libksba | (LGPLv3+ or GPLv2+) and GPLv3+ | CMS and X.509 library 
libldac | ASL 2.0 | A lossy audio codec for Bluetooth connections 
libldb | LGPLv3+ | LDAP-like embedded database 
liblockfile | GPLv2+ and LGPLv2+ | This implements a number of functions found in -lmail on SysV systems 
liblognorm | LGPLv2+ | Fast samples-based log normalization library 
liblouis | LGPLv3+ | Braille translation and back-translation library 
liblzf | BSD or GPLv2+ | Small data compression library 
libmatchbox | LGPLv2+ | Libraries for the Matchbox Desktop 
libmaxminddb | ASL 2.0 and BSD | C library for the MaxMind DB file format 
libmbim | LGPLv2+ | Support library for the Mobile Broadband Interface Model protocol 
libmd | BSD-2-Clause AND BSD-3-Clause AND ISC AND Beerware AND Public-Domain | Library that provides message digest functions from BSD systems 
libmemcached-awesome | BSD-3-Clause | Client library and command line tools for memcached server 
libmetalink | MIT | C library to read Metalink XML download description format 
libmicrohttpd | LGPLv2+ | Lightweight library for embedding a webserver in applications 
libmng | zlib | Library for Multiple-image Network Graphics support 
libmnl | LGPLv2+ | A minimalistic Netlink library 
libmodulemd | MIT | Module metadata manipulation library 
libmpc | LGPLv3+ | C library for multiple precision complex arithmetic 
libmspack | LGPLv2 | Library for CAB and related files compression and decompression 
libmtp | LGPLv2+ | Software library for MTP media players 
libmysofa | BSD | C functions for reading HRTFs 
libnbd | LGPL-2.0-or-later AND BSD-3-Clause | NBD client library in userspace 
libndp | LGPLv2+ | Library for Neighbor Discovery Protocol 
libnet | BSD | C library for portable packet creation and injection 
libnetfilter_conntrack | GPLv2+ | A userspace library providing a API 
libnetfilter_cthelper | GPLv2 | User-space infrastructure for connection tracking helpers 
libnetfilter_cttimeout | GPLv2+ | Timeout policy tuning for Netfilter/conntrack 
libnetfilter_queue | GPLv2 | Netfilter queue userspace library 
libnfnetlink | GPLv2+ | Netfilter netlink userspace library 
libnftnl | GPLv2+ | Library for low-level interaction with nftables Netlink's API over libmnl 
libnice | LGPLv2 and MPLv1.1 | GLib ICE implementation 
libnl3 | LGPLv2 | Convenience library for kernel netlink sockets 
libnma | GPLv2+ and LGPLv2+ | NetworkManager GUI library 
libnotify | LGPLv2+ | Desktop notification library 
libnsl2 | BSD and LGPLv2+ | Public client interface library for NIS(YP) and NIS+ 
libnvme | LGPLv2+ | Linux-native nvme device management library 
libogg | BSD | The Ogg bitstream file format library 
libomp | Apache-2.0 WITH LLVM-exception OR NCSA | OpenMP runtime for clang 
libosinfo | LGPLv2+ | A library for managing OS information for virtualization 
libotf | LGPLv2+ | A Library for handling OpenType Font 
libpaper | LGPL-2.1-or-later | Library and tools for handling papersize 
libpcap | BSD with advertising | A system-independent interface for user-level packet capture 
libpciaccess | MIT | PCI access library 
libpeas | LGPL-2.1-or-later | Plug-ins implementation convenience library 
libpfm | MIT | Library to encode performance events for use by perf tool 
libpinyin | GPLv3+ | Library to deal with pinyin 
libpipeline | GPLv3+ | A pipeline manipulation library 
libplist | LGPLv2+ | Library for manipulating Apple Binary and XML Property Lists 
libpng | zlib | A library of functions for manipulating PNG image format files 
libportal | LGPLv3 | Flatpak portal library 
libpq | PostgreSQL | PostgreSQL client library 
libproxy | LGPLv2+ | A library that provides automatic proxy configuration management. 
libpsl | MIT | C library to handle the Publix Suffix List 
libpsm2 | BSD or GPLv2 | Intel PSM Libraries 
libpwquality | BSD-3-Clause OR GPL-2.0-or-later | A library for password generation and password quality checking 
libqb | LGPLv2+ | Library providing high performance logging, tracing, ipc, and poll 
libqmi | LGPLv2+ | Support library to use the Qualcomm MSM Interface (QMI) protocol 
libqrtr-glib | LGPLv2+ | Support library to use and manage the QRTR (Qualcomm IPC Router) bus. 
librabbitmq | MIT | a C-language AMQP client library for use with v2.0+ of the RabbitMQ broker. 
libraqm | MIT | Complex Textlayout Library 
LibRaw | BSD and (CDDL-1.0 or LGPLv2) | Library for reading RAW files obtained from digital photo cameras 
libraw1394 | LGPLv2+ | Library providing low-level IEEE-1394 access - 2.1.2- 
librdkafka | BSD | The Apache Kafka C library 
librelp | GPLv3+ | The Reliable Event Logging Protocol library 
librepo | LGPL-2.1-or-later | Repodata downloading library 
libreport | GPLv2+ | Generic library for reporting various problems 
libreswan | GPLv2 | Internet Key Exchange (IKEv1 and IKEv2) implementation for IPsec 
librevenge | ( LGPL-2.1-or-later OR MPL-2.0 ) AND BSD-3-Clause | A base library for writing document import filters 
librsvg2 | LGPLv2+ | An SVG library based on cairo 
libsamplerate | BSD | Sample rate conversion library for audio data 
libsass | MIT | C/C++ port of the Sass CSS precompiler 
libseccomp | LGPLv2 | Enhanced seccomp library 
libsecret | LGPLv2+ | Library for storing and retrieving passwords and other secrets 
libselinux | LicenseRef-Fedora-Public-Domain | SELinux library and simple utilities 
libsemanage | LGPLv2+ | SELinux binary policy manipulation library 
libsepol | LGPLv2+ | SELinux binary policy manipulation library 
libserf | ASL 2.0 | High-Performance Asynchronous HTTP Client Library 
libshout | LGPLv2+ and MIT | Icecast source streaming library 
libsigc++20 | LGPLv2+ | Typesafe signal framework for C++ 
libsigsegv | GPLv2+ | Library for handling page faults in user mode 
libslirp | BSD and MIT | A general purpose TCP-IP emulator 
libSM | MIT | X.Org X11 SM runtime library 
libsmbios | GPLv2+ or OSL 2.1 | Libsmbios C/C++ shared libraries 
libsmi | GPLv2+ and BSD | A library to access SMI MIB information 
libsndfile | LGPLv2+ and GPLv2+ and BSD | Library for reading and writing sound files 
libsodium | ISC | The Sodium crypto library 
libsolv | BSD | Package dependency solver 
libsoup | LGPLv2 | Soup, an HTTP library implementation 
libsoup3 | LGPL-2.0-or-later | Soup, an HTTP library implementation 
libspectre | GPLv2+ | A library for rendering PostScript(TM) documents 
libspiro | GPLv3+ | Library to simplify the drawing of beautiful curves 
libsrtp | BSD | An implementation of the Secure Real-time Transport Protocol (SRTP) 
libssh | LGPLv2+ | A library implementing the SSH protocol 
libssh2 | BSD | A library implementing the SSH2 protocol 
libstoragemgmt | LGPLv2+ | Storage array management library 
libtalloc | LGPLv3+ | The talloc library 
libtasn1 | GPLv3+ and LGPLv2+ | The ASN.1 library used in GNUTLS 
libtdb | LGPLv3+ | The tdb library 
libteam | LGPLv2+ | Library for controlling team network device 
libtevent | LGPLv3+ | The tevent library 
libthai | LGPLv2+ | Thai language support routines 
libtheora | BSD | Theora Video Compression Codec 
libtiff | libtiff | Library of functions for manipulating TIFF format image files 
libtimezonemap | GPLv3 | Time zone map widget for Gtk+ 
libtirpc | SISSL and BSD | Transport Independent RPC Library 
libtool | GPLv2+ and LGPLv2+ and GFDL | The GNU Portable Library Tool 
libtpms | BSD | Library providing Trusted Platform Module (TPM) functionality 
libtraceevent | LGPLv2+ and GPLv2+ | Library to parse raw trace event formats 
libudfread | LGPLv2+ | UDF reader library 
libuninameslist | BSD | A library providing Unicode character names and annotations 
libunistring | GPLv2+ or LGPLv3+ | library for manipulating Unicode strings and C strings 
libunwind | BSD | An unwinding library 
liburing | (GPLv2 with exceptions and LGPLv2+) or MIT | Linux-native io_uring I/O access library 
libusb | LGPLv2+ | Compatibility shim around libusb-1.0 offering the old 0.1 API 
libusbmuxd | LGPLv2+ | Client library USB multiplex daemon for Apple's iOS devices 
libusbx | LGPLv2+ | Library for accessing USB devices 
libuser | LGPLv2+ | A user and group account administration library 
libutempter | LGPLv2+ | A privileged helper for utmp/wtmp updates 
libuv | MIT and BSD and ISC | Platform layer for node.js 
libva | MIT | Video Acceleration (VA) API for Linux 
libvdpau | MIT | Wrapper library for the Video Decode and Presentation API 
libverto | MIT | Main loop abstraction library 
libvirt | GPL-2.0-or-later AND LGPL-2.1-only AND LGPL-2.1-or-later AND OFL-1.1 | Library providing a simple virtualization API 
libvirt-glib | LGPLv2+ | libvirt glib integration for events 
libvirt-python | LGPLv2+ | The libvirt virtualization API python3 binding 
libvisual | LGPLv2+ | Abstraction library for audio visualisation plugins 
libvncserver | GPLv2+ | Library to make writing a VNC server easy 
libvoikko | GPL-2.0-or-later | Voikko is a library for spellcheckers and hyphenators 
libvorbis | BSD | The Vorbis General Audio Compression Codec 
libvpx | BSD | VP8/VP9 Video Codec SDK 
libwacom | MIT | Tablet Information Client Library 
libwebp | BSD | Library and tools for the WebP graphics format 
libwmf | LGPLv2+ and GPLv2+ and GPL+ | Windows MetaFile Library 
libwnck3 | LGPLv2+ | Window Navigator Construction Kit 
libwpd | LGPLv2+ or MPLv2.0 | A library for import of WordPerfect documents 
libwpe | BSD | General-purpose library for the WPE-flavored port of WebKit 
libwpg | LGPL-2.1-or-later OR MPL-2.0 | A library for import of WordPerfect Graphics images 
libX11 | MIT | Core X11 protocol client library 
libXau | MIT | Sample Authorization Protocol for X 
libXaw | MIT | X Athena Widget Set 
libxcb | MIT | A C binding to the X11 protocol 
libXcomposite | MIT | X Composite Extension library 
libxcrypt | LGPLv2+ and BSD and Public Domain | Extended crypt library for descrypt, md5crypt, bcrypt, and others 
libXcursor | MIT | Cursor management library 
libxcvt | MIT | VESA CVT standard timing modelines generator 
libXdamage | MIT | X Damage extension library 
libXdmcp | MIT | X Display Manager Control Protocol library 
libXext | MIT | X.Org X11 libXext runtime library 
libxfce4ui | LGPLv2+ | Commonly used Xfce widgets 
libxfce4util | LGPLv2+ | Utility library for the Xfce4 desktop environment 
libXfixes | MIT | X Fixes library 
libXfont2 | MIT | X.Org X11 libXfont2 runtime library 
libXft | MIT | X.Org X11 libXft runtime library 
libXi | MIT | X.Org X11 libXi runtime library 
libXinerama | MIT | X.Org X11 libXinerama runtime library 
libxkbcommon | MIT | X.Org X11 XKB parsing library 
libxkbfile | MIT | X.Org X11 libxkbfile runtime library 
libxklavier | LGPLv2+ | High-level API for X Keyboard Extension 
libxml2 | MIT | Library providing XML and HTML support 
libxml++ | LGPLv2+ | C++ wrapper for the libxml2 XML parser library 
libxmlb | LGPLv2+ | Library for querying compressed XML metadata 
libXmu | MIT | X.Org X11 libXmu/libXmuu runtime libraries 
libXpm | MIT | X.Org X11 libXpm runtime library 
libXpresent | MIT | A Xlib-compatible API for the Present extension 
libXrandr | MIT | X.Org X11 libXrandr runtime library 
libXrender | MIT | X.Org X11 libXrender runtime library 
libXres | MIT | X-Resource extension client library 
libXScrnSaver | MIT | X.Org X11 libXss runtime library 
libxshmfence | MIT | X11 shared memory fences 
libxslt | MIT | Library providing the Gnome XSLT engine 
libXt | MIT | X.Org X11 libXt runtime library 
libXtst | MIT | X.Org X11 libXtst runtime library 
libXv | MIT | X.Org X11 libXv runtime library 
libXvMC | MIT | X.Org X11 libXvMC runtime library 
libXxf86vm | MIT | X.Org X11 libXxf86vm runtime library 
libyaml | MIT | A C library for parsing and emitting YAML 
libzip | BSD | C library for reading, creating, and modifying zip archives 
lilypond | GPLv3 | A typesetting system for music notation 
linuxconsoletools | GPLv2+ | Tools for connecting joysticks & legacy devices to the kernel's input subsystem 
linuxdoc-tools | MIT | A text formatting package based on SGML 
linux-firmware | GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted | Firmware files used by the Linux kernel 
lksctp-tools | GPLv2 and GPLv2+ and LGPLv2 and MIT | Linux Kernel Stream Control Transmission Protocol Tools 
lld | Apache-2.0 WITH LLVM-exception OR NCSA | The LLVM Linker 
lldb | Apache-2.0 WITH LLVM-exception OR NCSA | Next generation high-performance debugger 
lldpad | GPLv2 | Intel LLDP Agent 
llhttp | MIT | Port of http_parser to llparse 
llvm15 | Apache-2.0 WITH LLVM-exception OR NCSA | The Low Level Virtual Machine 
llvm | Apache-2.0 WITH LLVM-exception OR NCSA | The Low Level Virtual Machine 
lmdb | OpenLDAP | An ultra-fast, ultra-compact, crash-proof, key-value, embedded data store 
lm_sensors | GPLv2+ and Verbatim and MIT | Hardware monitoring tools 
lockdev | LGPLv2 | A library for locking devices 
logrotate | GPLv2+ | Rotates, compresses, removes and mails system log files 
logwatch | MIT | Analyzes and Reports on system logs 
lohit-assamese-fonts | OFL | Free Assamese font 
lohit-bengali-fonts | OFL | Free Bengali script font 
lohit-devanagari-fonts | OFL | Free Devanagari Script Font 
lohit-gujarati-fonts | OFL | Free Gujarati font 
lohit-kannada-fonts | OFL | Free Kannada font 
lohit-marathi-fonts | OFL | Free truetype font for Marathi language 
lohit-odia-fonts | OFL | Free truetype font for Odia language 
lohit-tamil-fonts | OFL | Free truetype font for Tamil language 
lohit-telugu-fonts | OFL | Open-source Telugu font family 
lorax | GPL-2.0-or-later | Tool for creating the anaconda install images 
lorax-templates-anolis | GPLv2+ | Anolis OS build templates for lorax and livemedia-creator 
low-memory-monitor | GPLv3+ | Monitors low-memory conditions 
lsb-release | GPL-2.0-or-later | Linux Standard Base Release Tools 
lshw | GPLv2 | Hardware lister 
lsof | zlib and Sendmail and LGPLv2+ | A utility which lists open files on a Linux/UNIX system 
lsscsi | GPLv2+ | List SCSI devices (or hosts) and associated information 
lttng-ust | LGPLv2 and GPLv2 and MIT | LTTng Userspace Tracer library 
lua | MIT | Powerful light-weight programming language 
luajit | MIT | Just-In-Time Compiler for Lua 
lua-posix | MIT | POSIX library for Lua 
luarocks | BSD | A deployment and management system for Lua modules 
lua-rpm-macros | MIT | The common Lua RPM macros 
lujavrite | Apache-2.0 | Lua library for calling Java code 
luksmeta | LGPLv2+ | Utility for storing small metadata in the LUKSv1 header 
lutok | BSD | Lightweight C++ API library for Lua 
lv2 | ISC | Audio Plugin Standard 
lvm2 | GPLv2 | Userland logical volume management tools 
lxc | LGPLv2+ and GPLv2 | Linux Resource Containers 
lz4 | GPLv2+ and BSD | Extremely fast compression algorithm 
lzip | GPL-3.0-or-later | LZMA compressor with integrity checking 
lzo | GPLv2+ | Data compression library with very fast (de)compression 
lzop | GPLv2+ | Real-time file compressor 
m17n-db | LGPLv2+ | Multilingualization datafiles for m17n-lib 
m17n-lib | LGPLv2+ | Multilingual text library 
m4 | GPLv3+ | GNU macro processor 
mailcap | Public Domain and MIT | Helper application and MIME type associations for file types 
mailx | BSD with advertising and MPLv1.1 | Enhanced implementation of the mailx command 
make | GPLv3+ | A GNU tool which simplifies the build process for users 
malcontent | LGPLv2+ | Parental controls implementation 
mallard-rng | MIT | RELAX NG schemas for all Mallard versions 
man-db | GPLv2+ and GPLv3+ | Tools for searching and reading man pages 
mandoc | ISC | A suite of tools for compiling mdoc and man 
mariadb | GPLv2 and LGPLv2 | A very fast and robust SQL database server 
mariadb-connector-c | LGPLv2+ | The MariaDB Native Client library (C driver) 
marshalparser | MIT | Parser for Python internal Marshal format 
matchbox-window-manager | GPLv2+ | Window manager for the Matchbox Desktop 
maven | ASL 2.0 and MIT | Java project management and project comprehension tool 
maven-antrun-plugin | ASL 2.0 | Maven AntRun Plugin 
maven-archiver | ASL 2.0 | Maven Archiver 
maven-common-artifact-filters | ASL 2.0 | Maven Common Artifact Filters 
maven-compiler-plugin | ASL 2.0 | Maven Compiler Plugin 
maven-dependency-tree | ASL 2.0 | Maven dependency tree artifact 
maven-file-management | ASL 2.0 | Maven File Management API 
maven-filtering | ASL 2.0 | Shared component providing resource filtering 
maven-jar-plugin | ASL 2.0 | Maven JAR Plugin 
maven-plugin-bundle | ASL 2.0 | Maven Bundle Plugin 
maven-remote-resources-plugin | ASL 2.0 | Maven Remote Resources Plugin 
maven-resolver | ASL 2.0 | Apache Maven Artifact Resolver library 
maven-resources-plugin | ASL 2.0 | Maven Resources Plugin 
maven-shared-incremental | ASL 2.0 | Maven Incremental Build support utilities 
maven-shared-io | ASL 2.0 | API for I/O support like logging, download or file scanning 
maven-shared-utils | ASL 2.0 | Maven shared utility classes 
maven-source-plugin | ASL 2.0 | Plugin creating source JAR 
maven-surefire | ASL 2.0 and CPL | Test framework project 
maven-verifier | ASL 2.0 | Apache Maven Verifier Component 
maven-wagon | ASL 2.0 | Tools to manage artifacts and deployment 
mcelog | GPLv2 | Tool to translate x86-64 CPU Machine Check Exception data 
mcstrans | GPL+ | SELinux Translation Daemon 
mdadm | GPLv2+ | The mdadm program controls Linux md devices (software RAID arrays) 
mdevctl | LGPLv2 | A mediated device management utility for Linux 
mecab | BSD or LGPLv2+ or GPL+ | Yet Another Part-of-Speech and Morphological Analyzer 
memcached | BSD-3-clause | High Performance, Distributed Memory Object Cache 
memstrack | GPLv3 | A memory allocation tracer, like a hot spot analyzer for memory allocation 
mercurial | GPLv2+ | A fast, lightweight Source Control Management system 
mesa | MIT AND BSD-3-Clause AND SGI-B-2.0 | Mesa graphics libraries 
mesa-demos | MIT | Mesa demos 
mesa-libGLU | SGI-B-1.1 AND SGI-B-2.0 AND MIT | Mesa libGLU library 
meson | ASL 2.0 | High productivity build system 
mftrace | GPLv2 | Utility for converting TeX bitmap fonts to Type 1 or TrueType fonts 
microcode_ctl | GPLv2+ and Redistributable, no modification permitted | Tool to transform and deploy CPU microcode update for x86 
microdnf | GPLv2+ | Lightweight implementation of DNF in C 
mimalloc | MIT | A general purpose allocator with excellent performance 
miniz | MIT AND Unlicense | Compression library implementing the zlib and Deflate 
minizip-ng | zlib | Minizip-ng contrib in zlib-ng with the latest bug fixes and advanced features 
mkfontscale | MIT | Tool to generate legacy X11 font system index files 
mlocate | GPLv2 | An utility for finding files by name 
mm-common | GPLv2+ | Common build files of the C++ bindings 
mobile-broadband-provider-info | Public Domain | Mobile broadband provider database 
mock | GPL-2.0-or-later | Builds packages inside chroots 
mockito | MIT | Tasty mocking framework for unit tests in Java 
mod_auth_gssapi | MIT | A GSSAPI Authentication module for Apache 
ModemManager | GPL-2.0+ | Mobile broadband modem management service 
mod_http2 | ASL 2.0 | module implementing HTTP/2 for Apache 2 
modulemaker-maven-plugin | Apache-2.0 | A plugin for creating module-info.class files 
mod_wsgi | ASL 2.0 | A WSGI interface for Python web applications in Apache 
mokutil | GPLv3+ | Tool to manage UEFI Secure Boot MoK Keys 
mold | AGPL-3.0-or-later AND (Apache-2.0 OR MIT) | A Modern Linker 
mousetweaks | GPLv3 and GFDL | Mouse accessibility support for the GNOME desktop 
mozilla-filesystem | MPLv1.1 | Mozilla filesytem layout 
mozjs102 | MPL-2.0 AND Apache-2.0 AND BSD-3-Clause AND BSD-2-Clause AND MIT AND GPL-3.0-or-later | SpiderMonkey JavaScript library 
mozjs78 | MPLv2.0 and MPLv1.1 and BSD and GPLv2+ and GPLv3+ and LGPLv2+ and AFL and ASL 2.0 | SpiderMonkey JavaScript library 
mpdecimal | BSD | Library for general decimal arithmetic 
mpfr | LGPLv3+ | C library for multiple-precision floating-point computations 
mpg123 | LGPLv2+ | Real time MPEG 1.0/2.0/2.5 audio player/decoder for layers 1, 2 and 3 
mpich | MIT | A high-performance implementation of MPI 
mtdev | MIT | Multitouch Protocol Translation Library 
mtools | GPLv3+ | Programs for accessing MS-DOS disks without mounting the disks 
mtr | GPLv2 | Network diagnostic tool combining 'traceroute' and 'ping' 
mt-st | GPL+ | Tool for controlling tape drives 
multipath-tools | GPLv2 | Tools to manage multipath devices using multipath-tool 
munge | GPLv3+ and LGPLv3+ | Enables uid & gid authentication across a host cluster 
mupdf | AGPL-3.0-or-later | A lightweight PDF viewer and toolkit 
mutter | GPLv2+ | Window and compositing manager based on Clutter 
mysql | GPLv2 with exceptions and LGPLv2 and BSD | MySQL client programs and shared libraries 
mysql-selinux | GPLv3 | SELinux policies for product 
nano | GPLv3+ | A small text editor 
nasm | BSD | A portable x86 assembler which uses Intel-like syntax 
nautilus | GPL-3.0-or-later | File manager for GNOME 
ncompress | Public Domain | Fast compression and decompression utilities 
ncurses | MIT | Ncurses support utilities 
ndctl | GPLv2 | Manage "libnvdimm" subsystem devices (Non-volatile Memory) 
nerdctl | Apache-2.0 | Docker-compatible CLI for containerd 
netpbm | BSD and GPLv2 and IJG and MIT and Public Domain | A library for handling different graphics file formats 
netperf | MIT and BSD | Benchmark to measure the performance of many different types of networking 
net-snmp | BSD | A collection of SNMP protocol tools and libraries 
nettle | LGPLv3+ or GPLv2+ | A low-level cryptographic library 
net-tools | GPLv2+ | Basic networking tools 
NetworkManager | GPL-2.0-or-later AND LGPL-2.1-or-later | Network connection manager and user applications 
network-manager-applet | GPLv2+ | Tray applet and an advanced network connection editor 
NetworkManager-libreswan | GPLv2+ | NetworkManager VPN plug-in for IPsec VPN 
newt | LGPLv2 | A library for text mode user interfaces 
nfs4-acl-tools | BSD | The nfs4 ACL tools 
nfs-utils | MIT and GPLv2 and GPLv2+ and BSD | NFS utilities and supporting clients and daemons for the kernel NFS server 
nftables | GPLv2 | Netfilter Tables userspace utilites 
nghttp2 | MIT | Experimental HTTP/2 client, server and proxy 
nginx | BSD | A high performance web server and reverse proxy server 
ninja-build | ASL 2.0 | Small build system with a focus on speed 
nload | GPLv2+ | A tool can monitor network traffic and bandwidth usage in real time 
nmap | Nmap | Network exploration tool and security scanner 
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime 
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging 
noslate-anode | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime 
npth | LGPLv2+ | The New GNU Portable Threads library 
nspr | MPLv2.0 | Netscape Portable Runtime 
nss | MPLv2.0 | Network Security Services 
nss-altfiles | LGPLv2+ | NSS module to look up users in /usr/lib/passwd too 
nss-mdns | LGPLv2+ | glibc plugin for .local name resolution 
nss_wrapper | BSD-3-Clause | A wrapper for the user, group and hosts NSS API 
ntfs-3g | GPLv2+ | Linux NTFS userspace driver 
ntfs-3g-system-compression | GPLv2+ | NTFS-3G plugin for reading "system compressed" files 
numactl | GPLv2 | Library for tuning for Non Uniform Memory Access machines 
numad | LGPLv2 | NUMA user daemon 
numatop | BSD-3-Clause | Memory access locality characterization and analysis 
numpy | BSD and Python and ASL 2.0 | A fast multidimensional array facility for Python 
nv-codec-headers | MIT | FFmpeg version of Nvidia Codec SDK headers 
nvme-cli | GPLv2 | NVMe management command line interface 
nvml | BSD | Persistent Memory Development Kit (formerly NVML) 
nydus-rs | Apache-2.0 | Nydus image service 
nydus-snapshotter | Apache-2.0 | A containerd snapshotter with data deduplication and lazy loading 
oath-toolkit | GPLv3+ | One-time password components 
objectweb-asm | BSD | Java bytecode manipulation and analysis framework 
objenesis | Apache-2.0 | A library for instantiating Java objects 
ocaml | QPL and (LGPLv2+ with exceptions) | OCaml compiler and programming environment 
ocaml-alcotest | ISC | Lightweight and colorful test framework for OCaml 
ocaml-astring | ISC | Alternative String module for OCaml 
ocaml-augeas | LGPLv2+ with exceptions | OCaml bindings for Augeas configuration API 
ocaml-base | MIT | Jane Street standard library for OCaml 
ocaml-bigarray-compat | ISC | Compatibility library to use Stdlib.Bigarray when possible 
ocaml-bisect-ppx | MIT AND BSD-3-Clause | Code coverage for OCaml and Reason 
ocaml-calendar | LGPLv2 | Objective Caml library for managing dates and times 
ocaml-cinaps | MIT | Trivial Metaprogramming tool using the OCaml toplevel 
ocaml-cmdliner | ISC | Declarative definition of command line interfaces for OCaml 
ocaml-compiler-libs-janestreet | MIT | OCaml compiler libraries repackaged 
ocaml-cppo | BSD | Equivalent of the C preprocessor for OCaml programs 
ocaml-csexp | MIT | Parsing and printing of S-expressions in canonical form 
ocaml-ctypes | MIT | Combinators for binding to C libraries without writing any C 
ocaml-curses | LGPLv2+ | OCaml bindings for ncurses 
ocaml-dune | MIT and LGPLv2 and LGPLv2 with exceptions and ISC | A composable build system for OCaml 
ocaml-fileutils | LGPLv2 with exceptions | OCaml library for common file and filename operations 
ocaml-findlib | MIT | Objective CAML package manager and build helper 
ocaml-fmt | ISC | OCaml Format pretty-printer combinators 
ocaml-gettext | LGPLv2+ with exceptions | OCaml library for i18n 
ocaml-integers | MIT | Various signed and unsigned integer types for OCaml 
ocaml-labltk | LGPLv2+ with exceptions | Tcl/Tk interface for OCaml 
ocaml-libvirt | LGPLv2+ | OCaml binding for libvirt 
ocaml-luv | MIT | OCaml binding to libuv for cross-platform asynchronous I/O 
ocaml-lwt | LGPLv2+ with exceptions | OCaml lightweight thread library 
ocaml-mmap | LGPLv2+ with exceptions | File mapping functionality 
ocaml-ocamlbuild | LGPLv2 with exceptions | Build tool for OCaml libraries and programs 
ocaml-ocplib-endian | LGPLv2+ with exceptions | Functions to read/write int16/32/64 from strings, bigarrays 
ocaml-ounit | MIT | Unit test framework for OCaml 
ocaml-ppx-derivers | BSD | Deriving plugin registry 
ocaml-ppxlib | MIT | Base library and tools for ppx rewriters 
ocaml-re | LGPLv2 with exceptions | A regular expression library for OCaml 
ocaml-react | ISC | OCaml framework for Functional Reactive Programming (FRP) 
ocaml-result | BSD | Compat result type 
ocaml-seq | LGPLv2+ with exceptions | Compatibility package for OCaml's standard iterator type 
ocaml-sexplib0 | MIT | Definition of S-expressions and some base converters 
ocaml-srpm-macros | GPLv2+ | OCaml architecture macros 
ocaml-stdio | MIT | Jane Street Standard I/O library for OCaml 
ocaml-topkg | ISC | The transitory OCaml software packager 
ocaml-uutf | ISC | Non-blocking streaming Unicode codec for OCaml 
oniguruma | BSD | Regular expressions library 
openal-soft | LGPL-2.0-or-later AND BSD-3-Clause | Open Audio Library 
openblas | BSD | An optimized BLAS library based on GotoBLAS2 
openblas-srpm-macros | MIT | OpenBLAS architecture macros 
opencl-filesystem | Public Domain | OpenCL filesystem layout 
opencl-headers | MIT | OpenCL (Open Computing Language) header files 
opencsd | BSD-3-Clause | An open source CoreSight(tm) Trace Decode library 
OpenEXR | BSD | Provides the specification and reference implementation of the EXR file format 
openjade | DMIT | A DSSSL implementation 
openjpeg2 | BSD and MIT | C-Library for JPEG 2000 
openldap | OpenLDAP | LDAP support libraries 
openmpi | BSD and MIT and Romio | Open Message Passing Interface 
opensc | LGPLv2+ | Smart card library and applications 
openscap | LGPLv2+ | Set of open source libraries enabling integration of the SCAP line of standards 
opensm | GPLv2 or BSD | OpenIB InfiniBand Subnet Manager and management utilities 
opensp | MIT | SGML and XML parser 
openssh | BSD-3-Clause AND BSD-2-Clause AND ISC AND SSH-OpenSSH AND ssh-keyscan AND sprintf AND LicenseRef-Fedora-Public-Domain AND X11-distribute-modifications-variant | An open source implementation of SSH protocol version 2 
openssl1.1 | OpenSSL and ASL 2.0 | Compatibility version of the OpenSSL library 
openssl | ASL 2.0 | Utilities from the general purpose cryptography library with TLS implementation 
openssl-pkcs11 | LGPLv2+ and BSD | A PKCS#11 engine for use with OpenSSL 
opentest4j | ASL 2.0 | Open Test Alliance for the JVM 
openvswitch | ASL 2.0 and LGPLv2+ and SISSL | Open vSwitch daemon/database/utilities 
openwsman | BSD-3-Clause AND MIT | Open source Implementation of WS-Management 
opus | BSD | An audio codec for use in low-delay speech and audio communication 
orangefs | LGPLv2+ and LGPLv2 and BSD and MIT and zlib | Parallel network file system client 
orc | BSD | The Oil Run-time Compiler 
osgi-annotation | ASL 2.0 | Annotations for use in compiling OSGi bundles 
osgi-compendium | ASL 2.0 | Interfaces and Classes for use in compiling OSGi bundles 
osgi-core | ASL 2.0 | OSGi Core API 
osinfo-db | LGPLv2+ | osinfo database files 
osinfo-db-tools | GPLv2+ | Tools for managing the osinfo database 
os-prober | GPLv2+ and GPL+ | Probes disks on the system for installed operating systems 
ostree | LGPLv2+ | Tool for managing bootable, immutable filesystem trees 
p11-kit | BSD | Library for loading and sharing PKCS#11 modules 
PackageKit | GPLv2+ and LGPLv2+ | Package management service 
paktype-naskh-basic-fonts | GPLv2 with exceptions | Fonts for Arabic, Farsi, Urdu and Sindhi from PakType 
pam | GPL-2.0-or-later OR BSD-3-Clause | A Security Interface for Applications in Authentication activities 
pam_wrapper | GPLv3+ | A tool to test PAM applications and PAM modules 
pango | LGPLv2+ | System for layout and rendering of internationalized text 
pangomm | LGPLv2+ | C++ interface for Pango 
parted | GPL-3.0-or-later | The GNU disk partition manipulation program 
passivetex | MIT | Macros to process XSL formatting objects 
passwd | BSD or GPL+ | An utility for setting or changing passwords using PAM 
patch | GPLv3+ | Utility for modifying/upgrading files 
patchelf | GPLv3+ | A utility for patching ELF binaries 
patchutils | GPLv2+ | A collection of programs for manipulating patch files 
pbzip2 | BSD | Parallel implementation of bzip2 
pcaudiolib | GPLv3+ | Portable C Audio Library 
pciutils | GPLv2+ | PCI bus related utilities 
pcmciautils | GPLv2 | PCMCIA utilities and initialization programs 
pcp | GPLv2+ and LGPLv2+ and CC-BY | System-level performance monitoring and performance management 
pcre2 | BSD | Perl-compatible regular expression library 
pcre | BSD | Perl-compatible regular expression library 
pcsc-lite | BSD | PC/SC Lite smart card framework and applications 
pcsc-lite-ccid | LGPLv2+ | Generic USB CCID smart card reader driver 
percona-toolkit | GPLv2 | Advanced MySQL and system command-line tools 
percona-xtrabackup-24 | GPLv2 | XtraBackup online backup for MySQL / InnoDB 
percona-xtrabackup-80 | GPLv2 | XtraBackup online backup for MySQL / InnoDB 
Percona-XtraDB-Cluster-57 | Copyright (c) 2000, 2010, Oracle and/or its affiliates.  All rights reserved.  Use is subject to license terms. Under the GNU General Public License (http | A High Availability solution based on Percona Server and Galera 
percona-xtradb-cluster | Copyright (c) 2000, 2010, Oracle and/or its affiliates.  All rights reserved.  Use is subject to license terms. Under the GNU General Public License (http | A High Availability solution based on Percona Server and Galera 
perl | GPL+ or Artistic | Practical Extraction and Report Language 
perl-Algorithm-C3 | GPL+ or Artistic | Module for merging hierarchies using the C3 algorithm 
perl-Algorithm-Diff | GPL+ or Artistic | Compute 'intelligent' differences between two files/lists 
perl-aliased | GPL+ or Artistic | Use shorter versions of class names 
perl-Archive-Any-Lite | GPL+ or Artistic | Simple CPAN package extractor 
perl-Archive-Tar | GPL+ or Artistic | A module for Perl manipulation of .tar files 
perl-Archive-Zip | (GPL+ or Artistic) and BSD | Archive 
perl-Array-Diff | GPL+ or Artistic | Find the differences between two arrays 
perl-Authen-SASL | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl library for performing SASL authentication 
perl-autodie | GPL+ or Artistic | Replace functions with ones that succeed or die 
perl-autovivification | GPL+ or Artistic | Lexically disable autovivification 
perl-B-COW | GPL-1.0-or-later OR Artistic-1.0-Perl | Additional B helpers to check Copy On Write status 
perl-B-Hooks-EndOfScope | GPL+ or Artistic | Execute code after scope compilation finishes 
perl-BibTeX-Parser | GPL+ or Artistic | Pure Perl BibTeX parser 
perl-bignum | GPL+ or Artistic | Transparent big number support for Perl 
perl-Bit-Vector | (GPLv2+ or Artistic) and LGPLv2+ | Efficient bit vector, set of integers and "big int" math library 
perl-B-Keywords | GPL-1.0-or-later OR Artistic-1.0-Perl | Lists of reserved barewords and symbol names 
perl-boolean | GPL-1.0-or-later OR Artistic-1.0-Perl | Boolean support for Perl 
perl-Business-ISBN | Artistic-2.0 | Perl module to work with International Standard Book Numbers 
perl-Business-ISBN-Data | Artistic-2.0 | The data pack for Business 
perl-Business-ISMN | Artistic 2.0 | Perl library for International Standard Music Numbers 
perl-Business-ISSN | Artistic 2.0 | Perl library for International Standard Serial Numbers 
perl-Canary-Stability | GPL-1.0-or-later OR Artistic-1.0-Perl | Canary to check perl compatibility for Schmorp's modules 
perl-Capture-Tiny | ASL 2.0 | Capture STDOUT and STDERR from Perl, XS or external programs 
perl-Carp | GPL-1.0-or-later OR Artistic-1.0-Perl | Alternative warn and die for modules 
perl-Carp-Clan | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module to print improved warning messages 
perl-CBOR-XS | GPLv3+ and (BSD or GPLv2+) | Concise Binary Object Representation (CBOR) 
perl-CGI | Artistic-2.0 | Handle Common Gateway Interface requests and responses 
perl-Class-Accessor | GPL+ or Artistic | Automated accessor generation 
perl-Class-Data-Inheritable | GPL+ or Artistic | Inheritable, overridable class data 
perl-Class-Inspector | GPL-1.0-or-later OR Artistic-1.0-Perl | Get information about a class and its structure 
perl-Class-Iterator | GPL+ or Artistic | Iterator class 
perl-Class-Load | GPL+ or Artistic | A working (require "Class 
perl-Class-Load-XS | Artistic 2.0 | XS implementation of parts of Class 
perl-Class-Method-Modifiers | GPL-1.0-or-later OR Artistic-1.0-Perl | Provides Moose-like method modifiers 
perl-Class-Singleton | GPL+ or Artistic | Implementation of a "Singleton" class 
perl-Class-Tiny | Apache-2.0 | Minimalist class construction 
perl-Class-XSAccessor | GPL+ or Artistic | Generate fast XS accessors without run-time compilation 
perl-Clone | GPL-1.0-or-later OR Artistic-1.0-Perl | Recursively copy perl data types 
perl-Color-ANSI-Util | GPL-1.0-or-later OR Artistic-1.0-Perl | Routines for dealing with ANSI colors 
perl-Color-RGB-Util | GPL-1.0-or-later OR Artistic-1.0-Perl | Utilities related to RGB colors 
perl-ColorThemeBase-Static | GPL-1.0-or-later OR Artistic-1.0-Perl | Base class for color theme modules with static list of items 
perl-ColorThemeRole-ANSI | GPL-1.0-or-later OR Artistic-1.0-Perl | Roles for using ColorTheme 
perl-ColorThemes-Standard | GPL-1.0-or-later OR Artistic-1.0-Perl | Standard collection of generic color themes 
perl-ColorThemeUtil-ANSI | GPL-1.0-or-later OR Artistic-1.0-Perl | Utility routines related to color themes and ANSI code 
perl-common-sense | GPL+ or Artistic | "Common sense" Perl defaults 
perl-Compress-Bzip2 | GPL-1.0-or-later OR Artistic-1.0-Perl | Interface to Bzip2 compression library 
perl-Compress-LZF | GPL+ or Artistic | Extremely light-weight Lempel-Ziv-Free compression 
perl-Compress-Raw-Bzip2 | GPL-1.0-or-later OR Artistic-1.0-Perl | Low-level interface to bzip2 compression library 
perl-Compress-Raw-Lzma | GPL+ or Artistic | Low-level interface to lzma compression library 
perl-Compress-Raw-Zlib | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND Zlib | Low-level interface to the zlib compression library 
perl-Config-AutoConf | GPL+ or Artistic | A module to implement some of AutoConf macros in pure Perl 
perl-Config-Perl-V | GPL+ or Artistic | A module that will return you the output of 'perl -V' in a structure 
perl-Config-Tiny | GPL+ or Artistic | Perl module for reading and writing .ini style configuration files 
perl-constant | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl pragma to declare constants 
perl-constant-defer | GPLv3+ | Constant subs with deferred value calculation 
perl-Const-Fast | GPL+ or Artistic | Facility for creating read-only scalars, arrays, and hashes 
perl-Contextual-Return | GPL+ or Artistic | Create context-sensitive return values 
perl-Convert-ASN1 | GPL-1.0-or-later OR Artistic-1.0-Perl | ASN.1 encode/decode library 
perl-Convert-Bencode | GPL+ or Artistic | Functions for converting to/from bencoded strings 
perl-Coro-Multicore | (GPL+ or Artistic) and (Public Domain or CC0) | Make Coro threads on multiple cores with specially supported modules 
perl-CPAN | GPL+ or Artistic | Query, download and build perl modules from CPAN sites 
perl-CPAN-Changes | GPL+ or Artistic | Read and write Changes files 
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename 
perl-Cpanel-JSON-XS | GPL+ or Artistic | JSON 
perl-CPAN-Meta | GPL+ or Artistic | Distribution metadata for a CPAN dist 
perl-CPAN-Meta-Check | GPL-1.0-or-later OR Artistic-1.0-Perl | Verify requirements in a CPAN 
perl-CPAN-Meta-Requirements | GPL+ or Artistic | Set of version requirements for a CPAN dist 
perl-CPAN-Meta-YAML | GPL+ or Artistic | Read and write a subset of YAML for CPAN Meta files 
perl-Crypt-PasswdMD5 | GPL+ or Artistic | Provides interoperable MD5-based crypt() functions 
perl-Crypt-RC4 | GPL+ or Artistic | Perl implementation of the RC4 encryption algorithm 
perl-Data-Binary | Artistic 2.0 | Simple detection of binary versus text in strings 
perl-Data-Compare | GPL+ or Artistic | Compare perl data structures 
perl-Data-Dump | GPL+ or Artistic | Pretty printing of data structures 
perl-Data-Dumper | GPL-1.0-or-later OR Artistic-1.0-Perl | Stringified perl data structures, suitable for printing and eval 
perl-Data-Dumper-Concise | GPL+ or Artistic | A convenient way to reproduce a set of Dumper options 
perl-Data-Dumper-Names | GPL-1.0-or-later OR Artistic-1.0-Perl | Data 
perl-Data-OptList | GPL+ or Artistic | Parse and validate simple name or value option pairs 
perl-Data-Peek | GPL-1.0-or-later OR Artistic-1.0-Perl | A collection of low-level debug functions 
perl-Data-Section | GPL+ or Artistic | Read multiple hunks of data out of your DATA section 
perl-Data-Section-Simple | GPL+ or Artistic | Read data from __DATA__ 
perl-Data-Uniqid | GPL+ or Artistic | Perl extension for simple generating of unique id's 
perl-Data-UUID | BSD and MIT | Globally/Universally Unique Identifiers (GUIDs/UUIDs) 
perl-Date-Calc | GPL-1.0-or-later OR Artistic-1.0-Perl | Gregorian calendar date calculations 
perl-Date-ISO8601 | GPL+ or Artistic | Three ISO 8601 numerical calendars 
perl-Date-Manip | GPL+ or Artistic | Date manipulation routines 
perl-DateTime | Artistic-2.0 | Date and time object for Perl 
perl-DateTime-Calendar-Julian | GPL+ or Artistic | Julian Calendar support for DateTime.pm 
perl-DateTime-Calendar-Mayan | GPL+ or Artistic | Mayan Long Count Calendar 
perl-DateTime-Format-Builder | Artistic-2.0 AND (GPL-1.0-or-later OR Artistic-1.0-Perl) | Create DateTime parser classes and objects 
perl-DateTime-Format-MySQL | GPL+ or Artistic | Parse and format MySQL dates and times 
perl-DateTime-Format-Strptime | Artistic 2.0 | Parse and format strptime and strftime patterns 
perl-DateTime-Locale | ( GPL-1.0-or-later OR Artistic-1.0-Perl ) AND Unicode-DFS-2015 | Localization support for DateTime.pm 
perl-DateTime-TimeZone | ( GPL-1.0-or-later OR Artistic-1.0-Perl ) AND LicenseRef-Fedora-Public-Domain | Time zone object base class and factory 
perl-DateTime-TimeZone-SystemV | GPL+ or Artistic | System V and POSIX timezone strings 
perl-DateTime-TimeZone-Tzfile | GPL+ or Artistic | Tzfile (zoneinfo) timezone files 
perl-DBD-MariaDB | GPL+ or Artistic | MariaDB and MySQL driver for the Perl5 Database Interface (DBI) 
perl-DBD-MySQL | GPL+ or Artistic | A MySQL interface for Perl 
perl-DBD-SQLite | ( GPL-1.0-or-later OR Artistic-1.0-Perl ) AND LicenseRef-Fedora-Public-Domain | SQLite DBI Driver 
perl-DB_File | GPL+ or Artistic | Perl5 access to Berkeley DB version 1.x 
perl-DBI | GPL-1.0-or-later OR Artistic-1.0-Perl | A database access API for perl 
perl-DBM-Deep | GPL+ or Artistic | A pure perl multi-level hash/array DBM 
perl-Declare-Constraints-Simple | GPL+ or Artistic | Declarative Validation of Data Structures 
perl-Devel-ArgNames | GPL+ or Artistic | Figure out the names of variables passed into subroutines 
perl-Devel-CallChecker | GPL-1.0-or-later OR Artistic-1.0-Perl | Custom op checking attached to subroutines 
perl-Devel-Caller | GPL+ or Artistic | Meatier versions of caller 
perl-Devel-CheckBin | GPL+ or Artistic | Check that a command is available 
perl-Devel-CheckLib | GPL+ or Artistic | Check that a library is available 
perl-Devel-Cycle | GPL+ or Artistic | Find memory cycles in objects 
perl-Devel-GlobalDestruction | GPL+ or Artistic | Expose PL_dirty, the flag that marks global destruction 
perl-Devel-Hide | GPL+ or Artistic | Forces the unavailability of specified Perl modules (for testing) 
perl-Devel-LexAlias | GPL+ or Artistic | Alias lexical variables 
perl-Devel-OverloadInfo | GPL+ or Artistic | Introspect overloaded operators 
perl-Devel-PartialDump | GPL+ or Artistic | Partial dumping of data structures, optimized for argument printing 
perl-Devel-PPPort | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl Pollution Portability header generator 
perl-Devel-Size | GPL+ or Artistic | Perl extension for finding the memory usage of Perl variables 
perl-Devel-StackTrace | Artistic-2.0 | Perl module implementing stack trace and stack trace frame objects 
perl-Devel-Symdump | GPL-1.0-or-later OR Artistic-1.0-Perl | A Perl module for inspecting Perl's symbol table 
perl-Digest | GPL+ or Artistic | Digest - Modules that calculate message digests 
perl-Digest-CRC | Public Domain | Generic CRC functions 
perl-Digest-HMAC | GPL-1.0-or-later OR Artistic-1.0-Perl | Keyed-Hashing for Message Authentication 
perl-Digest-MD4 | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl interface to the MD4 Algorithm 
perl-Digest-MD5 | (GPL+ or Artistic) and BSD | Perl interface to the MD5 algorithm 
perl-Digest-Perl-MD5 | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl implementation of Ron Rivest's MD5 Algorithm 
perl-Digest-SHA1 | GPL+ or Artistic | Digest-SHA1 Perl module 
perl-Digest-SHA | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl extension for SHA-1/224/256/384/512 
perl-Dist-CheckConflicts | GPL+ or Artistic | Declare version conflicts for your dist 
perl-DynaLoader-Functions | GPL-1.0-or-later OR Artistic-1.0-Perl | Deconstructed dynamic C library loading 
perl-Email-Date-Format | GPL-1.0-or-later OR Artistic-1.0-Perl | Produce RFC 2822 date strings 
perl-Email-Valid | GPL+ or Artistic | Check validity of internet email address 
perl-Encode | (GPL+ or Artistic) and Artistic 2.0 and UCD | Character encodings in Perl 
perl-Encode-EUCJPASCII | GPL+ or Artistic | EucJP-ascii - An eucJP-open mapping 
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding 
perl-Env | GPL+ or Artistic | Perl module that imports environment variables as scalars or arrays 
perl-Env-Sanctify | GPL+ or Artistic | Lexically scoped sanctification of %ENV 
perl-Error | (GPL+ or Artistic) and MIT | Error/exception handling in an OO-ish way 
perl-Eval-Closure | GPL+ or Artistic | Safely and cleanly create closures via string eval 
perl-Exception-Class | GPL+ or Artistic | Module that allows you to declare real exception classes in Perl 
perl-experimental | GPL+ or Artistic | Experimental features made easy 
perl-Exporter | GPL+ or Artistic | Implements default import method for modules 
perl-Exporter-Tiny | GPL-1.0-or-later OR Artistic-1.0-Perl | An exporter with the features of Sub 
perl-ExtUtils-CBuilder | GPL+ or Artistic | Compile and link C code for Perl modules 
perl-ExtUtils-CChecker | GPL+ or Artistic | Configure-time utilities for using C headers, libraries, or OS features 
perl-ExtUtils-Config | GPL+ or Artistic | A wrapper for perl's configuration 
perl-ExtUtils-Depends | GPL+ or Artistic | Easily build XS extensions that depend on XS extensions 
perl-ExtUtils-HasCompiler | GPL-1.0-or-later OR Artistic-1.0-Perl | Check for the presence of a compiler 
perl-ExtUtils-Helpers | GPL+ or Artistic | Various portability utilities for module builders 
perl-ExtUtils-Install | GPL+ or Artistic | Install Perl files from here to there 
perl-ExtUtils-InstallPaths | GPL+ or Artistic | Build.PL install path logic made easy 
perl-ExtUtils-LibBuilder | GPL+ or Artistic | Perl library to build C libraries and programs 
perl-ExtUtils-MakeMaker | GPL-1.0-or-later OR Artistic-1.0-Perl | Create a module Makefile 
perl-ExtUtils-MakeMaker-CPANfile | GPL-1.0-or-later OR Artistic-1.0-Perl | CPANfile support for ExtUtils 
perl-ExtUtils-Manifest | GPL+ or Artistic | Utilities to write and check a MANIFEST file 
perl-ExtUtils-ParseXS | GPL+ or Artistic | Module and a script for converting Perl XS code into C code 
perl-ExtUtils-PkgConfig | LGPLv2+ | Simplistic interface to pkg-config 
perl-Fedora-VSP | GPL-3.0-or-later | Perl version normalization for RPM 
perl-File-BaseDir | GPL-1.0-or-later OR Artistic-1.0-Perl | Use the Freedesktop.org base directory specification 
perl-File-chdir | GPL+ or Artistic | A more sensible way to change directories 
perl-File-Copy-Recursive | GPL-1.0-or-later OR Artistic-1.0-Perl | Extension for recursively copying files and directories 
perl-File-DesktopEntry | GPL+ or Artistic | Object to handle .desktop files 
perl-File-DirList | GPL-1.0-or-later OR Artistic-1.0-Perl | Provide a sorted list of directory content 
perl-File-Fetch | GPL+ or Artistic | Generic file fetching mechanism 
perl-File-Find-Iterator | GPL+ or Artistic | Iterator interface for search files 
perl-File-Find-Object | GPL-2.0-or-later OR Artistic-2.0 | Object oriented File 
perl-File-Find-Object-Rule | GPL+ or Artistic | Alternative interface to File 
perl-File-Find-Rule | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module implementing an alternative interface to File 
perl-File-Find-Rule-Perl | GPL-1.0-or-later OR Artistic-1.0-Perl | Common rules for searching for Perl things 
perl-FileHandle-Fmode | GPL+ or Artistic | FileHandle 
perl-File-HomeDir | GPL+ or Artistic | Find your home and other directories on any platform 
perl-File-Listing | GPL-1.0-or-later OR Artistic-1.0-Perl | Parse directory listing 
perl-File-MimeInfo | GPL-1.0-or-later OR Artistic-1.0-Perl | Determine file type and open application 
perl-File-Path | GPL+ or Artistic | Create or remove directory trees 
perl-File-pushd | ASL 2.0 | Change directory temporarily for a limited scope 
perl-File-ReadBackwards | GPL-1.0-or-later OR Artistic-1.0-Perl | Read a file backwards by lines 
perl-File-Remove | GPL-1.0-or-later OR Artistic-1.0-Perl | Convenience module for removing files and directories 
perl-File-ShareDir | GPL+ or Artistic | Locate per-dist and per-module shared files 
perl-File-ShareDir-Install | GPL+ or Artistic | Install shared files 
perl-File-Slurp | GPL-1.0-or-later OR Artistic-1.0-Perl | Efficient Reading/Writing of Complete Files 
perl-File-Slurper | GPL-1.0-or-later OR Artistic-1.0-Perl | Simple, sane and efficient module to slurp a file 
perl-File-Temp | GPL+ or Artistic | Return name and handle of a temporary file safely 
perl-File-Touch | GPL+ or Artistic | Update access, modification timestamps, creating nonexistent files 
perl-File-TreeCreate | MIT | Recursively create a directory tree 
perl-File-Which | GPL+ or Artistic | Portable implementation of the 'which' utility 
perl-Filter | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl source filters 
perl-Filter-Simple | GPL+ or Artistic | Simplified Perl source filtering 
perl-Font-AFM | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl interface to Adobe Font Metrics files 
perl-Font-TTF | Artistic-2.0 | Perl library for modifying TTF font files 
perl-FreezeThaw | GPL-1.0-or-later OR Artistic-1.0-Perl | Convert Perl structures to strings and back 
perl-Future | GPL+ or Artistic | Perl object system to represent an operation awaiting completion 
perl-Future-AsyncAwait | GPL-1.0-or-later OR Artistic-1.0-Perl | Deferred subroutine syntax for futures 
perl-GD | GPL-1.0-or-later OR Artistic-2.0 | Perl interface to the GD graphics library 
perl-generators | GPL-1.0-or-later | RPM Perl dependencies generators 
perl-Getopt-Long | GPLv2+ or Artistic | Extended processing of command line options 
perl-gettext | GPL+ or Artistic | Interface to gettext family of functions 
perl-GitLab-API-v4 | GPL+ or Artistic | Complete GitLab API v4 client 
perl-Git-Wrapper | GPL+ or Artistic | Wrap git command-line interface for Perl 
perl-Glib | LGPLv2+ | Perl interface to GLib 
perl-Graphics-ColorNamesLite-WWW | GPL-1.0-or-later OR Artistic-1.0-Perl | WWW color names and equivalent RGB values 
perl-GSSAPI | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl extension providing access to the GSSAPIv2 library 
perl-Hook-LexWrap | GPL+ or Artistic | Lexically scoped subroutine wrappers 
perl-HTML-Form | GPL+ or Artistic | Class that represents an HTML form element 
perl-HTML-Formatter | GPL-1.0-or-later OR Artistic-1.0-Perl | HTML formatter modules 
perl-HTML-Parser | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module for parsing HTML 
perl-HTML-Tagset | GPL+ or Artistic | HTML 
perl-HTML-Tree | GPL+ or Artistic | HTML tree handling modules for Perl 
perl-HTTP-Cookies | GPL-1.0-or-later OR Artistic-1.0-Perl | HTTP cookie jars 
perl-HTTP-Daemon | GPL-1.0-or-later OR Artistic-1.0-Perl | Simple HTTP server class 
perl-HTTP-Date | GPL-1.0-or-later OR Artistic-1.0-Perl | Date conversion routines 
perl-HTTP-Message | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND CC0-1.0 | HTTP style message 
perl-HTTP-Negotiate | GPL-1.0-or-later OR Artistic-1.0-Perl | Choose a variant to serve 
perl-HTTP-Tiny | GPL+ or Artistic | A simple, small correct HTTP/1.1 client 
perl-HTTP-Tiny-Multipart | Artistic 2.0 | Add post_multipart to HTTP 
perl-Importer | GPL+ or Artistic | Alternative interface to modules that export symbols 
perl-Import-Into | GPL+ or Artistic | Import packages into other packages 
perl-inc-latest | Apache-2.0 | Use modules bundled in inc/ if they are newer than installed ones 
perl-Inline | GPL-1.0-or-later OR Artistic-1.0-Perl | Inline Perl module 
perl-Inline-C | GPL-1.0-or-later OR Artistic-1.0-Perl | Write Perl subroutines in C 
perl-Inline-Files | GPL-1.0-or-later OR Artistic-1.0-Perl | Allows for multiple inline files in a single Perl file 
perl-IO-All | GPL-1.0-or-later OR Artistic-1.0-Perl | IO 
perl-IO-CaptureOutput | GPL+ or Artistic | Capture STDOUT/STDERR from sub-processes and XS/C modules 
perl-IO-Compress | GPL-1.0-or-later OR Artistic-1.0-Perl | Read and write compressed data 
perl-IO-Compress-Brotli | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl bindings for Brotli compression 
perl-IO-Compress-Lzma | GPL+ or Artistic | Read and write lzma compressed data 
perl-IO-HTML | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND LicenseRef-Fedora-Public-Domain | Open an HTML file with automatic character set detection 
perl-IO-Prompter | GPL+ or Artistic | Prompt for input, read it, clean it, return it 
perl-IO-Socket-INET6 | GPL+ or Artistic | Perl Object interface for AF_INET|AF_INET6 domain sockets 
perl-IO-Socket-IP | GPL+ or Artistic | Drop-in replacement for IO 
perl-IO-Socket-SSL | (GPL+ or Artistic) and MPLv2.0 | Perl library for transparent SSL 
perl-IO-String | GPL+ or Artistic | Emulate file interface for in-core strings 
perl-IO-stringy | GPL+ or Artistic | I/O on in-core objects like strings and arrays for Perl 
perl-IO-Tty | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND BSD-2-Clause | Perl interface to pseudo tty's 
perl-IO-Zlib | GPL+ or Artistic | Perl IO 
perl-IPC-Cmd | GPL+ or Artistic | Finding and running system commands made easy 
perl-IPC-Run | GPL+ or Artistic | Perl module for interacting with child processes 
perl-IPC-Run3 | GPL-1.0-or-later OR Artistic-1.0-Perl OR BSD-2-Clause | Run a subprocess in batch mode 
perl-IPC-System-Simple | GPL+ or Artistic | Run commands simply, with detailed diagnostics 
perl-IPC-SysV | GPL+ or Artistic | IPC-SysV 
perl-Jcode | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl extension interface for converting Japanese text 
perl-JSON | GPL-1.0-or-later OR Artistic-1.0-Perl | Parse and convert to JSON (JavaScript Object Notation) 
perl-JSON-Color | GPL-1.0-or-later OR Artistic-1.0-Perl | Encode to colored JSON 
perl-JSON-MaybeXS | GPL-1.0-or-later OR Artistic-1.0-Perl | Use Cpanel 
perl-JSON-PP | GPL+ or Artistic | JSON 
perl-JSON-XS | GPL+ or Artistic | JSON serializing/de-serializing, done correctly and fast 
perl-LaTeX-ToUnicode | GPL+ or Artistic | Convert LaTeX commands to Unicode 
perl-LDAP | GPL-1.0-or-later OR Artistic-1.0-Perl | LDAP Perl module 
perl-libintl-perl | GPLv3+ and LGPLv2+ | Internationalization library for Perl, compatible with gettext 
perl-libnet | GPL+ or Artistic | Perl clients for various network protocols 
perl-libwww-perl | GPL-1.0-or-later OR Artistic-1.0-Perl | A Perl interface to the World-Wide Web 
perl-libxml-perl | (GPL+ or Artistic) and Public Domain | A collection of Perl modules for working with XML 
perl-Lingua-EN-Inflect | GPL+ or Artistic | Convert singular to plural, select "a" or "an" 
perl-Lingua-Translit | GPL+ or Artistic | Transliterates text between writing systems 
perl-List-AllUtils | Artistic-2.0 AND CC-BY-4.0 | Combines List 
perl-List-Compare | GPL-1.0-or-later OR Artistic-1.0-Perl | Compare elements of two or more lists 
perl-List-MoreUtils | (GPL+ or Artistic) and ASL 2.0 | Provide the stuff missing in List 
perl-List-MoreUtils-XS | (GPL+ or Artistic) and ASL 2.0 | Provide compiled List 
perl-List-SomeUtils | GPL-1.0-or-later OR Artistic-1.0-Perl | Provide the stuff missing in List 
perl-List-UtilsBy | GPL-1.0-or-later OR Artistic-1.0-Perl | Higher-order list utility functions 
perl-Locale-Codes | GPL-1.0-or-later OR Artistic-1.0-Perl | Distribution of modules to handle locale codes 
perl-Locale-Maketext | GPL+ or Artistic | Framework for localization 
perl-Locale-US | GPL+ or Artistic | Two letter codes for state identification in the United States and vice versa 
perl-local-lib | GPL+ or Artistic | Create and use a local lib/ for perl modules with PERL5LIB 
perl-Log-Any | GPL-1.0-or-later OR Artistic-1.0-Perl | Bringing loggers and listeners together 
perl-Log-Any-Adapter-Screen | GPL+ or Artistic | Send logs to screen, with colors and some other features 
perl-Log-Dispatch | Artistic-2.0 | Dispatches messages to one or more outputs 
perl-Log-Dispatch-FileRotate | GPL+ or Artistic | Log to files that archive/rotate themselves 
perl-Log-Log4perl | GPL-1.0-or-later OR Artistic-1.0-Perl | Log4j implementation for Perl 
perl-LWP-MediaTypes | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND CC0-1.0 | Guess media type for a file or a URL 
perl-LWP-Protocol-https | GPL-1.0-or-later OR Artistic-1.0-Perl | Provide HTTPS support for LWP 
perl-Mail-Sender | GPL+ or Artistic | Module for sending mails with attachments through an SMTP server 
perl-Mail-Sendmail | GPL+ or Artistic | Simple platform independent mailer for Perl 
perl-MailTools | GPL+ or Artistic | Various ancient mail-related perl modules 
perl-Math-BigInt | GPL-1.0-or-later OR Artistic-1.0-Perl | Arbitrary-size integer and float precision mathematics 
perl-Math-BigInt-FastCalc | GPL-1.0-or-later OR Artistic-1.0-Perl | Math 
perl-Math-BigInt-GMP | GPL-1.0-or-later OR Artistic-1.0-Perl | Use the GMP library for Math 
perl-Math-BigRat | GPL+ or Artistic | Arbitrary big rational constants 
perl-MCE | GPL-1.0-or-later OR Artistic-1.0-Perl | Many-core Engine for Perl providing parallel processing capabilities 
perl-MIME-Base64 | (GPL+ or Artistic) and MIT | Encoding and decoding of Base64 and quoted-printable strings 
perl-MIME-Charset | GPL-1.0-or-later OR Artistic-1.0-Perl | Charset Informations for MIME 
perl-MIME-Lite | GPL+ or Artistic | MIME 
perl-MIME-Types | GPL-1.0-or-later OR Artistic-1.0-Perl | MIME types module for Perl 
perl-Mixin-Linewise | GPL-1.0-or-later OR Artistic-1.0-Perl | Write your linewise code for handles; this does the rest 
perl-MLDBM | GPL+ or Artistic | Store multi-level hash structure in single level tied hash 
perl-Module-Build | GPL-1.0-or-later OR Artistic-1.0-Perl | Build and install Perl modules 
perl-Module-Build-Tiny | GPL+ or Artistic | A tiny replacement for Module 
perl-Module-CoreList | GPL-1.0-or-later OR Artistic-1.0-Perl | What modules are shipped with versions of perl 
perl-Module-CPANfile | GPL-1.0-or-later OR Artistic-1.0-Perl | Parse cpanfile 
perl-Module-CPANTS-Analyse | GPL+ or Artistic | Generate Kwalitee ratings for a distribution 
perl-Module-Find | GPL+ or Artistic | Find and use installed modules in a (sub)category 
perl-Module-Implementation | Artistic 2.0 | Loads one of several alternate underlying implementations for a module 
perl-Module-Install | GPL+ or Artistic | Standalone, extensible Perl module installer 
perl-Module-Install-AuthorRequires | GPL-1.0-or-later OR Artistic-1.0-Perl | Declare author-only dependencies 
perl-Module-Install-AuthorTests | GPL-1.0-or-later OR Artistic-1.0-Perl | Designate tests only run by module authors 
perl-Module-Install-AutoLicense | perl-Module-Install-AutoLicense
 GPL-1.0-or-later OR Artistic-1.0-Perl
 https | Module 
perl-Module-Install-GithubMeta | GPL-1.0-or-later OR Artistic-1.0-Perl | A Module 
perl-Module-Install-ManifestSkip | GPL-1.0-or-later OR Artistic-1.0-Perl | Generate a MANIFEST.SKIP file 
perl-Module-Install-ReadmeFromPod | GPL-1.0-or-later OR Artistic-1.0-Perl | Module 
perl-Module-Install-ReadmeMarkdownFromPod | GPL-1.0-or-later OR Artistic-1.0-Perl | Create README.mkdn from POD 
perl-Module-Install-Repository | GPL-1.0-or-later OR Artistic-1.0-Perl | Automatically sets repository URL from Svn/Svk/Git checkout 
perl-Module-Load | GPL+ or Artistic | Run-time require of both modules and files 
perl-Module-Load-Conditional | GPL+ or Artistic | Looking up module information / loading at run-time 
perl-Module-Load-Util | GPL-1.0-or-later OR Artistic-1.0-Perl | Some utility routines related to module loading 
perl-Module-Manifest | GPL-1.0-or-later OR Artistic-1.0-Perl | Parse and examine a Perl distribution MANIFEST file 
perl-Module-Manifest-Skip | GPL-1.0-or-later OR Artistic-1.0-Perl | MANIFEST.SKIP Manangement for Modules 
perl-Module-Metadata | GPL+ or Artistic | Gather package and POD information from perl module files 
perl-Module-Package | GPL-1.0-or-later OR Artistic-1.0-Perl | Postmodern Perl Module Packaging 
perl-Module-Package-Au | CC0 | Reusable Module 
perl-Module-Pluggable | GPL-1.0-or-later OR Artistic-1.0-Perl | Automatically give your module the ability to have plugins 
perl-Module-Refresh | GPL-1.0-or-later OR Artistic-1.0-Perl | Refresh %INC files when updated on disk 
perl-Module-Runtime | GPL-1.0-or-later OR Artistic-1.0-Perl | Runtime module handling 
perl-Module-Runtime-Conflicts | GPL+ or Artistic | Provide information on conflicts for Module 
perl-Module-ScanDeps | GPL-1.0-or-later OR Artistic-1.0-Perl | Recursively scan Perl code for dependencies 
perl-Module-Signature | CC0 | CPAN signature management utilities and modules 
perl-Mojolicious | Artistic 2.0 | A next generation web framework for Perl 
perl-Moo | GPL+ or Artistic | Minimalist Object Orientation (with Moose compatibility) 
perl-Moose | GPL-1.0-or-later OR Artistic-1.0-Perl | Complete modern object system for Perl 5 
perl-Mozilla-CA | MPLv2.0 | Mozilla's CA cert bundle in PEM format 
perl-MRO-Compat | GPL+ or Artistic | Mro 
perl-namespace-autoclean | GPL+ or Artistic | Keep imports out of your namespace 
perl-namespace-clean | GPL+ or Artistic | Keep your namespace tidy 
perl-Net-DNS | (GPL+ or Artistic) and MIT | DNS resolver modules for Perl 
perl-Net-Domain-TLD | GPL+ or Artistic | Work with TLD names 
perl-Net-HTTP | GPL-1.0-or-later OR Artistic-1.0-Perl | Low-level HTTP connection (client) 
perl-Net-IMAP-Simple | GPL-1.0-or-later OR Artistic-1.0-Perl | Simple IMAP account handling 
perl-Net-IMAP-Simple-SSL | GPL-1.0-or-later OR Artistic-1.0-Perl | Simple IMAP account handling with SSL 
perl-Net-LibIDN2 | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl binding for GNU Libidn2 
perl-Net-Ping | GPL+ or Artistic | Check a remote host for reachability 
perl-Net-SMTP-SSL | GPL+ or Artistic | SSL support for Net 
perl-Net-SSLeay | Artistic 2.0 | Perl extension for using OpenSSL 
perl-NTLM | GPL+ or Artistic | NTLM Perl module 
perl-Number-Compare | GPL+ or Artistic | Perl module for numeric comparisons 
perl-Object-Deadly | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module providing an object that dies whenever examined 
perl-Object-HashBase | GPL+ or Artistic | Build hash-based classes 
perl-OLE-Storage_Lite | GPL+ or Artistic | Simple Class for OLE document interface 
perl-Package-DeprecationManager | Artistic 2.0 | Manage deprecation warnings for your distribution 
perl-Package-Generator | GPL+ or Artistic | Generate new packages quickly and easily 
perl-Package-Stash | GPL+ or Artistic | Routines for manipulating stashes 
perl-Package-Stash-XS | GPL+ or Artistic | Faster and more correct implementation of the Package 
perl-PadWalker | GPL+ or Artistic | Play with other people's lexical variables 
perl-Parallel-ForkManager | GPL+ or Artistic | Simple parallel processing fork manager 
perl-Params-Check | GPL+ or Artistic | Generic input parsing/checking mechanism 
perl-Params-Classify | GPL+ or Artistic | Argument type classification 
perl-Params-Coerce | GPL+ or Artistic | Allows your classes to do coercion of parameters 
perl-Params-Util | GPL+ or Artistic | Simple standalone parameter-checking functions 
perl-Params-Validate | Artistic-2.0 AND (GPL-1.0-or-later OR Artistic-1.0-Perl) | Params-Validate Perl module 
perl-Params-ValidationCompiler | Artistic-2.0 | Build an optimized subroutine parameter validator once, use it forever 
perl-parent | GPL+ or Artistic | Establish an ISA relationship with base classes at compile time 
perl-Parse-Distname | GPL+ or Artistic | Parse a distribution name 
perl-Parse-RecDescent | (GPL+ or Artistic) and (GPLv2+ or Artistic) | Generate Recursive-Descent Parsers 
perl-Parse-Yapp | GPL+ or Artistic | Perl extension for generating and using LALR parsers 
perl-Path-Class | GPL+ or Artistic | Cross-platform path specification manipulation 
perl-Path-Tiny | Apache-2.0 | File path utility 
perl-PathTools | (GPL+ or Artistic) and BSD | PathTools Perl module (Cwd, File 
perl-Pegex | GPL-1.0-or-later OR Artistic-1.0-Perl | Pegex Parser Generator 
perl-Perl4-CoreLibs | GPL+ or Artistic | Libraries historically supplied with Perl 4 
perl-Perl-Critic | GPL-1.0-or-later OR Artistic-1.0-Perl | Critique Perl source code for best-practices 
perl-Perl-Critic-Pulp | GPLv3+ | Some add-on perlcritic policies 
perl-Perl-Destruct-Level | GPL+ or Artistic | Allows you to change perl's internal destruction level 
perl-perlfaq | (GPL+ or Artistic) and Public Domain | Frequently asked questions about Perl 
perl-PerlIO-utf8_strict | GPL-1.0-or-later OR Artistic-1.0-Perl | Fast and correct UTF-8 I/O 
perl-PerlIO-via-QuotedPrint | GPL+ or Artistic | PerlIO layer for quoted-printable strings 
perl-Perl-MinimumVersion | GPL-1.0-or-later OR Artistic-1.0-Perl | Find a minimum required version of perl for Perl code 
perl-Perl-OSType | GPL+ or Artistic | Map Perl operating system names to generic types 
perl-Perl-PrereqScanner-NotQuiteLite | GPL+ or Artistic | A tool to scan your Perl code for its prerequisites 
perl-Pod-Checker | GPL+ or Artistic | Check POD documents for syntax errors 
perl-Pod-Coverage | GPL+ or Artistic | Checks if the documentation of a module is comprehensive 
perl-Pod-Coverage-TrustPod | GPL-1.0-or-later OR Artistic-1.0-Perl | Allow a module's pod to contain Pod 
perl-Pod-Escapes | GPL+ or Artistic | Resolve POD escape sequences 
perl-Pod-Eventual | GPL-1.0-or-later OR Artistic-1.0-Perl | Read a POD document as a series of trivial events 
perl-podlators | (GPL+ or Artistic) and FSFAP | Format POD source into various output format 
perl-podlinkcheck | GPLv3+ | Check Perl POD L<> link references 
perl-Pod-Markdown | GPL-1.0-or-later OR Artistic-1.0-Perl | Convert POD to Markdown 
perl-Pod-MinimumVersion | GPL-3.0-or-later | Perl version for POD directives used 
perl-Pod-Parser | GPL-1.0-or-later OR Artistic-1.0-Perl | Basic perl modules for handling Plain Old Documentation (POD) 
perl-Pod-Perldoc | GPL+ or Artistic | Look up Perl documentation in Pod format 
perl-Pod-Simple | GPL+ or Artistic | Pramework for parsing POD documentation 
perl-Pod-Spell | Artistic-2.0 | A formatter for spell-checking POD 
perl-Pod-Usage | GPL+ or Artistic-1.0 | Print a usage message from embedded pod documentation 
perl-PPI | GPL+ or Artistic | Parse, Analyze and Manipulate Perl 
perl-PPIx-QuoteLike | GPL-1.0-or-later OR Artistic-1.0-Perl | Parse Perl string literals and string-literal-like things 
perl-PPIx-Regexp | GPL+ or Artistic | Represent a regular expression of some sort 
perl-PPIx-Utilities | GPL+ or Artistic | Extensions to PPI 
perl-PPIx-Utils | GPL-1.0-or-later OR Artistic-1.0-Perl | Utility functions for PPI 
perl-Probe-Perl | GPL-1.0-or-later OR Artistic-1.0-Perl | Information about the currently running perl 
perl-Readonly | GPL+ or Artistic | Facility for creating read-only scalars, arrays, hashes 
perl-Ref-Util | MIT | Utility functions for checking references 
perl-Ref-Util-XS | MIT | Utility functions for checking references 
perl-Regexp-Common | Artistic-2.0 OR MIT OR BSD-3-Clause | Regexp 
perl-Regexp-Pattern-Perl | GPL-1.0-or-later OR Artistic-1.0-Perl | Regexp patterns related to Perl 
perl-Regexp-Trie | GPL+ or Artistic | Build trie-ized regexp 
perl-Role-Tiny | GPL+ or Artistic | A nouvelle cuisine portion size slice of Moose 
perl-Scalar-List-Utils | GPL+ or Artistic | A selection of general-utility scalar and list subroutines 
perl-Scalar-Properties | GPL+ or Artistic | Run-time properties on scalar variables 
perl-Scope-Guard | GPL+ or Artistic | Lexically scoped resource management 
perl-Sereal | GPL-1.0-or-later OR Artistic-1.0-Perl | Fast, compact, powerful binary (de-)serialization 
perl-Sereal-Decoder | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl deserialization for Sereal format 
perl-Sereal-Encoder | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND LGPL-2.1-or-later | Perl serialization into Sereal format 
perl-SGMLSpm | GPLv2+ | Perl library for parsing the output of nsgmls 
perl-Socket | GPL+ or Artistic | Networking constants and support functions 
perl-Socket6 | BSD | IPv6 related part of the C socket.h defines and structure manipulators 
perl-Software-License | perl-Software-License
 GPL+ or Artistic
 https | Package that provides templated software licenses 
perl-Sort-Key | GPL-1.0-or-later OR Artistic-1.0-Perl | Fastest way to sort anything in Perl 
perl-Sort-Versions | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module for sorting of revision-like numbers 
perl-Specio | Artistic-2.0 AND (GPL-1.0-or-later OR Artistic-1.0-Perl) | Type constraints and coercions for Perl 
perl-Spellunker | GPL+ or Artistic | Pure perl spelling checker implementation 
perl-Spiffy | GPL+ or Artistic | Framework for doing object oriented (OO) programming in Perl 
perl-Spreadsheet-ParseExcel | GPL+ or Artistic | Extract information from an Excel file 
perl-Spreadsheet-WriteExcel | GPL+ or Artistic | Write formatted text and numbers to a cross-platform Excel binary file 
perl-srpm-macros | GPLv3+ | RPM macros for building Perl source package from source repository 
perl-Storable | GPL-1.0-or-later OR Artistic-1.0-Perl | Persistence for Perl data structures 
perl-strictures | GPL-1.0-or-later OR Artistic-1.0-Perl | Turn on strict and make most warnings fatal 
perl-String-Format | GPLv2 | Sprintf-like string formatting capabilities with arbitrary format definitions 
perl-String-ShellQuote | (GPL+ or Artistic) and GPLv2+ | Perl module for quoting strings for passing through the shell 
perl-Sub-Exporter | GPL+ or Artistic | Sophisticated exporter for custom-built routines 
perl-Sub-Exporter-Progressive | GPL+ or Artistic | Only use Sub 
perl-Sub-Identify | GPL+ or Artistic | Retrieve names of code references 
perl-Sub-Info | GPL-1.0-or-later OR Artistic-1.0-Perl | Tool for inspecting Perl subroutines 
perl-Sub-Install | GPL-1.0-or-later OR Artistic-1.0-Perl | Install subroutines into packages easily 
perl-Sub-Name | GPL+ or Artistic | Name - or rename - a sub 
perl-Sub-Quote | GPL-1.0-or-later OR Artistic-1.0-Perl | Efficient generation of subroutines via string eval 
perl-Sub-Uplevel | GPL+ or Artistic | Apparently run a function in a higher stack frame 
perl-SUPER | GPL+ or Artistic | Sane superclass method dispatcher 
perl-Switch | GPL+ or Artistic | A switch statement for Perl 
perl-Syntax-Keyword-Defer | GPL-1.0-or-later OR Artistic-1.0-Perl | Add defer block syntax to Perl 
perl-Syntax-Keyword-Try | GPL-1.0-or-later OR Artistic-1.0-Perl | try/catch/finally syntax for perl 
perl-Sys-CPU | (GPL+ or Artistic) and (LGPLv3 or Artistic 2.0) | Getting CPU information 
perl-Sys-MemInfo | GPL-1.0-or-later OR Artistic-1.0-Perl | Memory information as Perl module 
perl-Sys-Syslog | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl interface to the UNIX syslog(3) calls 
perl-Task-Weaken | GPL+ or Artistic | Ensure that a platform has weaken support 
perl-Term-ANSIColor | GPL+ or Artistic | Color screen output using ANSI escape sequences 
perl-Term-Cap | GPL+ or Artistic | Perl termcap interface 
perl-TermReadKey | (Copyright only) and (Artistic or GPL+) | A perl module for simple terminal control 
perl-Term-Size-Any | GPL+ or Artistic | Retrieve terminal size 
perl-Term-Size-Perl | GPL+ or Artistic | Perl extension for retrieving terminal size (Perl version) 
perl-Term-Table | GPL+ or Artistic | Format a header and rows into a table 
perl-Test2-Plugin-NoWarnings | Artistic 2.0 | Fail if tests warn 
perl-Test2-Suite | GPL-1.0-or-later OR Artistic-1.0-Perl | Set of tools built upon the Test2 framework 
perl-Test-Base | GPL-1.0-or-later OR Artistic-1.0-Perl | Data Driven Testing Framework 
perl-Test-CleanNamespaces | GPL+ or Artistic | Check for uncleaned imports 
perl-Test-Compile | GPL-1.0-or-later OR Artistic-1.0-Perl | Check whether Perl module files compile correctly 
perl-Test-CPAN-Meta | Artistic-2.0 | Validation of the META.yml file in a CPAN distribution 
perl-Test-CPAN-Meta-JSON | Artistic 2.0 | Validate a META.json file within a CPAN distribution 
perl-Test-Deep | GPL-1.0-or-later OR Artistic-1.0-Perl | Extremely flexible deep comparison 
perl-Test-Differences | GPL+ or Artistic | Test strings and data structures and show differences if not OK 
perl-Test-DistManifest | GPL-1.0-or-later OR Artistic-1.0-Perl | Author test that validates a package MANIFEST 
perl-Test-EOL | GPL+ or Artistic | Check the correct line endings in your project 
perl-Test-Exception | GPL+ or Artistic | Library of test functions for exception based Perl code 
perl-Test-FailWarnings | ASL 2.0 | Add test failures if warnings are caught 
perl-Test-Fatal | GPL-1.0-or-later OR Artistic-1.0-Perl | Incredibly simple helpers for testing code with exceptions 
perl-Test-File | Artistic-2.0 | Test file attributes through Test 
perl-Test-File-ShareDir | GPL-1.0-or-later OR Artistic-1.0-Perl | Create a Fake ShareDir for your modules for testing 
perl-Test-Harness | GPL-1.0-or-later OR Artistic-1.0-Perl | Run Perl standard test scripts with statistics 
perl-Test-HasVersion | GPL+ or Artistic | Check Perl modules have version numbers 
perl-Test-Identity | GPL-1.0-or-later OR Artistic-1.0-Perl | Assert the referential identity of a reference 
perl-Test-InDistDir | WTFPL | Test environment setup for development with IDE 
perl-Test-Inter | GPL-1.0-or-later OR Artistic-1.0-Perl | Framework for more readable interactive test scripts 
perl-Test-Kwalitee | GPL+ or Artistic | Test the Kwalitee of a distribution before you release it 
perl-Test-LeakTrace | GPL+ or Artistic | Trace memory leaks 
perl-Test-LongString | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module to test long strings 
perl-Test-Manifest | Artistic 2.0 | Test case module for Perl 
perl-Test-Memory-Cycle | Artistic 2.0 | Check for memory leaks and circular memory references 
perl-Test-MemoryGrowth | GPL-1.0-or-later OR Artistic-1.0-Perl | Assert that code does not cause growth in memory usage 
perl-Test-MinimumVersion | GPL-1.0-or-later OR Artistic-1.0-Perl | Check whether your code requires a newer perl 
perl-TestML | GPL-1.0-or-later OR Artistic-1.0-Perl | Generic software Testing Meta Language 
perl-Test-Mojibake | GPL+ or Artistic | Check your source for encoding misbehavior 
perl-Test-Needs | GPL+ or Artistic | Skip tests when modules not available 
perl-Test-NoTabs | GPL+ or Artistic | Check the presence of tabs in your project 
perl-Test-NoWarnings | LGPLv2+ | Make sure you didn't emit any warnings while testing 
perl-Test-Object | GPL+ or Artistic | Thoroughly testing objects via registered handlers 
perl-Test-Output | Artistic-2.0 | Utilities to test STDOUT and STDERR messages 
perl-Test-Perl-Critic | GPL+ or Artistic | Use Perl 
perl-Test-Pod | GPL-1.0-or-later OR Artistic-1.0-Perl | Test POD files for correctness 
perl-Test-Pod-Coverage | Artistic 2.0 | Check for pod coverage in your distribution 
perl-Test-Pod-LinkCheck | GPL-1.0-or-later OR Artistic-1.0-Perl | Tests POD for invalid links 
perl-Test-Portability-Files | GPL-1.0-or-later OR Artistic-1.0-Perl | Check file names portability 
perl-Test-RandomResult | GPL-1.0-or-later OR Artistic-1.0-Perl | Test that results of a running code look random 
perl-Test-Refcount | GPL+ or Artistic | Assert reference counts on objects 
perl-Test-Regexp | MIT | Test your regular expressions 
perl-Test-Requires | GPL+ or Artistic | Checks to see if a given module can be loaded 
perl-Test-Script | GPL-1.0-or-later OR Artistic-1.0-Perl | Cross-platform basic tests for scripts 
perl-Test-Simple | (GPL+ or Artistic) and CC0 and Public Domain | Basic utilities for writing tests 
perl-Test-Spelling | GPL+ or Artistic | Check for spelling errors in POD files 
perl-Test-SubCalls | GPL+ or Artistic | Track the number of times subs are called 
perl-Test-Synopsis | GPL+ or Artistic | Test your SYNOPSIS code 
perl-Test-Taint | GPL-1.0-or-later OR Artistic-1.0-Perl | Tools to test taintedness 
perl-Test-TrailingSpace | MIT | Test for trailing space in source files 
perl-Test-UseAllModules | GPL+ or Artistic | Do use_ok() for all the MANIFESTed modules 
perl-Test-Valgrind | GPL+ or Artistic | Generate suppressions, analyze and test any command with valgrind 
perl-Test-Vars | GPL+ or Artistic | Detects unused variables 
perl-Test-Version | Artistic 2.0 | Check to see that versions in modules are sane 
perl-Test-Warn | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl extension to test methods for warnings 
perl-Test-Warnings | GPL+ or Artistic | Test for warnings and the lack of them 
perl-Test-Without-Module | GPL+ or Artistic | Test fallback behavior in absence of modules 
perl-Test-YAML | GPL+ or Artistic | Testing Module for YAML Implementations 
perl-Text-Balanced | GPL+ or Artistic | Extract delimited text sequences from strings 
perl-Text-BibTeX | GPL+ or Artistic | Interface to read and parse BibTeX files 
perl-Text-CharWidth | GPL+ or Artistic | Get number of occupied columns of a string on terminal 
perl-Text-CSV | GPL-1.0-or-later OR Artistic-1.0-Perl | Comma-separated values manipulator 
perl-Text-CSV_XS | GPL-1.0-or-later OR Artistic-1.0-Perl | Comma-separated values manipulation routines 
perl-Text-Diff | (GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT | Perform diffs on files and record sets 
perl-Text-Glob | GPL+ or Artistic | Perl module to match globbing patterns against text 
perl-Text-ParseWords | GPL+ or Artistic | Parse text into an array of tokens or array of arrays 
perl-Text-Roman | GPL+ or Artistic | Conversion between Roman algorisms and Arabic numerals 
perl-Text-Soundex | (Copyright only) and (GPL+ or Artistic) | Implementation of the soundex algorithm 
perl-Text-Tabs+Wrap | TTWL | Expand tabs and do simple line wrapping 
perl-Text-Template | GPL+ or Artistic | Expand template text with embedded Perl 
perl-Text-Unidecode | GPL+ or Artistic | US-ASCII transliterations of Unicode text 
perl-Text-WrapI18N | GPL+ or Artistic | Line wrapping with support for several locale setups 
perl-Thread-Queue | GPL+ or Artistic | Thread-safe queues 
perl-threads | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl interpreter-based threads 
perl-threads-shared | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl extension for sharing data structures between threads 
perltidy | GPL-2.0-or-later | Tool for indenting and re-formatting Perl scripts 
perl-Tie-Cycle | Artistic 2.0 | Cycle through a list of values via a scalar 
perl-Tie-IxHash | GPL+ or Artistic | Ordered associative arrays for Perl 
perl-Tie-RefHash | GPL+ or Artistic | Use references as hash keys 
perl-TimeDate | GPL-1.0-or-later OR Artistic-1.0-Perl | A Perl module for time and date manipulation 
perl-Time-HiRes | GPL-1.0-or-later OR Artistic-1.0-Perl | High resolution alarm, sleep, gettimeofday, interval timers 
perl-Time-Local | GPL+ or Artistic | Efficiently compute time from local and GMT time 
perl-Tk | (GPL+ or Artistic) and SWL | Perl Graphical User Interface ToolKit 
perl-Try-Tiny | MIT | Minimal try/catch with proper localization of $@ 
perl-Types-Serialiser | GPL+ or Artistic | Simple data types for common serialization formats 
perl-Type-Tiny | GPL-1.0-or-later OR Artistic-1.0-Perl | Tiny, yet Moo(se)-compatible type constraint 
perl-Unicode-CheckUTF8 | UCD and (GPL+ or Artistic) | Checks if scalar is valid UTF-8 
perl-Unicode-Collate | (GPL+ or Artistic) and Unicode | Unicode Collation Algorithm 
perl-Unicode-EastAsianWidth | CC0 | East Asian Width properties 
perl-Unicode-LineBreak | GPL+ or Artistic | UAX #14 Unicode Line Breaking Algorithm 
perl-Unicode-Map | GPL+ or Artistic | Perl module for mapping charsets from and to utf16 unicode 
perl-Unicode-Normalize | GPL-1.0-or-later OR Artistic-1.0-Perl | Unicode Normalization Forms 
perl-UNIVERSAL-isa | GPL+ or Artistic | Hack around module authors using UNIVERSAL 
perl-URI | GPL-1.0-or-later OR Artistic-1.0-Perl | A Perl module implementing URI parsing and manipulation 
perl-utf8-all | GPL-1.0-or-later OR Artistic-1.0-Perl | Turn on Unicode everywhere 
perl-Variable-Magic | GPL-1.0-or-later OR Artistic-1.0-Perl | Associate user-defined magic to variables from Perl 
perl-version | GPL+ or Artistic | Perl extension for Version Objects 
perl-Want | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module implementing a generalisation of wantarray 
perl-WWW-Mechanize | GPL+ or Artistic | Automates web page form & link interaction 
perl-WWW-RobotRules | GPL-1.0-or-later OR Artistic-1.0-Perl | Database of robots.txt-derived permissions 
perl-XML-Catalog | GPL-1.0-or-later OR Artistic-1.0-Perl | Resolve public identifiers and remap system identifiers 
perl-XML-DOM | GPL+ or Artistic | DOM extension to XML 
perl-XML-LibXML | (GPL-1.0-or-later OR Artistic-1.0-Perl) AND MIT | Perl interface to the libxml2 library 
perl-XML-LibXML-Simple | GPL+ or Artistic | Read XML strings or files 
perl-XML-LibXSLT | ( GPL-1.0-or-later OR Artistic-1.0-Perl ) AND MIT | Perl module for interfacing to GNOME's libxslt 
perl-XML-NamespaceSupport | GPL-1.0-or-later OR Artistic-1.0-Perl | A simple generic name space support class 
perl-XML-Parser | GPL-1.0-or-later OR Artistic-1.0-Perl | A perl module for parsing XML documents 
perl-XML-RegExp | GPL+ or Artistic | Regular expressions for XML tokens 
perl-XML-SAX | GPL-1.0-or-later OR Artistic-1.0-Perl | SAX parser access API for Perl 
perl-XML-SAX-Base | GPL-1.0-or-later OR Artistic-1.0-Perl | Base class SAX drivers and filters 
perl-XML-TokeParser | GPL+ or Artistic | Simplified interface to XML 
perl-XML-Twig | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl module for processing huge XML documents in tree mode 
perl-XML-Writer | LicenseRef-Fedora-Public-Domain | A simple Perl module for writing XML documents 
perl-XML-XPath | Artistic-2.0 AND (GPL-1.0-or-later OR Artistic-1.0-Perl) | XPath parser and evaluator for Perl 
perl-XML-XPathEngine | GPL-1.0-or-later OR Artistic-1.0-Perl | Re-usable XPath engine for DOM-like trees 
perl-XS-Parse-Keyword | GPL-1.0-or-later OR Artistic-1.0-Perl | XS functions to assist in parsing keyword syntax 
perl-XS-Parse-Sublike | GPL+ or Artistic | XS functions to assist in parsing sub-like syntax 
perl-XString | GPL+ or Artistic | Isolated String helpers from B 
perl-XXX | GPL-1.0-or-later OR Artistic-1.0-Perl | See Your Data in the Nude 
perl-YAML | GPL+ or Artistic | YAML Ain't Markup Language (tm) 
perl-YAML-LibYAML | GPL-1.0-or-later OR Artistic-1.0-Perl | Perl YAML Serialization using XS and libyaml 
perl-YAML-PP | GPL-1.0-or-later OR Artistic-1.0-Perl | YAML 1.2 processor 
perl-YAML-Syck | BSD and MIT | Fast, lightweight YAML loader and dumper 
perl-YAML-Tiny | GPL+ or Artistic | Read/Write YAML files with as little code as possible 
pesign | GPL-2.0-only | Signing utility for UEFI binaries 
phodav | LGPLv2+ | A WebDAV server using libsoup3 
php | PHP and Zend and BSD and MIT and ASL 1.0 and NCSA and Boost | PHP scripting language for creating dynamic web sites 
pigz | zlib | Parallel implementation of gzip 
pinentry | GPLv2+ | Collection of simple PIN or passphrase entry dialogs 
pipewire | MIT | Media Sharing Server 
pixman | MIT | Pixel manipulation library 
pkgconf | ISC | Package compiler and linker metadata toolkit 
plexus-archiver | ASL 2.0 | Plexus Archiver Component 
plexus-build-api | ASL 2.0 | Plexus Build API 
plexus-cipher | ASL 2.0 | Plexus Cipher 
plexus-classworlds | ASL 2.0 and Plexus | Plexus Classworlds Classloader Framework 
plexus-compiler | MIT and ASL 2.0 | Compiler call initiators for Plexus 
plexus-components-pom | ASL 2.0 | Plexus Components POM 
plexus-containers | ASL 2.0 and MIT and xpp | Containers for Plexus 
plexus-interpolation | ASL 2.0 and ASL 1.1 and MIT | Plexus Interpolation API 
plexus-io | ASL 2.0 | Plexus IO Components 
plexus-languages | ASL 2.0 | Plexus Languages 
plexus-pom | ASL 2.0 | Root Plexus Projects POM 
plexus-resources | MIT | Plexus Resource Manager 
plexus-sec-dispatcher | ASL 2.0 | Plexus Security Dispatcher Component 
plexus-utils | ASL 1.1 and ASL 2.0 and xpp and BSD and Public Domain | Plexus Common Utilities 
plotutils | GPLv2+ and GPLv3+ | GNU vector and raster graphics utilities and libraries 
plymouth | GPLv2+ | Graphical Boot Animation and Logger 
pmix | BSD | Process Management Interface Exascale (PMIx) 
pngcrush | zlib | Optimizer for PNG (Portable Network Graphics) files 
po4a | GPL+ | A tool maintaining translations anywhere 
polardb | Apache License | An open-source database system based on PostgreSQL. 
policycoreutils | GPLv2 | SELinux policy core utilities 
polkit | LGPLv2+ | An authorization framework 
polkit-gnome | LGPLv2+ | PolicyKit integration for the GNOME desktop 
polkit-pkla-compat | LGPLv2+ | Rules for polkit to add compatibility with pklocalauthority 
poppler | (GPLv2 or GPLv3) and GPLv2+ and LGPLv2+ and MIT | PDF rendering library 
poppler-data | BSD and GPLv2 | Encoding files for use with poppler 
popt | MIT | C library for parsing command line parameters 
postfix | (IBM and GPLv2+) or (EPL-2.0 and GPLv2+) | Postfix Mail Transport Agent 
postgresql | PostgreSQL | PostgreSQL client programs 
potrace | GPLv2+ | Transform bitmaps into vector graphics 
powertop | GPLv2 | Power consumption monitor 
ppp | BSD and LGPLv2+ and GPLv2+ and Public Domain | The Point-to-Point Protocol daemon 
pps-tools | GPLv2+ | LinuxPPS user-space tools 
pre-commit | MIT | Framework for managing and maintaining multi-language pre-commit hooks 
prefixdevname | MIT | Udev helper utility that provides network interface naming using user defined prefix 
procmail | GPLv2+ or Artistic | Mail processing program 
procps-ng | GPL+ and GPLv2 and GPLv2+ and GPLv3+ and LGPLv2+ | System and process monitoring utilities 
protobuf | BSD | Protocol Buffers - Google's data interchange format 
protobuf-c | BSD | C bindings for Google's Protocol Buffers 
psmisc | GPLv2+ | Utilities for managing processes on your system 
pstoedit | GPLv2+ | Translates PostScript and PDF graphics into other vector formats 
psutils | GPLv3+ and psutils | PostScript utilities 
publicsuffix-list | MPLv2.0 | Cross-vendor public domain suffix database 
pulseaudio | LGPLv2+ | Improved Linux Sound Server 
py3c | MIT and CC-BY-SA | Guide and compatibility macros for porting extensions to Python 3 
pyatspi | LGPLv2 and GPLv2 | Python bindings for at-spi 
pybind11 | BSD | Seamless operability between C++11 and Python 
pycairo | MPLv1.1 or LGPLv2 | Python bindings for the cairo library 
pyflakes | MIT | A simple program which checks Python source files for errors 
pygobject3 | LGPLv2+ and MIT | Python bindings for GObject Introspection 
pykickstart | GPLv2 and MIT | Python utilities for manipulating kickstart files. 
pyodbc | MIT | Python DB API 2.0 Module for ODBC 
pyOpenSSL | ASL 2.0 | Python wrapper module around the OpenSSL library 
pyparsing | MIT | Python library for creating PEG parsers 
pyparted | GPLv2+ | Python bindings for GNU parted (libparted) 
pyproject-rpm-macros | MIT | RPM macros for PEP 517 Python packages 
PyQt-builder | GPLv2 or GPLv3 | The PEP 517 compliant PyQt build system 
pytest | MIT | Simple powerful testing with Python 
python3.11 | Python | Version 3.11 of the Python interpreter 
python-aiodns | MIT | Simple DNS resolver for asyncio 
python-aiohttp | Apache-2.0 | Python HTTP client/server for asyncio 
python-aiosignal | Apache-2.0 | List of registered asynchronous callbacks 
python-aiosmtpd | ASL 2.0 | Asyncio-based SMTP server 
python-anyio | MIT | Compatibility layer for multiple asynchronous event loop implementations 
python-anytree | ASL 2.0 | Powerful and Lightweight Python Tree Data Structure 
python-appdirs | MIT | Python module for determining platform-specific directories 
python-argcomplete | ASL 2.0 | Bash tab completion for argparse 
python-argon2-cffi | MIT | The secure Argon2 password hashing algorithm 
python-argon2-cffi-bindings | MIT | Low-level CFFI bindings for Argon2 
python-arrow | ASL 2.0 | Better dates and times for Python 
python-asgiref | BSD and ASL 2.0 | ASGI specs, helper code, and adapters 
python-asn1crypto | MIT | Fast Python ASN.1 parser and serializer 
python-astroid | LGPL-2.1-or-later | Common base representation of python source code for pylint and other projects 
python-asttokens | ASL 2.0 | Module to annotate Python abstract syntax trees with source code positions 
python-async-generator | MIT or ASL 2.0 | Async generators and context managers 
python-async-lru | MIT | Simple lru_cache for asyncio 
python-asyncssh | EPL-2.0 or GPLv2+ | Asynchronous SSH for Python 
python-async-timeout | ASL 2.0 | An asyncio-compatible timeout context manager 
python-atpublic | Apache-2.0 | Decorator for populating a Python module's __all__ 
python-attrs | MIT | Python attributes without boilerplate 
python-automat | MIT | Self-service finite-state machines for the programmer on the go 
python-backcall | BSD | Specifications for callback functions passed in to an API 
python-bcrypt | ASL 2.0 and Public Domain and BSD | Modern password hashing for your software and your servers 
python-beautifulsoup4 | MIT | HTML/XML parser for quick-turnaround applications like screen-scraping 
python-betamax | Apache-2.0 | VCR imitation for python-requests 
python-black | MIT | The uncompromising code formatter 
python-bleach | Apache-2.0 | An easy whitelist-based HTML-sanitizing tool 
python-blivet | LGPLv2+ | A python module for system storage configuration 
python-breathe | BSD | Adds support for Doxygen xml output to reStructuredText and Sphinx 
python-build | MIT | A simple, correct PEP517 package builder 
python-cachetools | MIT | Extensible memoizing collections and decorators 
python-cairocffi | BSD | cffi-based cairo bindings for Python 
python-certifi | MPL-2.0 | Python package for providing Mozilla's CA Bundle 
python-cffi | MIT | Foreign Function Interface for Python to call C code 
python-cfgv | MIT | Validate configuration and produce human readable error messages 
python-chardet | LGPL-2.0-or-later | Python character encoding detector 
python-charset-normalizer | MIT | Truly universal encoding detector in pure Python 
python-cheetah | MIT | Template engine and code generator 
python-cheroot | BSD | Highly-optimized, pure-python HTTP server 
python-cherrypy | BSD | Pythonic, object-oriented web development framework 
python-click | BSD | Simple wrapper around optparse for powerful command line utilities 
python-cloudpickle | BSD | Extended pickling support for Python objects 
python-cmarkgfm | MIT | Minimal bindings to GitHub's fork of cmark 
python-cmd2 | MIT | Extra features for standard library's cmd module 
python-colorama | BSD | Cross-platform colored terminal text 
python-colorlog | MIT | Colored formatter for the Python logging module 
python-CommonMark | BSD | Python parser for the CommonMark Markdown spec 
python-configobj | BSD-3-Clause | Config file reading, writing, and validation 
python-configshell | ASL 2.0 | A framework to implement simple but nice CLIs 
python-constantly | MIT | Symbolic constants in Python 
python-construct | MIT | A powerful declarative parser/builder for binary data 
python-contourpy | BSD | Python library for calculating contours in 2D quadrilateral grids 
python-coverage | Apache-2.0 | Code coverage testing module for Python 
python-cppy | BSD | C++ headers for C extension development 
python-cryptography | (Apache-2.0 OR BSD-3-Clause) AND PSF-2.0 | PyCA's cryptography library 
python-cssselect | BSD | Parses CSS3 Selectors and translates them to XPath 1.0 
python-cups | GPLv2+ | Python bindings for CUPS 
python-curio | BSD | Building blocks for performing concurrent I/O 
python-cycler | BSD | Cycle through lists in various ways (used by matplotlib) 
python-dasbus | LGPLv2+ | DBus library in Python 3 
python-dateutil | BSD or (BSD and ASL 2.0) | Powerful extensions to the standard datetime module 
python-dbusmock | LGPLv3+ | Mock D-Bus objects 
python-decorator | BSD | Module to simplify usage of decorators 
python-defusedxml | Python | XML bomb protection for Python stdlib modules 
python-deprecation | ASL 2.0 | A library to handle automated deprecations 
python-distlib | Python license | Distribution utilities 
python-distro | ASL 2.0 | A much more elaborate replacement for removed Python's `platform.linux_distribution()` method 
python-distutils-extra | GPLv2+ | Integrate more support into Python's distutils 
python-django | BSD-3-Clause | A high-level Python Web framework 
python-dns | ISC | DNS toolkit for Python 
python-docopt | MIT | Pythonic argument parser, that will make you smile 
python-docs | Python-2.0.1 AND (Python-2.0.1 OR 0BSD) | Documentation for the Python 3 programming language 
python-docs-theme | PSF-2.0 | The Sphinx theme for the CPython docs and related projects 
python-docutils | Public Domain and BSD and Python and GPLv3+ | System for processing plaintext documentation 
python-editables | MIT | Editable installations 
python-enchant | LGPLv2+ | Python bindings for Enchant spellchecking library 
python-entrypoints | MIT | Discover and load entry points from installed packages 
python-ethtool | GPLv2 | Python module to interface with ethtool 
python-eventlet | MIT | Highly concurrent networking library 
python-exceptiongroup | MIT | Backport of PEP 654 (exception groups) 
python-execnet | MIT | Distributed Python deployment and communication 
python-executing | MIT | Python library for inspecting the current frame run footprint 
python-fastjsonschema | BSD-3-Clause | Fastest Python implementation of JSON schema 
python-filelock | Unlicense | A platform independent file lock. 
python-flake8 | MIT | Python code checking using pyflakes, pycodestyle, and mccabe 
python-flaky | ASL 2.0 | Plugin for pytest that automatically reruns flaky tests 
python-flit | BSD and ASL 2.0 and Python | Simplified packaging of Python modules 
python-flit-scm | MIT | PEP 518 build backend that uses setuptools_scm and flit 
python-fqdn | MPLv2.0 | Validates fully-qualified domain names against RFC 1123 
python-freezegun | ASL 2.0 | Let your Python tests travel through time 
python-frozenlist | Apache-2.0 | List-like structure which can be made immutable 
python-fs | MIT | Python's Filesystem abstraction layer 
python-genshi | BSD | Toolkit for stream-based generation of output for the web 
python-geoip2 | ASL 2.0 | MaxMind GeoIP2 API 
python-gevent | MIT | A coroutine-based Python networking library 
python-gmpy2 | LGPL-3.0-or-later | Python interface to GMP, MPFR, and MPC 
python-google-auth | ASL 2.0 | Google Auth Python Library 
python-greenlet | MIT | Lightweight in-process concurrent programming 
python-gssapi | ISC | Python Bindings for GSSAPI (RFC 2743/2744 and extensions) 
python-gunicorn | MIT | Python WSGI HTTP Server 
python-h11 | MIT | A pure-Python, bring-your-own-I/O implementation of HTTP/1.1 
python-hamcrest | BSD-3-Clause | Hamcrest matchers for Python 
python-hatch-fancy-pypi-readme | MIT | Hatch plugin for writing fancy PyPI readmes 
python-hatch-jupyter-builder | BSD-3-Clause | A hatch plugin to help build Jupyter packages 
python-hatchling | MIT | The build backend used by Hatch 
python-hatch-vcs | MIT | Hatch plugin for versioning with your preferred VCS 
python-hidapi | GPLv3+ or BSD or Public Domain | Interface to the hidapi library 
python-html5lib | MIT | A python based HTML parser/tokenizer 
python-httpcore | BSD | Minimal low-level HTTP client 
python-httpx | BSD | Python HTTP client 
python-hyperlink | MIT and BSD and OFL | A featureful, immutable, and correct URL for Python 
python-hypothesis | MPLv2.0 | Library for property based testing 
python-hypothesmith | MPLv2.0 | Hypothesis strategies for generating Python programs 
python-identify | MIT | File identification library for Python 
python-idna | BSD-3-Clause and Python and Unicode | Internationalized Domain Names in Applications (IDNA) 
python-imagesize | MIT | Python module for analyzing image file headers and returning image sizes 
python-importlib-metadata | ASL 2.0 | Library to access the metadata for a Python package 
python-incremental | MIT | It versions your Python projects 
python-iniconfig | MIT | Brain-dead simple parsing of ini files 
python-ipykernel | BSD-3-Clause | IPython Kernel for Jupyter 
python-isodate | BSD | An ISO 8601 date/time/duration parser and formatter 
python-isoduration | ISC | Operations with ISO 8601 durations 
python-isort | MIT | Python utility / library to sort Python imports 
python-jaraco-classes | MIT | Utility functions for Python class constructs 
python-jaraco-collections | MIT | Collection objects similar to those in stdlib by jaraco 
python-jaraco-functools | MIT | Functools like those found in stdlib 
python-jaraco-packaging | MIT | Tools to supplement packaging Python releases 
python-jaraco-text | MIT | Module for text manipulation 
python-jedi | MIT and ASL 2.0 | An auto completion tool for Python that can be used for text editors 
python-jeepney | MIT | Low-level, pure Python DBus protocol wrapper 
python-jinja2 | BSD | General purpose template engine 
python-joblib | BSD | Lightweight pipelining 
python-json5 | ASL 2.0 | Python implementation of the JSON5 data format 
python-json-logger | BSD | A python library adding a json log formatter 
python-jsonpatch | BSD | Applying JSON Patches in Python 
python-jsonpointer | BSD | Resolve JSON Pointers in Python 
python-jsonschema | MIT | Implementation of JSON Schema validation for Python 
python-jupyter-client | BSD | Jupyter protocol implementation and client libraries 
python-jupyter-core | BSD | The base package for Jupyter projects 
python-jupyter-events | BSD-3-Clause | Jupyter Event System library 
python-jupyterlab_pygments | BSD | Pygments theme 
python-jupyterlab-server | BSD-3-Clause | A set of server components for JupyterLab and JupyterLab like applications 
python-jupyter-lsp | BSD-3-Clause | Multi-Language Server WebSocket proxy for Jupyter Notebook/Lab server 
python-jupyter-packaging | BSD-3-Clause | Tools to help build and install Jupyter Python packages 
python-jupyter-server | BSD-3-Clause | The backend for Jupyter web applications 
python-jupyter-server-terminals | BSD-3-Clause | A Jupyter Server Extension Providing Terminals 
python-kdcproxy | MIT | MS-KKDCP (kerberos proxy) WSGI module 
python-keyring | MIT OR Python-2.0.1 | Store and access your passwords safely 
python-kiwisolver | BSD-3-Clause AND HPND-sell-variant | A fast implementation of the Cassowary constraint solver 
python-kmod | LGPLv2+ | Python module to work with kernel modules 
python-kubernetes | ASL 2.0 | Python client for the kubernetes API. 
python-lark-parser | MIT | Lark is a modern general-purpose parsing library for Python 
python-lazy-object-proxy | BSD-2-Clause | A fast and thorough lazy object proxy 
python-libcst | MIT and (MIT and Python) and ASL 2.0 | A concrete syntax tree with AST-like properties for Python 3 
python-libevdev | MIT | Python bindings to the libevdev evdev device wrapper library 
python-libnacl | ASL 2.0 | Python bindings for libsodium based on ctypes 
python-linkify-it-py | MIT | Link recognition library with full Unicode support 
python-linux-procfs | GPLv2 | Linux /proc abstraction classes 
python-lit | NCSA | Tool for executing llvm test suites 
python-littleutils | MIT | Small collection of Python utilities 
python-logutils | BSD | Logging utilities 
python-lxml | BSD and MIT and zlib | XML processing library combining libxml2/libxslt with the ElementTree API 
python-lz4 | BSD | LZ4 Bindings for Python 
python-mako | MIT and Python | Mako Templates for Python 
python-mallard-ducktype | MIT | Parse Ducktype files and convert them to Mallard 
python-markdown | BSD | Markdown implementation in Python 
python-markdown-it-py | MIT | Python port of markdown-it 
python-markupsafe | BSD | Implements a XML/HTML/XHTML Markup safe string for Python 
python-matplotlib | PSF-2.0 and MIT and CC0-1.0 | Python 2D plotting library 
python-matplotlib-inline | BSD | Inline Matplotlib backend for Jupyter 
python-maxminddb | ASL 2.0 | Reader for the MaxMind DB format 
python-mccabe | MIT | McCabe complexity checker 
python-mdit-py-plugins | MIT | Collection of plugins for markdown-it-py 
python-mdurl | MIT | Markdown URL utilities 
python-meh | GPLv2+ | A python library for handling exceptions 
python-mistune | BSD | Markdown parser for Python 
python-mock | BSD-2-Clause | Deprecated, use unittest.mock from the standard library instead 
python-more-itertools | MIT | More routines for operating on Python iterables, beyond itertools 
python-mpmath | BSD | A pure Python library for multiprecision floating-point arithmetic 
python-multidict | Apache-2.0 | MultiDict implementation 
python-multilib | GPLv2 | A module for determining if a package is multilib or not 
python-munkres | ASL 2.0 | A Munkres algorithm for Python 
python-mypy | MIT and Apache-2.0 | A static type checker for Python 
python-mypy_extensions | MIT | Extensions for mypy (separated out from mypy/extensions) 
python-mysqlclient | GPLv2 | MySQL/mariaDB database connector for Python 
python-myst-parser | MIT | A commonmark compliant parser, with bridges to docutils & sphinx 
python-natsort | MIT | Python library that sorts lists using the "natural order" sort 
python-nbclient | BSD | A client library for executing notebooks 
python-nbconvert | BSD and MIT | Converting Jupyter Notebooks 
python-nbformat | BSD | The Jupyter Notebook format 
python-nest-asyncio | BSD | Patch asyncio to allow nested event loops 
python-netaddr | BSD | A pure Python network address representation and manipulation library 
python-netifaces | MIT | Python library to retrieve information about network interfaces 
python-networkx | BSD | Creates and Manipulates Graphs and Networks 
python-nocasedict | LGPLv2 | A case-insensitive ordered dictionary for Python 
python-nocaselist | Apache-2.0 | A case-insensitive list for Python 
python-nodeenv | BSD | Node.js virtual environment builder 
python-notebook | BSD | A web-based notebook environment for interactive computing 
python-notebook-shim | BSD-3-Clause | A shim layer for notebook traits and config 
python-numpydoc | BSD | Sphinx extension to support docstrings in NumPy format 
python-oauthlib | BSD | An implementation of the OAuth request-signing logic 
python-olefile | BSD | Python package to parse, read and write Microsoft OLE2 files 
python-outcome | MIT or ASL 2.0 | Capture the outcome of Python function calls 
python-packaging | BSD-2-Clause or Apache-2.0 | Core utilities for Python packages 
python-pandas | BSD-3-Clause AND (Apache-2.0 OR BSD-2-Clause) AND (BSD-3-Clause AND Apache-2.0) AND (BSD-3-Clause AND MIT) AND (BSD-3-Clause AND Python-2.0.1) AND MIT AND (BSD-3-Clause AND BSD-2-Clause) | Python library providing high-performance data analysis tools 
python-pandocfilters | BSD | Python module for writing pandoc filters 
python-parameterized | BSD-2-Clause-Views | Parameterized testing with any Python test framework 
python-paramiko | LGPL-2.1-or-later | SSH2 protocol library for python 
python-parso | MIT and Python | Parser that supports error recovery and round-trip parsing 
python-path | MIT | Python module wrapper for os.path 
python-pathspec | MPLv2.0 | Utility library for gitignore style pattern matching of file paths 
python-pbr | ASL 2.0 | Python Build Reasonableness 
python-pdm-pep517 | MIT AND Apache-2.0 AND Public Domain AND BSD-3-Clause AND ISC | Yet another PEP 517 backend 
python-pecan | BSD | A lean WSGI object-dispatching web framework 
python-pefile | MIT | Python module for working with Portable Executable files 
python-pep440 | MIT | A simple package with utils to check whether versions number match Pep 440 
python-pep517 | MIT and ASL 2.0 | Wrappers to build Python packages using PEP 517 hooks 
python-pexpect | ISC | Unicode-aware Pure Python Expect-like module 
python-pickleshare | MIT | Tiny 'shelve'-like database with concurrency support 
python-pid | ASL 2.0 | PID file management library 
python-pillow | MIT | Python image processing library 
python-pip | MIT AND Python-2.0.1 AND Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND ISC AND LGPL-2.1-only AND MPL-2.0 AND (Apache-2.0 OR BSD-2-Clause) | A tool for installing and managing Python package 
python-pkgconfig | MIT | Python interface to the pkg-config command line tool 
python-platformdirs | MIT License | A small Python module for determining appropriate platform-specific dirs, e.g. a "user data dir". 
python-pluggy | MIT | The plugin manager stripped of pytest specific details 
python-ply | BSD | Python Lex-Yacc 
python-poetry-core | MIT and (ASL 2.0 or BSD) and Python | Poetry PEP 517 Build Backend 
python-portend | MIT | TCP port monitoring utilities 
python-prettytable | BSD (3 clause) | A simple Python library for easily displaying tabular data in a visually appealing ASCII table format 
python-productmd | LGPLv2+ | Library providing parsers for metadata related to OS installation 
python-progressbar2 | BSD | Text progress bar library for Python 
python-prometheus_client | ASL 2.0 | Python client for Prometheus 
python-prompt-toolkit | BSD-3-Clause | Library for building powerful interactive command line applications in Python 
python-psutil | BSD | A process and system utilities module for Python 
python-psycopg2 | LGPLv3+ with exceptions | Psycopg is the most popular PostgreSQL database adapter for the Python programming language 
python-ptyprocess | ISC | Run a subprocess in a pseudo terminal 
python-pure-eval | MIT | Safely evaluate AST nodes without side effects 
python-py | MIT and Public Domain | Library with cross-python path, ini-parsing, io, code, log facilities 
python-pyasn1 | BSD | ASN.1 tools for Python 
python-pybeam | MIT | Python module to parse Erlang BEAM files 
python-pycares | MIT | Python interface for c-ares 
python-pycdio | GPLv3+ | A Python interface to the CD Input and Control library 
python-pycodestyle | MIT | Python style guide checker 
python-pycparser | BSD | C parser and AST generator written in Python 
python-pycurl | LGPLv2+ or MIT | A Python interface to libcurl 
python-pyfakefs | Apache-2.0 | pyfakefs implements a fake file system that mocks the Python file system modules. 
python-pygments | BSD | Syntax highlighting engine written in Python 
python-PyJWT | MIT | JSON Web Token implementation in Python 
python-pylibmc | BSD | Memcached client for Python 
python-pymemcache | ASL 2.0 | A comprehensive, fast, pure Python memcached client 
python-pymongo | ASL 2.0 and MIT | Python driver for MongoDB 
python-pynacl | ASL 2.0 | Python binding to the Networking and Cryptography (NaCl) library 
python-pyperclip | BSD-3-Clause | A cross-platform clipboard module for Python 
python-pyproject-api | MIT | API to interact with the python pyproject.toml based projects 
python-pyproject-hooks | MIT | Wrappers to call pyproject.toml-based build backend hooks 
python-pyqt5-sip | GPLv2 or GPLv3 | The sip module support for PyQt5 
python-pyrad | BSD | Python RADIUS client 
python-pyroute2 | GPLv2+ | Pure Python netlink library 
python-pyrsistent | MIT AND BSD-3-Clause | Persistent/Immutable/Functional data structures for Python 
python-pyserial | BSD | Python Serial Port Extension 
python-pysocks | BSD | A Python SOCKS client module 
python-pytest-asyncio | ASL 2.0 | Pytest support for asyncio 
python-pytest-console-scripts | MIT | Pytest plugin for testing console scripts 
python-pytest-cov | MIT | Coverage plugin for pytest 
python-pytest-datadir | MIT | Pytest plugin for test data directories and files 
python-pytest-forked | MIT | py.test plugin for running tests in isolated forked subprocesses 
python-pytest-httpserver | MIT | HTTP server for pytest 
python-pytest-jupyter | BSD-3-Clause AND MIT | A pytest plugin for testing Jupyter libraries and extensions 
python-pytest-mock | MIT | Thin-wrapper around the mock package for easier use with py.test 
python-pytest-param-files | MIT | Create pytest parametrize decorators from external files 
python-pytest-regressions | MIT | Pytest fixtures for writing regression tests 
python-pytest-rerunfailures | MPL-2.0 | A py.test plugin that re-runs failed tests to eliminate flakey failures 
python-pytest-runner | MIT | Invoke py.test as distutils command with dependency resolution 
python-pytest-timeout | MIT | py.test plugin to abort hanging tests 
python-pytest-trio | MIT or ASL 2.0 | Pytest plugin for trio 
python-pytest-xdist | MIT | pytest plugin for distributed testing and loop-on-failing modes 
python-pytoml | MIT | Parser for TOML 
python-pyudev | LGPLv2+ | A libudev binding 
python-pyxdg | LGPLv2 | Python library to access freedesktop.org standards 
python-qt5 | GPLv3 | PyQt5 is Python bindings for Qt5 
python-rdflib | BSD-3-Clause | Python library for working with RDF 
python-re-assert | MIT | Show where your regex match assertion failed! 
python-recommonmark | MIT | docutils-compatibility bridge to CommonMark 
python-redis | MIT | Python interface to the Redis key-value store 
python-regex | Python and CNRI | Alternative regular expression module, to replace re 
python-remoto | MIT | Execute remote commands or processes 
python-repoze-lru | BSD | A tiny LRU cache implementation and decorator 
python-repoze-sphinx-autointerface | BSD | Auto-generate Sphinx API docs from Zope interfaces 
python-requests | ASL 2.0 | HTTP library, written in Python, for human beings 
python-requests-cache | BSD | Persistent cache for requests library 
python-requests-download | MIT | Download files using requests and save them to a target path 
python-requests-file | ASL 2.0 | Transport adapter for using file 
python-requests-ftp | Apache-2.0 | FTP transport adapter for python-requests 
python-requests-gssapi | ISC | A GSSAPI/SPNEGO authentication handler for python-requests 
python-requests-mock | Apache-2.0 | Mock out responses from the requests package 
python-requests-oauthlib | ISC | OAuthlib authentication support for Requests. 
python-requests-toolbelt | Apache-2.0 | Utility belt for advanced users of python-requests 
python-requests-unixsocket | ASL 2.0 | Use requests to talk HTTP via a UNIX domain socket 
python-responses | ASL 2.0 | Python library to mock out calls with Python requests 
python-restructuredtext-lint | Unlicense | reStructuredText linter 
python-rfc3339-validator | MIT | Pure python RFC3339 validator 
python-rfc3986 | ASL 2.0 | Validating URI References per RFC 3986 
python-rfc3986-validator | MIT | Pure python RFC3986 validator 
python-rfc3987 | GPL-3.0-or-later | Parsing and validation of URIs (RFC 3986) and IRIs (RFC 3987) 
python-rich | MIT | Render rich text and beautiful formatting in the terminal 
python-routes | MIT | Routing Recognition and Generation Tools 
python-rpm-generators | GPLv2+ | Dependency generators for Python RPMs 
python-rpm-macros | MIT and Python and GPLv2+ | The common Python RPM macros 
python-rsa | ASL 2.0 | Pure-Python RSA implementation 
python-rst-linker | MIT | Can add links and perform other custom replacements to rst 
python-rtslib | ASL 2.0 | API for Linux kernel LIO SCSI target 
python-ruamel-yaml | MIT | YAML 1.2 loader/dumper package for Python 
python-ruamel-yaml-clib | MIT | C version of reader, parser and emitter for ruamel.yaml 
python-schedutils | GPLv2 | Linux scheduler python bindings 
python-scikit-learn | BSD and MIT | Machine learning in Python 
python-scour | Apache-2.0 AND BSD-3-Clause | An SVG scrubber 
python-SecretStorage | BSD | Python bindings to FreeDesktop.org Secret Service API 
python-selenium | ASL 2.0 | Python bindings for Selenium 
python-semantic_version | BSD | Library implementing the 'SemVer' scheme 
python-semver | BSD | Python helper for Semantic Versioning 
python-send2trash | BSD-3-Clause | Python library to natively send files to Trash 
python-service-identity | MIT | Service identity verification for pyOpenSSL 
python-setproctitle | BSD | Python module to customize a process title 
python-setuptools | MIT and ASL 2.0 and (BSD or ASL 2.0) and Python | Easily build and distribute Python packages 
python-setuptools-rust | MIT | Setuptools Rust extension plugin 
python-setuptools_scm | MIT | Blessed package to manage your versions by SCM tags 
python-sh | MIT | Python subprocess replacement 
python-shellingham | ISC | Tool to detect surrounding Shell 
python-simplejson | (MIT or AFL) and (MIT or GPLv2) | Simple, fast, extensible JSON encoder/decoder for Python 
python-simpleline | LGPLv3+ | A Python library for creating text UI 
python-six | MIT | Python 2 and 3 compatibility utilities 
python-slip | GPLv2+ | Convenience, extension and workaround code for Python 2.x 
python-smartypants | BSD | plug-in that easily translates ASCII punctuation characters into smart entities 
python-sniffio | MIT or ASL 2.0 | Sniff out which async library your code is running under 
python-sortedcontainers | ASL 2.0 | Pure Python sorted container types 
python-soupsieve | MIT | CSS selector library 
python-sphinx | BSD and Python and MIT | Python documentation generator 
python-sphinxcontrib-applehelp | BSD | Sphinx extension for Apple help books 
python-sphinxcontrib-devhelp | BSD | Sphinx extension for Devhelp documents 
python-sphinxcontrib-github-alt | BSD-2-Clause | Link to GitHub issues, pull requests, commits and users from Sphinx docs 
python-sphinxcontrib-htmlhelp | BSD | Sphinx extension for HTML help files 
python-sphinxcontrib-httpdomain | BSD | Sphinx domain for documenting HTTP APIs 
python-sphinxcontrib-jquery | 0BSD AND BSD-2-Clause AND MIT | Extension to include jQuery on newer Sphinx releases 
python-sphinxcontrib-jsmath | BSD | Sphinx extension for math in HTML via JavaScript 
python-sphinxcontrib-qthelp | BSD | Sphinx extension for QtHelp documents 
python-sphinxcontrib-serializinghtml | BSD | Sphinx extension for serialized HTML 
python-sphinxcontrib-spelling | BSD | A spelling checker for Sphinx-based documentation 
python-sphinxcontrib-trio | MIT or ASL 2.0 | Make Sphinx better at documenting Python functions and methods 
python-sphinx_lv2_theme | ISC | A minimal pure-CSS theme for Sphinx 
python-sphinx-pytest | MIT | Helpful pytest fixtures for sphinx extensions 
python-sphinx_rtd_theme | MIT | Sphinx theme for readthedocs.org 
python-sphinx-theme-alabaster | BSD | Configurable sidebar-enabled Sphinx theme 
python-sphinx-typlog-theme | BSD-3-Clause | A Sphinx theme sponsored by Typlog 
python-sqlparse | BSD | Non-validating SQL parser for Python 
python-stack-data | MIT | Extract data from python stack frames and tracebacks for informative displays 
python-strict-rfc3339 | GPL-3.0-only | Strict, simple, lightweight RFC3339 functions 
python-sybil | MIT | Automated testing for the examples in your documentation 
python-sympy | BSD-3-Clause AND MIT | A Python library for symbolic mathematics 
python-systemd | LGPLv2+ | Python wrappers for systemd functionality 
python-tblib | BSD | Traceback serialization library 
python-templated-dictionary | GPL-2.0-or-later | Dictionary with Jinja2 expansion 
python-tempora | MIT | Objects and routines pertaining to date and time (tempora) 
python-terminado | BSD-2-Clause | Terminals served to term.js using Tornado websockets 
python-testpath | MIT | Test utilities for code working with files and commands 
python-threadpoolctl | BSD | Thread-pool Controls 
python-tinycss2 | BSD | Low-level CSS parser for Python 
python-tokenize-rt | MIT | Wrapper for Python's stdlib `tokenize` supporting roundtrips 
python-toml | MIT | A deprecated Python Library for Tom's Obvious, Minimal Language 
python-tomli | MIT | A little TOML parser for Python 
python-tomli-w | MIT | A Python library for writing TOML 
python-tomlkit | MIT | Style preserving TOML library 
python-tornado | ASL 2.0 | Scalable, non-blocking web server and tools 
python-tox | MIT | Virtualenv-based automation of test activities 
python-tox-current-env | MIT | Tox plugin to run tests in current Python environment 
python-tpm2-pytss | BSD-2-Clause | TPM 2.0 TSS Bindings for Python 
python-traitlets | BSD-3-Clause | A lightweight derivative of Enthought Traits for configuring Python objects 
python-trio | Apache-2.0 OR MIT | A friendly Python library for async concurrency and I/O 
python-trove-classifiers | Apache-2.0 | Canonical source for classifiers on PyPI (pypi.org) 
python-trustme | MIT or ASL 2.0 | #1 quality TLS certs while you wait, for the discerning tester 
python-twisted | MIT | Twisted is a networking engine written in Python 
python-typed_ast | Apache-2.0 | A fork of the ast module with type annotations 
python-typeguard | MIT | Run-time type checker for Python 
python-types-pyyaml | ASL 2.0 | Typing stubs for PyYAML 
python-types-requests | ASL 2.0 | Typing stubs for requests 
python-typing-extensions | PSF-2.0 | Python Typing Extensions 
python-typing-inspect | MIT | Runtime inspection utilities for typing module 
python-typogrify | BSD | Filters to enhance web typography, with support for Django & Jinja templates 
python-uc-micro-py | MIT | Micro subset of Unicode data files for linkify-it.py projects 
python-ukkonen | MIT | Implementation of bounded Levenshtein distance (Ukkonen) 
python-unicodedata2 | Apache-2.0 | Unicodedata backport updated to the latest Unicode version 
python-uri-template | MIT | RFC 6570 URI Template Processor 
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post 
python-urwid | LGPLv2+ | Console user interface library 
python-userpath | MIT | Cross-platform tool for adding locations to the user PATH 
python-utils | BSD-3-Clause | A module with some convenient utilities not included with the standard Python install 
python-uvloop | MIT or ASL 2.0 | Ultra fast implementation of asyncio event loop on top of libuv 
python-virt-firmware | GPLv2 | Tools for virtual machine firmware volumes 
python-virtualenv | MIT | Tool to create isolated Python environments 
python-waitress | ZPLv2.1 | Waitress WSGI server 
python-wcwidth | MIT | Python library that measures the width of unicode strings rendered to a terminal 
python-webcolors | BSD-3-Clause | A library for working with HTML and CSS color names and value formats 
python-webencodings | BSD | Character encoding for the web 
python-webob | MIT | WSGI request and response object 
python-websocket-client | ASL 2.0 | WebSocket client for python 
python-websockets | BSD | Implementation of the WebSocket Protocol for Python 
python-werkzeug | BSD | Comprehensive WSGI web application library 
python-wheel | MIT | A built-package format for Python 
python-widgetsnbextension | BSD | Interactive HTML widgets for Jupyter notebooks 
python-wrapt | BSD | A Python module for decorators, wrappers and monkey patching 
python-xcffib | Apache-2.0 | A drop in replacement for xpyb, an XCB python binding 
python-xmltodict | MIT | Python to transform XML to JSON 
python-yamlloader | MIT | Ordered YAML loader and dumper for PyYAML 
python-yarl | ASL 2.0 | Python module to handle URLs 
python-zc-lockfile | ZPLv2.1 | Basic Inter-Process Locks 
python-zipp | MIT | Backport of pathlib-compatible object wrapper for zip files 
python-zmq | LGPLv3+ and ASL 2.0 and BSD | Software library for fast, message-based applications 
python-zope-event | ZPLv2.1 | Zope Event Publication 
python-zope-interface | ZPLv2.1 | Zope 3 Interface Infrastructure 
python-zope-testing | ZPLv2.1 | Zope Testing Framework 
python-zopfli | Apache-2.0 | Zopfli module for python 
python-zstandard | BSD and GPLv2 | Zstandard bindings for Python 
python-zstd | BSD | Zstd Bindings for Python 
pytz | MIT | World Timezone Definitions for Python 
pywbem | LGPLv2 | Python WBEM client interface and related utilities 
pyxattr | LGPLv2+ | Extended attributes library wrapper for Python 
PyYAML | MIT | YAML parser and emitter for Python 
qatengine | BSD and OpenSSL | Intel® QuickAssist Technology OpenSSL* Engine(QAT_Engine) 
qatlib | BSD and (BSD or GPLv2) | Intel QuickAssist user space library 
qatzip | BSD | Intel QuickAssist Technology (QAT) QATzip Library 
qdox | ASL 2.0 | Extract class/interface/method definitions from sources 
qemu | GPLv2 and BSD and MIT and CC-BY | QEMU is a FAST! processor emulator 
qhull | Qhull | General dimension convex hull programs 
qpdf | (Artistic 2.0 or ASL 2.0) and MIT | Command-line tools and library for transforming PDF files 
qpid-proton | ASL 2.0 | A high performance, lightweight messaging library 
qpl | MIT | Intel® Query Processing Library (Intel® QPL) 
qpress | GPL-1.0-only and GPL-2.0-only and GPL-3.0-only | Portable high-speed file archiver 
qrencode | LGPLv2+ | Generate QR 2D barcodes 
qt5 | GPLv3 | Qt5 meta package 
qt5-qt3d | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Qt3D QML bindings and C++ APIs 
qt5-qtbase | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtBase components 
qt5-qtconnectivity | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Connectivity components 
qt5-qtdeclarative | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtDeclarative component 
qt5-qtdoc | GFDL | Main Qt5 Reference Documentation 
qt5-qtgraphicaleffects | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtGraphicalEffects component 
qt5-qtimageformats | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtImageFormats component 
qt5-qtlocation | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Location component 
qt5-qtmultimedia | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Multimedia support 
qt5-qtquick3d | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Quick 3D bindings and C++ APIs component 
qt5-qtquickcontrols2 | GPLv2+ or LGPLv3 and GFDL | Qt5 - module with set of QtQuick controls for embedded 
qt5-qtquickcontrols | LGPLv2 or LGPLv3 and GFDL | Qt5 - module with set of QtQuick controls 
qt5-qtremoteobjects | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Qt Remote Objects 
qt5-qtscript | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtScript component 
qt5-qtsensors | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Sensors component 
qt5-qtserialbus | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - SerialBus component 
qt5-qtserialport | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - SerialPort component 
qt5-qtsvg | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Support for rendering and displaying SVG 
qt5-qttools | LGPLv3 or LGPLv2 | Qt5 - QtTool components 
qt5-qttranslations | LGPLv2 with exceptions or GPLv3 with exceptions and GFDL | Qt5 - QtTranslations module 
qt5-qtwayland | LGPLv3 | Qt5 - Wayland platform support and QtCompositor module 
qt5-qtwebchannel | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - WebChannel component 
qt5-qtwebkit | LGPLv2 and BSD | Qt5 - QtWebKit components 
qt5-qtwebsockets | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - WebSockets component 
qt5-qtx11extras | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - X11 support library 
qt5-qtxmlpatterns | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtXmlPatterns component 
quota | GPLv2 and GPLv2+ | System administration tools for monitoring users disk usage 
radvd | BSD with advertising | A Router Advertisement daemon 
rapidjson | MIT | Fast JSON parser and generator for C++ 
rasdaemon | GPLv2 | Utility to receive RAS error tracings 
ras-tools | GPLv2 | ras-tools package 
rdma-core | GPLv2 or BSD | RDMA core userspace libraries and daemons 
re2 | BSD | a regular expression library 
readline | GPLv3+ | A library for editing typed command lines 
realmd | LGPLv2+ | DBus service for configuring kerberos and other online identities. 
redis | BSD-3-Clause AND BSD-2-Clause AND MIT | A persistent key-value database 
resolv_wrapper | BSD-3-Clause | A wrapper for dns name resolving or dns faking 
resource-agents | GPLv2+ and LGPLv2+ | Open Source HA Reusable Cluster Resource Scripts 
rest | LGPLv2 | A library for access to RESTful web services 
rng-tools | GPLv2+ | Random number generator related utilities 
rootfiles | Public Domain | The basic required files for the root user's directory 
rpcbind | BSD | Universal Addresses to RPC Program Number Mapper 
rpcsvc-proto | BSD and LGPLv2+ | RPC protocol definitions 
rpm | GPLv2+ | The RPM package management system 
rpmdeplint | GPLv2+ | Tool to find errors in RPM packages in the context of their dependency graph 
rpmdevtools | GPLv2+ and GPLv2 | RPM Development Tools 
rpmlint | GPL-2.0-or-later | Tool for checking common errors in RPM packages 
rpm-mpi-hooks | MIT | RPM dependency generator hooks for MPI packages 
rpm-ostree | LGPLv2+ | Hybrid image/package system 
rrdtool | GPLv2+ with exceptions | Round Robin Database Tool to store and display time-series data 
rsync | GPLv3+ | A program for synchronizing files over a network 
rsyslog | (GPLv3+ and ASL 2.0) | Enhanced system logging and kernel message trapping daemon 
rtkit | GPLv3+ and BSD | REALTIMEKIT Realtime Policy and Watchdog Daemon 
ruby | (Ruby or BSD) and Public Domain and MIT and CC0 and zlib and UCD | An interpreter of object-oriented scripting language 
rubygem-asciidoctor | MIT | A fast, open source AsciiDoc implementation in Ruby 
rubygem-json | Ruby or BSD | A JSON implementation in Ruby 
runc | ASL 2.0 | CLI for running Open Containers 
rust | (Apache-2.0 OR MIT) AND (Artistic-2.0 AND BSD-3-Clause AND ISC AND MIT AND MPL-2.0 AND Unicode-DFS-2016) | The Rust Programming Language 
rust-cargo-c | MIT | Helper program to build and install c-like libraries 
rust-packaging | MIT | RPM macros and generators for building Rust packages 
rust-rav1e | BSD-2-Clause AND ISC | Fastest and safest AV1 encoder 
rust-zram-generator | MIT | Systemd unit generator for zram swap devices 
samba | GPL-3.0-or-later AND LGPL-3.0-or-later | Server and Client software to interoperate with Windows machines 
sane-airscan | GPLv2+ with exceptions | SANE backend for AirScan (eSCL) and WSD document scanners 
sane-backends | GPLv2+ and GPLv2+ with exceptions and Public Domain and IJG and LGPLv2+ and MIT | Scanner access software 
sanlock | GPLv2 and GPLv2+ and LGPLv2+ | A shared storage lock manager 
sassc | MIT | Wrapper around libsass to compile CSS stylesheet 
satyr | GPLv2+ | Automatic problem management with anonymous reports 
sbc | GPLv2 and LGPLv2+ | Sub Band Codec used by bluetooth A2DP 
sblim-sfcc | EPL-1.0 | Small Footprint CIM Client Library 
scap-security-guide | BSD | Security guidance and baselines in SCAP formats 
scipy | BSD and Boost and Public Domain | Scientific Tools for Python 
scons | MIT | An Open Source software construction tool 
scrub | GPLv2+ | Disk scrubbing program 
sdl12-compat | zlib and (Public Domain or MIT-0) and MIT | SDL 1.2 runtime compatibility library using SDL 2.0 
SDL2 | zlib and MIT | Cross-platform multimedia library 
seabios | LGPLv3 | Open-source legacy BIOS implementation 
sed | GPLv3+ | A GNU stream text editor 
selinux-policy | GPLv2+ | SELinux policy configuration 
sendmail | Sendmail | A widely used Mail Transport Agent (MTA) 
sensible-utils | GPL-2.0-or-later | Utilities for sensible alternative selection 
setools | GPLv2 | Policy analysis tools for SELinux 
setroubleshoot | GPL-2.0-or-later | Helps troubleshoot SELinux problems 
setroubleshoot-plugins | GPL-2.0-or-later | Analysis plugins for use with setroubleshoot 
setserial | GPL+ | A utility for configuring serial ports 
setup | Public Domain | A set of system configuration and setup files 
setxkbmap | MIT | X11 keymap client 
sg3_utils | GPLv2+ and BSD | Utilities for devices that use SCSI command sets 
sgabios | ASL 2.0 | Serial graphics BIOS option rom 
sgml-common | GPL+ | Common SGML catalog and DTD files 
sgpio | GPLv2+ | SGPIO captive backplane tool 
shaderc | Apache-2.0 | A collection of tools, libraries, and tests for Vulkan shader compilation 
shadow | BSD and GPLv2+ | Utilities for managing accounts and shadow password files 
shared-mime-info | GPLv2+ | Shared MIME information database 
sharutils | GPLv3+ and (GPLv3+ and BSD) and (LGPLv3+ or BSD) and LGPLv2+ and Public Domain and GFDL | The GNU shar utilities for packaging and unpackaging shell archives 
shim | BSD | First-stage UEFI bootloader 
shim-unsigned-x64 | BSD | First-stage UEFI bootloader 
sil-padauk-fonts | OFL | Padauk pan Myanmar font 
sip | (GPLv2 or GPLv3) and (GPLv2+ with exceptions) | SIP - Python/C++ Bindings Generator 
sisu | EPL-1.0 and BSD | Eclipse dependency injection framework 
skopeo | Apache-2.0 and BSD-2-Clause and BSD-3-Clause and ISC and MIT and MPL-2.0 | Inspect container images and repositories on registries 
slang | GPLv2+ | Shared library for the S-Lang extension language 
slf4j | MIT and ASL 2.0 | Simple Logging Facade for Java 
slirp4netns | GPL-2.0-only AND MIT AND BSD-2-Clause | User-mode networking for unprivileged network namespaces 
smartmontools | GPLv2+ | http 
snappy | BSD | Fast compression and decompression library 
snowball | BSD-3-Clause | Snowball compiler and stemming algorithms 
socat | GPLv2 | Bidirectional data relay between two data channels ('netcat++') 
socket_wrapper | BSD-3-Clause | A library passing all socket communications through Unix sockets 
sockperf | BSD | Network benchmarking utility for testing latency and throughput 
softhsm | BSD | Software version of a PKCS#11 Hardware Security Module 
sombok | GPLv2+ or Artistic clarified | Unicode Text Segmentation Package 
sos | GPL-2.0-or-later | A set of tools to gather troubleshooting information from a system 
sound-theme-freedesktop | GPLv2+ and LGPLv2+ and CC-BY-SA and CC-BY | freedesktop.org sound theme 
soundtouch | LGPLv2+ | Audio Processing library for changing Tempo, Pitch and Playback Rates 
source-highlight | GPLv3+ | Produces a document with syntax highlighting 
sparsehash | BSD | Extremely memory-efficient C++ hash_map implementation 
speex | BSD | A voice compression format (codec) 
speexdsp | BSD | A voice compression format (DSP) 
spice | LGPLv2+ | Implements the SPICE protocol 
spice-gtk | LGPLv2+ | A GTK+ widget for SPICE clients 
spice-protocol | BSD and LGPLv2+ | Spice protocol header files 
spice-vdagent | GPLv3+ | Agent for Spice guests 
spirv-headers | MIT | Header files from the SPIR-V registry 
spirv-tools | ASL 2.0 | API and commands for processing SPIR-V modules 
sqlite | Public Domain | Library that implements an embeddable SQL database engine 
squashfs-tools | GPLv2+ | Utility for the creation of squashfs filesystems 
sscg | GPLv3+ with exceptions | Simple Signed Certificate Generator 
sssd | GPLv3+ | System Security Services Daemon 
star | CDDL | An archiving tool with ACL support 
startup-notification | LGPLv2 | Library for tracking application startup 
stix-fonts | OFL | STIX, a scientific and engineering font family 
strace | LGPL-2.1+ and GPL-2.0+ | Tracks and displays system calls associated with a running process 
subversion | ASL 2.0 | A Modern Concurrent Version Control System 
sudo | ISC | Grants restricted root access for specified users 
supermin | GPLv2+ | Tool for creating supermin appliances 
svt-av1 | BSD-3-Clause and MIT and ISC and Public Domain | Scalable Video Technology for AV1 Encoder 
swig | GPL-3.0-or-later AND BSD-3-Clause | Connects C/C++/Objective C to some high-level programming languages 
switcheroo-control | GPLv3 | D-Bus service to check the availability of dual-GPU 
swtpm | BSD | TPM Emulator 
symlinks | Copyright only | A utility which maintains a system's symbolic links 
sysfsutils | GPLv2 | Utilities for interfacing with sysfs 
syslinux | GPLv2+ | Simple kernel loader which boots from a FAT filesystem 
sysprof | GPLv3+ | A system-wide Linux profiler 
sysstat | GPLv2+ | Collection of performance monitoring tools for Linux 
systemd | LGPLv2+ and MIT and GPLv2+ | System and Service Manager 
system-rpm-config | MulanPSLv2 and MIT | Anolis OS specific rpm configuration files 
systemtap | GPLv2+ | Programmable system-wide instrumentation system 
t1lib | LGPLv2+ | PostScript Type 1 font rasterizer 
t1utils | MIT | Collection of Type 1 and 2 font manipulation utilities 
taglib | LGPLv2 or MPLv1.1 | Audio Meta-Data Library 
tang | GPL-3.0-or-later | Network Presence Binding Daemon 
tar | GPLv3+ | GNU file archiving program 
targetcli | ASL 2.0 | An administration shell for storage targets 
tbb | Apache-2.0 AND BSD-3-Clause | Intel® oneAPI Threading Building Blocks library abstracts low-level threading details 
tcl | TCL | Tool Command Language, pronounced tickle 
tcpdump | BSD with advertising | A network traffic monitoring tool 
tcsh | BSD | An enhanced version of csh, the C shell 
teckit | (LGPLv2+ or CPL) and (LGPLv2+ or GPLv2+ or MPLv2.0 or MPLv1.1) | Conversion library and mapping compiler 
telnet | BSD | The client program for the Telnet remote login protocol 
tengine | BSD | A distribution of Nginx with some advanced features 
tepl | LGPLv3+ | Text editor product line 
tesseract | Apache-2.0 | Raw OCR Engine 
tesseract-tessdata | Apache-2.0 | Trained models for the Tesseract Open Source OCR Engine 
testng | ASL 2.0 | Java-based testing framework 
texi2html | GPL-2.0-or-later AND LicenseRef-OFSFDL AND (CC-BY-SA-3.0 OR GPL-2.0-only) | A highly customizable texinfo to HTML and other formats translator 
texinfo | GPLv3+ | Tools needed to create Texinfo format documentation files 
texlive | Apache-2.0 AND Artistic-2.0 AND CC0-1.0 AND CC-BY-3.0 AND CC-BY-4.0 AND CC-BY-SA-3.0 AND CC-BY-SA-4.0 AND GPL-2.0-only AND GPL-2.0-or-later AND GPL-3.0-or-later AND LGPL-2.1-or-later AND LPPL-1.2 AND LPPL-1.3c AND MIT AND LicenseRef-Fedora-Public-Domain AND LicenseRef-Utopia | TeX formatting system 
texlive-base | Apache-2.0 AND Artistic-2.0 AND BSD AND GFDL-1.1-or-later AND GPL-1.0-or-later AND GPL-2.0-only AND GPL-2.0-or-later AND GPL-3.0-only AND GPL-3.0-or-later AND Knuth-CTAN AND LGPL-2.1-or-later AND LGPL-3.0-or-later AND LPPL-1.2 AND LPPL-1.3 AND LPPL-1.3c AND MIT AND OFL-1.1 AND LicenseRef-Fedora-Public-Domain | TeX formatting system 
tftp | BSD | The client for the Trivial File Transfer Protocol (TFTP) 
thai-scalable-fonts | GPLv2+ and Bitstream Vera | Thai TrueType fonts 
thrift | ASL 2.0 and BSD and zlib | Software framework for cross-language services development 
tigervnc | GPLv2+ | A TigerVNC remote display system 
time | GPLv3+ and GFDL | A GNU utility for monitoring a program's use of system resources 
timedatex | GPLv2+ | D-Bus service for system clock and RTC settings 
tinycdb | Public Domain | Utility and library for manipulating constant databases 
tix | TCL | A set of extension widgets for Tk 
tk | TCL | The graphical toolkit for the Tcl scripting language 
tmux | ISC and BSD | A terminal multiplexer 
tokyocabinet | LGPLv2+ | A modern implementation of a DBM 
tomcat | ASL 2.0 | Apache Servlet/JSP Engine, RI for Servlet 4.0/JSP 2.3 API 
tongsuo | Apache-2.0 | A Modern Cryptographic Primitives and Protocols Library 
totem-pl-parser | LGPLv2+ | Totem Playlist Parser library 
tpm2-abrmd | BSD | A system daemon implementing TPM2 Access Broker and Resource Manager 
tpm2-abrmd-selinux | BSD | SELinux policies for tpm2-abrmd 
tpm2-pkcs11 | BSD | PKCS#11 interface for TPM 2.0 hardware 
tpm2-tools | BSD | A bunch of TPM testing toolS build upon tpm2-tss 
tpm2-tss | BSD and TCGL | TPM2.0 Software Stack 
tracker | GPLv2+ | Desktop-neutral metadata database and search tool 
tracker-miners | GPLv2+ and LGPLv2+ | Tracker miners and metadata extractors 
transfig | Xfig | Utility for converting FIG files (made by xfig) to other formats 
tree | GPLv2+ | File system tree viewer 
trousers | BSD | The open-source TCG Software Stack 
ttembed | Public Domain | Remove embedding limitations from TrueType fonts 
ttfautohint | FTL or GPLv2 | Automated hinting utility for TrueType fonts 
ttmkfdir | LGPLv2+ | Utility to create fonts.scale files for truetype fonts 
tuna | GPLv2 | Application tuning GUI & command line utility 
tuned | GPLv2+ | A dynamic adaptive system tuning daemon 
twolame | LGPLv2+ | Optimized MPEG Audio Layer 2 encoding library based on tooLAME 
tzdata | Public Domain | Timezone data 
uboot-tools | GPLv2+ BSD LGPL-2.1+ LGPL-2.0+ | U-Boot utilities 
uchardet | MPLv1.1 or GPLv2+ or LGPLv2+ | An encoding detector library ported from Mozilla 
ucs-miscfixed-fonts | Public Domain | Selected set of bitmap fonts 
ucx | BSD-3-Clause AND MIT AND CC-PDDC AND (BSD-3-Clause OR Apache-2.0) | UCX is a communication library implementing high-performance messaging 
udisks2 | GPLv2+ | Disk Manager 
udma | GPLv2 | Memory management for Userspace DMA 
uid_wrapper | GPL-3.0-or-later | A wrapper for privilege separation 
umockdev | LGPLv2+ | Mock hardware devices 
unbound | BSD | Unbound is a validating, recursive, caching DNS resolver 
unicode-ucd | MIT | Unicode Character Database 
unifdef | BSD | Tool for removing ifdef lines 
univocity-parsers | ASL 2.0 | Collection of parsers for Java 
unixODBC | GPLv2+ and LGPLv2+ | A complete ODBC driver manager for Linux 
unzip | BSD | A utility for unpacking zip files 
upower | GPLv2+ | Power Management Service 
urw-base35-fonts | AGPLv3 and GPLv2+ | Core Font Set containing 35 freely distributable fonts from (URW)++ 
usbmuxd | GPLv3+ or GPLv2+ | Daemon for communicating with Apple's iOS devices 
usbredir | LGPLv2+ | USB network redirection protocol libraries 
usbutils | GPLv2+ | Linux USB utilities 
usermode | GPLv2+ | Tools for certain user account management tasks 
userspace-rcu | LGPLv2+ | RCU (read-copy-update) implementation in user-space 
utf8proc | Unicode and MIT | Library for processing UTF-8 encoded Unicode strings 
util-linux | GPLv2 and GPLv2+ and LGPLv2+ and BSD with advertising and Public Domain | Collection of basic system utilities 
uuid | MIT | Universally Unique Identifier library 
v4l-utils | GPLv2+ and GPLv2 | Utilities for video4linux and DVB devices 
vala | LGPLv2+ and BSD | A modern programming language for GNOME 
valgrind | GPLv2+ | Dynamic analysis tools to detect memory or thread bugs and profile 
vconfig | GPLv2+ | Linux 802.1q VLAN configuration utility 
velocity | ASL 2.0 | Java-based template engine 
vim | Vim and MIT | Vim - the ubiquitous text editor 
virglrenderer | MIT | Virgl Rendering library. 
virtiofsd | Apache-2.0 AND BSD-3-Clause | Virtio-fs vhost-user device daemon (Rust version) 
virt-manager | GPLv2+ | Desktop tool for managing virtual machines via libvirt 
virt-top | GPLv2+ | Utility like top(1) for displaying virtualization stats 
virt-viewer | GPLv2+ | Virtual Machine Viewer 
virt-what | GPLv2+ | Detect if we are running in a virtual machine 
vmaf | BSD-2-Clause-Patent | Video Multi-Method Assessment Fusion 
volume_key | GPLv2 and (MPLv1.1 or GPLv2) | An utility for manipulating storage encryption keys and passphrases 
vorbis-tools | GPLv2 | The Vorbis General Audio Compression Codec tools 
vsftpd | GPL-2.0-only WITH vsftpd-openssl-exception | Very Secure Ftp Daemon 
vtable-dumper | GPL+ or LGPLv2+ | A tool to list content of virtual tables in a C++ shared library 
vte291 | LGPLv3+ and MIT | Terminal emulator library 
vulkan-headers | ASL 2.0 | Vulkan header files and API registry 
vulkan-loader | ASL 2.0 | Vulkan ICD desktop loader 
wavpack | BSD | A completely open audiocodec 
wayland | MIT | Wayland Compositor Infrastructure 
wayland-protocols | MIT | Wayland protocols that adds functionality not available in the core protocol 
web-assets | MIT | A simple framework for bits pushed to browsers 
webkit2gtk3 | LGPLv2 | GTK Web content engine library 
webkitgtk | LGPLv2 | GTK Web content engine library 
webp-pixbuf-loader | LGPLv2+ | WebP image loader for GTK+ applications 
webrtc-audio-processing | BSD and MIT | Library for echo cancellation 
wget | GPLv3+ | A utility for retrieving files using the HTTP or FTP protocols 
which | GPLv3 | Displays where a particular program in your path is located 
whois | GPLv2+ | Improved WHOIS client 
wireless-regdb | ISC | Regulatory database for 802.11 wireless networking 
wireplumber | MIT | A modular session/policy manager for PipeWire 
wireshark | GPL+ | Network traffic analyzer 
woff2 | MIT | Web Open Font Format 2.0 library 
words | Public Domain | A dictionary of English words for the /usr/share/dict directory 
wpa_supplicant | BSD | WPA/WPA2/IEEE 802.1X Supplicant 
wpebackend-fdo | BSD | A WPE backend designed for Linux desktop systems 
wsdl4j | CPL | Web Services Description Language Toolkit for Java 
xapian-core | GPL-2.0-or-later | The Xapian Probabilistic Information Retrieval Library 
Xaw3d | MIT and GPLv3+ | A version of the MIT Athena widget set for X 
xbean | ASL 2.0 | Java plugin based web server 
xcb-proto | MIT | XCB protocol descriptions 
xcb-util | MIT | Convenience libraries sitting on top of libxcb 
xcb-util-image | MIT | Port of Xlib's XImage and XShmImage functions on top of libxcb 
xcb-util-keysyms | MIT | Standard X key constants and keycodes conversion on top of libxcb 
xcb-util-renderutil | MIT | Additional extensions to the XCB library 
xcb-util-wm | MIT | Client and window-manager helper library on top of libxcb 
xdelta | ASL 2.0 | A binary file delta generator 
xdg-dbus-proxy | LGPLv2+ | Filtering proxy for D-Bus connections 
xdg-desktop-portal | LGPLv2+ | Portal frontend service to flatpak 
xdg-desktop-portal-gnome | LGPL-2.1-or-later | Backend implementation for xdg-desktop-portal using GNOME 
xdg-desktop-portal-gtk | LGPLv2+ | Backend implementation for xdg-desktop-portal using GTK+ 
xdg-user-dirs | GPLv2+ and MIT | Handles user special directories 
xdg-user-dirs-gtk | GPL-2.0-or-later | Gnome integration of special directories 
xdg-utils | MIT | Basic desktop integration functions 
xerces-c | ASL 2.0 | Validating XML Parser 
xfconf | GPLv2 | Hierarchical configuration system for Xfce 
xfsdump | GPL+ | Backup and restore utilities for the XFS filesystem 
xfsprogs | GPL+ and LGPLv2+ | Userspace utilities that manage XFS filesystems 
xfwm4 | GPLv2+ | Next generation window manager for Xfce 
xhost | MIT | Manage the list of hosts or users allowed to connect to the X server 
xisxwayland | MIT | Tool to check if the X server is XWayland 
xkbcomp | MIT | X Keyboard Extension keymap compiler 
xkeyboard-config | MIT | X Keyboard Extension configuration data 
xmlrpc-c | BSD and MIT | Lightweight RPC library based on XML and HTTP 
xmlsec1 | MIT | Library providing support for "XML Signature" and "XML Encryption" standards 
xmlstarlet | MIT | Command Line XML Toolkit 
xmlto | GPLv2+ | A tool for converting XML files to various formats 
xmltoman | GPLv2+ | Scripts for converting XML to roff or HTML 
xmodmap | MIT | utility for modifying keymaps and pointer button mappings in X 
xmvn | ASL 2.0 | Local Extensions for Apache Maven 
xmvn-connector-ivy | ASL 2.0 | XMvn Connector for Apache Ivy 
xorg-x11-drivers | MIT | X.Org X11 driver installation package 
xorg-x11-drv-ati | MIT | Xorg X11 ati video driver 
xorg-x11-drv-dummy | MIT | Xorg X11 dummy video driver 
xorg-x11-drv-evdev | MIT | Xorg X11 evdev input driver 
xorg-x11-drv-fbdev | MIT | Xorg X11 fbdev video driver 
xorg-x11-drv-intel | MIT | Xorg X11 Intel video driver 
xorg-x11-drv-libinput | MIT | Xorg X11 libinput input driver 
xorg-x11-drv-nouveau | MIT | Xorg X11 nouveau video driver for NVIDIA graphics chipsets 
xorg-x11-drv-openchrome | MIT | Xorg X11 openchrome video driver 
xorg-x11-drv-qxl | MIT | Xorg X11 qxl video driver 
xorg-x11-drv-vesa | MIT | Xorg X11 vesa video driver 
xorg-x11-drv-vmware | MIT | Xorg X11 vmware video driver 
xorg-x11-drv-wacom | GPLv2+ | Xorg X11 wacom input driver 
xorg-x11-fonts | MIT and Lucida and Public Domain | X.Org X11 fonts 
xorg-x11-font-utils | MIT | X.Org X11 font utilities 
xorg-x11-proto-devel | BSD-2-Clause AND HPND AND HPND-sell-variant AND ICU AND MIT AND MIT-open-group AND SGI-B-2.0 AND SMLNJ AND X11 AND X11-distribute-modifications-variant | X.Org X11 Protocol headers 
xorg-x11-server | MIT | X.Org X11 X server 
xorg-x11-server-Xwayland | MIT | Xwayland 
xorg-x11-util-macros | MIT | X.Org X11 Autotools macros 
xorg-x11-xauth | MIT | X.Org X11 X authority utilities 
xorg-x11-xinit | MIT | X.Org X11 X Window System xinit startup scripts 
xorg-x11-xtrans-devel | MIT | X.Org X11 developmental X transport library 
xpdf | (GPLv2 or GPLv3) and BSD | A PDF file viewer for the X Window System 
xprop | MIT | X property display utility 
xrandr | MIT | Commandline utility to change output properties 
xrdb | MIT | X server resource database utility 
xrdp | ASL 2.0 and GPLv2+ and MIT | Open source remote desktop protocol (RDP) server 
xsimd | BSD | C++ wrappers for SIMD intrinsics 
xterm | MIT | Terminal emulator for the X Window System 
xxhash | BSD and GPLv2+ | Extremely fast hash algorithm 
xz | GPLv2+ and Public Domain | LZMA compression utilities 
xz-java | Public Domain | Java implementation of XZ data compression 
yajl | ISC | Yet Another JSON Library (YAJL) 
yarnpkg | BSD | Fast, reliable, and secure dependency management. 
yasm | BSD and (GPLv2+ or Artistic or LGPLv2+) and LGPLv2 | Modular Assembler 
yelp | LGPLv2+ and ASL 2.0 and GPLv2+ | Help browser for the GNOME desktop 
yelp-tools | GPLv2+ | Create, manage, and publish documentation for Yelp 
yelp-xsl | LGPLv2+ and GPLv2+ | XSL stylesheets for the yelp help browser 
zchunk | BSD and MIT | Compressed file format that allows easy deltas 
zd1211-firmware | GPLv2 | Firmware for wireless devices based on zd1211 chipset 
zenity | LGPLv2+ | Display dialog boxes from shell scripts 
zeromq | LGPLv3+ | Software library for fast, message-based applications 
zip | BSD | A file compression and packaging utility compatible with PKZIP 
zita-alsa-pcmi | GPLv3+ | ALSA C++ library 
zita-resampler | GPLv3+ | Fast, high-quality sample rate conversion library 
zlib | zlib and Boost | Compression and decompression library 
zopfli | ASL 2.0 | Zlib compatible better compressor 
zsh | MIT | Powerful interactive shell 
zstd | BSD and GPLv2 | Zstd compression library 
zziplib | LGPLv2+ or MPLv1.1 | Lightweight library to easily extract data from zip files 

## 2. DDE
### 2.1 DDE 软件包清单
DDE 是统信软件自主研发的桌面环境，已成为全球范围内主流的操作系统桌面环境，并被开发者广泛移植到更多的操作系统开源社区。

下表列出了 Anolis OS 23.1 DDE 软件包库中的所有软件包及其许可协议。

软件包 | 许可协议 | 功能简述
-------|----------|---------
breeze-icon-theme | LGPLv3+ | Breeze icon theme
dareader | GPL-3.0+ | Read image/video from socket file
dbusmenu-qt | LGPLv2+ | A Qt implementation of the DBusMenu protocol
ddcutil | GPLv2+ | Query and update monitor settings
dde | GPLv3 | Deepin New Desktop Environment - Next
dde-account-faces | GPLv2+ | Account faces for Linux Deepin
dde-api | GPLv3+ | Go-lang bingding for dde-daemon
dde-app-services | LGPL-3.0+ | Service collection of DDE applications
dde-calendar | GPLv3 | Calendar is a smart daily planner to schedule all things in life
dde-clipboard | GPLv3+ | Qt platform theme integration plugins for DDE
dde-control-center | GPLv3 | New control center for Linux Deepin
dde-daemon | GPLv3 | Daemon handling the DDE session settings
dde-device-formatter | GPLv3+ | A simple graphical interface for creating file system in a block device
dde-dock | GPLv3 | Deepin desktop-environment - Dock module
dde-file-manager | GPL-3.0-or-later | Deepin File Manager
dde-kwin | GPLv3+ | KWin configuration for Deepin Desktop Environment
dde-launcher | GPLv3 | dde desktop-environment - Launcher module
dde-network-core | GPLv3 | Deepin desktop-environment - network core files
dde-polkit-agent | GPLv3 | Deepin Polkit Agent
dde-qt5integration | GPLv3+ and BSD and LGPLv2+ | Qt platform theme integration plugins for DDE
dde-qt5platform-plugins | LGPL-3.0-or-later | Qt platform integration plugins for Deepin Desktop Environment
dde-qt-dbus-factory | GPLv3+ and LGPLv2+ | A repository stores auto-generated Qt5 dbus code
dde-server-industry-config | LGPLv3 | DDE desktop-server industry version special configuration file.
dde-session-shell | GPLv3+ | deepin-session-shell - Deepin desktop-environment - session-shell module
dde-session-ui | GPLv3 | Deepin desktop-environment - Session UI module
deepin-boot-maker | GPLv3 | Simple tool to write system image files into CD/DVD, USB flash drive and other media.
deepin-calculator | GPLv3+ | An easy to use calculator for ordinary users
deepin-compressor | GPLv3+ | A fast and lightweight application for creating and extracting archives
deepin-default-settings | GPLv3 | default settings for deepin destkop environment
deepin-desktop-base | GPLv3 | Base files for Deepin Desktop
deepin-desktop-schemas | GPLv3 | GSettings deepin desktop-wide schemas
deepin-devicemanager | GPLv3+ | Device Manager is a handy tool for viewing hardware information and managing the devices
deepin-editor | GPLv3 | Simple editor for Linux Deepin
deepin-fcitxconfigtool-plugin | GPLv2+ | An input method framework
deepin-gettext-tools | GPLv3 | Deepin Gettext Tools
deepin-gtk-theme | GPLv3 | Deepin GTK Theme
deepin-icon-theme | GPLv3 | Icons for the Deepin Desktop Environment
deepin-log-viewer | GPLv3+ | Log Viewer is a useful tool for viewing system logs
deepin-menu | GPLv3+ | Deepin menu service
deepin-proxy | GPLv3 | Most simple RPM package
deepin-pw-check | GPLv3 | Used to check password and manager the configuration for password.
deepin-reader | GPLv3+ | A simple PDF reader, supporting bookmarks, highlights and annotations
deepin-screensaver | GPLv3+ | Screensaver Tool
deepin-shortcut-viewer | GPLv3 | Deepin Shortcut Viewer
deepin-sound-theme | GPLv3 | Deepin sound theme
deepin-system-monitor | GPLv3 | A more user-friendly system monitor
deepin-terminal | GPLv3+ | Default terminal emulation application for Deepin
deepin-wallpapers | GPLv3 | Deepin Wallpapers provides wallpapers of dde
disomaster | GPLv3+ | Library to manipulate DISC burning
docparser | GPLv3+ and Boost and MIT and zlib | A document parser library ported from document2html
dpa-ext-gnomekeyring | GPLv3+ | GNOME keyring extension for dde-polkit-agent.
dtkcommon | GPLv3 | dtk common files
dtkcore | LGPLv3+ | Deepin tool kit core modules
dtkgui | LGPLv3+ | Deepin dtkgui
dtkwidget | LGPLv3+ | Deepin tool kit widget modules
exfat-utils | GPLv2+ | Utilities for exFAT file system
fcitx | GPLv2+ | An input method framework
fcitx-configtool | GPLv2+ | Gtk+-based configuring tools for Fcitx
fcitx-qt5 | GPLv2+ and BSD | Fcitx IM module for Qt5
fuse-exfat | GPLv2+ | Free exFAT file system implementation
gdk-pixbuf2-xlib | LGPLv2+ | Deprecated Xlib integration for gdk-pixbuf2
gio-qt | LGPLv3+ | Gio wrapper for Qt applications
gparted | GPLv2+ | Gnome Partition Editor
gsettings-qt | LGPLv3 | Qt/QML bindings for GSettings
hwinfo | GPL+ | Hardware information tool
imsettings | LGPLv2+ | Delivery framework for general Input Method configuration
imwheel | GPLv2+ | Mouse Event to Key Event Mapper Daemon
kdecoration | LGPLv2 | A plugin-based library to create window decorations
kde-filesystem | Public Domain | KDE filesystem layout
kde-settings | MIT | Config files for kde
kf5 | BSD | Filesystem and RPM macros for KDE Frameworks 5
kf5-attica | LGPLv2+ | KDE Frameworks Tier 1 Addon with Open Collaboration Services API
kf5-kactivities | GPLv2+ and LGPLv2+ | A KDE Frameworks 5 Tier 3 to organize user work into separate activities
kf5-karchive | LGPLv2+ and BSD | KDE Frameworks 5 Tier 1 addon with archive functions
kf5-kauth | LGPLv2+ | KDE Frameworks 5 Tier 2 integration module to perform actions as privileged user
kf5-kbookmarks | LGPLv2+ | KDE Frameworks 5 Tier 3 addon for bookmarks manipulation
kf5-kcmutils | LGPLv2+ | KDE Frameworks 5 Tier 3 addon with extra API to write KConfigModules
kf5-kcodecs | GPLv2+ and LGPLv2+ and BSD | KDE Frameworks 5 Tier 1 addon with string manipulation methods
kf5-kcompletion | LGPLv2+ | KDE Frameworks 5 Tier 2 addon with auto completion widgets and classes
kf5-kconfig | GPLv2+ and LGPLv2+ and MIT | KDE Frameworks 5 Tier 1 addon with advanced configuration system
kf5-kconfigwidgets | GPLv2+ and LGPLv2+ and MIT | KDE Frameworks 5 Tier 3 addon for creating configuration dialogs
kf5-kcoreaddons | LGPLv2+ | KDE Frameworks 5 Tier 1 addon with various classes on top of QtCore
kf5-kcrash | LGPLv2+ | KDE Frameworks 5 Tier 2 addon for handling application crashes
kf5-kdbusaddons | LGPLv2+ | KDE Frameworks 5 Tier 1 addon with various classes on top of QtDBus
kf5-kdeclarative | LGPLv2+ | KDE Frameworks 5 Tier 3 addon for Qt declarative
kf5-kdoctools | GPLv2+ and MIT | KDE Frameworks 5 Tier 2 addon for generating documentation
kf5-kglobalaccel | LGPLv2+ | KDE Frameworks 5 Tier 3 integration module for global shortcuts
kf5-kguiaddons | GPLv2+ and LGPLv2+ | KDE Frameworks 5 Tier 1 addon with various classes on top of QtGui
kf5-ki18n | LGPLv2+ | KDE Frameworks 5 Tier 1 addon for localization
kf5-kiconthemes | LGPLv2+ and GPLv2+ | KDE Frameworks 5 Tier 3 integration module with icon themes
kf5-kidletime | GPLv2+ and LGPLv2+ | KDE Frameworks 5 Tier 1 integration module for idle time detection
kf5-kinit | LGPLv2+ and BSD | KDE Frameworks 5 tier 3 solution for process launching
kf5-kio | GPLv2+ and MIT and BSD | KDE Frameworks 5 Tier 3 solution for filesystem abstraction
kf5-kitemviews | GPLv2+ and LGPLv2+ | KDE Frameworks 5 Tier 1 addon with item views
kf5-kjobwidgets | LGPLv2+ | KDE Frameworks 5 Tier 2 addon for KJobs
kf5-knewstuff | LGPLv2+ | KDE Frameworks 5 Tier 3 module for downloading application assets
kf5-knotifications | LGPLv2+ | KDE Frameworks 5 Tier 2 solution with abstraction for system notifications
kf5-kpackage | LGPLv2+ | KDE Frameworks 5 Tier 2 library to load and install packages as plugins
kf5-kservice | LGPLv2 | KDE Frameworks 5 Tier 3 solution for advanced plugin and service introspection
kf5-ktextwidgets | LGPLv2+ | KDE Frameworks 5 Tier 3 addon with advanced text editing widgets
kf5-kwallet | LGPLv2+ | KDE Frameworks 5 Tier 3 solution for password management
kf5-kwayland | GPLv2+ | KDE Frameworks 5 library that wraps Client and Server Wayland libraries
kf5-kwidgetsaddons | GPLv2+ and LGPLv2+ | KDE Frameworks 5 Tier 1 addon with various classes on top of QtWidgets
kf5-kwindowsystem | LGPLv2+ and MIT | KDE Frameworks 5 Tier 1 integration module with classes for windows management
kf5-kxmlgui | GPLv2+ and LGPLv2+ | KDE Frameworks 5 Tier 3 solution for user-configurable main windows
kf5-networkmanager-qt | LGPLv2+ | A Tier 1 KDE Frameworks 5 module that wraps NetworkManager DBus API
kf5-plasma | GPLv2+ and LGPLv2+ and BSD | KDE Frameworks 5 Tier 3 framework is foundation to build a primary user interface
kf5-solid | LGPLv2+ | KDE Frameworks 5 Tier 1 integration module that provides hardware information
kf5-sonnet | LGPLv2+ | KDE Frameworks 5 Tier 1 solution for spell checking
kf5-syntax-highlighting | MIT | KDE Frameworks 5 Syntax highlighting engine for Kate syntax definitions
kscreenlocker | GPLv2+ | Library and components for secure lock screen architecture
kwin | GPLv2 or GPLv3 | KDE Window manager
libchardet | MPLv1.1 or LGPLv2+ or GPLv2+ | Mozilla's universal character set detector
libmediainfo | BSD | Library for supplies technical and tag information about a video or audio file
libqtxdg | LGPLv2+ | QtXdg, a Qt5 implementation of XDG standards
libx86emu | BSD | x86 emulation library
libzen | zlib | Shared library for libmediainfo and medianfo*
lightdm | (LGPLv2 or LGPLv3) and GPLv3+ | A cross-desktop Display Manager
lrzsz | GPLv2+ | The lrz and lsz modem communications programs
lucene++ | ASL 2.0 or LGPLv3+ | A high-performance, full-featured text search engine written in C++
media-player-info | BSD | Data files describing media player capabilities
minizip1.2 | zlib | Library for manipulation with .zip archives
onboard | GPLv3 and BSD | On-screen keyboard for TabletPC and mobility impaired users (Xorg only)
p7zip | LGPLv2 and (LGPLv2+ or CPL) | Very high compression ratio file archiver
papirus-icon-theme | GPLv3 and CC-BY-SA | Free and open source SVG icon theme based on Paper Icon Set
perl-XML-LibXML-PrettyPrint | (GPL+ or Artistic) and (GPL+ or Artistic or CC-BY-SA) and Public Domain | Add pleasant white space to an XML tree
polkit-qt-1 | GPLv2+ | Qt bindings for PolicyKit
proxychains-ng | GPLv2+ | Redirect connections through proxy servers
qt | (LGPLv2 with exceptions or GPLv3 with exceptions) and ASL 2.0 and BSD and FTL and MIT | Qt toolkit
qt-at-spi | LGPLv2+ | Qt plugin that bridges Qt's accessibility API to AT-SPI2
startdde | GPLv3 | Starter of deepin desktop environment
tinyxml2 | zlib | Simple, small and efficient C++ XML parser
udisks2-qt5 | GPLv3+ | Qt5 binding for udisks2
unrar | Freeware with further limitations | Utility for extracting, testing and viewing RAR archives
xcb-util-cursor | MIT | Cursor library on top of libxcb
xdotool | BSD | Fake keyboard/mouse input
xorg-x11-resutils | MIT | X.Org X11 X resource utilities
xscreensaver | MIT | X screen saver and locker
zssh | GPLv3 | SSH and Telnet client with ZMODEM file transfer capability
