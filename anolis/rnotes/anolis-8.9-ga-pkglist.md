Anolis OS 8.9 GA  软件包发布清单
============================

<!-- toc -->
龙蜥操作系统 (Anolis OS) 8 是 OpenAnolis 龙蜥社区发行的开源 Linux 操作系统发行版，支持多计算架构，具备稳定、高性能、安全、可靠等特点。本文提供了 Anolis OS 8.9 GA 的软件包清单。

修订历史：

日期 | 版本 | 修订内容
------|------|-------
2024年5月13日 | V1.0 | 初稿

## 1. BaseOS
### 1.1 BaseOS 软件包清单
BaseOS 软件包库提供一套核心的底层操作系统的功能，作为所有应用安装的基础。

下表列出了 Anolis OS 8.9 GA BaseOS 软件包库中的所有软件包及其许可协议。

软件包 | 许可协议 | 功能简述
-------|----------|---------
ModemManager | GPLv2+ | Mobile broadband modem management service
NetworkManager | GPLv2+ and LGPLv2+ | Network connection manager and user applications
NetworkManager | GPLv2+ and LGPLv2+ | Network connection manager and user applications
OpenIPMI | LGPLv2+ and GPLv2+ or BSD | IPMI (Intelligent Platform Management Interface) library and tools
PyYAML | MIT | YAML parser and emitter for Python
aajohan-comfortaa-fonts | OFL | Modern style true type font
accel-config | GPLv2 and LGPLv2+ and MIT and CC0 | Configure accelerator subsystem devices
acl | GPLv2+ | Access control list utilities
acpica-tools | GPLv2 | ACPICA tools for the development and debug of ACPI tables
adcli | LGPLv2+ | Active Directory enrollment
adobe-source-code-pro-fonts | OFL | A set of mono-spaced OpenType fonts designed for coding environments
alsa-sof-firmware | BSD | Firmware and topology files for Sound Open Firmware project
anolis-indexhtml | Distributable | Browser default start page for Anolis OS
anolis-logos | Licensed only for approved usage, see COPYING for details. | Anolis OS related icons and pictures
anolis-release | MulanPSLv2 | Anolis OS 8 release file
arpwatch | BSD with advertising | Network monitoring tools for tracking IP addresses on a network
at | GPLv3+ and GPLv2+ and ISC and MIT and Public Domain | Job spooling tools
atlas | BSD | Automatically Tuned Linear Algebra Software
attr | GPLv2+ | Utilities for managing filesystem extended attributes
audit | GPLv2+ | User space tools for kernel auditing
augeas | LGPLv2+ | A library for changing configuration files
authselect | GPLv3+ | Configures authentication and identity sources from supported profiles
autofs | GPLv2+ | A tool for automatically mounting and unmounting filesystems
autofs | GPLv2+ | A tool for automatically mounting and unmounting filesystems
avahi | LGPLv2+ | Local network service discovery
babeltrace | MIT and GPLv2 | Trace Viewer and Converter, mainly for the Common Trace Format
basesystem | Public Domain | The skeleton package which defines a simple Anolis OS system
bash | GPLv3+ | The GNU Bourne Again shell
bash-completion | GPLv2+ | Programmable completion for Bash
bc | GPLv2+ | GNU's bc (a numeric processing language) and dc (a calculator)
bind | MPLv2.0 | The Berkeley Internet Name Domain (BIND) DNS (Domain Name System) server
binutils | GPLv3+ | A GNU collection of binary utilities
biosdevname | GPLv2 | Udev helper for naming devices per BIOS names
blktrace | GPLv2+ | Utilities for performing block layer IO tracing in the Linux kernel
bluez | GPLv2+ | Bluetooth utilities
bolt | LGPLv2+ | Thunderbolt device manager
boom-boot | GPLv2 | A set of libraries and tools for managing boot loader entries
brotli | MIT | Lossless compression algorithm
bubblewrap | LGPLv2+ | Core execution tool for unprivileged containers
bzip2 | BSD | A file compression utility
c-ares | MIT | A library that performs asynchronous DNS operations
ca-certificates | Public Domain | The Mozilla CA root certificate bundle
cachefilesd | GPLv2+ | CacheFiles user-space management daemon
cairo | LGPLv2 or MPLv1.1 | A 2D graphics library
checkpolicy | GPLv2 | SELinux policy compiler
chkconfig | GPLv2 | A system tool for maintaining the /etc/rc*.d hierarchy
chrony | GPLv2 | An NTP client/server
chrpath | GPL+ | Modify rpath of compiled programs
cifs-utils | GPLv3 | Utilities for mounting and managing CIFS mounts
cockpit | LGPL-2.1-or-later | Web Console for Linux servers
conntrack-tools | GPLv2 | Manipulate netfilter connection tracking table and run High Availability
coreutils | GPLv3+ | A set of basic GNU tools commonly used in shell scripts
cpio | GPLv3+ | A GNU archiving program
cracklib | LGPLv2+ | A password-checking library
crda | ISC | Regulatory compliance daemon for 802.11 wireless networking
cronie | MIT and BSD and ISC and GPLv2+ | Cron daemon for executing programs at set times
crontabs | Public Domain and GPLv2 | Root crontab files used to schedule the execution of programs
crypto-policies | LGPLv2+ | System-wide crypto policies
cryptsetup | GPLv2+ and LGPLv2+ | A utility for setting up encrypted disks
cups | GPLv2+ and LGPLv2 with exceptions and AML | CUPS printing system
cups | GPLv2+ and LGPLv2 with exceptions and AML | CUPS printing system
curl | MIT | A utility for getting files from remote servers (FTP, HTTP, and others)
cyrus-sasl | BSD with advertising | The Cyrus SASL library
dbus | (GPLv2+ or AFL) and GPLv2+ | D-BUS message bus
dbus-glib | AFL and GPLv2+ | GLib bindings for D-Bus
dbus-python | MIT | D-Bus Python Bindings
dbxtool | GPLv2 | Secure Boot DBX updater
dejavu-fonts | Bitstream Vera and Public Domain | DejaVu fonts
device-mapper-multipath | GPLv2 | Tools to manage multipath devices using device-mapper
device-mapper-multipath | GPLv2 | Tools to manage multipath devices using device-mapper
device-mapper-persistent-data | GPLv3+ | Device-mapper Persistent Data Tools
dhcp | ISC | Dynamic host configuration protocol software
diffutils | GPLv3+ | A GNU collection of diff utilities
ding-libs | LGPLv3+ | "Ding is not GLib" assorted utility libraries
dlm | GPLv2 and GPLv2+ and LGPLv2+ | dlm control daemon and tool
dmidecode | GPLv2+ | Tool to analyse BIOS DMI data
dnf | GPLv2+ | Package manager
dnf-plugins-core | GPLv2+ | Core Plugins for DNF
dos2unix | BSD | Text file format converters
dosfstools | GPLv3+ | Utilities for making and checking MS-DOS FAT filesystems on Linux
dracut | GPLv2+ and LGPLv2+ | Initramfs generator using udev
dump | BSD | Programs for backing up and restoring ext2/ext3/ext4 filesystems
e2fsprogs | GPLv2 | Utilities for managing ext2, ext3, and ext4 file systems
ed | GPLv3+ and GFDL | The GNU line editor
efi-rpm-macros | GPLv3+ | Common RPM Macros for building EFI-related packages
efibootmgr | GPLv2+ | EFI Boot Manager
efivar | LGPL-2.1 | Tools to manage UEFI variables
elfutils | GPLv3+ and (GPLv2+ or LGPLv3+) and GFDL | A collection of utilities and DSOs to handle ELF files and DWARF data
emacs | GPLv3+ and CC0-1.0 | GNU Emacs text editor
emacs | GPLv3+ and CC0-1.0 | GNU Emacs text editor
environment-modules | GPLv2+ | Provides dynamic modification of a user's environment
ethtool | GPLv2 | Settings tool for Ethernet NICs
expat | MIT | An XML parser library
expect | Public Domain | A program-script interaction and testing utility
fcoe-utils | GPLv2 | Fibre Channel over Ethernet utilities
file | BSD | A utility for determining file types
filesystem | Public Domain | The basic directory layout for a Linux system
findutils | GPLv3+ | The GNU versions of find utilities (find and xargs)
fipscheck | BSD | A library for integrity verification of FIPS validated modules
firewalld | GPLv2+ | A firewall daemon with D-Bus interface providing a dynamic firewall
fontconfig | MIT and Public Domain and UCD | Font configuration and customization library
fontpackages | LGPLv3+ | Common directory and macro definitions used by font packages
freeipmi | GPLv3+ | IPMI remote console and system management software
freetype | (FTL or GPLv2+) and BSD and MIT and Public Domain and zlib with acknowledgement | A free and portable font rendering engine
fuse | GPL+ | File System in Userspace (FUSE) v2 utilities
fwupd | LGPLv2+ | Firmware update daemon
fwupdate | GPLv2+ | Tools to manage UEFI firmware updates
fxload | GPLv2+ | A helper program to download firmware into FX and FX2 EZ-USB devices
gamin | LGPLv2 | Library providing the FAM File Alteration Monitor API
gawk | GPLv3+ and GPLv2+ and LGPLv2+ and BSD | The GNU version of the AWK text processing utility
gcab | LGPLv2+ | Cabinet file library and tool
gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | Various compilers (C, C++, Objective-C, ...)
gcc-toolset-10 | GPLv2+ | Package that installs gcc-toolset-10
gcc-toolset-10-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | Various compilers (C, C++, Objective-C, ...)
gdbm | GPLv3+ | A GNU set of database routines which use extensible hashing
gdisk | GPLv2 | An fdisk-like partitioning tool for GPT disks
gdk-pixbuf2 | LGPLv2+ | An image loading library
genwqe-tools | ASL 2.0 | GenWQE userspace tools
gettext | GPLv3+ and LGPLv2+ | GNU libraries and utilities for producing multi-lingual messages
gfs2-utils | GPLv2+ and LGPLv2+ | Utilities for managing the global file system (GFS2)
glib-networking | LGPLv2+ | Networking support for GLib
glib2 | LGPLv2+ | A library of handy utility functions
glibc | LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL | The GNU libc libraries
glibc | LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL | The GNU libc libraries
glusterfs | GPLv2 or LGPLv3+ | Distributed File System
gmp | LGPLv3+ or GPLv2+ | A GNU arbitrary precision library
gnupg2 | GPLv3+ | Utility for secure communication and data storage
gnutls | GPLv3+ and LGPLv2+ | A TLS protocol implementation
gobject-introspection | GPLv2+ and LGPLv2+ and MIT | Introspection system for GObject-based libraries
gpgme | LGPLv2+ and GPLv3+ | GnuPG Made Easy - high level crypto API
graphite2 | (LGPLv2+ or GPLv2+ or MPLv1.1) and (Netscape or GPLv2+ or LGPLv2+) | Font rendering capabilities for complex non-Roman writing systems
grep | GPLv3+ | Pattern matching utilities
groff | GPLv3+ and GFDL and BSD and MIT | A document formatting system
grub2 | GPLv3+ | Bootloader with support for Linux, Multiboot and more
grubby | GPLv2+ | Command line tool for updating BootLoaderSpec files
gsettings-desktop-schemas | LGPLv2+ | A collection of GSettings schemas
gssproxy | MIT | GSSAPI Proxy
gzip | GPLv3+ and GFDL | The GNU data compression program
hardlink | GPLv2+ | Create a tree of hardlinks
harfbuzz | MIT | Text shaping library
hdparm | BSD | A utility for displaying and/or setting hard disk parameters
hostname | GPLv2+ | Utility to set/show the host name or domain name
hwdata | GPLv2+ | Hardware identification and configuration data
hwloc | BSD | Portable Hardware Locality - portable abstraction of hierarchical architectures
icu | MIT and UCD and Public Domain | International Components for Unicode
ima-evm-utils | GPLv2 | IMA/EVM support utilities
initscripts | GPLv2 | Basic support for legacy System V init scripts
intel-cmt-cat | BSD | Provides command line interface to CMT, MBM, CAT, CDP and MBA technologies
iotop | GPLv2+ | Top like utility for I/O
ipcalc | GPLv2+ | IP network address calculator
iproute | GPL-2.0-or-later AND NIST-PD | Advanced IP routing and network device configuration tools
iprutils | CPL | Utilities for the IBM Power Linux RAID adapters
ipset | GPLv2 | Manage Linux IP sets
iptables | GPLv2 and Artistic 2.0 and ISC | Tools for managing Linux kernel packet filtering capabilities
iptraf-ng | GPLv2+ | A console-based network monitoring utility
iptstate | zlib | A top-like display of IP Tables state table entries
iputils | BSD and GPLv2+ | Network monitoring tools including ping
irqbalance | GPLv2 | IRQ balancing daemon
iscsi-initiator-utils | GPLv2+ | iSCSI daemon and utility programs
isns-utils | LGPLv2+ | The iSNS daemon and utility programs
iw | ISC | A nl80211 based wireless configuration tool
jansson | MIT | C library for encoding, decoding and manipulating JSON data
jimtcl | BSD | A small embeddable Tcl interpreter
json-c | MIT | JSON implementation in C
json-glib | LGPLv2+ | Library for JavaScript Object Notation format
kabi-dw | GPLv3+ | Detect changes in the ABI between kernel builds
kbd | GPLv2+ | Tools for configuring the console (keyboard, virtual terminals, etc.)
kernel | GPLv2 and Redistributable, no modification permitted | The Linux kernel, based on version 4.18.0, heavily modified with backports
kernel | GPLv2 and Redistributable, no modification permitted | The Linux kernel, based on version 4.18.0, heavily modified with backports
kernel | GPLv2 and Redistributable, no modification permitted | The Linux kernel, based on version 4.19.190, heavily modified with backports
kexec-tools | GPLv2 | The kexec/kdump userspace component
keyutils | GPLv2+ and LGPLv2+ | Linux Key Management Utilities
kmod | GPLv2+ | Linux kernel module management utilities
kmod-kvdo | GPLv2+ | Kernel Modules for Virtual Data Optimizer
kpatch | GPLv2 | Dynamic kernel patch manager
krb5 | MIT | The Kerberos network authentication system
ksc | GPLv2+ | Kernel source code checker
ledmon | GPLv2+ | Enclosure LED Utilities
less | GPLv3+ or BSD | A text file browser similar to more, but better
libX11 | MIT | Core X11 protocol client library
libXau | MIT | Sample Authorization Protocol for X
libXext | MIT | X.Org X11 libXext runtime library
libXrender | MIT | X.Org X11 libXrender runtime library
libaio | LGPLv2+ | Linux-native asynchronous I/O access library
libappstream-glib | LGPLv2+ | Library for AppStream metadata
libarchive | BSD | A library for handling streaming archive formats
libassuan | LGPLv2+ and GPLv3+ | GnuPG IPC library
libbpf | LGPLv2 or BSD | Libbpf library
libcap | BSD or GPLv2 | Library for getting and setting POSIX.1e capabilities
libcap | BSD or GPLv2 | Library for getting and setting POSIX.1e capabilities
libcap-ng | LGPLv2+ | An alternate posix capabilities library
libcgroup | LGPLv2+ | Library to control and monitor control groups
libcomps | GPLv2+ | Comps XML file manipulation library
libconfig | LGPLv2+ | C/C++ configuration file library
libcroco | LGPLv2 | A CSS2 parsing library
libdaemon | LGPLv2+ | Library for writing UNIX daemons
libdb | BSD and LGPLv2 and Sleepycat | The Berkeley DB database library for C
libdnf | LGPLv2+ | Library providing simplified C and Python API to libsolv
libedit | BSD | The NetBSD Editline library
liberation-fonts | OFL | Fonts to replace commonly used Microsoft Windows fonts
liberation-narrow-fonts | Liberation | Sans-serif Narrow fonts to replace commonly used Microsoft Arial Narrow
libevent | BSD and ISC | Abstract asynchronous event notification library
libfabric | BSD or GPLv2 | Open Fabric Interfaces
libffi | MIT | A portable foreign function interface library
libgcrypt | LGPLv2+ | A general-purpose cryptography library
libgpg-error | LGPLv2+ | Library for error values used by GnuPG components
libgudev | LGPLv2+ | GObject-based wrapper library for libudev
libgusb | LGPLv2+ | GLib wrapper around libusb1
libhbaapi | SNIA | SNIA HBAAPI library
libhbalinux | LGPLv2 | FC-HBAAPI implementation using scsi_transport_fc interfaces
libhugetlbfs | LGPLv2+ | A library which provides easy access to huge pages of memory
libical | LGPLv2 or MPLv2.0 | Reference implementation of the iCalendar data type and serialization format
libidn2 | (GPLv2+ or LGPLv3+) and GPLv3+ | Library to support IDNA2008 internationalized domain names
libjpeg-turbo | IJG | A MMX/SSE2/SIMD accelerated library for manipulating JPEG image files
libkcapi | BSD or GPLv2 | User space interface to the Linux Kernel Crypto API
libkeepalive | MIT | Enable TCP keepalive in dynamic binaries
libksba | (LGPLv3+ or GPLv2+) and GPLv3+ | CMS and X.509 library
libldb | LGPL-3.0-or-later | A schema-less, ldap like, API and database
libmbim | LGPLv2+ | Support library for the Mobile Broadband Interface Model protocol
libmetalink | MIT | Metalink library written in C
libmicrohttpd | LGPLv2+ | Lightweight library for embedding a webserver in applications
libmnl | LGPLv2+ | A minimalistic Netlink library
libmodman | LGPLv2+ | A simple library for managing C++ modules (plug-ins)
libmodulemd | MIT | Module metadata manipulation library
libndp | LGPLv2+ | Library for Neighbor Discovery Protocol
libnetfilter_conntrack | GPLv2+ | Netfilter conntrack userspace library
libnetfilter_cthelper | GPLv2 | User-space infrastructure for connection tracking helpers
libnetfilter_cttimeout | GPLv2+ | Timeout policy tuning for Netfilter/conntrack
libnetfilter_queue | GPLv2 | Netfilter queue userspace library
libnfnetlink | GPLv2+ | Netfilter netlink userspace library
libnftnl | GPLv2+ | Library for low-level interaction with nftables Netlink's API over libmnl
libnl3 | LGPLv2 | Convenience library for kernel netlink sockets
libnsl2 | BSD and LGPLv2+ | Public client interface library for NIS(YP) and NIS+
libpcap | BSD with advertising | A system-independent interface for user-level packet capture
libpciaccess | MIT | PCI access library
libpeas | LGPLv2+ | Plug-ins implementation convenience library
libpipeline | GPLv3+ | A pipeline manipulation library
libpng | zlib | A library of functions for manipulating PNG image format files
libproxy | LGPLv2+ | A library handling all the details of proxy configuration
libpsl | MIT | C library for the Publix Suffix List
libpsm2 | BSD or GPLv2 | Intel PSM Libraries
libpwquality | BSD or GPLv2+ | A library for password generation and password quality checking
libqb | LGPLv2+ | An IPC library for high performance servers
libqmi | LGPLv2+ | Support library to use the Qualcomm MSM Interface (QMI) protocol
libqrtr-glib | LGPLv2+ | Support library to use and manage the QRTR (Qualcomm IPC Router) bus.
librabbitmq | MIT | Client library for AMQP
librepo | LGPLv2+ | Repodata downloading library
libreport | GPLv2+ | Generic library for reporting various problems
librhsm | LGPLv2+ | Red Hat Subscription Manager library
libseccomp | LGPLv2 | Enhanced seccomp library
libsecret | LGPLv2+ | Library for storing and retrieving passwords and other secrets
libselinux | Public Domain | SELinux library and simple utilities
libsemanage | LGPLv2+ | SELinux binary policy manipulation library
libsepol | LGPLv2+ | SELinux binary policy manipulation library
libsigsegv | GPLv2+ | Library for handling page faults in user mode
libsmbios | GPLv2+ or OSL 2.1 | Libsmbios C/C++ shared libraries
libsolv | BSD | Package dependency solver
libsoup | LGPLv2 | Soup, an HTTP library implementation
libssh | LGPLv2+ | A library implementing the SSH protocol
libstemmer | BSD | C stemming algorithm library
libstoragemgmt | LGPLv2+ | Storage array management library
libtalloc | LGPL-3.0-or-later | The talloc library
libtasn1 | GPLv3+ and LGPLv2+ | The ASN.1 library used in GNUTLS
libtdb | LGPL-3.0-or-later | The tdb library
libteam | LGPLv2+ | Library for controlling team network device
libtevent | LGPL-3.0-or-later | The tevent library
libtirpc | SISSL and BSD | Transport Independent RPC Library
libtool | GPLv2+ and LGPLv2+ and GFDL | The GNU Portable Library Tool
libtraceevent | LGPLv2+ and GPLv2+ | Library to parse raw trace event formats
libunistring | GPLv2+ or LGPLv3+ | GNU Unicode string library
liburing | LGPLv2+ | Linux-native io_uring I/O access library
libusb | LGPLv2+ | Compatibility shim around libusb-1.0 offering the old 0.1 API
libusbx | LGPLv2+ | Library for accessing USB devices
libuser | LGPLv2+ | A user and group account administration library
libutempter | LGPLv2+ | A privileged helper for utmp/wtmp updates
libvarlink | ASL 2.0 | Varlink C Library
libverto | MIT | Main loop abstraction library
libxcb | MIT | A C binding to the X11 protocol
libxcrypt | LGPLv2+ and BSD and Public Domain | Extended crypt library for DES, MD5, Blowfish and others
libxml2 | MIT | Library providing XML and HTML support
libxmlb | LGPLv2+ | Library for querying compressed XML metadata
libxslt | MIT | Library providing the Gnome XSLT engine
libyaml | MIT | YAML 1.1 parser and emitter written in C
linux-firmware | GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted | Firmware files used by the Linux kernel
lksctp-tools | GPLv2 and GPLv2+ and LGPLv2 and MIT | User-space access to Linux Kernel SCTP
lldpad | GPLv2 | Intel LLDP Agent
lm_sensors | GPLv2+ and Verbatim and MIT | Hardware monitoring tools
lmdb | OpenLDAP | Memory-mapped key-value database
lockdev | LGPLv2 | A library for locking devices
logrotate | GPLv2+ | Rotates, compresses, removes and mails system log files
logwatch | MIT | A log file analysis program
lrzsz | GPLv2+ | The lrz and lsz modem communications programs
lshw | GPLv2 | Hardware lister
lsof | zlib and Sendmail and LGPLv2+ | A utility which lists open files on a Linux/UNIX system
lsscsi | GPLv2+ | List SCSI devices (or hosts) and associated information
lua | MIT | Powerful light-weight programming language
lvm2 | GPLv2 | Userland logical volume management tools
lz4 | GPLv2+ and BSD | Extremely fast compression algorithm
lzo | GPLv2+ | Data compression library with very fast (de)compression
lzop | GPLv2+ | Real-time file compressor
m4 | GPLv3+ | The GNU macro processor
mailcap | Public Domain and MIT | Helper application and MIME type associations for file types
mailx | BSD with advertising and MPLv1.1 | Enhanced implementation of the mailx command
make | GPLv3+ | A GNU tool which simplifies the build process for users
man-db | GPLv2+ and GPLv3+ | Tools for searching and reading man pages
man-pages | GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE | Linux kernel and C library user-space interface documentation
mcelog | GPLv2 | Tool to translate x86-64 CPU Machine Check Exception data
mcstrans | GPL+ | SELinux Translation Daemon
mdadm | GPLv2+ | The mdadm program controls Linux md devices (software RAID arrays)
memstrack | GPLv3 | A memory allocation tracer, like a hot spot analyzer for memory allocation
memtest86+ | GPLv2 | Stand-alone memory tester for x86 and x86-64 computers
microcode_ctl | CC0 and Redistributable, no modification permitted | CPU microcode updates for Intel x86 processors
microdnf | GPLv2+ | Lightweight implementation of DNF in C
minicom | GPLv2+ and LGPLv2+ and Public Domain | A text-based modem control and terminal emulation program
mksh | MirOS and ISC and BSD | MirBSD enhanced version of the Korn Shell
mlocate | GPLv2 | An utility for finding files by name
mobile-broadband-provider-info | Public Domain | Mobile broadband provider database
mokutil | GPLv3+ | Tool to manage UEFI Secure Boot MoK Keys
mozjs52 | MPLv2.0 and MPLv1.1 and BSD and GPLv2+ and GPLv3+ and LGPLv2.1 and LGPLv2.1+ and AFL and ASL 2.0 | SpiderMonkey JavaScript library
mozjs60 | MPLv2.0 and MPLv1.1 and BSD and GPLv2+ and GPLv3+ and LGPLv2+ and AFL and ASL 2.0 | SpiderMonkey JavaScript library
mpfr | LGPLv3+ and GPLv3+ and GFDL | A C library for multiple-precision floating-point computations
mtools | GPLv3+ | Programs for accessing MS-DOS disks without mounting the disks
mtr | GPLv2 | Network diagnostic tool combining 'traceroute' and 'ping'
nano | GPLv3+ | A small text editor
ncurses | MIT | Ncurses support utilities
ndctl | GPLv2 | Manage "libnvdimm" subsystem devices (Non-volatile Memory)
net-snmp | BSD | A collection of SNMP protocol tools and libraries
net-tools | GPLv2+ | Basic networking tools
netlabel_tools | GPLv2 | Tools to manage the Linux NetLabel subsystem
nettle | LGPLv3+ or GPLv2+ | A low-level cryptographic library
newt | LGPLv2 | A library for text mode user interfaces
nfs-utils | MIT and GPLv2 and GPLv2+ and BSD | NFS utilities and supporting clients and daemons for the kernel NFS server
nfs4-acl-tools | BSD | The nfs4 ACL tools
nftables | GPLv2 | Netfilter Tables userspace utillites
nghttp2 | MIT | Experimental HTTP/2 client, server and proxy
npth | LGPLv2+ | The New GNU Portable Threads library
nss_nis | LGPLv2+ | Name Service Switch (NSS) module using NIS
numactl | GPLv2 | Library for tuning for Non Uniform Memory Access machines
numad | LGPLv2 | NUMA user daemon
numatop | BSD | Memory access locality characterization and analysis
nvme-cli | GPLv2+ | NVMe management command line interface
nvmetcli | ASL 2.0 | An adminstration shell for NVMe storage targets
opa-ff | BSD or GPLv2 | Intel Omni-Path basic tools and libraries for fabric management
opa-fm | GPLv2 or BSD | Intel Omni-Path Fabric Management Software
opencryptoki | CPL | Implementation of the PKCS#11 (Cryptoki) specification v3.0
opencsd | BSD | An open source CoreSight(tm) Trace Decode library
openhpi | BSD | Hardware Platform Interface library and tools
openldap | OpenLDAP | LDAP support libraries
opensc | LGPLv2+ | Smart card library and applications
opensm | GPLv2 or BSD | OpenIB InfiniBand Subnet Manager and management utilities
openssh | BSD | An open source implementation of SSH protocol version 2
openssl | OpenSSL and ASL 2.0 | Utilities from the general purpose cryptography library with TLS implementation
openssl-ibmpkcs11 | OpenSSL | IBM OpenSSL PKCS#11 engine
openssl-pkcs11 | LGPLv2+ and BSD | A PKCS#11 engine for use with OpenSSL
os-prober | GPLv2+ and GPL+ | Probes disks on the system for installed operating systems
p11-kit | BSD | Library for loading and sharing PKCS#11 modules
pam | BSD and GPLv2+ | An extensible library which provides authentication for applications
parted | GPLv3+ | The GNU disk partition manipulation program
passwd | BSD or GPL+ | An utility for setting or changing passwords using PAM
patch | GPLv3+ | Utility for modifying/upgrading files
pciutils | GPLv2+ | PCI bus related utilities
pcre | BSD | Perl-compatible regular expression library
pcre2 | BSD | Perl-compatible regular expression library
pcsc-lite | BSD | PC/SC Lite smart card framework and applications
pcsc-lite-ccid | LGPLv2+ | Generic USB CCID smart card reader driver
perftest | GPLv2 or BSD | IB Performance Tests
perl | GPL+ or Artistic | Practical Extraction and Report Language
perl-Algorithm-Diff | GPL+ or Artistic | Compute `intelligent' differences between two files/lists
perl-Archive-Tar | GPL+ or Artistic | A module for Perl manipulation of .tar files
perl-Carp | GPL+ or Artistic | Alternative warn and die for modules
perl-Compress-Raw-Bzip2 | GPL+ or Artistic | Low-level interface to bzip2 compression library
perl-Compress-Raw-Zlib | (GPL+ or Artistic) and zlib | Low-level interface to the zlib compression library
perl-DBD-SQLite | (GPL+ or Artistic) and Public Domain | SQLite DBI Driver
perl-DBI | GPL+ or Artistic | A database access API for perl
perl-Data-Dumper | GPL+ or Artistic | Stringify perl data structures, suitable for printing and eval
perl-Date-Manip | GPL+ or Artistic | Date manipulation routines
perl-Digest | GPL+ or Artistic | Modules that calculate message digests
perl-Digest-MD5 | (GPL+ or Artistic) and BSD | Perl interface to the MD5 algorithm
perl-Encode | (GPL+ or Artistic) and Artistic 2.0 and UCD | Character encodings in Perl
perl-Exporter | GPL+ or Artistic | Implements default import method for modules
perl-File-Path | GPL+ or Artistic | Create or remove directory trees
perl-File-Temp | GPL+ or Artistic | Return name and handle of a temporary file safely
perl-Getopt-Long | GPLv2+ or Artistic | Extended processing of command line options
perl-HTTP-Tiny | GPL+ or Artistic | Small, simple, correct HTTP/1.1 client
perl-HTTP-Tiny | GPL+ or Artistic | Small, simple, correct HTTP/1.1 client
perl-IO-Compress | GPL+ or Artistic | Read and write compressed data
perl-IO-Socket-IP | GPL+ or Artistic | Drop-in replacement for IO::Socket::INET supporting both IPv4 and IPv6
perl-MIME-Base64 | (GPL+ or Artistic) and MIT | Encoding and decoding of Base64 and quoted-printable strings
perl-Math-BigInt | GPL+ or Artistic | Arbitrary-size integer and float mathematics
perl-Parse-Yapp | GPL+ or Artistic | Perl extension for generating and using LALR parsers
perl-PathTools | (GPL+ or Artistic) and BSD | PathTools Perl module (Cwd, File::Spec)
perl-Pod-Escapes | GPL+ or Artistic | Resolve POD escape sequences
perl-Pod-Perldoc | GPL+ or Artistic | Look up Perl documentation in Pod format
perl-Pod-Simple | GPL+ or Artistic | Framework for parsing POD documentation
perl-Pod-Usage | GPL+ or Artistic | Print a usage message from embedded POD documentation
perl-Scalar-List-Utils | GPL+ or Artistic | A selection of general-utility scalar and list subroutines
perl-Socket | GPL+ or Artistic | Networking constants and support functions
perl-Storable | GPL+ or Artistic | Persistence for Perl data structures
perl-Sys-CPU | (GPL+ or Artistic) and (LGPLv3 or Artistic 2.0) | Getting CPU information
perl-Sys-MemInfo | GPL+ or Artistic | Memory information as Perl module
perl-Term-ANSIColor | GPL+ or Artistic | Color screen output using ANSI escape sequences
perl-Term-Cap | GPL+ or Artistic | Perl termcap interface
perl-Text-Diff | (GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT | Perform diffs on files and record sets
perl-Text-ParseWords | GPL+ or Artistic | Parse text into an array of tokens or array of arrays
perl-Text-Tabs+Wrap | TTWL | Expand tabs and do simple line wrapping
perl-Time-Local | GPL+ or Artistic | Efficiently compute time from local and GMT time
perl-URI | GPL+ or Artistic | A Perl module implementing URI parsing and manipulation
perl-Unicode-Normalize | GPL+ or Artistic | Unicode Normalization Forms
perl-constant | GPL+ or Artistic | Perl pragma to declare constants
perl-libnet | GPL+ or Artistic | Perl clients for various network protocols
perl-parent | GPL+ or Artistic | Establish an ISA relationship with base classes at compile time
perl-podlators | (GPL+ or Artistic) and FSFAP | Format POD source into various output formats
perl-threads | GPL+ or Artistic | Perl interpreter-based threads
perl-threads-shared | GPL+ or Artistic | Perl extension for sharing data structures between threads
pigz | zlib | Parallel implementation of gzip
pixman | MIT | Pixel manipulation library
pkgconf | ISC | Package compiler and linker metadata toolkit
policycoreutils | GPLv2 | SELinux policy core utilities
polkit | LGPLv2+ | An authorization framework
polkit-pkla-compat | LGPLv2+ | Rules for polkit to add compatibility with pklocalauthority
popt | MIT | C library for parsing command line parameters
portreserve | GPLv2+ | TCP port reservation utility
postfix | (IBM and GPLv2+) or (EPL-2.0 and GPLv2+) | Postfix Mail Transport Agent
ppp | BSD and LGPLv2+ and GPLv2+ and Public Domain | The Point-to-Point Protocol daemon
prefixdevname | MIT | Udev helper utility that provides network interface naming using user defined prefix
procps-ng | GPL+ and GPLv2 and GPLv2+ and GPLv3+ and LGPLv2+ | System and process monitoring utilities
ps_mem | LGPLv2 | Memory profiling tool
psacct | GPLv3+ | Utilities for monitoring process activities
psmisc | GPLv2+ | Utilities for managing processes on your system
publicsuffix-list | MPLv2.0 | Cross-vendor public domain suffix database
pygobject3 | LGPLv2+ and MIT | Python bindings for GObject Introspection
pyparsing | MIT | Python package with an object-oriented approach to text processing
python-asn1crypto | MIT | Fast Python ASN.1 parser and serializer
python-cffi | MIT | Foreign Function Interface for Python to call C code
python-chardet | LGPLv2 | Character encoding auto-detection in Python
python-configobj | BSD | Config file reading, writing, and validation
python-configshell | ASL 2.0 | A framework to implement simple but nice CLIs
python-cryptography | ASL 2.0 or BSD | PyCA's cryptography library
python-cryptography | ASL 2.0 or BSD | PyCA's cryptography library
python-dateutil | BSD | Powerful extensions to the standard datetime module
python-decorator | BSD | Module to simplify usage of decorators
python-dmidecode | GPLv2 | Python module to access DMI data
python-dns | MIT | DNS toolkit for Python
python-ethtool | GPLv2 | Python module to interface with ethtool
python-idna | BSD and Python and Unicode | Internationalized Domain Names in Applications (IDNA)
python-iniparse | MIT and Python | Python Module for Accessing and Modifying Configuration Data in INI files
python-inotify | MIT | Monitor filesystem events with Python under Linux
python-jwt | MIT | JSON Web Token implementation in Python
python-kmod | LGPLv2+ | Python module to work with kernel modules
python-linux-procfs | GPLv2 | Linux /proc abstraction classes
python-oauthlib | BSD | An implementation of the OAuth request-signing logic
python-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python packages
python-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python packages
python-ply | BSD | Python Lex-Yacc
python-pycparser | BSD | C parser and AST generator written in Python
python-pysocks | BSD | A Python SOCKS client module
python-pyudev | LGPLv2+ | A libudev binding
python-requests | ASL 2.0 | HTTP library, written in Python, for human beings
python-requests-oauthlib | ISC | OAuthlib authentication support for Requests.
python-rtslib | ASL 2.0 | API for Linux kernel LIO SCSI target
python-schedutils | GPLv2 | Linux scheduler python bindings
python-setuptools | MIT | Easily build and distribute Python packages
python-six | MIT | Python 2 and 3 compatibility utilities
python-slip | GPLv2+ | Convenience, extension and workaround code for Python
python-systemd | LGPLv2+ | Python module wrapping systemd functionality
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post
python-urwid | LGPLv2+ | Console user interface library
python-varlink | ASL 2.0 | Python implementation of Varlink
python3 | Python | Interpreter of the Python programming language
pywbem | LGPLv2 | Python WBEM Client and Provider Interface
quota | GPLv2 and GPLv2+ | System administration tools for monitoring users' disk usage
rasdaemon | GPLv2 | Utility to receive RAS error tracings
rdma-core | GPLv2 or BSD | RDMA core userspace libraries and daemons
readline | GPLv3+ | A library for editing typed command lines
realmd | LGPLv2+ | Kerberos realm enrollment service
rng-tools | GPLv2+ | Random number generator related utilities
rootfiles | Public Domain | The basic required files for the root user's directory
rpcbind | BSD | Universal Addresses to RPC Program Number Mapper
rpm | GPLv2+ | The RPM package management system
rsync | GPLv3+ | A program for synchronizing files over a network
s-nail | ISC and BSD with advertising and BSD | Environment for sending and receiving mail
samba | GPL-3.0-or-later AND LGPL-3.0-or-later | Server and Client software to interoperate with Windows machines
samba | GPL-3.0-or-later AND LGPL-3.0-or-later | Server and Client software to interoperate with Windows machines
sanlock | GPLv2 and GPLv2+ and LGPLv2+ | A shared storage lock manager
scl-utils | GPLv2+ | Utilities for alternative packaging
sed | GPLv3+ | A GNU stream text editor
selinux-policy | GPLv2+ | SELinux policy configuration
selinux-policy | GPLv2+ | SELinux policy configuration
setools | GPLv2 | Policy analysis tools for SELinux
setserial | GPL+ | A utility for configuring serial ports
setup | Public Domain | A set of system configuration and setup files
sg3_utils | GPLv2+ and BSD | Utilities for devices that use SCSI command sets
sgml-common | GPL+ | Common SGML catalog and DTD files
sgpio | GPLv2+ | SGPIO captive backplane tool
shadow-utils | BSD and GPLv2+ | Utilities for managing accounts and shadow password files
shared-mime-info | GPLv2+ | Shared MIME information database
shim | BSD | First-stage UEFI bootloader
slang | GPLv2+ | The shared library for the S-Lang extension language
smartmontools | GPLv2+ | Tools for monitoring SMART capable hard disks
smc-tools | EPL | Shared Memory Communication Tools
snappy | BSD | Fast compression and decompression library
sos | GPL-2.0-or-later | A set of tools to gather troubleshooting information from a system
sos | GPL-2.0-or-later | A set of tools to gather troubleshooting information from a system
sqlite | Public Domain | Library that implements an embeddable SQL database engine
squashfs-tools | GPLv2+ | Utility for the creation of squashfs filesystems
sssd | GPLv3+ | System Security Services Daemon
sssd | GPLv3+ | System Security Services Daemon
star | CDDL | An archiving tool with ACL support
strace | LGPL-2.1+ and GPL-2.0+ | Tracks and displays system calls associated with a running process
stunnel | GPLv2 | A TLS-encrypting socket wrapper
sudo | ISC | Allows restricted root access for specified users
symlinks | Copyright only | A utility which maintains a system's symbolic links
sysfsutils | GPLv2 | Utilities for interfacing with sysfs
syslinux | GPLv2+ | Simple kernel loader which boots from a FAT filesystem
sysprof | GPLv3+ | A system-wide Linux profiler
system-storage-manager | GPLv2+ | A single tool to manage your storage
systemd | LGPLv2+ and MIT and GPLv2+ | System and Service Manager
systemd | LGPLv2+ and MIT and GPLv2+ | System and Service Manager
tar | GPLv3+ | A GNU file archiving program
tboot | BSD | Performs a verified launch using Intel TXT
tcl | TCL | Tool Command Language, pronounced tickle
texinfo | GPLv3+ | Tools needed to create Texinfo format documentation files
time | GPLv3+ and GFDL | A GNU utility for monitoring a program's use of system resources
timedatex | GPLv2+ | D-Bus service for system clock and RTC settings
tmpwatch | GPLv2 | A utility for removing files based on when they were last accessed
tmux | ISC and BSD | A terminal multiplexer
tpm-quote-tools | BSD | TPM-based attestation using the TPM quote operation (tools)
tpm-tools | CPL | Management tools for the TPM hardware
tpm2-abrmd | BSD | A system daemon implementing TPM2 Access Broker and Resource Manager
tpm2-abrmd-selinux | BSD | SELinux policies for tpm2-abrmd
tpm2-tools | BSD | A TPM2.0 testing tool build upon TPM2.0-TSS
tpm2-tss | BSD | TPM2.0 Software Stack
trace-cmd | GPLv2 and LGPLv2 | A user interface to Ftrace
traceroute | GPLv2+ | Traces the route taken by packets over an IPv4/IPv6 network
tree | GPLv2+ | File system tree viewer
trousers | BSD | TCG's Software Stack v1.2
tss2 | BSD | IBM's TCG Software Stack (TSS) for TPM 2.0 and related utilities
tuna | GPLv2 | Application tuning GUI & command line utility
tuned | GPLv2+ | A dynamic adaptive system tuning daemon
tzdata | Public Domain | Timezone data
units | GPLv3+ | A utility for converting amounts from one unit to another
unzip | BSD | A utility for unpacking zip files
usb_modeswitch | GPLv2+ | USB Modeswitch gets mobile broadband cards in operational mode
usb_modeswitch-data | GPLv2+ | USB Modeswitch gets mobile broadband cards in operational mode
usbutils | GPLv2+ | Linux USB utilities
usermode | GPLv2+ | Tools for certain user account management tasks
userspace-rcu | LGPLv2+ | RCU (read-copy-update) implementation in user-space
util-linux | GPLv2 and GPLv2+ and LGPLv2+ and BSD with advertising and Public Domain | A collection of basic system utilities
util-linux | GPLv2 and GPLv2+ and LGPLv2+ and BSD with advertising and Public Domain | A collection of basic system utilities
vdo | GPLv2 | Management tools for Virtual Data Optimizer
vhostmd | GPLv2+ | Virtualization host metrics daemon
vim | Vim and MIT | The VIM editor
virt-what | GPLv2+ | Detect if we are running in a virtual machine
watchdog | GPLv2+ | Software and/or Hardware watchdog daemon
which | GPLv3 | Displays where a particular program in your path is located
words | Public Domain | A dictionary of English words for the /usr/share/dict directory
wpa_supplicant | BSD | WPA/WPA2/IEEE 802.1X Supplicant
x3270 | BSD | An X Window System based IBM 3278/3279 terminal emulator
xdelta | ASL 2.0 | A binary file delta generator
xfsdump | GPL+ | Administrative utilities for the XFS filesystem
xfsdump | GPL+ | Administrative utilities for the XFS filesystem
xfsprogs | GPL+ and LGPLv2+ | Utilities for managing the XFS filesystem
xmlrpc-c | BSD and MIT | Lightweight RPC library based on XML and HTTP
xz | GPLv2+ and Public Domain | LZMA compression utilities
zip | BSD | A file compression and packaging utility compatible with PKZIP
zlib | zlib and Boost | The compression and decompression library
zsh | MIT | Powerful interactive shell
zstd | BSD and GPLv2 | Zstd compression library

## 2. AppStream
### 2.1 AppStream 软件包清单
应用流(AppStream)软件包库中的内容包括其他用户空间应用程序，运行时语言和数据库，以支持各种工作负载和用例。AppStream 引入了模块的概念，可以为单个软件包提供多个版本并在 Anolis OS 8.9 GA 中进行灵活安装。

下表列出了 Anolis OS 8.9 GA AppStream 存储库中的所有软件包及其许可协议。

软件包 | 许可协议 | 功能简述
-------|----------|---------
389-ds-base | GPLv3+ and (ASL 2.0 or MIT) | 389 Directory Server (base)
Box2D | zlib | A 2D Physics Engine for Games
CUnit | LGPLv2+ | Unit testing framework for C
Cython | ASL 2.0 | Language for writing Python extension modules
Cython | ASL 2.0 | Language for writing Python extension modules
Cython | ASL 2.0 | Language for writing Python extension modules
Cython | ASL 2.0 | Language for writing Python extension modules
GConf2 | LGPLv2+ and GPLv2+ | A process-transparent configuration system
HdrHistogram | BSD and CC0 | A High Dynamic Range (HDR) Histogram
HdrHistogram_c | BSD and Public Domain | C port of the HdrHistogram
Judy | LGPLv2+ | General purpose dynamic array
Judy | LGPLv2+ | General purpose dynamic array
LibRaw | BSD and LGPLv2 | Library for reading RAW files obtained from digital photo cameras
NetworkManager-libreswan | GPLv2+ | NetworkManager VPN plug-in for IPsec VPN
OpenEXR | BSD | A high dynamic-range (HDR) image file format
PackageKit | GPLv2+ and LGPLv2+ | Package management service
PyYAML | MIT | YAML parser and emitter for Python
PyYAML | MIT | YAML parser and emitter for Python
PyYAML | MIT | YAML parser and emitter for Python
SDL | LGPLv2+ | A cross-platform multimedia library
SDL2 | zlib and MIT | Cross-platform multimedia library
SuperLU | BSD and GPLV2+ | Subroutines to solve sparse linear systems
WALinuxAgent | ASL 2.0 | Microsoft Azure Linux Agent
Xaw3d | MIT | A version of the MIT Athena widget set for X
a52dec | GPLv2 | Small test program for liba52
aardvark-dns | ASL 2.0 and BSD and MIT | Authoritative DNS server for A/AAAA container records
abattis-cantarell-fonts | OFL | Humanist sans serif font
abrt | GPLv2+ | Automatic bug detection and reporting tool
abrt-java-connector | GPLv2+ | JNI Agent library converting Java exceptions to ABRT problems
accountsservice | GPLv3+ | D-Bus interfaces for querying and manipulating user account information
acpid | GPLv2+ | ACPI Event Daemon
adobe-mappings-cmap | BSD | CMap resources for Adobe's character collections
adobe-mappings-pdf | BSD | PDF mapping resources from Adobe
adwaita-icon-theme | LGPLv3+ or CC-BY-SA | Adwaita icon theme
adwaita-qt | LGPLv2+ and GPLv2+ | Adwaita theme for Qt-based applications
aide | GPLv2+ | Intrusion detection environment
alibaba-puhui-fonts | Alibaba PuHui v3 | AlibabaPuHuiTi
alsa-firmware | GPL+ and BSD and GPLv2+ and GPLv2 and LGPLv2+ | Firmware for several ALSA-supported sound cards
alsa-lib | LGPLv2+ | The Advanced Linux Sound Architecture (ALSA) library
alsa-plugins | GPLv2+ and LGPLv2+ and BSD | The Advanced Linux Sound Architecture (ALSA) Plugins
alsa-tools | GPLv2+ | Specialist tools for ALSA
alsa-utils | GPLv2+ | Advanced Linux Sound Architecture (ALSA) utilities
amanda | BSD and GPLv3+ and GPLv2+ and GPLv2 | A network-capable tape backup solution
anaconda | GPLv2+ and MIT | Graphical system installer
anaconda-user-help | CC-BY-SA | Content for the Anaconda built-in help system
annobin | GPLv3+ | Annotate and examine compiled binary files
ansible-collection-microsoft-sql | MIT | The Ansible collection for Microsoft SQL Server management
ansible-core | GPLv3+ | SSH-based configuration management, deployment, and task execution system
ansible-freeipa | GPL-3.0-or-later | Roles and playbooks to deploy FreeIPA servers, replicas and clients
ansible-pcp | MIT | Ansible Metric collection for Performance Co-Pilot
ant | ASL 2.0 | Java build tool
aopalliance | Public Domain | Java/J2EE AOP standards
aopalliance | Public Domain | Java/J2EE AOP standards
apache-commons-cli | ASL 2.0 | Command Line Interface Library for Java
apache-commons-cli | ASL 2.0 | Command Line Interface Library for Java
apache-commons-codec | ASL 2.0 | Implementations of common encoders and decoders
apache-commons-codec | ASL 2.0 | Implementations of common encoders and decoders
apache-commons-collections | ASL 2.0 | Provides new interfaces, implementations and utilities for Java Collections
apache-commons-compress | ASL 2.0 | Java API for working with compressed files and archivers
apache-commons-io | ASL 2.0 | Utilities to assist with developing IO functionality
apache-commons-io | ASL 2.0 | Utilities to assist with developing IO functionality
apache-commons-jxpath | ASL 2.0 | Simple XPath interpreter
apache-commons-lang | ASL 2.0 | Provides a host of helper utilities for the java.lang API
apache-commons-lang3 | ASL 2.0 | Provides a host of helper utilities for the java.lang API
apache-commons-lang3 | ASL 2.0 | Provides a host of helper utilities for the java.lang API
apache-commons-logging | ASL 2.0 | Apache Commons Logging
apache-commons-net | ASL 2.0 | Internet protocol suite Java library
apiguardian | ASL 2.0 | API Guardian Java annotation
appstream-data | CC0 and CC-BY and CC-BY-SA and GFDL | Cached AppStream metadata
apr | ASL 2.0 and BSD with advertising and ISC and BSD | Apache Portable Runtime library
apr-util | ASL 2.0 | Apache Portable Runtime Utility library
asciidoc | GPL+ and GPLv2+ | Text based document generation
aspell | LGPLv2+ and LGPLv2 and GPLv2+ and BSD | Spell checker
aspell-en | MIT and BSD | English dictionaries for Aspell
at-spi2-atk | LGPLv2+ | A GTK+ module that bridges ATK to D-Bus at-spi
at-spi2-core | LGPLv2+ | Protocol definitions and daemon for D-Bus at-spi
atinject | ASL 2.0 | Dependency injection specification for Java (JSR-330)
atinject | ASL 2.0 | Dependency injection specification for Java (JSR-330)
atk | LGPLv2+ | Interfaces for accessibility support
atkmm | LGPLv2+ | C++ interface for the ATK library
authd | GPLv2+ | A RFC 1413 ident protocol daemon
autoconf | GPLv2+ and GFDL | A GNU tool for automatically configuring source code
autoconf-archive | GPLv3+ with exceptions | The Autoconf Macro Archive
autoconf213 | GPLv2+ | A GNU tool for automatically configuring source code
autogen | GPLv3+ | Automated text file generator
automake | GPLv2+ and GFDL and Public Domain and MIT | A GNU tool for automatically creating Makefiles
autotrace | GPLv2+ and LGPLv2+ | Utility for converting bitmaps to vector graphics
babel | BSD | Tools for internationalizing Python applications
babel | BSD | Tools for internationalizing Python applications
babel | BSD | Tools for internationalizing Python applications
babl | LGPLv3+ and GPLv3+ | A dynamic, any to any, pixel format conversion library
bacula | AGPLv3 with exceptions | Cross platform network backup for Linux, Unix, Mac and Windows
baobab | GPLv2+ and GFDL | A graphical directory tree analyzer
batik | ASL 2.0 and W3C | Scalable Vector Graphics for Java
bcc | ASL 2.0 | BPF Compiler Collection (BCC)
bea-stax | ASL 1.1 and ASL 2.0 | Streaming API for XML
bind-dyndb-ldap | GPLv2+ | LDAP back-end plug-in for BIND
bind9.16 | MPLv2.0 | The Berkeley Internet Name Domain (BIND) DNS (Domain Name System) server
bison | GPLv3+ | A GNU general-purpose parser generator
bitmap-fonts | GPLv2 and MIT and Lucida | Selected set of bitmap fonts
bogofilter | GPLv2 | Fast anti-spam filtering by Bayesian statistical analysis
boost | Boost and MIT and Python | The free peer-reviewed portable C++ source libraries
bpftrace | ASL 2.0 | High-level tracing language for Linux eBPF
bpg-fonts | GPL+ with exceptions | Georgian Unicode fonts
brasero | GPLv3+ | Gnome CD/DVD burning application
brltty | LGPLv2+ | Braille display driver for Linux/Unix
buildah | ASL 2.0 | A command line tool used for creating OCI Images
buildah | ASL 2.0 | A command line tool used for creating OCI Images
buildah | ASL 2.0 | A command line tool used for creating OCI Images
buildah | ASL 2.0 | A command line tool used for creating OCI Images
buildah | ASL 2.0 | A command line tool used for creating OCI Images
byacc | Public Domain | Berkeley Yacc, a parser generator
byteman | LGPLv2+ | Java agent-based bytecode injection tool
c2esp | GPLv2+ | CUPS driver for Kodak AiO printers
cairomm | LGPLv2+ | C++ API for the cairo graphics library
cdi-api | ASL 2.0 | CDI API
cdi-api | ASL 2.0 | CDI API
cdparanoia | GPLv2 and LGPLv2 | Compact Disc Digital Audio (CDDA) extraction tool (or ripper)
cdrdao | GPLv2+ | Writes audio CD-Rs in disk-at-once (DAO) mode
cdrkit | GPLv2 | A collection of CD/DVD utilities
celt051 | BSD | An audio codec for use in low-delay speech and audio communication
ceph | LGPL-2.1 and CC-BY-SA-1.0 and GPL-2.0 and BSL-1.0 and BSD-3-Clause and MIT | User space components of the Ceph file system
certmonger | GPLv3+ | Certificate status monitor and PKI enrollment client
cgdcbxd | GPLv2 | DCB network priority management daemon
chan | ASL 2.0 | Pure C implementation of Go channels
check | LGPLv2+ | A unit test framework for C
cheese | GPLv2+ | Application for taking pictures and movies from a webcam
chrome-gnome-shell | GPLv3+ | Support for managing GNOME Shell Extensions through web browsers
cim-schema | DMTF | Common Information Model (CIM) Schema
cjose | MIT | C library implementing the Javascript Object Signing and Encryption (JOSE)
clang | NCSA | A C language family front-end for LLVM
cldr-emoji-annotation | Unicode | Emoji annotation files in CLDR
clevis | GPLv3+ | Automated decryption framework
cloud-init | Dual-licesed GPLv3 or Apache 2.0 | Cloud instance init scripts
cloud-utils-growpart | GPLv3 | Script for growing a partition
clucene | LGPLv2+ or ASL 2.0 | A C++ port of Lucene
clutter | LGPLv2+ | Open Source software library for creating rich graphical user interfaces
clutter-gst2 | LGPLv2+ | GStreamer integration for Clutter
clutter-gst3 | LGPLv2+ | GStreamer integration library for Clutter
clutter-gtk | LGPLv2+ | A basic GTK clutter widget
cmake | BSD and MIT and zlib | Cross-platform make system
cmocka | ASL 2.0 | An elegant unit testing framework for C with support for mock objects
cockpit-appstream | LGPL-2.1-or-later | Web Console for Linux servers
cockpit-composer | MIT | Composer GUI for use with Cockpit
cockpit-podman | LGPLv2+ | Cockpit component for Podman containers
cockpit-podman | LGPLv2+ | Cockpit component for Podman containers
cockpit-podman | LGPLv2+ | Cockpit component for Podman containers
cockpit-podman | LGPL-2.1-or-later | Cockpit component for Podman containers
cockpit-session-recording | LGPL-2.1-or-later | Cockpit Session Recording
codemodel | CDDL-1.1 or GPLv2 with exceptions | Java library for code generators
cogl | LGPLv2+ | A library for using 3D graphics hardware to draw pretty pictures
color-filesystem | Public Domain | Color filesystem layout
colord | GPLv2+ and LGPLv2+ | Color daemon
colord-gtk | LGPLv2+ | GTK support library for colord
compat-exiv2-026 | GPLv2+ | Compatibility package with the exiv2 library in version 0.26
compat-guile18 | LGPLv2+ | A GNU implementation of Scheme for application extensibility
compat-libgfortran-48 | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | Compatibility Fortran runtime library version 4.8.5
compat-libtiff3 | libtiff | Compatibility package for libtiff 3
compat-openssl10 | OpenSSL | Compatibility version of the OpenSSL library
compiler-rt | NCSA or MIT | LLVM "compiler-rt" runtime libraries
conmon | ASL 2.0 | OCI container runtime monitor
conmon | ASL 2.0 | OCI container runtime monitor
conmon | ASL 2.0 | OCI container runtime monitor
conmon | ASL 2.0 | OCI container runtime monitor
container-exception-logger | GPLv2+ | Logging from a container to a host
container-selinux | GPLv2 | SELinux policies for container runtimes
container-selinux | GPLv2 | SELinux policies for container runtimes
container-selinux | GPLv2 | SELinux policies for container runtimes
container-selinux | GPLv2 | SELinux policies for container runtimes
container-selinux | GPLv2 | SELinux policies for container runtimes
container-selinux | GPLv2 | SELinux policies for container runtimes
containernetworking-plugins | ASL 2.0 | Some CNI network plugins, maintained by the containernetworking team.
containernetworking-plugins | ASL 2.0 | CNI network plugins
containernetworking-plugins | ASL 2.0 | CNI network plugins
containernetworking-plugins | ASL 2.0 | CNI network plugins
containernetworking-plugins | ASL 2.0 | CNI network plugins
containers-common | ASL 2.0 | Common configuration and documentation for containers
containers-common | ASL 2.0 | Common configuration and documentation for containers
convmv | GPLv2 or GPLv3 | Convert filename encodings
copy-jdk-configs | BSD | JDKs configuration files copier
coreos-installer | ASL 2.0 | Installer for Fedora CoreOS and RHEL CoreOS
corosync | BSD | The Corosync Cluster Engine and Application Programming Interfaces
cppcheck | GPLv3+ | Tool for static C/C++ code analysis
cppunit | LGPLv2+ | C++ unit testing framework
crash | GPLv3 | Kernel analysis utility for live systems, netdump, diskdump, kdump, LKCD or mcore dumpfiles
crash-gcore-command | GPLv2 | Gcore extension module for the crash utility
crash-ptdump-command | GPLv2 | ptdump extension module for the crash utility
crash-trace-command | GPLv2 | Trace extension module for the crash utility
createrepo_c | GPLv2+ | Creates a common metadata repository
criu | GPLv2 | Tool for Checkpoint/Restore in User-space
criu | GPLv2 | Tool for Checkpoint/Restore in User-space
criu | GPLv2 | Tool for Checkpoint/Restore in User-space
criu | GPLv2 | Tool for Checkpoint/Restore in User-space
criu | GPLv2 | Tool for Checkpoint/Restore in User-space
crun | GPLv2+ | OCI runtime written in C
crun | GPLv2+ | OCI runtime written in C
crun | GPLv2+ | OCI runtime written in C
cscope | BSD and GPLv2+ | C source code tree search and browse tool
ctags | GPLv2+ and LGPLv2+ and Public Domain | A C programming language indexing and/or cross-reference tool
culmus-fonts | GPLv2 | Fonts for Hebrew from Culmus project
cups-filters | GPLv2 and GPLv2+ and GPLv3 and GPLv3+ and LGPLv2+ and MIT and BSD with advertising | OpenPrinting CUPS filters and backends
cups-filters | GPLv2 and GPLv2+ and GPLv3 and GPLv3+ and LGPLv2+ and MIT and BSD with advertising | OpenPrinting CUPS filters and backends
cups-pk-helper | GPLv2+ | A helper that makes system-config-printer use PolicyKit
custodia | GPLv3+ | A service to manage, retrieve and store secrets for other processes
cyrus-imapd | BSD | A high-performance email, contacts and calendar server
dblatex | GPLv2+ and GPLv2 and LPPL and DMIT and Public Domain | DocBook to LaTeX/ConTeXt Publishing
dbus-c++ | LGPLv2+ | Native C++ bindings for D-Bus
dconf | LGPLv2+ and GPLv2+ and GPLv3+ | A configuration system
dconf-editor | LGPLv2+ | Configuration editor for dconf
dcraw | GPLv2+ | Tool for decoding raw image data from digital cameras
dejagnu | GPLv3+ | A front end for testing other programs
delve | MIT | A debugger for the Go programming language
delve | MIT | A debugger for the Go programming language
desktop-file-utils | GPLv2+ | Utilities for manipulating .desktop files
devhelp | GPLv3+ | API documentation browser
dialog | LGPLv2 | A utility for creating TTY dialog boxes
diffstat | MIT | A utility which provides statistics based on the output of diff
directory-maven-plugin | ASL 2.0 | Establish locations for files in multi-module builds
disruptor | ASL 2.0 | Concurrent Programming Framework
dleyna-connector-dbus | LGPLv2 | D-Bus connector for dLeyna services
dleyna-core | LGPLv2 | Utilities for higher level dLeyna libraries
dleyna-renderer | LGPLv2 | Service for interacting with Digital Media Renderers
dleyna-server | LGPLv2 | Service for interacting with Digital Media Servers
dnsmasq | GPLv2 or GPLv3 | A lightweight DHCP/caching DNS server
dnssec-trigger | BSD | Tool for dynamic reconfiguration of validating resolver Unbound
docbook-dtds | Copyright only | SGML and XML document type definitions for DocBook
docbook-style-dsssl | DMIT | Norman Walsh's modular stylesheets for DocBook
docbook-style-xsl | DMIT | Norman Walsh's XSL stylesheets for DocBook XML
docbook-utils | GPLv2+ | Shell scripts for managing DocBook documents
docbook2X | MIT | Convert docbook into man and Texinfo
docbook5-schemas | Freely redistributable without restriction | Norman Walsh's schemas (DTD, Relax NG, W3C schema) for Docbook 5.X
dotconf | LGPLv2 | Libraries to parse configuration files
dotnet | MIT and ASL 2.0 and BSD | .NET Core CLI tools and runtime
dotnet-build-reference-packages | MIT | Reference packages needed by the .NET Core SDK build
dotnet3.0 | MIT and ASL 2.0 and BSD | .NET Core CLI tools and runtime
dotnet3.1 | MIT and ASL 2.0 and BSD | .NET Core CLI tools and runtime
dotnet5.0 | MIT and ASL 2.0 and BSD and LGPLv2+ and CC-BY and CC0 and MS-PL and EPL-1.0 and GPL+ and GPLv2 and ISC and OFL and zlib | .NET Runtime and SDK
dotnet6.0 | MIT and ASL 2.0 and BSD and LGPLv2+ and CC-BY and CC0 and MS-PL and EPL-1.0 and GPL+ and GPLv2 and ISC and OFL and zlib | .NET Runtime and SDK
dotnet6.0 | MIT and ASL 2.0 and BSD and LGPLv2+ and CC-BY and CC0 and MS-PL and EPL-1.0 and GPL+ and GPLv2 and ISC and OFL and zlib | .NET Runtime and SDK
dovecot | MIT and LGPLv2 | Secure imap and pop3 server
doxygen | GPL+ | A documentation system for C/C++
dpdk | BSD and LGPLv2 and GPLv2 | Set of libraries and drivers for fast packet processing
driverctl | LGPLv2 | Device driver control utility
dropwatch | GPLv2+ | Kernel dropped packet monitor
drpm | LGPLv2+ and BSD | A library for making, reading and applying deltarpm packages
dtc | GPLv2+ | Device Tree Compiler
dvd+rw-tools | GPLv2 | Toolchain to master DVD+RW/+R media
dwarves | GPLv2 | Debugging Information Manipulation Tools (pahole & friends)
dwz | GPLv2+ and GPLv3+ | DWARF optimization and duplicate removal tool
dyninst | LGPLv2+ | An API for Run-time Code Generation
eclipse | EPL-2.0 | An open, extensible IDE
eclipse-ecf | EPL-2.0 and ASL 2.0 and BSD | Eclipse Communication Framework (ECF) Eclipse plug-in
eclipse-emf | EPL-2.0 | EMF and XSD Eclipse plug-ins
edk2 | BSD-2-Clause-Patent and OpenSSL and MIT | UEFI firmware for 64-bit virtual machines
ee4j-parent | EPL-2.0 or GPLv2 with exceptions | Parent POM file for Eclipse Enterprise for Java projects
egl-wayland | MIT | Wayland EGL External Platform library
eglexternalplatform | MIT | EGL External Platform Interface headers
eigen3 | MPLv2.0 and LGPLv2+ and BSD | A lightweight C++ template library for vector and matrix math
elinks | GPLv2 | A text-mode Web browser
enca | GPLv2 | Character set analyzer and detector
enchant | LGPLv2+ | An Enchanting Spell Checking Library
enchant2 | LGPLv2+ | An Enchanting Spell Checking Library
enscript | GPLv3+ and LGPLv2+ and GPLv2+ | A plain ASCII to PostScript converter
eog | GPLv2+ and GFDL | Eye of GNOME image viewer
esc | GPL+ | Enterprise Security Client Smart Card Client
espeak-ng | GPLv3+ | eSpeak NG Text-to-Speech
eth-tools | BSD | Intel Ethernet Fabric Suite basic tools and libraries for fabric management
evemu | GPLv3+ | Event Device Query and Emulation Program
evince | GPLv2+ and GPLv3+ and LGPLv2+ and MIT and Afmparse | Document viewer
evolution | GPLv2+ and GFDL | Mail and calendar client for GNOME
evolution-data-server | LGPLv2+ | Backend data server for Evolution
evolution-ews | LGPLv2+ | Evolution extension for Exchange Web Services
evolution-mapi | LGPLv2+ | Evolution extension for MS Exchange 2007 servers
execstack | GPLv2+ | Utility to set/clear/query executable stack bit
exempi | BSD | Library for easy parsing of XMP metadata
exiv2 | GPLv2+ | Exif and Iptc metadata manipulation library
fabtests | BSD and (BSD or GPLv2) and MIT | Test suite for libfabric API
fapolicyd | GPLv3+ | Application Whitelisting Daemon
farstream02 | LGPLv2+ and GPLv2+ | Libraries for videoconferencing
fdk-aac-free | FDK-AAC | Third-Party Modified Version of the Fraunhofer FDK AAC Codec Library for Android
felix-gogo-command | ASL 2.0 | Apache Felix Gogo command line shell for OSGi
felix-gogo-runtime | ASL 2.0 and MIT | Apache Felix Gogo command line shell for OSGi
felix-gogo-shell | ASL 2.0 | Apache Felix Gogo command line shell for OSGi
felix-scr | ASL 2.0 | Apache Felix Service Component Runtime (SCR)
fence-agents | GPLv2+ and LGPLv2+ | Set of unified programs capable of host isolation ("fencing")
fence-agents | GPLv2+ and LGPLv2+ | Set of unified programs capable of host isolation ("fencing")
fence-virt | GPLv2+ | A pluggable fencing framework for virtual machines
fetchmail | GPL+ and Public Domain | A remote mail retrieval and forwarding utility
fftw | GPLv2+ | A Fast Fourier Transform library
fido-device-onboard | BSD | An implementation of the FIDO Device Onboard Specification written in rust
file-roller | GPLv2+ | Tool for viewing and creating archives
fio | GPLv2 | Multithreaded IO generation tool
firefox | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Firefox Web browser
firefox | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Firefox Web browser
firefox | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Firefox Web browser
flac | BSD and GPLv2+ and GFDL | An encoder/decoder for the Free Lossless Audio Codec
flatpak | LGPLv2+ | Application deployment framework for desktop apps
flatpak-builder | LGPLv2+ and GPLv2+ | Tool to build flatpaks from source
flatpak-xdg-utils | LGPLv2+ | Command-line tools for use inside Flatpak sandboxes
flex | BSD and LGPLv2+ | A tool for generating scanners (text pattern recognizers)
flite | MIT | Small, fast speech synthesis engine (text-to-speech)
fltk | LGPLv2+ with exceptions | C++ user interface toolkit
flute | W3C and LGPLv2+ | Java CSS parser using SAC
fontawesome-fonts | OFL | Iconic font set
fontawesome-fonts | OFL | Iconic font set
fontforge | GPLv3+ | Outline and bitmap font editor
fonts-tweak-tool | LGPLv3+ | Tool for customizing fonts per language
foomatic | GPLv2+ | Tools for using the foomatic database of printers and printer drivers
foomatic-db | GPLv2+ | Database of printers and printer drivers
fprintd | GPLv2+ | D-Bus service for Fingerprint reader access
freeglut | MIT | A freely licensed alternative to the GLUT library
freeradius | GPLv2+ and LGPLv2+ | High-performance and highly configurable free RADIUS server
freerdp | ASL 2.0 | Free implementation of the Remote Desktop Protocol (RDP)
frei0r-plugins | GPLv2+ | Frei0r - a minimalist plugin API for video effects
fribidi | LGPLv2+ and UCD | Library implementing the Unicode Bidirectional Algorithm
frr | GPLv2+ | Routing daemon
fstrm | MIT | Frame Streams implementation in C
ftp | BSD with advertising | The standard UNIX FTP (File Transfer Protocol) client
fuse-overlayfs | GPLv3+ | FUSE overlay+shiftfs implementation for rootless containers
fuse-overlayfs | GPLv3+ | FUSE overlay+shiftfs implementation for rootless containers
fuse-overlayfs | GPLv3+ | FUSE overlay+shiftfs implementation for rootless containers
fuse-overlayfs | GPLv3+ | FUSE overlay+shiftfs implementation for rootless containers
fuse-overlayfs | GPLv3+ | FUSE overlay+shiftfs implementation for rootless containers
fuse-sshfs | GPLv2 | FUSE-Filesystem to access remote filesystems via SSH
galera | GPLv2 | Synchronous multi-master wsrep provider (replication engine)
galera | GPLv2 | Synchronous multi-master wsrep provider (replication engine)
gavl | GPLv3+ | A library for handling uncompressed audio and video data
gc | BSD | A garbage collector for C and C++
gcc-toolset-10 | GPLv2+ | Package that installs gcc-toolset-10
gcc-toolset-10-annobin | GPLv3+ | Annotate and examine compiled binary files
gcc-toolset-10-binutils | GPLv3+ | A GNU collection of binary utilities
gcc-toolset-10-dwz | GPLv2+ and GPLv3+ | DWARF optimization and duplicate removal tool
gcc-toolset-10-dyninst | LGPLv2+ | An API for Run-time Code Generation
gcc-toolset-10-elfutils | GPLv3+ and (GPLv2+ or LGPLv3+) and GFDL | A collection of utilities and DSOs to handle ELF files and DWARF data
gcc-toolset-10-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | Various compilers (C, C++, Objective-C, ...)
gcc-toolset-10-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-10-ltrace | GPLv2+ | Tracks runtime library calls from dynamically linked executables
gcc-toolset-10-make | GPLv3+ | A GNU tool which simplifies the build process for users
gcc-toolset-10-strace | LGPL-2.1+ and GPL-2.0+ | Tracks and displays system calls associated with a running process
gcc-toolset-10-systemtap | GPLv2+ | Programmable system-wide instrumentation system
gcc-toolset-10-valgrind | GPLv2+ | Tool for finding memory management bugs in programs
gcc-toolset-11 | GPLv2+ | Package that installs gcc-toolset-11
gcc-toolset-11-annobin | GPLv3+ | Annotate and examine compiled binary files
gcc-toolset-11-binutils | GPLv3+ | A GNU collection of binary utilities
gcc-toolset-11-dwz | GPLv2+ and GPLv3+ | DWARF optimization and duplicate removal tool
gcc-toolset-11-dyninst | LGPLv2+ | An API for Run-time Code Generation
gcc-toolset-11-elfutils | GPLv3+ and (GPLv2+ or LGPLv3+) and GFDL | A collection of utilities and DSOs to handle ELF files and DWARF data
gcc-toolset-11-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | GCC version 11
gcc-toolset-11-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-11-ltrace | GPLv2+ | Tracks runtime library calls from dynamically linked executables
gcc-toolset-11-make | GPLv3+ | A GNU tool which simplifies the build process for users
gcc-toolset-11-strace | LGPL-2.1+ and GPL-2.0+ | Tracks and displays system calls associated with a running process
gcc-toolset-11-systemtap | GPLv2+ | Programmable system-wide instrumentation system
gcc-toolset-11-valgrind | GPLv2+ | Tool for finding memory management bugs in programs
gcc-toolset-12 | GPLv2+ | Package that installs gcc-toolset-12
gcc-toolset-12-binutils | GPLv3+ | A GNU collection of binary utilities
gcc-toolset-12-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | GCC version 12
gcc-toolset-9 | GPLv2+ | Package that installs gcc-toolset-9
gcc-toolset-9-annobin | GPLv3+ | Binary annotation plugin for GCC
gcc-toolset-9-binutils | GPLv3+ | A GNU collection of binary utilities
gcc-toolset-9-dwz | GPLv2+ and GPLv3+ | DWARF optimization and duplicate removal tool
gcc-toolset-9-dyninst | LGPLv2+ | An API for Run-time Code Generation
gcc-toolset-9-elfutils | GPLv3+ and (GPLv2+ or LGPLv3+) | A collection of utilities and DSOs to handle ELF files and DWARF data
gcc-toolset-9-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD | GCC version 9
gcc-toolset-9-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-9-ltrace | GPLv2+ | Tracks runtime library calls from dynamically linked executables
gcc-toolset-9-make | GPLv3+ | A GNU tool which simplifies the build process for users
gcc-toolset-9-strace | LGPL-2.1+ and GPL-2.0+ | Tracks and displays system calls associated with a running process
gcc-toolset-9-systemtap | GPLv2+ | Programmable system-wide instrumentation system
gcc-toolset-9-valgrind | GPLv2+ | Tool for finding memory management bugs in programs
gcr | LGPLv2+ | A library for bits of crypto UI and parsing
gd | MIT | A graphics library for quick creation of PNG or JPEG images
gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A stub package for GNU source-level debugger
gdk-pixbuf2-xlib | LGPLv2+ | Deprecated Xlib integration for gdk-pixbuf2
gdm | GPLv2+ | The GNOME Display Manager
gedit | GPLv2+ and GFDL | Text editor for the GNOME desktop
gedit-plugins | GPLv2+ | Plugins for gedit
gegl | LGPLv3+ and GPLv3+ | A graph based image processing framework
gegl04 | LGPLv3+ | Graph based image processing framework
geoclue2 | GPLv2+ | Geolocation service
geocode-glib | LGPLv2+ | Geocoding helper library
geoipupdate | GPLv2 | Update GeoIP2 and GeoIP Legacy binary databases from MaxMind
geolite2 | CC-BY-SA | Free IP geolocation databases
geronimo-annotation | ASL 2.0 | Java EE: Annotation API v1.3
geronimo-annotation | ASL 2.0 | Java EE: Annotation API v1.3
gfbgraph | LGPLv2+ | GLib/GObject wrapper for the Facebook Graph API
gflags | BSD | Library for commandline flag processing
ghc-srpm-macros | GPLv2+ | RPM macros for building Haskell source packages
ghostscript | AGPLv3+ | Interpreter for PostScript language & PDF
giflib | MIT | A library and utilities for processing GIFs
gimp | GPLv3+ and GPLv3 | GNU Image Manipulation Program
git | GPLv2 | Fast Version Control System
git-lfs | MIT | Git extension for versioning large files
gjs | MIT and (MPLv1.1 or GPLv2+ or LGPLv2+) and MPLv2.0 and MPLv1.1 and BSD and GPLv2+ and GPLv3+ and LGPLv2+ and AFL and ASL 2.0 | Javascript Bindings for GNOME
gl-manpages | MIT and Open Publication | OpenGL manpages
glade | GPLv2+ and LGPLv2+ | User Interface Designer for GTK+
glassfish-annotation-api | CDDL-1.1 or GPLv2 with exceptions | Common Annotations API Specification (JSR 250)
glassfish-el | CDDL-1.1 or GPLv2 with exceptions | J2EE Expression Language Implementation
glassfish-el | CDDL-1.1 or GPLv2 with exceptions | J2EE Expression Language Implementation
glassfish-fastinfoset | ASL 2.0 | Fast Infoset
glassfish-jaxb | CDDL-1.1 and GPLv2 with exceptions | JAXB Reference Implementation
glassfish-jaxb-api | CDDL or GPLv2 with exception | Java Architecture for XML Binding
glassfish-jsp | (CDDL-1.1 or GPLv2 with exceptions) and ASL 2.0 | Glassfish J2EE JSP API implementation
glassfish-jsp-api | (CDDL-1.1 or GPLv2 with exceptions) and ASL 2.0 | Glassfish J2EE JSP API specification
glassfish-servlet-api | (CDDL or GPLv2 with exceptions) and ASL 2.0 | Java Servlet API
glew | BSD and MIT | The OpenGL Extension Wrangler Library
glibmm24 | LGPLv2+ | C++ interface for the GLib library
glm | MIT | C++ mathematics library for graphics programming
glog | BSD | A C++ application logging library
gnome-abrt | GPLv2+ | A utility for viewing problems that have occurred with the system
gnome-autoar | LGPLv2+ | Archive library
gnome-backgrounds | GPLv2 | Desktop backgrounds packaged with the GNOME desktop
gnome-bluetooth | GPLv2+ | Bluetooth graphical utilities
gnome-boxes | LGPLv2+ | A simple GNOME 3 application to access remote or virtual systems
gnome-calculator | GPLv3+ | A desktop calculator
gnome-characters | BSD and GPLv2+ | Character map application for GNOME
gnome-color-manager | GPLv2+ | Color management tools for GNOME
gnome-common | GPLv2+ | Useful things common to building GNOME packages from scratch
gnome-control-center | GPLv2+ and CC-BY-SA | Utilities to configure the GNOME desktop
gnome-desktop3 | GPLv2+ and LGPLv2+ | Library with common API for various GNOME modules
gnome-disk-utility | GPLv2+ | Disks
gnome-font-viewer | GPLv2+ | Utility for previewing fonts for GNOME
gnome-getting-started-docs | CC-BY-SA | Help a new user get started in GNOME
gnome-initial-setup | GPLv2+ | Bootstrapping your OS
gnome-keyring | GPLv2+ and LGPLv2+ | Framework for managing passwords and other secrets
gnome-logs | GPLv3+ | Log viewer for the systemd journal
gnome-menus | LGPLv2+ | A menu system for the GNOME project
gnome-online-accounts | LGPLv2+ | Single sign-on framework for GNOME
gnome-online-miners | GPLv2+ and LGPLv2+ and MIT | Crawls through your online content
gnome-photos | GPLv3+ and LGPLv2+ | Access, organize and share your photos on GNOME
gnome-remote-desktop | GPLv2+ | GNOME Remote Desktop screen share service
gnome-screenshot | GPLv2+ | A screenshot utility for GNOME
gnome-session | GPLv2+ | GNOME session manager
gnome-settings-daemon | GPLv2+ | The daemon sharing settings from GNOME to GTK+/KDE applications
gnome-shell | GPLv2+ | Window management and application launching for GNOME
gnome-shell | GPLv2+ | Window management and application launching for GNOME
gnome-shell-extensions | GPLv2+ | Modify and extend GNOME Shell functionality and behavior
gnome-shell-extensions | GPLv2+ | Modify and extend GNOME Shell functionality and behavior
gnome-software | GPLv2+ | A software center for GNOME
gnome-system-monitor | GPLv2+ | Process and resource monitor
gnome-terminal | GPLv3+ and GFDL and LGPLv2+ | Terminal emulator for GNOME
gnome-themes-extra | LGPLv2+ | GNOME Extra Themes
gnome-themes-standard | LGPLv2+ | Standard themes for GNOME applications
gnome-tweaks | GPLv3 and CC0 | Customize advanced GNOME 3 options
gnome-user-docs | CC-BY-SA | GNOME User Documentation
gnome-video-effects | GPLv2 | Collection of GStreamer video effects
gnu-efi | BSD | Development Libraries and headers for EFI
gnu-free-fonts | GPLv3+ with exceptions | Free UCS Outline Fonts
gnuplot | gnuplot and MIT | A program for plotting mathematical expressions and data
go-compilers | GPLv3+ | Go language compilers for various architectures
go-srpm-macros | GPLv3+ | RPM macros for building Golang packages for various architectures
go-toolset | BSD and Public Domain | Package that installs go-toolset
go-toolset | BSD and Public Domain | Package that installs go-toolset
golang | BSD and Public Domain | The Go Programming Language
golang | BSD and Public Domain | The Go Programming Language
gom | LGPLv2+ | GObject to SQLite object mapper library
google-crosextra-caladea-fonts | ASL 2.0 | Serif font metric-compatible with Cambria font
google-crosextra-carlito-fonts | OFL | Sans-serif font metric-compatible with Calibri font
google-droid-fonts | ASL 2.0 | General-purpose fonts released by Google as part of Android
google-gson | ASL 2.0 | Java lib for conversion of Java objects into JSON representation
google-guice | ASL 2.0 | Lightweight dependency injection framework for Java 5 and above
google-guice | ASL 2.0 | Lightweight dependency injection framework for Java 5 and above
google-noto-cjk-fonts | OFL | Google Noto Sans CJK Fonts
google-noto-emoji-fonts | OFL and ASL 2.0 | Google “Noto Emoji” Black-and-White emoji font
google-noto-fonts | OFL | Hinted and Non Hinted OpenType fonts for Unicode scripts
google-roboto-slab-fonts | ASL 2.0 | Google Roboto Slab fonts
gperf | GPLv3+ | A perfect hash function generator
gpm | GPLv2 and GPLv2+ with exceptions and GPLv3+ and Verbatim and Copyright only | A mouse server for the Linux console
grafana | AGPLv3 | Metrics dashboard and graph editor
grafana-pcp | ASL 2.0 | Performance Co-Pilot Grafana Plugin
graphene | MIT | Thin layer of types for graphic libraries
graphviz | EPL-1.0 | Graph Visualization Tools
greenboot | LGPLv2+ | Generic Health Check Framework for systemd
grilo | LGPLv2+ | Content discovery framework
grilo-plugins | LGPLv2+ | Plugins for the Grilo framework
gsl | GPLv3+ | The GNU Scientific Library for numerical analysis
gsm | MIT | Shared libraries for GSM speech compressor
gsound | LGPLv2 | Small gobject library for playing system sounds
gspell | LGPLv2+ | Spell-checking library for GTK+
gssdp | LGPLv2+ | Resource discovery and announcement over SSDP
gssntlmssp | LGPLv3+ | GSSAPI NTLMSSP Mechanism
gstreamer1 | LGPLv2+ | GStreamer streaming media framework runtime
gstreamer1-plugins-bad-free | LGPLv2+ and LGPLv2 | GStreamer streaming media framework "bad" plugins
gstreamer1-plugins-base | LGPLv2+ | GStreamer streaming media framework base plugins
gstreamer1-plugins-good | LGPLv2+ | GStreamer plugins with good code and licensing
gstreamer1-plugins-ugly-free | LGPLv2+ and LGPLv2 | GStreamer streaming media framework "ugly" plugins
gtest | BSD and ASL2.0 | Google C++ testing framework
gtk-doc | GPLv2+ and GFDL | API documentation generation tool for GTK+ and GNOME
gtk-vnc | LGPLv2+ | A GTK widget for VNC clients
gtk2 | LGPLv2+ | GTK+ graphical user interface library
gtk3 | LGPLv2+ | GTK+ graphical user interface library
gtk4 | LGPLv2+ | GTK graphical user interface library
gtkmm24 | LGPLv2+ | C++ interface for GTK2 (a GUI library for X)
gtkmm30 | LGPLv2+ | C++ interface for the GTK+ library
gtksourceview3 | LGPLv2+ | A library for viewing source files
gtksourceview4 | LGPLv2+ | Source code editing widget
gtkspell | GPLv2+ | On-the-fly spell checking for GtkTextView widgets
gtkspell3 | GPLv2+ | On-the-fly spell checking for GtkTextView widgets
guava | ASL 2.0 and CC0 | Google Core Libraries for Java
guava20 | ASL 2.0 and CC0 | Google Core Libraries for Java
gubbi-fonts | GPLv3+ with exceptions | Free Kannada Opentype serif font
guile | LGPLv3+ | A GNU implementation of Scheme for application extensibility
gupnp | LGPLv2+ | A framework for creating UPnP devices & control points
gupnp-av | LGPLv2+ | A collection of helpers for building UPnP AV applications
gupnp-dlna | LGPLv2+ | A collection of helpers for building UPnP AV applications
gupnp-igd | LGPLv2+ | Library to handle UPnP IGD port mapping
gutenprint | GPLv2+ | Printer Drivers Package
gvfs | GPLv3 and LGPLv2+ and BSD and MPLv2.0 | Backends for the gio framework in GLib
hamcrest | BSD | Library of matchers for building test expressions
haproxy | GPLv2+ | HAProxy reverse proxy for high availability environments
hawtjni | ASL 2.0 and EPL and BSD | Code generator that produces the JNI code
hawtjni | ASL 2.0 and EPL-1.0 and BSD | Code generator that produces the JNI code
help2man | GPLv3+ | Create simple man pages from --help output
hesiod | MIT | Shared libraries for querying the Hesiod naming service
hexchat | GPLv2+ | A popular and easy to use graphical IRC (chat) client
hexedit | GPLv2+ | A hexadecimal file viewer and editor
hicolor-icon-theme | GPLv2+ | Basic requirement for icon themes
highlight | GPLv3 | Universal source code to formatted text converter
hivex | LGPLv2 | Read and write Windows Registry binary hive files
hostapd | BSD | IEEE 802.11 AP, IEEE 802.1X/WPA/WPA2/EAP/RADIUS Authenticator
hplip | GPLv2+ and MIT and BSD and IJG and Public Domain and GPLv2+ with exceptions and ISC | HP Linux Imaging and Printing Project
hspell | AGPLv3 | A Hebrew spell checker
http-parser | MIT | HTTP request/response parser for C
httpcomponents-client | ASL 2.0 | HTTP agent implementation based on httpcomponents HttpCore
httpcomponents-client | ASL 2.0 | HTTP agent implementation based on httpcomponents HttpCore
httpcomponents-core | ASL 2.0 | Set of low level Java HTTP transport components for HTTP services
httpcomponents-core | ASL 2.0 | Set of low level Java HTTP transport components for HTTP services
httpd | ASL 2.0 | Apache HTTP Server
httpd | ASL 2.0 | Apache HTTP Server
hunspell | LGPLv2+ or GPLv2+ or MPLv1.1 | A spell checker and morphological analyzer library
hunspell-af | LGPLv2+ | Afrikaans hunspell dictionary
hunspell-ak | LGPLv3 | Akan hunspell dictionaries
hunspell-am | GPL+ | Amharic hunspell dictionaries
hunspell-ar | GPLv2 or LGPLv2 or MPLv1.1 | Arabic hunspell dictionaries
hunspell-as | GPLv2+ or LGPLv2+ or MPLv1.1 | Assamese hunspell dictionaries
hunspell-ast | GPL+ or LGPLv2+ | Asturian hunspell dictionaries
hunspell-az | GPLv2+ | Azerbaijani hunspell dictionaries
hunspell-be | GPL+ and LGPLv2+ | Belarusian hunspell dictionaries
hunspell-ber | GPL+ or LGPLv2+ or MPLv1.1 | Amazigh hunspell dictionaries
hunspell-bg | GPLv2+ or LGPLv2+ or MPLv1.1 | Bulgarian hunspell dictionaries
hunspell-bn | GPLv2+ | Bengali hunspell dictionaries
hunspell-br | LGPLv2+ | Breton hunspell dictionaries
hunspell-ca | GPLv2+ | Catalan hunspell dictionaries
hunspell-cop | GPLv3+ | Coptic hunspell dictionaries
hunspell-csb | GPLv2+ | Kashubian hunspell dictionaries
hunspell-cv | GPLv3+ or LGPLv3+ or MPLv1.1 | Chuvash hunspell dictionaries
hunspell-cy | GPL+ | Welsh hunspell dictionaries
hunspell-da | GPLv2+ | Danish hunspell dictionaries
hunspell-de | GPLv2 or GPLv3 | German hunspell dictionaries
hunspell-dsb | GPLv2+ | Lower Sorbian hunspell dictionaries
hunspell-el | GPLv2+ or LGPLv2+ or MPLv1.1 | Greek hunspell dictionaries
hunspell-en | LGPLv2+ and LGPLv2 and BSD | English hunspell dictionaries
hunspell-eo | GPLv2+ | Esperanto hunspell dictionaries
hunspell-es | LGPLv3+ or GPLv3+ or MPLv1.1 | Spanish hunspell dictionaries
hunspell-et | LGPLv2+ and LPPL | Estonian hunspell dictionaries
hunspell-eu | LGPLv3+ | Basque hunspell dictionaries
hunspell-fa | GPLv2+ | Farsi hunspell dictionaries
hunspell-fj | LGPLv2+ or GPLv2+ or MPLv1.1 | Fijian hunspell dictionaries
hunspell-fo | GPLv2+ | Faroese hunspell dictionaries
hunspell-fr | MPLv2.0 | French hunspell dictionaries
hunspell-fur | GPLv2+ | Friulian hunspell dictionaries
hunspell-fy | LGPLv2+ | Frisian hunspell dictionaries
hunspell-ga | GPLv2+ | Irish hunspell dictionaries
hunspell-gd | GPLv2+ and GPLv3+ | Scots Gaelic hunspell dictionaries
hunspell-gl | GPLv2 | Galician hunspell dictionaries
hunspell-grc | GPL+ or LGPLv2+ | Ancient Greek hunspell dictionaries
hunspell-gu | GPL+ | Gujarati hunspell dictionaries
hunspell-gv | GPL+ | Manx hunspell dictionaries
hunspell-haw | GPLv2+ | Hawaiian hunspell dictionaries
hunspell-hi | GPLv2+ | Hindi hunspell dictionaries
hunspell-hil | GPLv2+ | Hiligaynon hunspell dictionaries
hunspell-hr | LGPLv2+ or SISSL | Croatian hunspell dictionaries
hunspell-hsb | GPLv2+ | Upper Sorbian hunspell dictionaries
hunspell-ht | GPLv3+ | Haitian Creole hunspell dictionaries
hunspell-hu | LGPLv2+ or GPLv2+ or MPLv1.1 | Hungarian hunspell dictionaries
hunspell-hy | GPLv2+ | Armenian hunspell dictionaries
hunspell-ia | LGPLv2+ | Interlingua hunspell dictionaries
hunspell-id | GPLv2 | Indonesian hunspell dictionaries
hunspell-is | GPLv2+ | Icelandic hunspell dictionaries
hunspell-it | GPLv3+ | Italian hunspell dictionaries
hunspell-kk | GPLv2+ or LGPLv2+ or MPLv1.1 | Kazakh hunspell dictionaries
hunspell-km | GPLv3 | Khmer hunspell dictionaries
hunspell-kn | GPLv2+ or LGPLv2+ or MPLv1.1 | Kannada hunspell dictionaries
hunspell-ko | MPLv1.1 or GPLv2 or LGPLv2 | Korean hunspell dictionaries
hunspell-ku | GPLv3 or LGPLv3 or MPLv1.1 | Kurdish hunspell dictionaries
hunspell-ky | GPLv2+ | Kirghiz hunspell dictionaries
hunspell-la | GPLv2+ | Latin hunspell dictionaries
hunspell-lb | EUPL 1.1 | Luxembourgish hunspell dictionaries
hunspell-ln | GPLv2+ | Lingala hunspell dictionaries
hunspell-lt | BSD | Lithuanian hunspell dictionaries
hunspell-mai | GPLv2+ or LGPLv2+ or MPLv1.1 | Maithili hunspell dictionaries
hunspell-mg | GPLv2+ | Malagasy hunspell dictionaries
hunspell-mi | GPLv3+ | Maori hunspell dictionaries
hunspell-mk | GPL+ | Macedonian hunspell dictionaries
hunspell-ml | GPLv3+ | Malayalam hunspell dictionaries
hunspell-mn | GPLv2 | Mongolian hunspell dictionaries
hunspell-mos | LGPLv3 | Mossi hunspell dictionaries
hunspell-mr | LGPLv2+ | Marathi hunspell dictionaries
hunspell-ms | GFDL and GPL+ | Malay hunspell dictionaries
hunspell-mt | LGPLv2+ | Maltese hunspell dictionaries
hunspell-nds | GPLv2+ | Lowlands Saxon hunspell dictionaries
hunspell-ne | LGPLv2 | Nepali hunspell dictionaries
hunspell-nl | BSD or CC-BY | Dutch hunspell dictionaries
hunspell-no | GPL+ | Norwegian hunspell dictionaries
hunspell-nr | LGPLv2+ | Southern Ndebele hunspell dictionaries
hunspell-nso | LGPLv2+ | Northern Sotho hunspell dictionaries
hunspell-ny | GPLv3+ | Chichewa hunspell dictionaries
hunspell-oc | GPLv3+ | Occitan hunspell dictionaries
hunspell-om | GPLv3+ | Oromo hunspell dictionaries
hunspell-or | GPLv2+ | Odia hunspell dictionaries
hunspell-pa | GPLv2+ | Punjabi hunspell dictionaries
hunspell-pl | LGPLv2+ or GPL+ or MPLv1.1 or ASL 2.0 or CC-BY-SA | Polish hunspell dictionaries
hunspell-pt | ((LGPLv3 or MPL) and LGPLv2) and (GPLv2 or LGPLv2 or MPLv1.1) | Portuguese hunspell dictionaries
hunspell-qu | AGPLv3 | Quechua Ecuador hunspell dictionaries
hunspell-quh | GPLv2+ | Quechua, South Bolivia hunspell dictionaries
hunspell-ro | GPLv2+ or LGPLv2+ or MPLv1.1 | Romanian hunspell dictionaries
hunspell-ru | BSD | Russian hunspell dictionaries
hunspell-rw | GPLv2+ | Kinyarwanda hunspell dictionaries
hunspell-sc | AGPLv3+ and GPLv2 | Sardinian hunspell dictionaries
hunspell-se | GPLv3 | Northern Saami hunspell dictionaries
hunspell-shs | GPLv2+ | Shuswap hunspell dictionaries
hunspell-si | GPLv2+ | Sinhala hunspell dictionaries
hunspell-sk | LGPLv2 or GPLv2 or MPLv1.1 | Slovak hunspell dictionaries
hunspell-sl | GPL+ or LGPLv2+ | Slovenian hunspell dictionaries
hunspell-smj | GPLv3 | Lule Saami hunspell dictionaries
hunspell-so | GPLv2+ | Somali hunspell dictionaries
hunspell-sq | GPLv2+ | Albanian hunspell dictionaries
hunspell-sr | LGPLv3 | Serbian hunspell dictionaries
hunspell-ss | LGPLv2+ | Swati hunspell dictionaries
hunspell-st | LGPLv2+ | Southern Sotho hunspell dictionaries
hunspell-sv | LGPLv3 | Swedish hunspell dictionaries
hunspell-sw | LGPLv2+ | Swahili hunspell dictionaries
hunspell-ta | GPLv2+ | Tamil hunspell dictionaries
hunspell-te | GPL+ | Telugu hunspell dictionaries
hunspell-tet | GPLv2+ | Tetum hunspell dictionaries
hunspell-th | LGPLv2+ | Thai hunspell dictionaries
hunspell-ti | GPL+ | Tigrigna hunspell dictionaries
hunspell-tk | GPLv2+ | Turkmen hunspell dictionaries
hunspell-tl | GPLv2+ | Tagalog hunspell dictionaries
hunspell-tn | GPLv3+ | Tswana hunspell dictionaries
hunspell-tpi | GPLv3+ | Tok Pisin hunspell dictionaries
hunspell-ts | LGPLv2+ | Tsonga hunspell dictionaries
hunspell-uk | GPLv2+ or LGPLv2+ or MPLv1.1 | Ukrainian hunspell dictionaries
hunspell-ur | LGPLv2+ | Urdu hunspell dictionaries
hunspell-uz | GPLv2+ | Uzbek hunspell dictionaries
hunspell-ve | LGPLv2+ | Venda hunspell dictionaries
hunspell-vi | GPLv2 | Vietnamese hunspell dictionaries
hunspell-wa | LGPLv2+ | Walloon hunspell dictionaries
hunspell-xh | LGPLv2+ | Xhosa hunspell dictionaries
hunspell-yi | LGPLv2+ or GPLv2+ or MPLv1.1 | Yiddish hunspell dictionaries
hunspell-zu | GPLv3+ | Zulu hunspell dictionaries
hyperv-daemons | GPLv2 | Hyper-V daemons suite
hyphen | GPLv2 or LGPLv2+ or MPLv1.1 | A text hyphenation library
hyphen-as | LGPLv3+ | Assamese hyphenation rules
hyphen-bg | GPLv2+ or LGPLv2+ or MPLv1.1 | Bulgarian hyphenation rules
hyphen-bn | LGPLv3+ | Bengali hyphenation rules
hyphen-ca | GPLv3 | Catalan hyphenation rules
hyphen-cy | LPPL | Welsh hyphenation rules
hyphen-da | LGPLv2+ | Danish hyphenation rules
hyphen-de | LGPLv2+ | German hyphenation rules
hyphen-el | LGPLv2+ | Greek hyphenation rules
hyphen-es | LGPLv3+ or GPLv3+ or MPLv1.1 | Spanish hyphenation rules
hyphen-eu | MIT | Basque hyphenation rules
hyphen-fa | LPPL | Farsi hyphenation rules
hyphen-fo | GPL+ | Faroese hyphenation rules
hyphen-fr | LGPLv2+ | French hyphenation rules
hyphen-ga | GPL+ | Irish hyphenation rules
hyphen-gl | GPLv3 | Galician hyphenation rules
hyphen-grc | LPPL | Ancient Greek hyphenation rules
hyphen-gu | LGPLv3+ | Gujarati hyphenation rules
hyphen-hi | LGPLv3+ | Hindi hyphenation rules
hyphen-hsb | LPPL | Upper Sorbian hyphenation rules
hyphen-hu | GPLv2 | Hungarian hyphenation rules
hyphen-ia | LPPL | Interlingua hyphenation rules
hyphen-id | GPL+ | Indonesian hyphenation rules
hyphen-is | LGPLv2+ or SISSL | Icelandic hyphenation rules
hyphen-it | LGPLv2+ | Italian hyphenation rules
hyphen-kn | LGPLv3+ | Kannada hyphenation rules
hyphen-ku | GPLv2+ or LGPLv2+ | Kurdish hyphenation rules
hyphen-lt | LPPL | Lithuanian hyphenation rules
hyphen-mi | GPLv3+ | Maori hyphenation rules
hyphen-ml | LGPLv3+ | Malayalam hyphenation rules
hyphen-mn | LPPL | Mongolian hyphenation rules
hyphen-mr | LGPLv3+ | Marathi hyphenation rules
hyphen-nl | GPLv2 | Dutch hyphenation rules
hyphen-or | LGPLv3+ | Odia hyphenation rules
hyphen-pa | LGPLv3+ | Punjabi hyphenation rules
hyphen-pl | LGPLv2+ | Polish hyphenation rules
hyphen-pt | LGPLv3 and GPL+ | Portuguese hyphenation rules
hyphen-ro | GPLv2+ | Romanian hyphenation rules
hyphen-ru | LGPLv2+ | Russian hyphenation rules
hyphen-sa | LPPL | Sanskrit hyphenation rules
hyphen-sk | GPL+ | Slovak hyphenation rules
hyphen-sl | LGPLv2+ | Slovenian hyphenation rules
hyphen-sv | LGPLv2+ or GPLv2+ | Swedish hyphenation rules
hyphen-ta | LGPLv3+ | Tamil hyphenation rules
hyphen-te | LGPLv3+ | Telugu hyphenation rules
hyphen-tk | Public Domain | Turkmen hyphenation rules
hyphen-uk | GPLv2+ | Ukrainian hyphenation rules
i2c-tools | GPLv2+ | A heterogeneous set of I2C tools for Linux
ibus | LGPLv2+ | Intelligent Input Bus for Linux OS
ibus-hangul | GPLv2+ | The Hangul engine for IBus input platform
ibus-kkc | GPLv2+ | Japanese Kana Kanji input method for ibus
ibus-libpinyin | GPLv2+ | Intelligent Pinyin engine based on libpinyin for IBus
ibus-libzhuyin | GPLv2+ | New Zhuyin engine based on libzhuyin for IBus
ibus-m17n | GPLv2+ | The M17N engine for IBus platform
ibus-sayura | GPLv2+ | The Sinhala engine for IBus input platform
ibus-table | LGPLv2+ | The Table engine for IBus platform
ibus-table-chinese | GPLv3+ | Chinese input tables for IBus
ibus-typing-booster | GPLv3+ | A completion input method
icedtea-web | LGPLv2+ and GPLv2 with exceptions | Additional Java components for OpenJDK - Java browser plug-in and Web Start implementation
icoutils | GPLv3+ | Utility for extracting and converting Microsoft icon and cursor files
icu4j | Unicode and MIT and BSD and Public Domain | International Components for Unicode for Java
iio-sensor-proxy | GPLv3+ | IIO accelerometer sensor to input device proxy
ilmbase | BSD | Abstraction/convenience libraries
imake | MIT | imake source code configuration and build system
initial-setup | GPLv2+ | Initial system configuration utility
intltool | GPLv2 with exceptions | Utility for internationalizing various kinds of data files
ipa | GPLv3+ | The Identity, Policy and Audit system
ipa | GPLv3+ | The Identity, Policy and Audit system
ipa | GPLv3+ | The Identity, Policy and Audit system
ipa-healthcheck | GPLv3 | Health check tool for IdM
ipa-healthcheck | GPLv3 | Health check tool for IdM
iperf3 | BSD | Measurement tool for TCP/UDP bandwidth performance
ipmitool | BSD | Utility for IPMI control
ipvsadm | GPLv2+ | Utility to administer the Linux Virtual Server
ipxe | GPLv2 with additional permissions and BSD | A network boot loader
irssi | GPLv2+ | Modular text mode IRC client with Perl scripting
isl | MIT | Integer point manipulation library
iso-codes | LGPLv2+ | ISO code lists and translations
isomd5sum | GPLv2+ | Utilities for working with md5sum implanted in ISO images
istack-commons | CDDL-1.1 and GPLv2 with exceptions | Common code for some Glassfish projects
itstool | GPLv3+ | ITS-based XML translation tool
jabberpy | LGPLv2+ | Python xmlstream and jabber IM protocol libs
jackson-annotations | ASL 2.0 | Core annotations for Jackson data processor
jackson-core | ASL 2.0 | Core part of Jackson
jackson-databind | ASL 2.0 and LGPLv2+ | General data-binding package for Jackson (2.x)
jackson-jaxrs-providers | ASL 2.0 | Jackson JAX-RS providers
jackson-module-jaxb-annotations | ASL 2.0 | JAXB annotations support for Jackson (2.x)
jaf | BSD | JavaBeans Activation Framework
jakarta-commons-httpclient | ASL 2.0 and (ASL 2.0 or LGPLv2+) | Jakarta Commons HTTPClient implements the client side of HTTP standards
jansi | ASL 2.0 | Jansi is a java library for generating and interpreting ANSI escape sequences
jansi | ASL 2.0 | Jansi is a java library for generating and interpreting ANSI escape sequences
jansi | ASL 2.0 | Jansi is a java library for generating and interpreting ANSI escape sequences
jansi-native | ASL 2.0 | Jansi Native implements the JNI Libraries used by the Jansi project
jansi-native | ASL 2.0 | Jansi Native implements the JNI Libraries used by the Jansi project
jasper | JasPer | Implementation of the JPEG-2000 standard, Part 1
java-1.8.0-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib | OpenJDK 8 Runtime Environment
java-1.8.0-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib | OpenJDK 8 Runtime Environment
java-11-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 11 Runtime Environment
java-11-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 11 Runtime Environment
java-17-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 17 Runtime Environment
java-17-openjdk | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 17 Runtime Environment
java-atk-wrapper | LGPLv2+ | Java ATK Wrapper
javapackages-tools | BSD | Macros and scripts for Java packaging support
javassist | MPLv1.1 or LGPLv2+ or ASL 2.0 | The Java Programming Assistant provides simple Java bytecode manipulation
jbig2dec | AGPLv3+ | A decoder implementation of the JBIG2 image compression format
jbigkit | GPLv2+ | JBIG1 lossless image compression tools
jboss-annotations-1.2-api | CDDL or GPLv2 with exceptions | Common Annotations 1.2 API
jboss-interceptors-1.2-api | CDDL or GPLv2 with exceptions | Java EE Interceptors 1.2 API
jboss-jaxrs-2.0-api | (CDDL or GPLv2 with exceptions) and ASL 2.0 | JAX-RS 2.0: The Java API for RESTful Web Services
jboss-logging | ASL 2.0 | The JBoss Logging Framework
jboss-logging-tools | ASL 2.0 and LGPLv2+ | JBoss Logging I18n Annotation Processor
jctools | ASL 2.0 | Java Concurrency Tools for the JVM
jdeparser | ASL 2.0 | Source generator library for Java
jetty | ASL 2.0 or EPL-1.0 | Java Webserver and Servlet Container
jigawatts | GPLv2 with exceptions | Java CRIU helper
jline | BSD | JLine is a Java library for handling console input
jmc | UPL | JDK Mission Control is a profiling and diagnostics tool
jmc-core | UPL | Core API for JDK Mission Control
jna | (LGPLv2+ or ASL 2.0) and ASL 2.0 | Pure Java access to native libraries
jolokia-jvm-agent | ASL 2.0 | Jolokia JVM Agent
jomolhari-fonts | OFL | Jomolhari a Bhutanese style font for Tibetan and Dzongkha
jose | ASL 2.0 | Tools for JSON Object Signing and Encryption (JOSE)
jq | MIT and ASL 2.0 and CC-BY and GPLv3 | Command-line JSON processor
js-d3-flame-graph | ASL 2.0 | A D3.js plugin that produces flame graphs
jsch | BSD | Pure Java implementation of SSH2
jsoup | MIT | Java library for working with real-world HTML
jsoup | MIT | Java library for working with real-world HTML
jsr-305 | BSD and CC-BY | Correctness annotations for Java code
jss | MPLv1.1 or GPLv2+ or LGPLv2+ | Java Security Services (JSS)
julietaula-montserrat-fonts | OFL | Sans-serif typeface inspired from Montserrat area
junit | EPL-1.0 | Java regression test package
junit5 | EPL-2.0 | Java regression testing framework
jzlib | BSD | Re-implementation of zlib in pure Java
kacst-fonts | GPLv2 | Fonts for arabic from arabeyes project
kdump-anaconda-addon | GPLv2 | Kdump configuration anaconda addon
keepalived | GPLv2+ | High Availability monitor built upon LVS, VRRP and service pollers
keybinder3 | MIT | A library for registering global keyboard shortcuts
keycloak-httpd-client-install | GPLv3 | Tools to configure Apache HTTPD as Keycloak client
khmeros-fonts | LGPLv2+ | Khmer font set created by Danh Hong of the Cambodian Open Institute
kronosnet | GPLv2+ and LGPLv2+ | Multipoint-to-Multipoint VPN daemon
ksh | EPL-1.0 | The Original ATT Korn Shell
kurdit-unikurd-web-fonts | GPLv3 | A widely used Kurdish font for Arabic-like scripts and Latin
kyotocabinet | GPLv3 | A straightforward implementation of DBM
ladspa | LGPLv2+ | Linux Audio Developer's Simple Plug-in API, examples and tools
lame | GPLv2+ | Free MP3 audio compressor
langpacks | GPLv2+ | Langpacks meta-package
langtable | GPLv3+ | Guessing reasonable defaults for locale, keyboard layout, territory, and language.
lapack | BSD | Numerical linear algebra package libraries
lasso | GPLv2+ | Liberty Alliance Single Sign On
latex2html | GPLv2+ | Converts LaTeX documents to HTML
lato-fonts | OFL | A sanserif typeface family
lcms2 | MIT | Color Management Engine
ldapjdk | MPLv1.1 or GPLv2+ or LGPLv2+ | LDAP SDK
ldns | BSD | Low-level DNS(SEC) library with API
lensfun | LGPLv3 and CC-BY-SA | Library to rectify defects introduced by photographic lenses
leptonica | BSD and Leptonica | C library for efficient image processing and image analysis operations
lftp | GPLv3+ | A sophisticated file transfer program
libEMF | LGPLv2+ and GPLv2+ | A library for generating Enhanced Metafiles
libICE | MIT | X.Org X11 ICE runtime library
libIDL | LGPLv2+ | Library for parsing IDL (Interface Definition Language)
libSM | MIT | X.Org X11 SM runtime library
libXNVCtrl | GPLv2+ | Library providing the NV-CONTROL API
libXScrnSaver | MIT | X.Org X11 libXss runtime library
libXaw | MIT | X Athena Widget Set
libXcomposite | MIT | X Composite Extension library
libXcursor | MIT | Cursor management library
libXdamage | MIT | X Damage extension library
libXdmcp | MIT | X Display Manager Control Protocol library
libXfixes | MIT | X Fixes library
libXfont2 | MIT | X.Org X11 libXfont2 runtime library
libXft | MIT | X.Org X11 libXft runtime library
libXi | MIT | X.Org X11 libXi runtime library
libXinerama | MIT | X.Org X11 libXinerama runtime library
libXmu | MIT | X.Org X11 libXmu/libXmuu runtime libraries
libXp | MIT | X.Org X11 libXp runtime library
libXpm | MIT | X.Org X11 libXpm runtime library
libXrandr | MIT | X.Org X11 libXrandr runtime library
libXres | MIT | X-Resource extension client library
libXt | MIT | X.Org X11 libXt runtime library
libXtst | MIT | X.Org X11 libXtst runtime library
libXv | MIT | X.Org X11 libXv runtime library
libXvMC | MIT | X.Org X11 libXvMC runtime library
libXxf86dga | MIT | X.Org X11 libXxf86dga runtime library
libXxf86misc | MIT | X.Org X11 libXxf86misc runtime library
libXxf86vm | MIT | X.Org X11 libXxf86vm runtime library
libabw | MPLv2.0 | A library for import of AbiWord files
libaec | BSD | Adaptive Entropy Coding library
libao | GPLv2+ | Cross Platform Audio Output Library
libappindicator | LGPLv2 and LGPLv3 | Application indicators library
libasyncns | LGPLv2+ | Asynchronous Name Service Library
libatasmart | LGPLv2+ | ATA S.M.A.R.T. Disk Health Monitoring Library
libatomic_ops | GPLv2 and MIT | Atomic memory update operations
libavc1394 | GPLv2+ and LGPLv2+ | Audio/Video Control library for IEEE-1394 devices
libbase | LGPLv2 | JFree Base Services
libblockdev | LGPLv2+ | A library for low-level manipulation with block devices
libbluray | LGPLv2+ | Library to access Blu-Ray disks for video playback
libburn | GPLv2+ | Library for reading, mastering and writing optical discs
libbytesize | LGPLv2+ | A library for working with sizes in bytes
libcacard | LGPLv2+ | CAC (Common Access Card) library
libcanberra | LGPLv2+ | Portable Sound Event Library
libcdio | GPLv3+ | CD-ROM input and control library
libcdio-paranoia | GPLv3+ | CD paranoia on top of libcdio
libcdr | MPLv2.0 and Public Domain | A library for import of CorelDRAW drawings
libchamplain | LGPLv2+ | Map view for Clutter
libcmis | GPLv2+ or LGPLv2+ or MPLv1.1 | A C/C++ client library for CM interfaces
libdap | LGPLv2+ | The C++ DAP2 library from OPeNDAP
libdatrie | LGPLv2+ | Implementation of Double-Array structure for representing trie
libdazzle | GPLv3+ | Experimental new features for GTK+ and GLib
libdbusmenu | LGPLv3 or LGPLv2 and GPLv3 | Library for passing menus over DBus
libdc1394 | LGPLv2+ | 1394-based digital camera control library
libdmapsharing | LGPLv2+ | A DMAP client and server library
libdmx | MIT | X.Org X11 DMX runtime library
libdnet | BSD | Simple portable interface to lowlevel networking routines
libdrm | MIT | Direct Rendering Manager runtime library
libdv | LGPLv2+ | Software decoder for DV format video
libdvdnav | GPLv2+ | A library for reading DVD video discs based on Ogle code
libdvdread | GPLv2+ | A library for reading DVD video discs based on Ogle code
libdwarf | LGPLv2 | Library to access the DWARF Debugging file format
libeasyfc | LGPLv3+ | Easy configuration generator interface for fontconfig
libecap | BSD | Squid interface for embedded adaptation modules
libecpg | PostgreSQL | ECPG - Embedded SQL in C
libeot | MPLv2.0 | A library for parsing Embedded OpenType font files
libepoxy | MIT | epoxy runtime library
libepubgen | MPLv2.0 | An EPUB generator library
libestr | LGPLv2+ | String handling essentials library
libetonyek | MPLv2.0 | A library for import of Apple iWork documents
libev | BSD or GPLv2+ | High-performance event loop/event model with lots of features
libevdev | MIT | Kernel Evdev Device Wrapper Library
libexif | LGPLv2+ | Library for extracting extra information from image files
libexttextcat | BSD | Text categorization library
libfastjson | MIT | A JSON implementation in C
libfontenc | MIT | X.Org X11 libfontenc runtime library
libfonts | LGPLv2 and UCD | TrueType Font Layouting
libformula | LGPLv2 | Formula Parser
libfprint | LGPLv2+ | Toolkit for fingerprint scanner
libfreehand | MPLv2.0 | A library for import of Macromedia/Adobe FreeHand documents
libgdata | LGPLv2+ | Library for the GData protocol
libgdither | GPLv2+ | Library for applying dithering to PCM audio sources
libgee | LGPLv2+ | GObject collection library
libgexiv2 | GPLv2+ | Gexiv2 is a GObject-based wrapper around the Exiv2 library
libgit2 | GPLv2 with exceptions | C implementation of the Git core methods as a library with a solid API
libgit2-glib | LGPLv2+ | Git library for GLib
libglvnd | MIT | The GL Vendor-Neutral Dispatch library
libgnomekbd | LGPLv2+ | A keyboard configuration library
libgovirt | LGPLv2+ | A GObject library for interacting with oVirt REST API
libgphoto2 | GPLv2+ and GPLv2 | Library for accessing digital cameras
libgpod | LGPLv2+ | Library to access the contents of an iPod
libgsf | LGPLv2 | GNOME Structured File library
libgtop2 | GPLv2+ | LibGTop library (version 2)
libguestfs | LGPLv2+ | Access and modify virtual machine disk images
libguestfs-winsupport | GPLv2+ | Add support for Windows guests to virt-v2v and virt-p2v
libgweather | GPLv2+ | A library for weather information
libgxps | LGPLv2+ | GObject based library for handling and rendering XPS documents
libhandy | LGPLv2+ | Building blocks for modern adaptive GNOME apps
libhangul | LGPLv2+ | Hangul input library
libidn | LGPLv2+ and GPLv3+ and GFDL | Internationalized Domain Name support library
libiec61883 | LGPLv2+ | Streaming library for IEEE1394
libieee1284 | GPLv2+ | A library for interfacing IEEE 1284-compatible devices
libijs | AGPLv3+ | IJS Raster Image Transport Protocol Library
libimobiledevice | LGPLv2+ | Library for connecting to mobile devices
libindicator | GPLv3 | Shared functions for Ayatana indicators
libinput | MIT | Input device library
libipt | BSD | Intel Processor Trace Decoder Library
libiptcdata | LGPLv2+ | IPTC tag library
libiscsi | LGPLv2+ | iSCSI client library
libisoburn | GPLv2+ | Library to enable creation and expansion of ISO-9660 filesystems
libisofs | GPLv2+ and LGPLv2+ | Library to create ISO 9660 disk images
libkkc | GPLv3+ | Japanese Kana Kanji conversion library
libkkc-data | GPLv3+ | Language model data for libkkc
liblangtag | LGPLv3+ or MPLv2.0 | An interface library to access tags for identifying languages
liblayout | LGPLv2+ and UCD | CSS based layouting framework
libldac | ASL 2.0 | A lossy audio codec for Bluetooth connections
libloader | LGPLv2 | Resource Loading Framework
liblockfile | GPLv2+ and LGPLv2+ | This implements a number of functions found in -lmail on SysV systems
liblognorm | LGPLv2+ | Fast samples-based log normalization library
liblouis | LGPLv3+ | Braille translation and back-translation library
libmad | GPLv2+ | MPEG audio decoder library
libmatchbox | LGPLv2+ | Libraries for the Matchbox Desktop
libmaxminddb | ASL 2.0 and BSD | C library for the MaxMind DB file format
libmediaart | LGPLv2+ | Library for managing media art caches
libmemcached | BSD | Client library and command line tools for memcached server
libmng | zlib | Library for Multiple-image Network Graphics support
libmpc | LGPLv3+ | C library for multiple precision complex arithmetic
libmpcdec | BSD | Musepack audio decoding library
libmspack | LGPLv2 | Library for CAB and related files compression and decompression
libmspub | MPLv2.0 | A library for import of Microsoft Publisher documents
libmtp | LGPLv2+ | Software library for MTP media players
libmusicbrainz5 | LGPLv2 | Library for accessing MusicBrainz servers
libmwaw | LGPLv2+ or MPLv2.0 | A library for import of many old Mac document formats
libnbd | LGPLv2+ | NBD client library in userspace
libnet | BSD | C library for portable packet creation and injection
libnice | LGPLv2 and MPLv1.1 | GLib ICE implementation
libnma | GPLv2+ and LGPLv2+ | NetworkManager GUI library
libnotify | LGPLv2+ | Desktop notification library
libnumbertext | (LGPLv3+ or BSD) and (LGPLv3+ or BSD or CC-BY-SA) | Number to number name and money text conversion library
liboauth | MIT | OAuth library functions
libodfgen | LGPLv2+ or MPLv2.0 | An ODF generator library
libogg | BSD | The Ogg bitstream file format library
liboggz | BSD | Simple programming interface for Ogg files and streams
libomp | NCSA | OpenMP runtime for clang
libopenraw | LGPLv3+ | Decode camera RAW files
liborcus | MPLv2.0 | Standalone file import filter library for spreadsheet documents
libosinfo | LGPLv2+ | A library for managing OS information for virtualization
libotf | LGPLv2+ | A Library for handling OpenType Font
libpagemaker | MPLv2.0 | A library for import of Adobe PageMaker documents
libpaper | GPLv2 | Library and tools for handling papersize
libpfm | MIT | Library to encode performance events for use by perf tool
libpinyin | GPLv3+ | Library to deal with pinyin
libplist | LGPLv2+ | Library for manipulating Apple Binary and XML Property Lists
libpmemobj-cpp | BSD | C++ bindings for libpmemobj
libpmemobj-cpp | BSD | C++ bindings for libpmemobj
libpng12 | zlib | Old version of libpng, needed to run old binaries
libpng15 | zlib | Old version of libpng, needed to run old binaries
libpq | PostgreSQL | PostgreSQL client library
libpst | GPLv2+ | Utilities to convert Outlook .pst files to other formats
libquvi | AGPLv3+ | A cross-platform library for parsing flash media stream
libquvi-scripts | AGPLv3+ | Embedded lua scripts for parsing the media details
libqxp | MPLv2.0 | Library for import of QuarkXPress documents
libraw1394 | LGPLv2+ | Library providing low-level IEEE-1394 access
librdkafka | BSD | The Apache Kafka C library
librelp | GPLv3+ | The Reliable Event Logging Protocol library
libreoffice | (MPLv1.1 or LGPLv3+) and LGPLv3 and LGPLv2+ and BSD and (MPLv1.1 or GPLv2 or LGPLv2 or Netscape) and Public Domain and ASL 2.0 and MPLv2.0 and CC0 | Free Software Productivity Suite
libreoffice-voikko | GPLv3+ | Finnish spellchecker and hyphenator extension for LibreOffice
librepository | LGPLv2 | Hierarchical repository abstraction layer
libreswan | GPLv2 | IPsec implementation with IKEv1 and IKEv2 keying protocols
librevenge | (LGPLv2+ or MPLv2.0) and BSD | A base library for writing document import filters
librsvg2 | LGPLv2+ | An SVG library based on cairo
librx | GPLv2+ | POSIX regexp functions
libsamplerate | BSD | Sample rate conversion library for audio data
libsass | MIT | C/C++ port of the Sass CSS precompiler
libselinux | Public Domain | SELinux library and simple utilities
libserf | ASL 2.0 | High-Performance Asynchronous HTTP Client Library
libserf | ASL 2.0 | High-Performance Asynchronous HTTP Client Library
libserializer | LGPLv2+ | JFreeReport General Serialization Framework
libshout | LGPLv2+ and MIT | Icecast source streaming library
libsigc++20 | LGPLv2+ | Typesafe signal framework for C++
libslirp | BSD and MIT | A general purpose TCP-IP emulator
libslirp | BSD and MIT | A general purpose TCP-IP emulator
libslirp | BSD and MIT | A general purpose TCP-IP emulator
libsmi | GPLv2+ and BSD | A library to access SMI MIB information
libsndfile | LGPLv2+ and GPLv2+ and BSD | Library for reading and writing sound files
libspectre | GPLv2+ | A library for rendering PostScript(TM) documents
libspiro | GPLv3+ | Library to simplify the drawing of beautiful curves
libsrtp | BSD | An implementation of the Secure Real-time Transport Protocol (SRTP)
libstaroffice | MPLv2.0 or LGPLv2+ | A library for import of binary StarOffice documents
libtar | MIT | Tar file manipulation API
libthai | LGPLv2+ | Thai language support routines
libtheora | BSD | Theora Video Compression Codec
libtiff | libtiff | Library of functions for manipulating TIFF format image files
libtimezonemap | GPLv3 | Time zone map widget for Gtk+
libtpms | BSD | Library providing Trusted Platform Module (TPM) functionality
libucil | GPLv2+ | Library to render text and graphic overlays onto video images
libunicap | GPLv2+ | Library to access different kinds of (video) capture devices
libuninameslist | BSD | A library providing Unicode character names and annotations
libusbmuxd | LGPLv2+ | Client library USB multiplex daemon for Apple's iOS devices
libuv | MIT and BSD and ISC | Platform layer for node.js
libva | MIT | Video Acceleration (VA) API for Linux
libvdpau | MIT | Wrapper library for the Video Decode and Presentation API
libvirt | LGPLv2+ | Library providing a simple virtualization API
libvirt-dbus | LGPLv2+ | libvirt D-Bus API binding
libvirt-glib | LGPLv2+ | libvirt glib integration for events
libvirt-python | LGPLv2+ | The libvirt virtualization API python3 binding
libvisio | MPLv2.0 | A library for import of Microsoft Visio diagrams
libvisual | LGPLv2+ | Abstraction library for audio visualisation plugins
libvma | GPLv2 or BSD | A library for boosting TCP and UDP traffic (over RDMA hardware)
libvncserver | GPLv2+ | Library to make writing a VNC server easy
libvoikko | GPLv2+ | Voikko is a library for spellcheckers and hyphenators
libvorbis | BSD | The Vorbis General Audio Compression Codec
libvpx | BSD | VP8/VP9 Video Codec SDK
libwacom | MIT | Tablet Information Client Library
libwebp | BSD | Library and tools for the WebP graphics format
libwmf | LGPLv2+ and GPLv2+ and GPL+ | Windows MetaFile Library
libwnck3 | LGPLv2+ | Window Navigator Construction Kit
libwpd | LGPLv2+ or MPLv2.0 | A library for import of WordPerfect documents
libwpe | BSD | General-purpose library for the WPE-flavored port of WebKit
libwpg | LGPLv2+ or MPLv2.0 | A library for import of WordPerfect Graphics images
libwps | LGPLv2+ or MPLv2.0 | A library for import of Microsoft Works documents
libxcvt | MIT | VESA CVT standard timing modelines generator
libxkbcommon | MIT | X.Org X11 XKB parsing library
libxkbfile | MIT | X.Org X11 libxkbfile runtime library
libxklavier | LGPLv2+ | High-level API for X Keyboard Extension
libxshmfence | MIT | X11 shared memory fences
libyami | ASL 2.0 | Yet Another Media Infrastructure library.
libyang | BSD | YANG data modeling language library
libzip | BSD | C library for reading, creating, and modifying zip archives
libzip | BSD | C library for reading, creating, and modifying zip archives
libzip | BSD | C library for reading, creating, and modifying zip archives
libzip | BSD | C library for reading, creating, and modifying zip archives
libzmf | MPLv2.0 | A library for import of Zoner document formats
linuxconsoletools | GPLv2+ | Tools for connecting joysticks & legacy devices to the kernel's input subsystem
linuxdoc-tools | MIT | A text formatting package based on SGML
linuxptp | GPLv2+ | PTP implementation for Linux
lklug-fonts | GPLv2 | Fonts for Sinhala language
lld | NCSA | The LLVM Linker
lldpd | ISC | ISC-licensed implementation of LLDP
llvm | NCSA | The Low Level Virtual Machine
llvm-toolset | NCSA | Package that installs llvm-toolset
log4j | ASL 2.0 | Java logging package
log4j12 | ASL 2.0 | Java logging package
lohit-assamese-fonts | OFL | Free Assamese font
lohit-bengali-fonts | OFL | Free Bengali script font
lohit-devanagari-fonts | OFL | Free Devanagari Script Font
lohit-gujarati-fonts | OFL | Free Gujarati font
lohit-gurmukhi-fonts | OFL | Free Gurmukhi truetype font for Punjabi language
lohit-kannada-fonts | OFL | Free Kannada font
lohit-malayalam-fonts | OFL | Free Malayalam font
lohit-marathi-fonts | OFL | Free truetype font for Marathi language
lohit-nepali-fonts | OFL | Free TrueType fonts for Nepali language
lohit-odia-fonts | OFL | Free truetype font for Odia language
lohit-tamil-fonts | OFL | Free truetype font for Tamil language
lohit-telugu-fonts | OFL | Free Telugu font
lorax | GPLv2+ | Tool for creating the anaconda install images
lorax-templates-anolis | GPLv2+ | Anolis OS build templates for lorax and livemedia-creator
lpsolve | LGPLv2+ | A Mixed Integer Linear Programming (MILP) solver
ltrace | GPLv2+ | Tracks runtime library calls from dynamically linked executables
lttng-ust | LGPLv2 and GPLv2 and MIT | LTTng Userspace Tracer library
lua-expat | MIT | SAX XML parser based on the Expat library
lua-filesystem | MIT | File System Library for the Lua Programming Language
lua-json | MIT | JSON Parser/Constructor for Lua
lua-lpeg | MIT | Parsing Expression Grammars for Lua
lua-lunit | MIT | Unit testing framework for Lua
lua-posix | MIT | A POSIX library for Lua
lua-socket | MIT | Network support for the Lua language
lucene | ASL 2.0 | High-performance, full-featured text search engine
luksmeta | LGPLv2+ | Utility for storing small metadata in the LUKSv1 header
lynx | GPLv2 | A text-based Web browser
lz4-java | ASL 2.0 and (BSD and GPLv2+) | LZ4 compression for Java
m17n-db | LGPLv2+ | Multilingualization datafiles for m17n-lib
m17n-lib | LGPLv2+ | Multilingual text library
madan-fonts | GPL+ | Font for Nepali language
mailman | GPLv2+ | Mailing list manager with built in Web access
malaga | GPLv2+ | A programming language for automatic language analysis
malaga-suomi-voikko | GPLv2+ | A description of Finnish morphology written in Malaga (Voikko edition)
mallard-rng | MIT | RELAX NG schemas for all Mallard versions
man-pages-overrides | GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE | Complementary and updated manual pages
mariadb | GPLv2 with exceptions and LGPLv2 and BSD | A very fast and robust SQL database server
mariadb | GPLv2 with exceptions and LGPLv2 and BSD | A very fast and robust SQL database server
mariadb-connector-c | LGPLv2+ | The MariaDB Native Client library (C driver)
mariadb-connector-odbc | LGPLv2+ | The MariaDB Native Client library (ODBC driver)
mariadb-java-client | BSD and LGPLv2+ | Connects applications developed in Java to MariaDB and MySQL databases
marisa | BSD or LGPLv2+ | Static and spece-efficient trie data structure library
matchbox-window-manager | GPLv2+ | Window manager for the Matchbox Desktop
maven | ASL 2.0 and MIT | Java project management and project comprehension tool
maven | ASL 2.0 and MIT | Java project management and project comprehension tool
maven-resolver | ASL 2.0 | Apache Maven Artifact Resolver library
maven-resolver | ASL 2.0 | Apache Maven Artifact Resolver library
maven-shared-utils | ASL 2.0 | Maven shared utility classes
maven-shared-utils | ASL 2.0 | Maven shared utility classes
maven-wagon | ASL 2.0 | Tools to manage artifacts and deployment
maven-wagon | ASL 2.0 | Tools to manage artifacts and deployment
mc | GPLv3+ | User-friendly text console file manager and visual shell
mcpp | BSD | Alternative C/C++ preprocessor
mdevctl | LGPLv2 | Mediated device management and persistence utility
meanwhile | LGPLv2+ | Lotus Sametime Community Client library
mecab | BSD or LGPLv2+ or GPL+ | Yet Another Part-of-Speech and Morphological Analyzer
mecab-ipadic | mecab-ipadic | IPA dictionary for MeCab
media-player-info | BSD | Data files describing media player capabilities
memcached | BSD | High Performance, Distributed Memory Object Cache
memkind | BSD | User Extensible Heap Manager
mercurial | GPLv2+ | Mercurial -- a distributed SCM
mesa | MIT | Mesa graphics libraries
mesa-demos | MIT | Mesa demos
mesa-libGLU | MIT | Mesa libGLU library
mesa-libGLw | MIT | Xt / Motif OpenGL widgets
meson | ASL 2.0 | High productivity build system
metacity | GPLv2+ | Unobtrusive window manager
metis | ASL 2.0 and BSD and LGPLv2+ | Serial Graph Partitioning and Fill-reducing Matrix Ordering
micropipenv | LGPLv3+ | A simple wrapper around pip to support Pipenv and Poetry files
mingw-binutils | GPLv2+ and LGPLv2+ and GPLv3+ and LGPLv3+ | Cross-compiled version of binutils for Win32 and Win64 environments
mingw-bzip2 | BSD | MinGW port of bzip2 file compression utility
mingw-cairo | LGPLv2 or MPLv1.1 | MinGW Windows Cairo library
mingw-crt | Public Domain and ZPLv2.1 | MinGW Windows cross-compiler runtime
mingw-expat | MIT | MinGW Windows port of expat XML parser library
mingw-filesystem | GPLv2+ | MinGW cross compiler base filesystem and environment
mingw-fontconfig | MIT | MinGW Windows Fontconfig library
mingw-freetype | FTL or GPLv2+ | Free and portable font rendering engine
mingw-gcc | GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions | MinGW Windows cross-compiler (GCC) for C
mingw-gettext | GPLv2+ and LGPLv2+ | GNU libraries and utilities for producing multi-lingual messages
mingw-glib2 | LGPLv2+ | MinGW Windows GLib2 library
mingw-gstreamer1 | LGPLv2+ | MinGW Windows Streaming-Media Framework Runtime
mingw-harfbuzz | MIT | MinGW Windows Harfbuzz library
mingw-headers | Public Domain and LGPLv2+ and ZPLv2.1 | Win32/Win64 header files
mingw-icu | MIT and UCD and Public Domain | MinGW compilation of International Components for Unicode Tools
mingw-libffi | BSD | A portable foreign function interface library for MinGW
mingw-libjpeg-turbo | wxWidgets | MinGW Windows Libjpeg-turbo library
mingw-libpng | zlib | MinGW Windows Libpng library
mingw-libtiff | libtiff | MinGW Windows port of the LibTIFF library
mingw-openssl | OpenSSL | MinGW port of the OpenSSL toolkit
mingw-pcre | BSD | MinGW Windows pcre library
mingw-pixman | MIT | MinGW Windows Pixman library
mingw-pkg-config | GPLv2+ | A tool for determining compilation options
mingw-readline | GPLv2+ | MinGW port of readline for editing typed command lines
mingw-sqlite | Public Domain | MinGW Windows port of sqlite embeddable SQL database engine
mingw-termcap | GPLv2+ | MinGW terminal feature database
mingw-win-iconv | Public Domain | Iconv implementation using Win32 API
mingw-winpthreads | MIT and BSD | MinGW pthread library
mingw-zlib | zlib | MinGW Windows zlib compression library
mod_auth_gssapi | MIT | A GSSAPI Authentication module for Apache
mod_auth_mellon | GPLv2+ | A SAML 2.0 authentication module for the Apache Httpd Server
mod_auth_openidc | ASL 2.0 | OpenID Connect auth module for Apache HTTP Server
mod_authnz_pam | ASL 2.0 | PAM authorization checker and PAM Basic Authentication provider
mod_fcgid | ASL 2.0 | FastCGI interface module for Apache 2
mod_http2 | ASL 2.0 | module implementing HTTP/2 for Apache 2
mod_http2 | ASL 2.0 | module implementing HTTP/2 for Apache 2
mod_intercept_form_submit | ASL 2.0 | Apache module to intercept login form submission and run PAM authentication
mod_lookup_identity | ASL 2.0 | Apache module to retrieve additional information about the authenticated user
mod_md | ASL 2.0 | Certificate provisioning using ACME for the Apache HTTP Server
mod_security | ASL 2.0 | Security module for the Apache HTTP Server
mod_security_crs | ASL 2.0 | ModSecurity Rules
mod_wsgi | ASL 2.0 | A WSGI interface for Python web applications in Apache
mod_wsgi | ASL 2.0 | A WSGI interface for Python web applications in Apache
mod_wsgi | ASL 2.0 | A WSGI interface for Python web applications in Apache
modulemd-tools | MIT | Collection of tools for parsing and generating modulemd YAML files
motif | LGPLv2+ | Run-time libraries and programs
mousetweaks | GPLv3 and GFDL | Mouse accessibility support for the GNOME desktop
mozilla-filesystem | MPLv1.1 | Mozilla filesytem layout
mozvoikko | GPLv2+ | Finnish Voikko spell-checker extension for Mozilla programs
mpg123 | LGPLv2+ | Real time MPEG 1.0/2.0/2.5 audio player/decoder for layers 1, 2 and 3
mpich | MIT | A high-performance implementation of MPI
mpitests | CPL and BSD | MPI Benchmarks and tests
mrtg | GPLv2+ | Multi Router Traffic Grapher
mstflint | GPLv2+ or BSD | Mellanox firmware burning tool
mt-st | GPL+ | Tool for controlling tape drives
mtdev | MIT | Multitouch Protocol Translation Library
mtx | GPLv2 | SCSI media changer control program
multilib-rpm-config | GPLv2+ | Multilib packaging helpers
munge | GPLv3+ and LGPLv3+ | Enables uid & gid authentication across a host cluster
mutt | GPLv2+ and Public Domain | A text mode mail user agent
mutter | GPLv2+ | Window and compositing manager based on Clutter
mvapich2 | BSD and MIT | OSU MVAPICH2 MPI package
mysql | GPLv2 with exceptions and LGPLv2 and BSD | MySQL client programs and shared libraries
mysql-selinux | GPLv3 | SELinux policy modules for MySQL and MariaDB packages
mythes | BSD and MIT | A thesaurus library
mythes-bg | GPLv2+ or LGPLv2+ or MPLv1.1 | Bulgarian thesaurus
mythes-ca | GPL+ | Catalan thesaurus
mythes-cs | MIT | Czech thesaurus
mythes-da | GPLv2 or LGPLv2 or MPLv1.1 | Danish thesaurus
mythes-de | LGPLv2+ | German thesaurus
mythes-el | GPLv2+ | Greek thesaurus
mythes-en | BSD and Artistic clarified | English thesaurus
mythes-es | LGPLv2+ | Spanish thesaurus
mythes-fr | LGPLv2+ | French thesaurus
mythes-ga | GFDL | Irish thesaurus
mythes-hu | GPLv2+ and (GPLv2+ or LGPLv2+ or MPLv1.1) and GPLv2 and (GPL+ or LGPLv2+ or MPLv1.1) | Hungarian thesaurus
mythes-it | AGPLv3+ | Italian thesaurus
mythes-mi | Public Domain | Maori thesaurus
mythes-ne | LGPLv2 | Nepali thesaurus
mythes-nl | BSD or CC-BY | Dutch thesaurus
mythes-pl | LGPLv2 | Polish thesaurus
mythes-pt | GPLv2+ | Portuguese thesaurus
mythes-ro | GPLv2+ | Romanian thesaurus
mythes-ru | LGPLv2+ | Russian thesaurus
mythes-sk | MIT | Slovak thesaurus
mythes-sl | LGPLv2+ | Slovenian thesaurus
mythes-sv | MIT | Swedish thesaurus
mythes-uk | (GPLv2+ or LGPLv2+) and (GPLv2+ or LGPLv2+ or MPLv1.1) and GPLv2+ | Ukrainian thesaurus
nafees-web-naskh-fonts | Bitstream Vera | Nafees Web font for writing Urdu in the Naskh script
nasm | BSD | A portable x86 assembler which uses Intel-like syntax
nautilus | GPLv3+ | File manager for GNOME
nautilus-sendto | GPLv2+ | Nautilus context menu for sending files
navilu-fonts | OFL | Free Kannada opentype sans-serif font
nbdkit | BSD | NBD server
ncompress | Public Domain | Fast compression and decompression utilities
neon | LGPLv2+ | An HTTP and WebDAV client library
netavark | ASL 2.0 and BSD and MIT | OCI network stack
netcf | LGPLv2+ | Cross-platform network configuration library
netpbm | BSD and GPLv2 and IJG and MIT and Public Domain | A library for handling different graphics file formats
network-manager-applet | GPLv2+ | A network control and status applet for NetworkManager
nginx | BSD | A high performance web server and reverse proxy server
nginx | BSD | A high performance web server and reverse proxy server
nginx | BSD | A high performance web server and reverse proxy server
nginx | BSD | A high performance web server and reverse proxy server
nginx | BSD | A high performance web server and reverse proxy server
ninja-build | ASL 2.0 | A small build system with a focus on speed
nispor | ASL 2.0 | API for network status querying
nkf | BSD | A Kanji code conversion filter
nmap | Nmap | Network exploration tool and security scanner
nmstate | LGPLv2+ | Declarative network manager API
nmstate | LGPLv2+ | Declarative network manager API
nmstate | LGPLv2+ | Declarative network manager API
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs | MIT and ASL 2.0 and ISC and BSD | JavaScript runtime
nodejs-nodemon | MIT | Simple monitor script for use during development of a node.js app
nodejs-nodemon | MIT | Simple monitor script for use during development of a node.js app
nodejs-nodemon | MIT | Simple monitor script for use during development of a node.js app
nodejs-nodemon | MIT | Simple monitor script for use during development of a node.js app
nodejs-nodemon | MIT | Simple monitor script for use during development of a node.js app
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging
nodejs-packaging | MIT | RPM Macros and Utilities for Node.js Packaging
nspr | MPLv2.0 | Netscape Portable Runtime
nss | MPLv2.0 | Network Security Services
nss-altfiles | LGPLv2+ | NSS module to look up users in /usr/lib/passwd too
nss-pam-ldapd | LGPLv2+ | An nsswitch module which uses directory servers
nss_wrapper | BSD | A wrapper for the user, group and hosts NSS API
ntpstat | MIT | Utility to print NTP synchronization status
numpy | BSD and Python | A fast multidimensional array facility for Python
numpy | BSD and Python | A fast multidimensional array facility for Python
numpy | BSD and Python and ASL 2.0 | A fast multidimensional array facility for Python
numpy | BSD and Python and ASL 2.0 | A fast multidimensional array facility for Python
objectweb-asm | BSD | Java bytecode manipulation and analysis framework
ocaml | QPL and (LGPLv2+ with exceptions) | OCaml compiler and programming environment
ocaml-camlp4 | LGPLv2+ with exceptions | Pre-Processor-Pretty-Printer for OCaml
ocaml-cppo | BSD | Equivalent of the C preprocessor for OCaml programs
ocaml-extlib | LGPLv2+ with exceptions | OCaml ExtLib additions to the standard library
ocaml-findlib | BSD | Objective CAML package manager and build helper
ocaml-labltk | LGPLv2+ with exceptions | Tcl/Tk interface for OCaml
ocaml-ocamlbuild | LGPLv2+ with exceptions | Build tool for OCaml libraries and programs
ocaml-srpm-macros | GPLv2+ | OCaml architecture macros
oci-seccomp-bpf-hook | ASL 2.0 | OCI Hook to generate seccomp json files based on EBF syscalls used by container
oci-seccomp-bpf-hook | ASL 2.0 | OCI Hook to generate seccomp json files based on EBF syscalls used by container
oci-seccomp-bpf-hook | ASL 2.0 | OCI Hook to generate seccomp json files based on EBF syscalls used by container
oci-systemd-hook | GPLv3+ | OCI systemd hook for docker
oci-umount | GPLv3+ | OCI umount hook for docker
ocl-icd | BSD | OpenCL Library (Installable Client Library) Bindings
oddjob | BSD | A D-Bus service which runs odd jobs on behalf of client applications
omping | ISC | Utility to test IP multicast functionality
ongres-scram | BSD | Salted Challenge Response Authentication Mechanism (SCRAM) - Java Implementation
oniguruma | BSD | Regular expressions library
open-sans-fonts | ASL 2.0 | Open Sans is a humanist sans-serif typeface designed by Steve Matteson
open-vm-tools | GPLv2 | Open Virtual Machine Tools for virtual machines hosted on VMware
openal-soft | LGPLv2+ | Open Audio Library
openblas | BSD | An optimized BLAS library based on GotoBLAS2
openblas-srpm-macros | MIT | OpenBLAS architecture macros
openchange | GPLv3+ and Public Domain | Provides access to Microsoft Exchange servers using native protocols
opencl-filesystem | Public Domain | OpenCL filesystem layout
opencl-headers | MIT | OpenCL (Open Computing Language) header files
opencv | BSD | Collection of algorithms for computer vision
opendnssec | BSD | DNSSEC key and zone management software
openjade | DMIT | A DSSSL implementation
openjpeg2 | BSD and MIT | C-Library for JPEG 2000
openmpi | BSD and MIT and Romio | Open Message Passing Interface
openoffice-lv | LGPLv2+ | Latvian linguistic dictionaries
openoffice.org-dict-cs_CZ | GPL+ | Czech spellchecker and hyphenation dictionaries for LibreOffice
openscap | LGPLv2+ | Set of open source libraries enabling integration of the SCAP line of standards
openscap | LGPLv2+ | Set of open source libraries enabling integration of the SCAP line of standards
openslp | BSD | Open implementation of Service Location Protocol V2
opensp | MIT | SGML and XML parser
opentest4j | ASL 2.0 | Open Test Alliance for the JVM
openwsman | BSD | Open source Implementation of WS-Management
opus | BSD | An audio codec for use in low-delay speech and audio communication
orc | BSD | The Oil Run-time Compiler
orca | LGPLv2+ | Assistive technology for people with visual impairments
osbuild | Apache-2.0 | A build system for OS images
osbuild | Apache-2.0 | A build system for OS images
osbuild-composer | Apache-2.0 | An image building service based on osbuild
osbuild-composer | Apache-2.0 | An image building service based on osbuild
oscap-anaconda-addon | GPLv2+ | Anaconda addon integrating OpenSCAP to the installation process
osinfo-db | LGPLv2+ | osinfo database files
osinfo-db-tools | GPLv2+ | Tools for managing the osinfo database
ostree | LGPLv2+ | Tool for managing bootable, immutable filesystem trees
overpass-fonts | OFL or LGPLv2+ | Typeface based on the U.S. interstate highway road signage type system
owasp-java-encoder | BSD | Collection of high-performance low-overhead contextual encoders
pacemaker | GPL-2.0-or-later AND LGPL-2.1-or-later AND BSD-3-Clause | Scalable High-Availability cluster resource manager
pakchois | LGPLv2+ | A wrapper library for PKCS#11
paktype-naqsh-fonts | GPLv2 with exceptions | Fonts for Arabic from PakType
paktype-naskh-basic-fonts | GPLv2 with exceptions | Fonts for Arabic, Farsi, Urdu and Sindhi from PakType
paktype-tehreer-fonts | GPLv2 with exceptions | Fonts for Arabic from PakType
pandoc | GPLv2+ | Conversion between markup formats
pango | LGPLv2+ | System for layout and rendering of internationalized text
pangomm | LGPLv2+ | C++ interface for Pango
papi | BSD | Performance Application Programming Interface
paps | LGPLv2+ | Plain Text to PostScript converter
paratype-pt-sans-fonts | OFL | A pan-Cyrillic typeface
parfait | ASL 2.0 | Java libraries for Performance Co-Pilot (PCP)
patchutils | GPLv2+ | A collection of programs for manipulating patch files
pavucontrol | GPLv2+ | Volume control for PulseAudio
pcaudiolib | GPLv3+ | Portable C Audio Library
pcm | BSD | Intel(r) Performance Counter Monitor
pcp | GPLv2+ and LGPLv2+ and CC-BY | System-level performance monitoring and performance management
pcp | GPLv2+ and LGPLv2+ and CC-BY | System-level performance monitoring and performance management
pentaho-libxml | LGPLv2 | Namespace aware SAX-Parser utility library
pentaho-reporting-flow-engine | LGPLv2+ | Pentaho Flow Reporting Engine
peripety | MIT | Storage event notification daemon
perl | (GPL+ or Artistic) and (GPLv2+ or Artistic) and BSD and Public Domain and UCD | Practical Extraction and Report Language
perl | GPL+ or Artistic | Practical Extraction and Report Language
perl | GPL+ or Artistic | Practical Extraction and Report Language
perl-Algorithm-Diff | GPL+ or Artistic | Compute `intelligent' differences between two files/lists
perl-Algorithm-Diff | GPL+ or Artistic | Compute `intelligent' differences between two files/lists
perl-Algorithm-Diff | GPL+ or Artistic | Compute `intelligent' differences between two files/lists
perl-AnyEvent | GPL+ or Artistic | Framework for multiple event loops
perl-App-cpanminus | GPL+ or Artistic | Get, unpack, build and install CPAN modules
perl-App-cpanminus | GPL+ or Artistic | Get, unpack, build and install CPAN modules
perl-App-cpanminus | GPL+ or Artistic | Get, unpack, build and install CPAN modules
perl-App-cpanminus | GPL+ or Artistic | Get, unpack, build and install CPAN modules
perl-Archive-Tar | GPL+ or Artistic | A module for Perl manipulation of .tar files
perl-Archive-Tar | GPL+ or Artistic | A module for Perl manipulation of .tar files
perl-Archive-Tar | GPL+ or Artistic | A module for Perl manipulation of .tar files
perl-Archive-Zip | (GPL+ or Artistic) and BSD | Perl library for accessing Zip archives
perl-Archive-Zip | (GPL+ or Artistic) and BSD | Perl library for accessing Zip archives
perl-Archive-Zip | (GPL+ or Artistic) and BSD | Perl library for accessing Zip archives
perl-Archive-Zip | (GPL+ or Artistic) and BSD | Perl library for accessing Zip archives
perl-Authen-SASL | GPL+ or Artistic | SASL Authentication framework for Perl
perl-B-Debug | GPL+ or Artistic | Walk Perl syntax tree, print debug information about op-codes
perl-B-Debug | GPL+ or Artistic | Walk Perl syntax tree, print debug information about op-codes
perl-B-Hooks-EndOfScope | GPL+ or Artistic | Execute code after scope compilation finishes
perl-B-Lint | GPL+ or Artistic | Perl lint
perl-Bit-Vector | (GPLv2+ or Artistic) and LGPLv2+ | Efficient bit vector, set of integers and "big int" math library
perl-CGI | (GPL+ or Artistic) and Artistic 2.0 | Handle Common Gateway Interface requests and responses
perl-CPAN | GPL+ or Artistic | Query, download and build perl modules from CPAN sites
perl-CPAN | GPL+ or Artistic | Query, download and build perl modules from CPAN sites
perl-CPAN | GPL+ or Artistic | Query, download and build perl modules from CPAN sites
perl-CPAN | GPL+ or Artistic | Query, download and build perl modules from CPAN sites
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename
perl-CPAN-DistnameInfo | GPL+ or Artistic | Extract distribution name and version from a distribution filename
perl-CPAN-Meta | GPL+ or Artistic | Distribution metadata for a CPAN dist
perl-CPAN-Meta | GPL+ or Artistic | Distribution metadata for a CPAN dist
perl-CPAN-Meta | GPL+ or Artistic | Distribution metadata for a CPAN dist
perl-CPAN-Meta | GPL+ or Artistic | Distribution metadata for a CPAN dist
perl-CPAN-Meta-Check | GPL+ or Artistic | Verify requirements in a CPAN::Meta object
perl-CPAN-Meta-Check | GPL+ or Artistic | Verify requirements in a CPAN::Meta object
perl-CPAN-Meta-Check | GPL+ or Artistic | Verify requirements in a CPAN::Meta object
perl-CPAN-Meta-Check | GPL+ or Artistic | Verify requirements in a CPAN::Meta object
perl-CPAN-Meta-Requirements | GPL+ or Artistic | Set of version requirements for a CPAN dist
perl-CPAN-Meta-Requirements | GPL+ or Artistic | Set of version requirements for a CPAN dist
perl-CPAN-Meta-Requirements | GPL+ or Artistic | Set of version requirements for a CPAN dist
perl-CPAN-Meta-Requirements | GPL+ or Artistic | Set of version requirements for a CPAN dist
perl-CPAN-Meta-YAML | GPL+ or Artistic | Read and write a subset of YAML for CPAN Meta files
perl-CPAN-Meta-YAML | GPL+ or Artistic | Read and write a subset of YAML for CPAN Meta files
perl-CPAN-Meta-YAML | GPL+ or Artistic | Read and write a subset of YAML for CPAN Meta files
perl-CPAN-Meta-YAML | GPL+ or Artistic | Read and write a subset of YAML for CPAN Meta files
perl-Canary-Stability | GPL+ or Artistic | Canary to check perl compatibility for Schmorp's modules
perl-Capture-Tiny | ASL 2.0 | Capture STDOUT and STDERR from Perl, XS or external programs
perl-Carp | GPL+ or Artistic | Alternative warn and die for modules
perl-Carp | GPL+ or Artistic | Alternative warn and die for modules
perl-Carp | GPL+ or Artistic | Alternative warn and die for modules
perl-Carp-Clan | GPL+ or Artistic | Perl module to print improved warning messages
perl-Class-Accessor | GPL+ or Artistic | Automated accessor generation
perl-Class-Data-Inheritable | GPL+ or Artistic | Inheritable, overridable class data
perl-Class-Factory-Util | GPL+ or Artistic | Provide utility methods for factory classes
perl-Class-ISA | GPL+ or Artistic | Report the search path for a class's ISA tree
perl-Class-Inspector | GPL+ or Artistic | Get information about a class and its structure
perl-Class-Method-Modifiers | GPL+ or Artistic | Provides Moose-like method modifiers
perl-Class-Singleton | GPL+ or Artistic | Implementation of a "Singleton" class
perl-Class-Tiny | ASL 2.0 | Minimalist class construction
perl-Class-XSAccessor | GPL+ or Artistic | Generate fast XS accessors without run-time compilation
perl-Clone | GPL+ or Artistic | Recursively copy perl data types
perl-Compress-Bzip2 | GPL+ or Artistic | Interface to Bzip2 compression library
perl-Compress-Bzip2 | GPL+ or Artistic | Interface to Bzip2 compression library
perl-Compress-Bzip2 | GPL+ or Artistic | Interface to Bzip2 compression library
perl-Compress-Bzip2 | GPL+ or Artistic | Interface to Bzip2 compression library
perl-Compress-Raw-Bzip2 | GPL+ or Artistic | Low-level interface to bzip2 compression library
perl-Compress-Raw-Bzip2 | GPL+ or Artistic | Low-level interface to bzip2 compression library
perl-Compress-Raw-Bzip2 | GPL+ or Artistic | Low-level interface to bzip2 compression library
perl-Compress-Raw-Lzma | GPL+ or Artistic | Low-level interface to lzma compression library
perl-Compress-Raw-Zlib | (GPL+ or Artistic) and zlib | Low-level interface to the zlib compression library
perl-Compress-Raw-Zlib | (GPL+ or Artistic) and zlib | Low-level interface to the zlib compression library
perl-Compress-Raw-Zlib | (GPL+ or Artistic) and zlib | Low-level interface to the zlib compression library
perl-Config-AutoConf | GPL+ or Artistic | A module to implement some of AutoConf macros in pure Perl
perl-Config-Perl-V | GPL+ or Artistic | Structured data retrieval of perl -V output
perl-Config-Perl-V | GPL+ or Artistic | Structured data retrieval of perl -V output
perl-Config-Perl-V | GPL+ or Artistic | Structured data retrieval of perl -V output
perl-Config-Perl-V | GPL+ or Artistic | Structured data retrieval of perl -V output
perl-Convert-ASN1 | GPL+ or Artistic | ASN.1 encode/decode library
perl-Crypt-OpenSSL-Bignum | GPL+ or Artistic | Perl interface to OpenSSL for Bignum
perl-Crypt-OpenSSL-RSA | GPL+ or Artistic | Perl interface to OpenSSL for RSA
perl-Crypt-OpenSSL-Random | GPL+ or Artistic | OpenSSL/LibreSSL pseudo-random number generator access
perl-DBD-MySQL | GPL+ or Artistic | A MySQL interface for Perl
perl-DBD-MySQL | GPL+ or Artistic | A MySQL interface for Perl
perl-DBD-MySQL | GPL+ or Artistic | A MySQL interface for Perl
perl-DBD-MySQL | GPL+ or Artistic | A MySQL interface for Perl
perl-DBD-Pg | GPLv2+ or Artistic | A PostgreSQL interface for perl
perl-DBD-Pg | GPLv2+ or Artistic | A PostgreSQL interface for perl
perl-DBD-Pg | GPLv2+ or Artistic | A PostgreSQL interface for perl
perl-DBD-Pg | GPLv2+ or Artistic | A PostgreSQL interface for perl
perl-DBD-SQLite | (GPL+ or Artistic) and Public Domain | SQLite DBI Driver
perl-DBD-SQLite | (GPL+ or Artistic) and Public Domain | SQLite DBI Driver
perl-DBD-SQLite | (GPL+ or Artistic) and Public Domain | SQLite DBI Driver
perl-DBD-SQLite | (GPL+ or Artistic) and Public Domain | SQLite DBI Driver
perl-DBI | GPL+ or Artistic | A database access API for perl
perl-DBI | GPL+ or Artistic | A database access API for perl
perl-DBI | GPL+ or Artistic | A database access API for perl
perl-DBI | GPL+ or Artistic | A database access API for perl
perl-DB_File | GPL+ or Artistic | Perl5 access to Berkeley DB version 1.x
perl-DB_File | GPL+ or Artistic | Perl5 access to Berkeley DB version 1.x
perl-DB_File | GPL+ or Artistic | Perl5 access to Berkeley DB version 1.x
perl-DB_File | GPL+ or Artistic | Perl5 access to Berkeley DB version 1.x
perl-Data-Dump | GPL+ or Artistic | Pretty printing of data structures
perl-Data-Dump | GPL+ or Artistic | Pretty printing of data structures
perl-Data-Dump | GPL+ or Artistic | Pretty printing of data structures
perl-Data-Dump | GPL+ or Artistic | Pretty printing of data structures
perl-Data-Dumper | GPL+ or Artistic | Stringify perl data structures, suitable for printing and eval
perl-Data-Dumper | GPL+ or Artistic | Stringify perl data structures, suitable for printing and eval
perl-Data-Dumper | GPL+ or Artistic | Stringify perl data structures, suitable for printing and eval
perl-Data-OptList | GPL+ or Artistic | Parse and validate simple name/value option pairs
perl-Data-OptList | GPL+ or Artistic | Parse and validate simple name/value option pairs
perl-Data-OptList | GPL+ or Artistic | Parse and validate simple name/value option pairs
perl-Data-OptList | GPL+ or Artistic | Parse and validate simple name/value option pairs
perl-Data-Section | GPL+ or Artistic | Read multiple hunks of data out of your DATA section
perl-Data-Section | GPL+ or Artistic | Read multiple hunks of data out of your DATA section
perl-Data-Section | GPL+ or Artistic | Read multiple hunks of data out of your DATA section
perl-Data-Section | GPL+ or Artistic | Read multiple hunks of data out of your DATA section
perl-Data-UUID | BSD and MIT | Globally/Universally Unique Identifiers (GUIDs/UUIDs)
perl-Date-Calc | GPL+ or Artistic | Gregorian calendar date calculations
perl-Date-ISO8601 | GPL+ or Artistic | Three ISO 8601 numerical calendars
perl-DateTime | Artistic 2.0 | Date and time object for Perl
perl-DateTime-Format-Builder | Artistic 2.0 and (GPL+ or Artistic) | Create DateTime parser classes and objects
perl-DateTime-Format-HTTP | GPL+ or Artistic | HTTP protocol date conversion routines
perl-DateTime-Format-ISO8601 | GPL+ or Artistic | Parses ISO8601 formats
perl-DateTime-Format-Mail | GPL+ or Artistic | Convert between DateTime and RFC2822/822 formats
perl-DateTime-Format-Strptime | Artistic 2.0 | Parse and format strptime and strftime patterns
perl-DateTime-Locale | (GPL+ or Artistic) and Unicode | Localization support for DateTime.pm
perl-DateTime-TimeZone | (GPL+ or Artistic) and Public Domain | Time zone object base class and factory
perl-DateTime-TimeZone-SystemV | GPL+ or Artistic | System V and POSIX timezone strings
perl-DateTime-TimeZone-Tzfile | GPL+ or Artistic | Tzfile (zoneinfo) timezone files
perl-Devel-CallChecker | GPL+ or Artistic | Custom op checking attached to subroutines
perl-Devel-Caller | GPL+ or Artistic | Meatier versions of caller
perl-Devel-CheckLib | GPL+ or Artistic | Check that a library is available
perl-Devel-GlobalDestruction | GPL+ or Artistic | Expose PL_dirty, the flag that marks global destruction
perl-Devel-LexAlias | GPL+ or Artistic | Alias lexical variables
perl-Devel-PPPort | GPL+ or Artistic | Perl Pollution Portability header generator
perl-Devel-PPPort | GPL+ or Artistic | Perl Pollution Portability header generator
perl-Devel-PPPort | GPL+ or Artistic | Perl Pollution Portability header generator
perl-Devel-PPPort | GPL+ or Artistic | Perl Pollution Portability header generator
perl-Devel-Size | GPL+ or Artistic | Perl extension for finding the memory usage of Perl variables
perl-Devel-Size | GPL+ or Artistic | Perl extension for finding the memory usage of Perl variables
perl-Devel-Size | GPL+ or Artistic | Perl extension for finding the memory usage of Perl variables
perl-Devel-Size | GPL+ or Artistic | Perl extension for finding the memory usage of Perl variables
perl-Devel-StackTrace | Artistic 2.0 | Perl module implementing stack trace and stack trace frame objects
perl-Devel-Symdump | GPL+ or Artistic | A Perl module for inspecting Perl's symbol table
perl-Digest | GPL+ or Artistic | Modules that calculate message digests
perl-Digest | GPL+ or Artistic | Modules that calculate message digests
perl-Digest | GPL+ or Artistic | Modules that calculate message digests
perl-Digest-CRC | Public Domain | Generic CRC functions
perl-Digest-HMAC | GPL+ or Artistic | Keyed-Hashing for Message Authentication
perl-Digest-HMAC | GPL+ or Artistic | Keyed-Hashing for Message Authentication
perl-Digest-HMAC | GPL+ or Artistic | Keyed-Hashing for Message Authentication
perl-Digest-HMAC | GPL+ or Artistic | Keyed-Hashing for Message Authentication
perl-Digest-MD5 | (GPL+ or Artistic) and BSD | Perl interface to the MD5 algorithm
perl-Digest-MD5 | (GPL+ or Artistic) and BSD | Perl interface to the MD5 algorithm
perl-Digest-MD5 | (GPL+ or Artistic) and BSD | Perl interface to the MD5 algorithm
perl-Digest-SHA | GPL+ or Artistic | Perl extension for SHA-1/224/256/384/512
perl-Digest-SHA | GPL+ or Artistic | Perl extension for SHA-1/224/256/384/512
perl-Digest-SHA | GPL+ or Artistic | Perl extension for SHA-1/224/256/384/512
perl-Digest-SHA | GPL+ or Artistic | Perl extension for SHA-1/224/256/384/512
perl-Digest-SHA1 | GPL+ or Artistic | Digest-SHA1 Perl module
perl-Dist-CheckConflicts | GPL+ or Artistic | Declare version conflicts for your dist
perl-DynaLoader-Functions | GPL+ or Artistic | Deconstructed dynamic C library loading
perl-Encode | (GPL+ or Artistic) and Artistic 2.0 and UCD | Character encodings in Perl
perl-Encode | (GPL+ or Artistic) and Artistic 2.0 and UCD | Character encodings in Perl
perl-Encode | (GPL+ or Artistic) and Artistic 2.0 and UCD | Character encodings in Perl
perl-Encode-Detect | MPLv1.1 or GPLv2+ or LGPLv2+ | Encode::Encoding subclass that detects the encoding of data
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding
perl-Encode-Locale | GPL+ or Artistic | Determine the locale encoding
perl-Env | GPL+ or Artistic | Perl module that imports environment variables as scalars or arrays
perl-Env | GPL+ or Artistic | Perl module that imports environment variables as scalars or arrays
perl-Env | GPL+ or Artistic | Perl module that imports environment variables as scalars or arrays
perl-Env | GPL+ or Artistic | Perl module that imports environment variables as scalars or arrays
perl-Error | (GPL+ or Artistic) and MIT | Error/exception handling in an OO-ish way
perl-Eval-Closure | GPL+ or Artistic | Safely and cleanly create closures via string eval
perl-Exception-Class | GPL+ or Artistic | Module that allows you to declare real exception classes in Perl
perl-Exporter | GPL+ or Artistic | Implements default import method for modules
perl-Exporter | GPL+ or Artistic | Implements default import method for modules
perl-Exporter | GPL+ or Artistic | Implements default import method for modules
perl-Exporter-Tiny | GPL+ or Artistic | An exporter with the features of Sub::Exporter but only core dependencies
perl-ExtUtils-CBuilder | GPL+ or Artistic | Compile and link C code for Perl modules
perl-ExtUtils-CBuilder | GPL+ or Artistic | Compile and link C code for Perl modules
perl-ExtUtils-CBuilder | GPL+ or Artistic | Compile and link C code for Perl modules
perl-ExtUtils-CBuilder | GPL+ or Artistic | Compile and link C code for Perl modules
perl-ExtUtils-Install | GPL+ or Artistic | Install Perl files from here to there
perl-ExtUtils-Install | GPL+ or Artistic | Install Perl files from here to there
perl-ExtUtils-Install | GPL+ or Artistic | Install Perl files from here to there
perl-ExtUtils-Install | GPL+ or Artistic | Install Perl files from here to there
perl-ExtUtils-MakeMaker | GPL+ or Artistic | Create a module Makefile
perl-ExtUtils-MakeMaker | GPL+ or Artistic | Create a module Makefile
perl-ExtUtils-MakeMaker | GPL+ or Artistic | Create a module Makefile
perl-ExtUtils-MakeMaker | GPL+ or Artistic | Create a module Makefile
perl-ExtUtils-Manifest | GPL+ or Artistic | Utilities to write and check a MANIFEST file
perl-ExtUtils-Manifest | GPL+ or Artistic | Utilities to write and check a MANIFEST file
perl-ExtUtils-Manifest | GPL+ or Artistic | Utilities to write and check a MANIFEST file
perl-ExtUtils-Manifest | GPL+ or Artistic | Utilities to write and check a MANIFEST file
perl-ExtUtils-ParseXS | GPL+ or Artistic | Module and a script for converting Perl XS code into C code
perl-ExtUtils-ParseXS | GPL+ or Artistic | Module and a script for converting Perl XS code into C code
perl-ExtUtils-ParseXS | GPL+ or Artistic | Module and a script for converting Perl XS code into C code
perl-ExtUtils-ParseXS | GPL+ or Artistic | Module and a script for converting Perl XS code into C code
perl-FCGI | OML | FastCGI Perl bindings
perl-FCGI | OML | FastCGI Perl bindings
perl-FCGI | OML | FastCGI Perl bindings
perl-FCGI | OML | FastCGI Perl bindings
perl-Fedora-VSP | GPLv3+ | Perl version normalization for RPM
perl-Fedora-VSP | GPLv3+ | Perl version normalization for RPM
perl-Fedora-VSP | GPLv3+ | Perl version normalization for RPM
perl-Fedora-VSP | GPLv3+ | Perl version normalization for RPM
perl-File-BaseDir | GPL+ or Artistic | Use the Freedesktop.org base directory specification
perl-File-CheckTree | GPL+ or Artistic | Run many file-test checks on a tree
perl-File-Copy-Recursive | GPL+ or Artistic | Extension for recursively copying files and directories
perl-File-DesktopEntry | GPL+ or Artistic | Object to handle .desktop files
perl-File-Fetch | GPL+ or Artistic | Generic file fetching mechanism
perl-File-Fetch | GPL+ or Artistic | Generic file fetching mechanism
perl-File-Fetch | GPL+ or Artistic | Generic file fetching mechanism
perl-File-Fetch | GPL+ or Artistic | Generic file fetching mechanism
perl-File-Find-Object | GPLv2+ or Artistic 2.0 | Object oriented File::Find replacement
perl-File-Find-Rule | GPL+ or Artistic | Perl module implementing an alternative interface to File::Find
perl-File-HomeDir | GPL+ or Artistic | Find your home and other directories on any platform
perl-File-HomeDir | GPL+ or Artistic | Find your home and other directories on any platform
perl-File-HomeDir | GPL+ or Artistic | Find your home and other directories on any platform
perl-File-HomeDir | GPL+ or Artistic | Find your home and other directories on any platform
perl-File-Listing | GPL+ or Artistic | Parse directory listing
perl-File-Listing | GPL+ or Artistic | Parse directory listing
perl-File-Listing | GPL+ or Artistic | Parse directory listing
perl-File-Listing | GPL+ or Artistic | Parse directory listing
perl-File-MimeInfo | GPL+ or Artistic | Determine file type and open application
perl-File-Path | GPL+ or Artistic | Create or remove directory trees
perl-File-Path | GPL+ or Artistic | Create or remove directory trees
perl-File-Path | GPL+ or Artistic | Create or remove directory trees
perl-File-ReadBackwards | GPL+ or Artistic | Read a file backwards by lines
perl-File-Remove | GPL+ or Artistic | Convenience module for removing files and directories
perl-File-ShareDir | GPL+ or Artistic | Locate per-dist and per-module shared files
perl-File-Slurp | GPL+ or Artistic | Efficient Reading/Writing of Complete Files
perl-File-Temp | GPL+ or Artistic | Return name and handle of a temporary file safely
perl-File-Temp | GPL+ or Artistic | Return name and handle of a temporary file safely
perl-File-Temp | GPL+ or Artistic | Return name and handle of a temporary file safely
perl-File-Which | GPL+ or Artistic | Portable implementation of the 'which' utility
perl-File-Which | GPL+ or Artistic | Portable implementation of the 'which' utility
perl-File-Which | GPL+ or Artistic | Portable implementation of the 'which' utility
perl-File-Which | GPL+ or Artistic | Portable implementation of the 'which' utility
perl-File-chdir | GPL+ or Artistic | A more sensible way to change directories
perl-File-pushd | ASL 2.0 | Change directory temporarily for a limited scope
perl-File-pushd | ASL 2.0 | Change directory temporarily for a limited scope
perl-File-pushd | ASL 2.0 | Change directory temporarily for a limited scope
perl-File-pushd | ASL 2.0 | Change directory temporarily for a limited scope
perl-Filter | GPL+ or Artistic | Perl source filters
perl-Filter | GPL+ or Artistic | Perl source filters
perl-Filter | GPL+ or Artistic | Perl source filters
perl-Filter | GPL+ or Artistic | Perl source filters
perl-Filter-Simple | GPL+ or Artistic | Simplified Perl source filtering
perl-Filter-Simple | GPL+ or Artistic | Simplified Perl source filtering
perl-Filter-Simple | GPL+ or Artistic | Simplified Perl source filtering
perl-Filter-Simple | GPL+ or Artistic | Simplified Perl source filtering
perl-GSSAPI | GPL+ or Artistic | Perl extension providing access to the GSSAPIv2 library
perl-Getopt-Long | GPLv2+ or Artistic | Extended processing of command line options
perl-Getopt-Long | GPLv2+ or Artistic | Extended processing of command line options
perl-Getopt-Long | GPLv2+ or Artistic | Extended processing of command line options
perl-HTML-Parser | GPL+ or Artistic | Perl module for parsing HTML
perl-HTML-Parser | GPL+ or Artistic | Perl module for parsing HTML
perl-HTML-Parser | GPL+ or Artistic | Perl module for parsing HTML
perl-HTML-Parser | GPL+ or Artistic | Perl module for parsing HTML
perl-HTML-Tagset | GPL+ or Artistic | HTML::Tagset - data tables useful in parsing HTML
perl-HTML-Tagset | GPL+ or Artistic | HTML::Tagset - data tables useful in parsing HTML
perl-HTML-Tagset | GPL+ or Artistic | HTML::Tagset - data tables useful in parsing HTML
perl-HTML-Tagset | GPL+ or Artistic | HTML::Tagset - data tables useful in parsing HTML
perl-HTML-Tree | GPL+ or Artistic | HTML tree handling modules for Perl
perl-HTTP-Cookies | GPL+ or Artistic | HTTP cookie jars
perl-HTTP-Cookies | GPL+ or Artistic | HTTP cookie jars
perl-HTTP-Cookies | GPL+ or Artistic | HTTP cookie jars
perl-HTTP-Cookies | GPL+ or Artistic | HTTP cookie jars
perl-HTTP-Daemon | GPL+ or Artistic | Simple HTTP server class
perl-HTTP-Date | GPL+ or Artistic | Date conversion routines
perl-HTTP-Date | GPL+ or Artistic | Date conversion routines
perl-HTTP-Date | GPL+ or Artistic | Date conversion routines
perl-HTTP-Date | GPL+ or Artistic | Date conversion routines
perl-HTTP-Message | GPL+ or Artistic | HTTP style message
perl-HTTP-Message | GPL+ or Artistic | HTTP style message
perl-HTTP-Message | GPL+ or Artistic | HTTP style message
perl-HTTP-Message | GPL+ or Artistic | HTTP style message
perl-HTTP-Negotiate | GPL+ or Artistic | Choose a variant to serve
perl-HTTP-Negotiate | GPL+ or Artistic | Choose a variant to serve
perl-HTTP-Negotiate | GPL+ or Artistic | Choose a variant to serve
perl-HTTP-Negotiate | GPL+ or Artistic | Choose a variant to serve
perl-HTTP-Tiny | GPL+ or Artistic | Small, simple, correct HTTP/1.1 client
perl-HTTP-Tiny | GPL+ or Artistic | Small, simple, correct HTTP/1.1 client
perl-HTTP-Tiny | GPL+ or Artistic | Small, simple, correct HTTP/1.1 client
perl-IO-All | GPL+ or Artistic | IO::All Perl module
perl-IO-Compress | GPL+ or Artistic | Read and write compressed data
perl-IO-Compress | GPL+ or Artistic | Read and write compressed data
perl-IO-Compress | GPL+ or Artistic | Read and write compressed data
perl-IO-Compress-Lzma | GPL+ or Artistic | Read and write lzma compressed data
perl-IO-HTML | GPL+ or Artistic | Open an HTML file with automatic character set detection
perl-IO-HTML | GPL+ or Artistic | Open an HTML file with automatic character set detection
perl-IO-HTML | GPL+ or Artistic | Open an HTML file with automatic character set detection
perl-IO-HTML | GPL+ or Artistic | Open an HTML file with automatic character set detection
perl-IO-Multiplex | GPL+ or Artistic | Manage IO on many file handles
perl-IO-Socket-INET6 | GPL+ or Artistic | Perl Object interface for AF_INET|AF_INET6 domain sockets
perl-IO-Socket-IP | GPL+ or Artistic | Drop-in replacement for IO::Socket::INET supporting both IPv4 and IPv6
perl-IO-Socket-IP | GPL+ or Artistic | Drop-in replacement for IO::Socket::INET supporting both IPv4 and IPv6
perl-IO-Socket-IP | GPL+ or Artistic | Drop-in replacement for IO::Socket::INET supporting both IPv4 and IPv6
perl-IO-Socket-SSL | (GPL+ or Artistic) and MPLv2.0 | Perl library for transparent SSL
perl-IO-Socket-SSL | (GPL+ or Artistic) and MPLv2.0 | Perl library for transparent SSL
perl-IO-Socket-SSL | (GPL+ or Artistic) and MPLv2.0 | Perl library for transparent SSL
perl-IO-Socket-SSL | (GPL+ or Artistic) and MPLv2.0 | Perl library for transparent SSL
perl-IO-String | GPL+ or Artistic | Emulate file interface for in-core strings
perl-IO-Tty | (GPL+ or Artistic) and BSD | Perl interface to pseudo tty's
perl-IO-stringy | GPL+ or Artistic | I/O on in-core objects like strings and arrays for Perl
perl-IPC-Cmd | GPL+ or Artistic | Finding and running system commands made easy
perl-IPC-Cmd | GPL+ or Artistic | Finding and running system commands made easy
perl-IPC-Cmd | GPL+ or Artistic | Finding and running system commands made easy
perl-IPC-Cmd | GPL+ or Artistic | Finding and running system commands made easy
perl-IPC-Run | GPL+ or Artistic | Perl module for interacting with child processes
perl-IPC-Run3 | GPL+ or Artistic or BSD | Run a subprocess in batch mode
perl-IPC-SysV | GPL+ or Artistic | Object interface to System V IPC
perl-IPC-SysV | GPL+ or Artistic | Object interface to System V IPC
perl-IPC-SysV | GPL+ or Artistic | Object interface to System V IPC
perl-IPC-SysV | GPL+ or Artistic | Object interface to System V IPC
perl-IPC-System-Simple | GPL+ or Artistic | Run commands simply, with detailed diagnostics
perl-IPC-System-Simple | GPL+ or Artistic | Run commands simply, with detailed diagnostics
perl-IPC-System-Simple | GPL+ or Artistic | Run commands simply, with detailed diagnostics
perl-IPC-System-Simple | GPL+ or Artistic | Run commands simply, with detailed diagnostics
perl-Import-Into | GPL+ or Artistic | Import packages into other packages
perl-Importer | GPL+ or Artistic | Alternative interface to modules that export symbols
perl-Importer | GPL+ or Artistic | Alternative interface to modules that export symbols
perl-Importer | GPL+ or Artistic | Alternative interface to modules that export symbols
perl-JSON | GPL+ or Artistic | Parse and convert to JSON (JavaScript Object Notation)
perl-JSON-PP | GPL+ or Artistic | JSON::XS compatible pure-Perl module
perl-JSON-PP | GPL+ or Artistic | JSON::XS compatible pure-Perl module
perl-JSON-PP | GPL+ or Artistic | JSON::XS compatible pure-Perl module
perl-JSON-PP | GPL+ or Artistic | JSON::XS compatible pure-Perl module
perl-JSON-XS | GPL+ or Artistic | JSON serializing/de-serializing, done correctly and fast
perl-LDAP | GPL+ or Artistic | LDAP Perl module
perl-LWP-MediaTypes | (GPL+ or Artistic) and Public Domain | Guess media type for a file or a URL
perl-LWP-MediaTypes | (GPL+ or Artistic) and Public Domain | Guess media type for a file or a URL
perl-LWP-MediaTypes | (GPL+ or Artistic) and Public Domain | Guess media type for a file or a URL
perl-LWP-MediaTypes | (GPL+ or Artistic) and Public Domain | Guess media type for a file or a URL
perl-LWP-Protocol-https | GPL+ or Artistic | Provide HTTPS support for LWP::UserAgent
perl-LWP-Protocol-https | GPL+ or Artistic | Provide HTTPS support for LWP::UserAgent
perl-LWP-Protocol-https | GPL+ or Artistic | Provide HTTPS support for LWP::UserAgent
perl-LWP-Protocol-https | GPL+ or Artistic | Provide HTTPS support for LWP::UserAgent
perl-List-MoreUtils | (GPL+ or Artistic) and ASL 2.0 | Provide the stuff missing in List::Util
perl-List-MoreUtils-XS | (GPL+ or Artistic) and ASL 2.0 | Provide compiled List::MoreUtils functions
perl-Locale-Codes | GPL+ or Artistic | Distribution of modules to handle locale codes
perl-Locale-Codes | GPL+ or Artistic | Distribution of modules to handle locale codes
perl-Locale-Maketext | GPL+ or Artistic | Framework for localization
perl-Locale-Maketext | GPL+ or Artistic | Framework for localization
perl-Locale-Maketext | GPL+ or Artistic | Framework for localization
perl-Locale-Maketext | GPL+ or Artistic | Framework for localization
perl-MIME-Base64 | (GPL+ or Artistic) and MIT | Encoding and decoding of Base64 and quoted-printable strings
perl-MIME-Base64 | (GPL+ or Artistic) and MIT | Encoding and decoding of Base64 and quoted-printable strings
perl-MIME-Base64 | (GPL+ or Artistic) and MIT | Encoding and decoding of Base64 and quoted-printable strings
perl-MIME-Charset | GPL+ or Artistic | Charset Informations for MIME
perl-MIME-Types | GPL+ or Artistic | MIME types module for Perl
perl-MRO-Compat | GPL+ or Artistic | Mro::* interface compatibility for Perls < 5.9.5
perl-MRO-Compat | GPL+ or Artistic | Mro::* interface compatibility for Perls < 5.9.5
perl-MRO-Compat | GPL+ or Artistic | Mro::* interface compatibility for Perls < 5.9.5
perl-MRO-Compat | GPL+ or Artistic | Mro::* interface compatibility for Perls < 5.9.5
perl-Mail-AuthenticationResults | GPL+ or Artistic | Object Oriented Authentication-Results Headers
perl-Mail-DKIM | GPL+ or Artistic | Sign and verify Internet mail with DKIM/DomainKey signatures
perl-Mail-SPF | BSD | Object-oriented implementation of Sender Policy Framework
perl-Mail-Sender | GPL+ or Artistic | Module for sending mails with attachments through an SMTP server
perl-MailTools | GPL+ or Artistic | Various ancient mail-related perl modules
perl-Math-BigInt | GPL+ or Artistic | Arbitrary-size integer and float mathematics
perl-Math-BigInt | GPL+ or Artistic | Arbitrary-size integer and float mathematics
perl-Math-BigInt | GPL+ or Artistic | Arbitrary-size integer and float mathematics
perl-Math-BigInt-FastCalc | GPL+ or Artistic | Math::BigInt::Calc with some XS for more speed
perl-Math-BigInt-FastCalc | GPL+ or Artistic | Math::BigInt::Calc with some XS for more speed
perl-Math-BigInt-FastCalc | GPL+ or Artistic | Math::BigInt::Calc with some XS for more speed
perl-Math-BigInt-FastCalc | GPL+ or Artistic | Math::BigInt::Calc with some XS for more speed
perl-Math-BigRat | GPL+ or Artistic | Arbitrary big rational numbers
perl-Math-BigRat | GPL+ or Artistic | Arbitrary big rational numbers
perl-Math-BigRat | GPL+ or Artistic | Arbitrary big rational numbers
perl-Math-BigRat | GPL+ or Artistic | Arbitrary big rational numbers
perl-Module-Build | GPL+ or Artistic | Build and install Perl modules
perl-Module-Build | GPL+ or Artistic | Build and install Perl modules
perl-Module-Build | GPL+ or Artistic | Build and install Perl modules
perl-Module-Build | GPL+ or Artistic | Build and install Perl modules
perl-Module-CPANfile | GPL+ or Artistic | Parse cpanfile
perl-Module-CPANfile | GPL+ or Artistic | Parse cpanfile
perl-Module-CPANfile | GPL+ or Artistic | Parse cpanfile
perl-Module-CPANfile | GPL+ or Artistic | Parse cpanfile
perl-Module-CoreList | GPL+ or Artistic | What modules are shipped with versions of perl
perl-Module-CoreList | GPL+ or Artistic | What modules are shipped with versions of perl
perl-Module-CoreList | GPL+ or Artistic | What modules are shipped with versions of perl
perl-Module-CoreList | GPL+ or Artistic | What modules are shipped with versions of perl
perl-Module-Implementation | Artistic 2.0 | Loads one of several alternate underlying implementations for a module
perl-Module-Install | GPL+ or Artistic | Standalone, extensible Perl module installer
perl-Module-Install-AuthorTests | GPL+ or Artistic | Designate tests only run by module authors
perl-Module-Install-ReadmeFromPod | GPL+ or Artistic | Module::Install extension to automatically convert POD to a README
perl-Module-Load | GPL+ or Artistic | Run-time require of both modules and files
perl-Module-Load | GPL+ or Artistic | Run-time require of both modules and files
perl-Module-Load | GPL+ or Artistic | Run-time require of both modules and files
perl-Module-Load | GPL+ or Artistic | Run-time require of both modules and files
perl-Module-Load-Conditional | GPL+ or Artistic | Looking up module information and loading at run-time
perl-Module-Load-Conditional | GPL+ or Artistic | Looking up module information and loading at run-time
perl-Module-Load-Conditional | GPL+ or Artistic | Looking up module information / loading at run-time
perl-Module-Load-Conditional | GPL+ or Artistic | Looking up module information / loading at run-time
perl-Module-Metadata | GPL+ or Artistic | Gather package and POD information from perl module files
perl-Module-Metadata | GPL+ or Artistic | Gather package and POD information from perl module files
perl-Module-Metadata | GPL+ or Artistic | Gather package and POD information from perl module files
perl-Module-Metadata | GPL+ or Artistic | Gather package and POD information from perl module files
perl-Module-Pluggable | GPL+ or Artistic | Automatically give your module the ability to have plugins
perl-Module-Runtime | GPL+ or Artistic | Runtime module handling
perl-Module-ScanDeps | GPL+ or Artistic | Recursively scan Perl code for dependencies
perl-Mozilla-CA | MPLv2.0 | Mozilla's CA cert bundle in PEM format
perl-Mozilla-CA | MPLv2.0 | Mozilla's CA cert bundle in PEM format
perl-Mozilla-CA | MPLv2.0 | Mozilla's CA cert bundle in PEM format
perl-Mozilla-CA | MPLv2.0 | Mozilla's CA cert bundle in PEM format
perl-Mozilla-LDAP | GPLv2+ and LGPLv2+ and MPLv1.1 | LDAP Perl module that wraps the OpenLDAP C SDK
perl-NTLM | GPL+ or Artistic | NTLM Perl module
perl-NTLM | GPL+ or Artistic | NTLM Perl module
perl-NTLM | GPL+ or Artistic | NTLM Perl module
perl-NTLM | GPL+ or Artistic | NTLM Perl module
perl-Net-DNS | (GPL+ or Artistic) and MIT | DNS resolver modules for Perl
perl-Net-HTTP | GPL+ or Artistic | Low-level HTTP connection (client)
perl-Net-HTTP | GPL+ or Artistic | Low-level HTTP connection (client)
perl-Net-HTTP | GPL+ or Artistic | Low-level HTTP connection (client)
perl-Net-HTTP | GPL+ or Artistic | Low-level HTTP connection (client)
perl-Net-SMTP-SSL | GPL+ or Artistic | SSL support for Net::SMTP
perl-Net-SSLeay | Artistic 2.0 | Perl extension for using OpenSSL
perl-Net-SSLeay | Artistic 2.0 | Perl extension for using OpenSSL
perl-Net-SSLeay | Artistic 2.0 | Perl extension for using OpenSSL
perl-Net-SSLeay | Artistic 2.0 | Perl extension for using OpenSSL
perl-Net-Server | GPL+ or Artistic | Extensible, general Perl server engine
perl-NetAddr-IP | GPLv2+ and (GPLv2+ or Artistic clarified) | Manages IPv4 and IPv6 addresses and subnets
perl-Number-Compare | GPL+ or Artistic | Perl module for numeric comparisons
perl-Object-HashBase | GPL+ or Artistic | Build hash-based classes
perl-Object-HashBase | GPL+ or Artistic | Build hash-based classes
perl-Package-DeprecationManager | Artistic 2.0 | Manage deprecation warnings for your distribution
perl-Package-Generator | GPL+ or Artistic | Generate new packages quickly and easily
perl-Package-Generator | GPL+ or Artistic | Generate new packages quickly and easily
perl-Package-Generator | GPL+ or Artistic | Generate new packages quickly and easily
perl-Package-Generator | GPL+ or Artistic | Generate new packages quickly and easily
perl-Package-Stash | GPL+ or Artistic | Routines for manipulating stashes
perl-Package-Stash-XS | GPL+ or Artistic | Faster and more correct implementation of the Package::Stash API
perl-PadWalker | GPL+ or Artistic | Play with other people's lexical variables
perl-Params-Check | GPL+ or Artistic | Generic input parsing/checking mechanism
perl-Params-Check | GPL+ or Artistic | Generic input parsing/checking mechanism
perl-Params-Check | GPL+ or Artistic | Generic input parsing/checking mechanism
perl-Params-Check | GPL+ or Artistic | Generic input parsing/checking mechanism
perl-Params-Classify | GPL+ or Artistic | Argument type classification
perl-Params-Util | GPL+ or Artistic | Simple standalone parameter-checking functions
perl-Params-Util | GPL+ or Artistic | Simple standalone parameter-checking functions
perl-Params-Util | GPL+ or Artistic | Simple standalone parameter-checking functions
perl-Params-Util | GPL+ or Artistic | Simple standalone parameter-checking functions
perl-Params-Validate | Artistic 2.0 and (GPL+ or Artistic) | Params-Validate Perl module
perl-Params-ValidationCompiler | Artistic 2.0 | Build an optimized subroutine parameter validator once, use it forever
perl-Parse-PMFile | GPL+ or Artistic | Parses .pm file as PAUSE does
perl-Parse-PMFile | GPL+ or Artistic | Parses .pm file as PAUSE does
perl-Parse-PMFile | GPL+ or Artistic | Parses .pm file as PAUSE does
perl-Parse-PMFile | GPL+ or Artistic | Parses .pm file as PAUSE does
perl-Path-Tiny | ASL 2.0 | File path utility
perl-PathTools | (GPL+ or Artistic) and BSD | PathTools Perl module (Cwd, File::Spec)
perl-PathTools | (GPL+ or Artistic) and BSD | PathTools Perl module (Cwd, File::Spec)
perl-PathTools | (GPL+ or Artistic) and BSD | PathTools Perl module (Cwd, File::Spec)
perl-Perl-Destruct-Level | GPL+ or Artistic | Allows you to change perl's internal destruction level
perl-Perl-OSType | GPL+ or Artistic | Map Perl operating system names to generic types
perl-Perl-OSType | GPL+ or Artistic | Map Perl operating system names to generic types
perl-Perl-OSType | GPL+ or Artistic | Map Perl operating system names to generic types
perl-Perl-OSType | GPL+ or Artistic | Map Perl operating system names to generic types
perl-PerlIO-utf8_strict | GPL+ or Artistic | Fast and correct UTF-8 I/O
perl-PerlIO-via-QuotedPrint | GPL+ or Artistic | PerlIO layer for quoted-printable strings
perl-PerlIO-via-QuotedPrint | GPL+ or Artistic | PerlIO layer for quoted-printable strings
perl-PerlIO-via-QuotedPrint | GPL+ or Artistic | PerlIO layer for quoted-printable strings
perl-PerlIO-via-QuotedPrint | GPL+ or Artistic | PerlIO layer for quoted-printable strings
perl-Pod-Checker | GPL+ or Artistic | Check POD documents for syntax errors
perl-Pod-Checker | GPL+ or Artistic | Check POD documents for syntax errors
perl-Pod-Checker | GPL+ or Artistic | Check POD documents for syntax errors
perl-Pod-Checker | GPL+ or Artistic | Check POD documents for syntax errors
perl-Pod-Coverage | GPL+ or Artistic | Checks if the documentation of a module is comprehensive
perl-Pod-Escapes | GPL+ or Artistic | Resolve POD escape sequences
perl-Pod-Escapes | GPL+ or Artistic | Resolve POD escape sequences
perl-Pod-Escapes | GPL+ or Artistic | Resolve POD escape sequences
perl-Pod-LaTeX | GPL+ or Artistic | Convert POD data to formatted LaTeX
perl-Pod-Markdown | GPL+ or Artistic | Convert POD to Markdown
perl-Pod-Parser | GPL+ or Artistic | Basic perl modules for handling Plain Old Documentation (POD)
perl-Pod-Parser | GPL+ or Artistic | Basic perl modules for handling Plain Old Documentation (POD)
perl-Pod-Parser | GPL+ or Artistic | Basic perl modules for handling Plain Old Documentation (POD)
perl-Pod-Parser | GPL+ or Artistic | Basic perl modules for handling Plain Old Documentation (POD)
perl-Pod-Perldoc | GPL+ or Artistic | Look up Perl documentation in Pod format
perl-Pod-Perldoc | GPL+ or Artistic | Look up Perl documentation in Pod format
perl-Pod-Perldoc | GPL+ or Artistic | Look up Perl documentation in Pod format
perl-Pod-Plainer | GPL+ or Artistic | Perl extension for converting Pod to old-style Pod
perl-Pod-Simple | GPL+ or Artistic | Framework for parsing POD documentation
perl-Pod-Simple | GPL+ or Artistic | Framework for parsing POD documentation
perl-Pod-Simple | GPL+ or Artistic | Framework for parsing POD documentation
perl-Pod-Usage | GPL+ or Artistic | Print a usage message from embedded POD documentation
perl-Pod-Usage | GPL+ or Artistic | Print a usage message from embedded POD documentation
perl-Pod-Usage | GPL+ or Artistic | Print a usage message from embedded POD documentation
perl-Readonly | GPL+ or Artistic | Facility for creating read-only scalars, arrays, hashes
perl-Ref-Util | MIT | Utility functions for checking references
perl-Ref-Util-XS | MIT | Utility functions for checking references
perl-Role-Tiny | GPL+ or Artistic | A nouvelle cuisine portion size slice of Moose
perl-SGMLSpm | GPLv2+ | Perl library for parsing the output of nsgmls
perl-SNMP_Session | Artistic 2.0 | SNMP support for Perl 5
perl-SUPER | GPL+ or Artistic | Sane superclass method dispatcher
perl-Scalar-List-Utils | GPL+ or Artistic | A selection of general-utility scalar and list subroutines
perl-Scalar-List-Utils | GPL+ or Artistic | A selection of general-utility scalar and list subroutines
perl-Scalar-List-Utils | GPL+ or Artistic | A selection of general-utility scalar and list subroutines
perl-Scope-Guard | GPL+ or Artistic | Lexically scoped resource management
perl-Socket | GPL+ or Artistic | Networking constants and support functions
perl-Socket | GPL+ or Artistic | Networking constants and support functions
perl-Socket | GPL+ or Artistic | Networking constants and support functions
perl-Socket6 | BSD | IPv6 related part of the C socket.h defines and structure manipulators
perl-Software-License | GPL+ or Artistic | Package that provides templated software licenses
perl-Software-License | GPL+ or Artistic | Package that provides templated software licenses
perl-Software-License | GPL+ or Artistic | Package that provides templated software licenses
perl-Software-License | GPL+ or Artistic | Package that provides templated software licenses
perl-Specio | Artistic 2.0 | Type constraints and coercions for Perl
perl-Storable | GPL+ or Artistic | Persistence for Perl data structures
perl-Storable | GPL+ or Artistic | Persistence for Perl data structures
perl-Storable | GPL+ or Artistic | Persistence for Perl data structures
perl-String-CRC32 | Public Domain | Perl interface for cyclic redundancy check generation
perl-String-ShellQuote | (GPL+ or Artistic) and GPLv2+ | Perl module for quoting strings for passing through the shell
perl-String-ShellQuote | (GPL+ or Artistic) and GPLv2+ | Perl module for quoting strings for passing through the shell
perl-String-ShellQuote | (GPL+ or Artistic) and GPLv2+ | Perl module for quoting strings for passing through the shell
perl-String-ShellQuote | (GPL+ or Artistic) and GPLv2+ | Perl module for quoting strings for passing through the shell
perl-Sub-Exporter | GPL+ or Artistic | Sophisticated exporter for custom-built routines
perl-Sub-Exporter | GPL+ or Artistic | Sophisticated exporter for custom-built routines
perl-Sub-Exporter | GPL+ or Artistic | Sophisticated exporter for custom-built routines
perl-Sub-Exporter | GPL+ or Artistic | Sophisticated exporter for custom-built routines
perl-Sub-Exporter-Progressive | GPL+ or Artistic | Only use Sub::Exporter if you need it
perl-Sub-Identify | GPL+ or Artistic | Retrieve names of code references
perl-Sub-Info | GPL+ or Artistic | Tool for inspecting Perl subroutines
perl-Sub-Install | GPL+ or Artistic | Install subroutines into packages easily
perl-Sub-Install | GPL+ or Artistic | Install subroutines into packages easily
perl-Sub-Install | GPL+ or Artistic | Install subroutines into packages easily
perl-Sub-Install | GPL+ or Artistic | Install subroutines into packages easily
perl-Sub-Name | GPL+ or Artistic | Name - or rename - a sub
perl-Sub-Uplevel | GPL+ or Artistic | Apparently run a function in a higher stack frame
perl-Switch | GPL+ or Artistic | A switch statement for Perl
perl-Sys-Syslog | GPL+ or Artistic | Perl interface to the UNIX syslog(3) calls
perl-Sys-Syslog | GPL+ or Artistic | Perl interface to the UNIX syslog(3) calls
perl-Sys-Syslog | GPL+ or Artistic | Perl interface to the UNIX syslog(3) calls
perl-Sys-Syslog | GPL+ or Artistic | Perl interface to the UNIX syslog(3) calls
perl-Sys-Virt | GPLv2+ or Artistic | Represent and manage a libvirt hypervisor connection
perl-Taint-Runtime | GPL+ or Artistic | Runtime enable taint checking
perl-Term-ANSIColor | GPL+ or Artistic | Color screen output using ANSI escape sequences
perl-Term-ANSIColor | GPL+ or Artistic | Color screen output using ANSI escape sequences
perl-Term-ANSIColor | GPL+ or Artistic | Color screen output using ANSI escape sequences
perl-Term-Cap | GPL+ or Artistic | Perl termcap interface
perl-Term-Cap | GPL+ or Artistic | Perl termcap interface
perl-Term-Cap | GPL+ or Artistic | Perl termcap interface
perl-Term-Size-Any | GPL+ or Artistic | Retrieve terminal size
perl-Term-Size-Perl | GPL+ or Artistic | Perl extension for retrieving terminal size (Perl version)
perl-Term-Table | GPL+ or Artistic | Format a header and rows into a table
perl-Term-Table | GPL+ or Artistic | Format a header and rows into a table
perl-Term-Table | GPL+ or Artistic | Format a header and rows into a table
perl-TermReadKey | (Copyright only) and (Artistic or GPL+) | A perl module for simple terminal control
perl-Test-Deep | GPL+ or Artistic | Extremely flexible deep comparison
perl-Test-Differences | GPL+ or Artistic | Test strings and data structures and show differences if not OK
perl-Test-Exception | GPL+ or Artistic | Library of test functions for exception based Perl code
perl-Test-Fatal | GPL+ or Artistic | Incredibly simple helpers for testing code with exceptions
perl-Test-Harness | GPL+ or Artistic | Run Perl standard test scripts with statistics
perl-Test-Harness | GPL+ or Artistic | Run Perl standard test scripts with statistics
perl-Test-Harness | GPL+ or Artistic | Run Perl standard test scripts with statistics
perl-Test-Harness | GPL+ or Artistic | Run Perl standard test scripts with statistics
perl-Test-LongString | GPL+ or Artistic | Perl module to test long strings
perl-Test-NoWarnings | LGPLv2+ | Make sure you didn't emit any warnings while testing
perl-Test-Pod | GPL+ or Artistic | Test POD files for correctness
perl-Test-Pod-Coverage | Artistic 2.0 | Check for pod coverage in your distribution
perl-Test-Requires | GPL+ or Artistic | Checks to see if a given module can be loaded
perl-Test-Simple | (GPL+ or Artistic) and CC0 and Public Domain | Basic utilities for writing tests
perl-Test-Simple | (GPL+ or Artistic) and CC0 and Public Domain | Basic utilities for writing tests
perl-Test-Simple | (GPL+ or Artistic) and CC0 and Public Domain | Basic utilities for writing tests
perl-Test-Simple | (GPL+ or Artistic) and CC0 and Public Domain | Basic utilities for writing tests
perl-Test-Taint | GPL+ or Artistic | Tools to test taintedness
perl-Test-Warn | GPL+ or Artistic | Perl extension to test methods for warnings
perl-Test-Warnings | GPL+ or Artistic | Test for warnings and the lack of them
perl-Test2-Suite | GPL+ or Artistic | Set of tools built upon the Test2 framework
perl-Text-Balanced | GPL+ or Artistic | Extract delimited text sequences from strings
perl-Text-Balanced | GPL+ or Artistic | Extract delimited text sequences from strings
perl-Text-Balanced | GPL+ or Artistic | Extract delimited text sequences from strings
perl-Text-Balanced | GPL+ or Artistic | Extract delimited text sequences from strings
perl-Text-CharWidth | GPL+ or Artistic | Get number of occupied columns of a string on terminal
perl-Text-Diff | (GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT | Perform diffs on files and record sets
perl-Text-Diff | (GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT | Perform diffs on files and record sets
perl-Text-Diff | (GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT | Perform diffs on files and record sets
perl-Text-Glob | GPL+ or Artistic | Perl module to match globbing patterns against text
perl-Text-Glob | GPL+ or Artistic | Perl module to match globbing patterns against text
perl-Text-Glob | GPL+ or Artistic | Perl module to match globbing patterns against text
perl-Text-Glob | GPL+ or Artistic | Perl module to match globbing patterns against text
perl-Text-ParseWords | GPL+ or Artistic | Parse text into an array of tokens or array of arrays
perl-Text-ParseWords | GPL+ or Artistic | Parse text into an array of tokens or array of arrays
perl-Text-ParseWords | GPL+ or Artistic | Parse text into an array of tokens or array of arrays
perl-Text-Soundex | (Copyright only) and (GPL+ or Artistic) | Implementation of the soundex algorithm
perl-Text-Tabs+Wrap | TTWL | Expand tabs and do simple line wrapping
perl-Text-Tabs+Wrap | TTWL | Expand tabs and do simple line wrapping
perl-Text-Tabs+Wrap | TTWL | Expand tabs and do simple line wrapping
perl-Text-Template | (GPL+ or Artistic) and (GPLv2+ or Artistic) | Expand template text with embedded Perl
perl-Text-Template | GPL+ or Artistic | Expand template text with embedded Perl
perl-Text-Template | GPL+ or Artistic | Expand template text with embedded Perl
perl-Text-Template | GPL+ or Artistic | Expand template text with embedded Perl
perl-Text-Unidecode | GPL+ or Artistic | US-ASCII transliterations of Unicode text
perl-Text-WrapI18N | GPL+ or Artistic | Line wrapping with support for several locale setups
perl-Thread-Queue | GPL+ or Artistic | Thread-safe queues
perl-Thread-Queue | GPL+ or Artistic | Thread-safe queues
perl-Thread-Queue | GPL+ or Artistic | Thread-safe queues
perl-Thread-Queue | GPL+ or Artistic | Thread-safe queues
perl-Tie-IxHash | GPL+ or Artistic | Ordered associative arrays for Perl
perl-Time-HiRes | GPL+ or Artistic | High resolution alarm, sleep, gettimeofday, interval timers
perl-Time-HiRes | GPL+ or Artistic | High resolution alarm, sleep, gettimeofday, interval timers
perl-Time-HiRes | GPL+ or Artistic | High resolution alarm, sleep, gettimeofday, interval timers
perl-Time-HiRes | GPL+ or Artistic | High resolution alarm, sleep, gettimeofday, interval timers
perl-Time-Local | GPL+ or Artistic | Efficiently compute time from local and GMT time
perl-Time-Local | GPL+ or Artistic | Efficiently compute time from local and GMT time
perl-Time-Local | GPL+ or Artistic | Efficiently compute time from local and GMT time
perl-TimeDate | GPL+ or Artistic | A Perl module for time and date manipulation
perl-TimeDate | GPL+ or Artistic | A Perl module for time and date manipulation
perl-TimeDate | GPL+ or Artistic | A Perl module for time and date manipulation
perl-TimeDate | GPL+ or Artistic | A Perl module for time and date manipulation
perl-Tk | (GPL+ or Artistic) and SWL | Perl Graphical User Interface ToolKit
perl-Try-Tiny | MIT | Minimal try/catch with proper localization of $@
perl-Try-Tiny | MIT | Minimal try/catch with proper localization of $@
perl-Try-Tiny | MIT | Minimal try/catch with proper localization of $@
perl-Try-Tiny | MIT | Minimal try/catch with proper localization of $@
perl-Types-Serialiser | GPL+ or Artistic | Simple data types for common serialization formats
perl-URI | GPL+ or Artistic | A Perl module implementing URI parsing and manipulation
perl-URI | GPL+ or Artistic | A Perl module implementing URI parsing and manipulation
perl-URI | GPL+ or Artistic | A Perl module implementing URI parsing and manipulation
perl-Unicode-Collate | (GPL+ or Artistic) and Unicode | Unicode Collation Algorithm
perl-Unicode-Collate | (GPL+ or Artistic) and Unicode | Unicode Collation Algorithm
perl-Unicode-Collate | (GPL+ or Artistic) and Unicode | Unicode Collation Algorithm
perl-Unicode-Collate | (GPL+ or Artistic) and Unicode | Unicode Collation Algorithm
perl-Unicode-EastAsianWidth | CC0 | East Asian Width properties
perl-Unicode-LineBreak | GPL+ or Artistic | UAX #14 Unicode Line Breaking Algorithm
perl-Unicode-Normalize | GPL+ or Artistic | Unicode Normalization Forms
perl-Unicode-Normalize | GPL+ or Artistic | Unicode Normalization Forms
perl-Unicode-Normalize | GPL+ or Artistic | Unicode Normalization Forms
perl-Unicode-UTF8 | GPL+ or Artistic | Encoding and decoding of UTF-8 encoding form
perl-Unix-Syslog | Artistic 2.0 | Perl interface to the UNIX syslog(3) calls
perl-Variable-Magic | GPL+ or Artistic | Associate user-defined magic to variables from Perl
perl-WWW-RobotRules | GPL+ or Artistic | Database of robots.txt-derived permissions
perl-WWW-RobotRules | GPL+ or Artistic | Database of robots.txt-derived permissions
perl-WWW-RobotRules | GPL+ or Artistic | Database of robots.txt-derived permissions
perl-WWW-RobotRules | GPL+ or Artistic | Database of robots.txt-derived permissions
perl-XML-Catalog | GPL+ or Artistic | Resolve public identifiers and remap system identifiers
perl-XML-DOM | GPL+ or Artistic | DOM extension to XML::Parser
perl-XML-LibXML | (GPL+ or Artistic) and MIT | Perl interface to the libxml2 library
perl-XML-NamespaceSupport | GPL+ or Artistic | A simple generic name space support class
perl-XML-Parser | GPL+ or Artistic | Perl module for parsing XML documents
perl-XML-RegExp | GPL+ or Artistic | Regular expressions for XML tokens
perl-XML-SAX | GPL+ or Artistic | SAX parser access API for Perl
perl-XML-SAX-Base | GPL+ or Artistic | Base class SAX drivers and filters
perl-XML-Simple | GPL+ or Artistic | Easy API to maintain XML in Perl
perl-XML-TokeParser | GPL+ or Artistic | Simplified interface to XML::Parser
perl-XML-Twig | GPL+ or Artistic | Perl module for processing huge XML documents in tree mode
perl-XML-XPath | Artistic 2.0 and (GPL+ or Artistic) | XPath parser and evaluator for Perl
perl-YAML | GPL+ or Artistic | YAML Ain't Markup Language (tm)
perl-YAML | GPL+ or Artistic | YAML Ain't Markup Language (tm)
perl-YAML | GPL+ or Artistic | YAML Ain't Markup Language (tm)
perl-YAML | GPL+ or Artistic | YAML Ain't Markup Language (tm)
perl-YAML-LibYAML | GPL+ or Artistic | Perl YAML Serialization using XS and libyaml
perl-YAML-Syck | BSD and MIT | Fast, lightweight YAML loader and dumper
perl-YAML-Tiny | GPL+ or Artistic | Read/Write YAML files with as little code as possible
perl-autodie | GPL+ or Artistic | Replace functions with ones that succeed or die
perl-autodie | GPL+ or Artistic | Replace functions with ones that succeed or die
perl-autodie | GPL+ or Artistic | Replace functions with ones that succeed or die
perl-autodie | GPL+ or Artistic | Replace functions with ones that succeed or die
perl-bignum | GPL+ or Artistic | Transparent big number support for Perl
perl-bignum | GPL+ or Artistic | Transparent big number support for Perl
perl-bignum | GPL+ or Artistic | Transparent big number support for Perl
perl-bignum | GPL+ or Artistic | Transparent big number support for Perl
perl-common-sense | GPL+ or Artistic | "Common sense" Perl defaults
perl-constant | GPL+ or Artistic | Perl pragma to declare constants
perl-constant | GPL+ or Artistic | Perl pragma to declare constants
perl-constant | GPL+ or Artistic | Perl pragma to declare constants
perl-experimental | GPL+ or Artistic | Experimental features made easy
perl-experimental | GPL+ or Artistic | Experimental features made easy
perl-experimental | GPL+ or Artistic | Experimental features made easy
perl-experimental | GPL+ or Artistic | Experimental features made easy
perl-generators | GPL+ | RPM Perl dependencies generators
perl-generators | GPL+ | RPM Perl dependencies generators
perl-generators | GPL+ | RPM Perl dependencies generators
perl-generators | GPL+ | RPM Perl dependencies generators
perl-gettext | GPL+ or Artistic | Interface to gettext family of functions
perl-inc-latest | ASL 2.0 | Use modules bundled in inc/ if they are newer than installed ones
perl-inc-latest | ASL 2.0 | Use modules bundled in inc/ if they are newer than installed ones
perl-inc-latest | ASL 2.0 | Use modules bundled in inc/ if they are newer than installed ones
perl-inc-latest | ASL 2.0 | Use modules bundled in inc/ if they are newer than installed ones
perl-libintl-perl | GPLv3+ and LGPLv2+ | Internationalization library for Perl, compatible with gettext
perl-libnet | GPL+ or Artistic | Perl clients for various network protocols
perl-libnet | GPL+ or Artistic | Perl clients for various network protocols
perl-libnet | GPL+ or Artistic | Perl clients for various network protocols
perl-libwww-perl | GPL+ or Artistic | A Perl interface to the World-Wide Web
perl-libwww-perl | GPL+ or Artistic | A Perl interface to the World-Wide Web
perl-libwww-perl | GPL+ or Artistic | A Perl interface to the World-Wide Web
perl-libwww-perl | GPL+ or Artistic | A Perl interface to the World-Wide Web
perl-libxml-perl | (GPL+ or Artistic) and Public Domain | A collection of Perl modules for working with XML
perl-local-lib | GPL+ or Artistic | Create and use a local lib/ for perl modules
perl-local-lib | GPL+ or Artistic | Create and use a local lib/ for perl modules
perl-local-lib | GPL+ or Artistic | Create and use a local lib/ for perl modules
perl-local-lib | GPL+ or Artistic | Create and use a local lib/ for perl modules
perl-namespace-autoclean | GPL+ or Artistic | Keep imports out of your namespace
perl-namespace-clean | GPL+ or Artistic | Keep your namespace tidy
perl-parent | GPL+ or Artistic | Establish an ISA relationship with base classes at compile time
perl-parent | GPL+ or Artistic | Establish an ISA relationship with base classes at compile time
perl-parent | GPL+ or Artistic | Establish an ISA relationship with base classes at compile time
perl-perlfaq | (GPL+ or Artistic) and Public Domain | Frequently asked questions about Perl
perl-perlfaq | (GPL+ or Artistic) and Public Domain | Frequently asked questions about Perl
perl-perlfaq | (GPL+ or Artistic) and Public Domain | Frequently asked questions about Perl
perl-perlfaq | (GPL+ or Artistic) and Public Domain | Frequently asked questions about Perl
perl-podlators | (GPL+ or Artistic) and MIT | Format POD source into various output formats
perl-podlators | (GPL+ or Artistic) and FSFAP | Format POD source into various output formats
perl-podlators | (GPL+ or Artistic) and FSFAP | Format POD source into various output formats
perl-prefork | GPL+ or Artistic | Optimized module loading for forking or non-forking processes
perl-srpm-macros | GPLv3+ | RPM macros for building Perl source package from source repository
perl-threads | GPL+ or Artistic | Perl interpreter-based threads
perl-threads | GPL+ or Artistic | Perl interpreter-based threads
perl-threads | GPL+ or Artistic | Perl interpreter-based threads
perl-threads-shared | GPL+ or Artistic | Perl extension for sharing data structures between threads
perl-threads-shared | GPL+ or Artistic | Perl extension for sharing data structures between threads
perl-threads-shared | GPL+ or Artistic | Perl extension for sharing data structures between threads
perl-version | GPL+ or Artistic | Perl extension for Version Objects
perl-version | GPL+ or Artistic | Perl extension for Version Objects
perl-version | GPL+ or Artistic | Perl extension for Version Objects
perl-version | GPL+ or Artistic | Perl extension for Version Objects
perltidy | GPLv2+ | Tool for indenting and re-formatting Perl scripts
pesign | GPLv2 | Signing utility for UEFI binaries
pg_repack | BSD | Reorganize tables in PostgreSQL databases without any locks
pg_repack | BSD | Reorganize tables in PostgreSQL databases without any locks
pg_repack | BSD | Reorganize tables in PostgreSQL databases without any locks
pgaudit | PostgreSQL | PostgreSQL Audit Extension
pgaudit | PostgreSQL | PostgreSQL Audit Extension
pgaudit | PostgreSQL | PostgreSQL Audit Extension
php | PHP and Zend and BSD and MIT and ASL 1.0 | PHP scripting language for creating dynamic web sites
php | PHP and Zend and BSD and MIT and ASL 1.0 and NCSA | PHP scripting language for creating dynamic web sites
php | PHP and Zend and BSD and MIT and ASL 1.0 and NCSA | PHP scripting language for creating dynamic web sites
php | PHP and Zend and BSD and MIT and ASL 1.0 and NCSA | PHP scripting language for creating dynamic web sites
php-pear | BSD and LGPLv3+ | PHP Extension and Application Repository framework
php-pear | BSD and LGPLv3+ | PHP Extension and Application Repository framework
php-pear | BSD and LGPLv3+ | PHP Extension and Application Repository framework
php-pear | BSD and LGPLv3+ | PHP Extension and Application Repository framework
php-pecl-apcu | PHP | APC User Cache
php-pecl-apcu | PHP | APC User Cache
php-pecl-apcu | PHP | APC User Cache
php-pecl-rrd | BSD | PHP Bindings for rrdtool
php-pecl-rrd | BSD | PHP Bindings for rrdtool
php-pecl-xdebug | PHP | PECL package for debugging PHP scripts
php-pecl-xdebug | PHP | PECL package for debugging PHP scripts
php-pecl-zip | PHP | A ZIP archive management extension
php-pecl-zip | PHP | A ZIP archive management extension
php-pecl-zip | PHP | A ZIP archive management extension
pidgin | BSD and GPLv2+ and GPLv2 and LGPLv2+ and MIT | A Gtk+ based multiprotocol instant messaging client
pidgin-sipe | GPLv2+ | Pidgin protocol plugin to connect to MS Office Communicator
pinentry | GPLv2+ | Collection of simple PIN or passphrase entry dialogs
pinfo | GPLv2 | An info file viewer
pipewire | MIT | Media Sharing Server
pipewire0.2 | LGPLv2+ | Media Sharing Server compat libraries
pki-core | GPLv2 and LGPLv2 | IDM PKI Package
pki-servlet-engine | ASL 2.0 | Apache Servlet/JSP Engine, RI for Servlet 4.0/JSP 2.3 API
plexus-cipher | ASL 2.0 | Plexus Cipher: encryption/decryption Component
plexus-cipher | ASL 2.0 | Plexus Cipher: encryption/decryption Component
plexus-classworlds | ASL 2.0 and Plexus | Plexus Classworlds Classloader Framework
plexus-classworlds | ASL 2.0 and Plexus | Plexus Classworlds Classloader Framework
plexus-containers | ASL 2.0 and MIT and xpp | Containers for Plexus
plexus-containers | ASL 2.0 and MIT and xpp | Containers for Plexus
plexus-interpolation | ASL 2.0 and ASL 1.1 and MIT | Plexus Interpolation API
plexus-interpolation | ASL 2.0 and ASL 1.1 and MIT | Plexus Interpolation API
plexus-sec-dispatcher | ASL 2.0 | Plexus Security Dispatcher Component
plexus-sec-dispatcher | ASL 2.0 | Plexus Security Dispatcher Component
plexus-utils | ASL 1.1 and ASL 2.0 and xpp and BSD and Public Domain | Plexus Common Utilities
plexus-utils | ASL 1.1 and ASL 2.0 and xpp and BSD and Public Domain | Plexus Common Utilities
plotutils | GPLv2+ and GPLv3+ | GNU vector and raster graphics utilities and libraries
plymouth | GPLv2+ | Graphical Boot Animation and Logger
pmdk | BSD | Persistent Memory Development Kit (former NVML)
pmdk | BSD | Persistent Memory Development Kit (former NVML)
pmdk-convert | BSD | Conversion tool for PMDK pools
pmix | BSD | Process Management Interface Exascale (PMIx)
pnm2ppa | GPLv2+ | Drivers for printing to HP PPA printers
po4a | GPL+ | A tool maintaining translations anywhere
podman | ASL 2.0 | Manage Pods, Containers and Container Images
podman | ASL 2.0 | Manage Pods, Containers and Container Images
podman | ASL 2.0 and GPLv3+ | Manage Pods, Containers and Container Images
podman | ASL 2.0 and GPLv3+ | Manage Pods, Containers and Container Images
podman | Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND ISC AND MIT AND MPL-2.0 | Manage Pods, Containers and Container Images
poppler | (GPLv2 or GPLv3) and GPLv2+ and LGPLv2+ and MIT | PDF rendering library
poppler | (GPLv2 or GPLv3) and GPLv2+ and LGPLv2+ and MIT | PDF rendering library
poppler-data | BSD and GPLv2 | Encoding files for use with poppler
postgres-decoderbufs | MIT | PostgreSQL Protocol Buffers logical decoder plugin
postgres-decoderbufs | MIT | PostgreSQL Protocol Buffers logical decoder plugin
postgres-decoderbufs | MIT | PostgreSQL Protocol Buffers logical decoder plugin
postgresql | PostgreSQL | PostgreSQL client programs
postgresql | PostgreSQL | PostgreSQL client programs
postgresql | PostgreSQL | PostgreSQL client programs
postgresql | PostgreSQL | PostgreSQL client programs
postgresql | PostgreSQL | PostgreSQL client programs
postgresql-jdbc | BSD | JDBC driver for PostgreSQL
postgresql-odbc | LGPLv2+ | PostgreSQL ODBC driver
potrace | GPLv2+ | Transform bitmaps into vector graphics
power-profiles-daemon | GPLv3+ | Makes power profiles handling available over D-Bus
powertop | GPLv2 | Power consumption monitor
pps-tools | GPLv2+ | LinuxPPS user-space tools
pptp | GPLv2+ | Point-to-Point Tunneling Protocol (PPTP) Client
procmail | GPLv2+ or Artistic | Mail processing program
prometheus-jmx-exporter | ASL 2.0 | Prometheus JMX Exporter
protobuf | BSD | Protocol Buffers - Google's data interchange format
protobuf-c | BSD | C bindings for Google's Protocol Buffers
pstoedit | GPLv2+ | Translates PostScript and PDF graphics into other vector formats
pulseaudio | LGPLv2+ | Improved Linux Sound Server
py3c | MIT and CC-BY-SA | Guide and compatibility macros for porting extensions to Python 3
pyOpenSSL | ASL 2.0 | Python wrapper module around the OpenSSL library
pyatspi | LGPLv2 and GPLv2 | Python bindings for at-spi
pybind11 | BSD | Seamless operability between C++11 and Python
pycairo | MPLv1.1 or LGPLv2 | Python bindings for the cairo library
pygobject2 | LGPLv2+, MIT | Python 2 bindings for GObject
pygtk2 | LGPLv2+ | Python bindings for GTK+
pykickstart | GPLv2 and MIT | Python utilities for manipulating kickstart files.
pyodbc | MIT | Python DB API 2.0 Module for ODBC
pyparted | GPLv2+ | Python module for GNU parted
pyserial | Python | Python serial port access library
pytest | MIT | Simple powerful testing with Python
pytest | MIT | Simple powerful testing with Python
pytest | MIT | Simple powerful testing with Python
python-PyMySQL | MIT | Pure-Python MySQL client library
python-PyMySQL | MIT | Pure-Python MySQL client library
python-PyMySQL | MIT | Pure-Python MySQL client library
python-PyMySQL | MIT | Pure-Python MySQL client library
python-argcomplete | ASL 2.0 | Bash tab completion for argparse
python-argh | LGPLv3+ | Unobtrusive argparse wrapper with natural syntax
python-asn1crypto | MIT | Fast Python ASN.1 parser and serializer
python-attrs | MIT | Python attributes without boilerplate
python-attrs | MIT | Python attributes without boilerplate
python-attrs | MIT | Python attributes without boilerplate
python-augeas | LGPLv2+ | Python bindings to augeas
python-backports | Public Domain | Namespace for backported Python features
python-backports-ssl_match_hostname | Python | The ssl.match_hostname() function from Python 3
python-blivet | LGPLv2+ | A python module for system storage configuration
python-cffi | MIT | Foreign Function Interface for Python to call C code
python-cffi | MIT | Foreign Function Interface for Python to call C code
python-chardet | LGPLv2 | Character encoding auto-detection in Python
python-chardet | LGPLv2 | Character encoding auto-detection in Python
python-chardet | LGPLv2 | Character encoding auto-detection in Python
python-click | BSD | Simple wrapper around optparse for powerful command line utilities
python-coverage | ASL 2.0 and MIT and (MIT or GPLv2) | Code coverage testing module for Python
python-coverage | ASL 2.0 and MIT and (MIT or GPL) | Code coverage testing module for Python
python-cpio | LGPLv2+ | A Python module for accessing cpio archives
python-cryptography | ASL 2.0 or BSD | PyCA's cryptography library
python-cryptography | ASL 2.0 or BSD | PyCA's cryptography library
python-cups | GPLv2+ | Python bindings for CUPS
python-dasbus | LGPLv2+ | DBus library in Python 3
python-dbus-client-gen | MPLv2.0 | Library for Generating D-Bus Client Code
python-dbus-python-client-gen | MPLv2.0 | Python Library for Generating dbus-python Client Code
python-dbus-signature-pyparsing | ASL 2.0 | Parser for a D-Bus Signature
python-distro | ASL 2.0 | Linux Distribution - a Linux OS platform information API
python-dns | MIT | DNS toolkit for Python
python-docs | Python | Documentation for the Python 2 programming language
python-docs | Python | Documentation for the Python 3 programming language
python-docutils | Public Domain and BSD and Python and GPLv3+ | System for processing plaintext documentation
python-docutils | Public Domain and BSD and Python and GPLv3+ | System for processing plaintext documentation
python-enchant | LGPLv2+ | Python bindings for Enchant spellchecking library
python-evdev | BSD | Python bindings for the Linux input handling subsystem
python-flask | BSD | A micro-framework for Python based on Werkzeug, Jinja 2 and good intentions
python-funcsigs | ASL 2.0 | Python function signatures from PEP362 for Python 2.6, 2.7 and 3.2+
python-gevent | MIT | A coroutine-based Python networking library
python-greenlet | MIT | Lightweight in-process concurrent programming
python-gssapi | ISC | Python Bindings for GSSAPI (RFC 2743/2744 and extensions)
python-html5lib | MIT | A python based HTML parser/tokenizer
python-httplib2 | MIT | A comprehensive HTTP client library
python-humanize | MIT | Turns dates in to human readable format, e.g '3 minutes ago'
python-hwdata | GPLv2 | Python bindings to hwdata package
python-hypothesis | MPLv2.0 | Library for property based testing
python-idna | BSD and Python and Unicode | Internationalized Domain Names in Applications (IDNA)
python-idna | BSD and Python and Unicode | Internationalized Domain Names in Applications (IDNA)
python-idna | BSD and Python and Unicode | Internationalized Domain Names in Applications (IDNA)
python-imagesize | MIT | Python module for analyzing image file headers and returning image sizes
python-iniconfig | MIT | Brain-dead simple parsing of ini files
python-into-dbus-python | ASL 2.0 | Transformer to dbus-python types
python-ipaddress | Python | Port of the python 3.3+ ipaddress module to 2.6+
python-iso8601 | MIT | Simple module to parse ISO 8601 dates
python-itsdangerous | BSD | Python library for passing trusted data to untrusted environments
python-jinja2 | BSD | General purpose template engine
python-jinja2 | BSD | General purpose template engine
python-jinja2 | BSD | General purpose template engine
python-jmespath | MIT | JSON Matching Expressions
python-jsonpatch | BSD | Applying JSON Patches in Python
python-jsonpointer | BSD | Resolve JSON Pointers in Python
python-jsonschema | MIT | An implementation of JSON Schema validation for Python
python-justbases | LGPLv2+ | A small library for precise conversion between arbitrary bases
python-justbytes | LGPLv2+ | Library for handling computation with address ranges in bytes
python-jwcrypto | LGPLv3+ | Implements JWK, JWS, JWE specifications using python-cryptography
python-jwcrypto | LGPLv3+ | Implements JWK, JWS, JWE specifications using python-cryptography
python-kdcproxy | MIT | MS-KKDCP (kerberos proxy) WSGI module
python-ldap | Python | An object-oriented API to access LDAP directory servers
python-lesscpy | MIT | Lesscss compiler
python-lit | NCSA | Tool for executing llvm test suites
python-lxml | BSD | XML processing library combining libxml2/libxslt with the ElementTree API
python-lxml | BSD | XML processing library combining libxml2/libxslt with the ElementTree API
python-lxml | BSD | XML processing library combining libxml2/libxslt with the ElementTree API
python-lxml | BSD | XML processing library combining libxml2/libxslt with the ElementTree API
python-mako | (MIT and Python) and (BSD or GPLv2) | Mako template library for Python
python-markdown | BSD | Markdown implementation in Python
python-markupsafe | BSD | Implements a XML/HTML/XHTML Markup safe string for Python
python-markupsafe | BSD | Implements a XML/HTML/XHTML Markup safe string for Python
python-markupsafe | BSD | Implements a XML/HTML/XHTML Markup safe string for Python
python-meh | GPLv2+ | A python library for handling exceptions
python-mock | BSD | A Python Mocking and Patching Library for Testing
python-mock | BSD | A Python Mocking and Patching Library for Testing
python-more-itertools | MIT | More routines for operating on Python iterables, beyond itertools
python-netaddr | BSD | A pure Python network address representation and manipulation library
python-netifaces | MIT | Python library to retrieve information about network interfaces
python-networkx | BSD | Creates and Manipulates Graphs and Networks
python-nose | LGPLv2+ and Public Domain | Discovery-based unit test extension for Python
python-nose | LGPLv2+ and Public Domain | Discovery-based unit test extension for Python
python-nss | MPLv2.0 or GPLv2+ or LGPLv2+ | Python bindings for Network Security Services (NSS)
python-ntplib | MIT | Python module that offers a simple interface to query NTP servers
python-ordered-set | MIT | A Custom MutableSet that remembers its order
python-packaging | BSD or ASL 2.0 | Core utilities for Python packages
python-packaging | BSD or ASL 2.0 | Core utilities for Python packages
python-pexpect | MIT | Unicode-aware Pure Python Expect-like module
python-pid | ASL 2.0 | PID file management library
python-pillow | MIT | Python image processing library
python-pluggy | MIT | The plugin manager stripped of pytest specific details
python-pluggy | MIT | The plugin manager stripped of pytest specific details
python-pluggy | MIT | The plugin manager stripped of pytest specific details
python-ply | BSD | Python Lex-Yacc
python-ply | BSD | Python Lex-Yacc
python-podman | ASL 2.0 | RESTful API for Podman
python-podman | ASL 2.0 | RESTful API for Podman
python-podman-api | LGPLv2 | Podman API
python-prettytable | BSD | Python library to display tabular data in tables
python-productmd | LGPLv2+ | Library providing parsers for metadata related to OS installation
python-psutil | BSD | A process and system utilities module for Python
python-psutil | BSD | A process and system utilities module for Python
python-psutil | BSD | A process and system utilities module for Python
python-psycopg2 | LGPLv3+ with exceptions | A PostgreSQL database adapter for Python
python-psycopg2 | LGPLv3+ with exceptions | A PostgreSQL database adapter for Python
python-psycopg2 | LGPLv3+ with exceptions | A PostgreSQL database adapter for Python
python-psycopg2 | LGPLv3+ with exceptions | A PostgreSQL database adapter for Python
python-ptyprocess | ISC | Run a subprocess in a pseudo terminal
python-py | MIT and Public Domain | Library with cross-python path, ini-parsing, io, code, log facilities
python-py | MIT and Public Domain | Library with cross-python path, ini-parsing, io, code, log facilities
python-py | MIT and Public Domain | Library with cross-python path, ini-parsing, io, code, log facilities
python-pyasn1 | BSD | ASN.1 tools for Python
python-pycparser | BSD | C parser and AST generator written in Python
python-pycparser | BSD | C parser and AST generator written in Python
python-pycurl | LGPLv2+ or MIT | A Python interface to libcurl
python-pydbus | LGPLv2+ | Pythonic DBus library
python-pyghmi | ASL 2.0 | Python General Hardware Management Initiative (IPMI and others)
python-pygments | BSD | Syntax highlighting engine written in Python
python-pygments | BSD | Syntax highlighting engine written in Python
python-pymongo | ASL 2.0 and MIT | Python driver for MongoDB
python-pymongo | ASL 2.0 and MIT | Python driver for MongoDB
python-pysocks | BSD | A Python SOCKS client module
python-pysocks | BSD | A Python SOCKS client module
python-pysocks | BSD | A Python SOCKS client module
python-pytest-mock | MIT | Thin-wrapper around the mock package for easier use with py.test
python-pytoml | MIT | Parser for TOML
python-qrcode | BSD | Python QR Code image generator
python-qrcode | BSD | Python QR Code image generator
python-qt5 | GPLv3 | PyQt5 is Python bindings for Qt5
python-reportlab | BSD | Library for generating PDFs and graphics
python-requests | ASL 2.0 | HTTP library, written in Python, for human beings
python-requests | ASL 2.0 | HTTP library, written in Python, for human beings
python-requests | ASL 2.0 | HTTP library, written in Python, for human beings
python-requests-file | ASL 2.0 | Transport adapter for using file:// URLs with python-requests
python-requests-ftp | ASL 2.0 | FTP transport adapter for python-requests
python-resolvelib | ISC | Resolve abstract dependencies into concrete ones
python-rpm-generators | GPLv2+ | Dependency generators for Python RPMs
python-rpm-macros | MIT | The unversioned Python RPM macros
python-rpmfluff | GPLv2+ | Lightweight way of building RPMs, and sabotaging them
python-semantic_version | BSD | Library implementing the 'SemVer' scheme
python-setuptools_scm | MIT | Blessed package to manage your versions by scm tags
python-setuptools_scm | MIT | Blessed package to manage your versions by scm tags
python-simpleline | GPLv2+ | A Python library for creating text UI
python-snowballstemmer | BSD | Provides 16 stemmer algorithms generated from Snowball algorithms
python-sphinx | BSD and Public Domain and Python and (MIT or GPLv2) | Python documentation generator
python-sphinx-theme-alabaster | BSD | Configurable sidebar-enabled Sphinx theme
python-sphinx_rtd_theme | MIT | Sphinx theme for readthedocs.org
python-sphinxcontrib-websupport | BSD | Sphinx API for Web Apps
python-sqlalchemy | MIT | Modular and flexible ORM library for python
python-sqlalchemy | MIT | Modular and flexible ORM library for python
python-suds | LGPLv3+ | A python SOAP client
python-sure | GPLv3+ | Utility belt for automated testing in Python
python-sushy | ASL 2.0 | Sushy is a Python library to communicate with Redfish based systems
python-toml | MIT | Python Library for Tom's Obvious, Minimal Language
python-unittest2 | BSD | The new features in unittest backported to Python 2.4+
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post
python-urllib3 | MIT | Python HTTP library with thread-safe connection pooling and file post
python-virtualenv | MIT | Tool to create isolated Python environments
python-virtualenv | MIT | Tool to create isolated Python environments
python-wcwidth | MIT | Measures number of Terminal column cells of wide-character codes
python-webencodings | BSD | Character encoding for the web
python-werkzeug | BSD | The Swiss Army knife of Python web development
python-wheel | MIT | Built-package format for Python
python-wheel | MIT | Built-package format for Python
python-wheel | MIT | Built-package format for Python
python-wheel | MIT and (ASL 2.0 or BSD) | Built-package format for Python
python-whoosh | BSD | Fast, pure-Python full text indexing, search, and spell checking library
python-yubico | BSD | Pure-python library for interacting with Yubikeys
python-yubico | BSD | Pure-python library for interacting with Yubikeys
python2 | Python | An interpreted, interactive, object-oriented programming language
python2-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python 2 packages
python2-pycairo | MPLv1.1 or LGPLv2 | Python bindings for the cairo library
python2-rpm-macros | MIT | RPM macros for building Python 2 packages
python2-setuptools | MIT | Easily build and distribute Python packages
python2-six | MIT | Python 2 and 3 compatibility utilities
python36 | Python | Interpreter of the Python programming language
python38 | Python | Interpreter of the Python programming language
python39 | Python | Version 3.9 of the Python interpreter
python3x-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python packages
python3x-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python packages
python3x-pip | MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD) | A tool for installing and managing Python packages
python3x-pyparsing | MIT | Python package with an object-oriented approach to text processing
python3x-setuptools | MIT and (BSD or ASL 2.0) | Easily build and distribute Python packages
python3x-setuptools | MIT and (BSD or ASL 2.0) | Easily build and distribute Python packages
python3x-six | MIT | Python 2 and 3 compatibility utilities
python3x-six | MIT | Python 2 and 3 compatibility utilities
pytz | MIT | World Timezone Definitions for Python
pytz | MIT | World Timezone Definitions for Python
pytz | MIT | World Timezone Definitions for Python
pyusb | BSD | Python bindings for libusb
pyusb | BSD | Python bindings for libusb
pyxattr | LGPLv2+ | Extended attributes library wrapper for Python
pyxdg | LGPLv2 | Python library to access freedesktop.org standards
qatengine | BSD and OpenSSL | Intel QuickAssist Technology (QAT) OpenSSL Engine
qatlib | BSD and (BSD or GPLv2) | Intel QuickAssist user space library
qatzip | BSD | Intel QuickAssist Technology (QAT) QATzip Library
qemu-kvm | GPLv2 and GPLv2+ and CC-BY | QEMU is a machine emulator and virtualizer
qgnomeplatform | LGPLv2+ | Qt Platform Theme aimed to accommodate Gnome settings
qhull | Qhull | General dimension convex hull programs
qpdf | (Artistic 2.0 or ASL 2.0) and MIT | Command-line tools and library for transforming PDF files
qperf | GPLv2 or BSD | Measure socket and RDMA performance
qrencode | LGPLv2+ | Generate QR 2D barcodes
qt5 | GPLv3 | Qt5 meta package
qt5-qt3d | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Qt3D QML bindings and C++ APIs
qt5-qtbase | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtBase components
qt5-qtcanvas3d | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Canvas3d component
qt5-qtconnectivity | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Connectivity components
qt5-qtdeclarative | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtDeclarative component
qt5-qtdoc | GFDL | Main Qt5 Reference Documentation
qt5-qtgraphicaleffects | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtGraphicalEffects component
qt5-qtimageformats | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtImageFormats component
qt5-qtlocation | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Location component
qt5-qtmultimedia | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Multimedia support
qt5-qtquickcontrols | LGPLv2 or LGPLv3 and GFDL | Qt5 - module with set of QtQuick controls
qt5-qtquickcontrols2 | GPLv2+ or LGPLv3 and GFDL | Qt5 - module with set of QtQuick controls for embedded
qt5-qtscript | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtScript component
qt5-qtsensors | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Sensors component
qt5-qtserialbus | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - SerialBus component
qt5-qtserialport | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - SerialPort component
qt5-qtsvg | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Support for rendering and displaying SVG
qt5-qtsvg | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - Support for rendering and displaying SVG
qt5-qttools | LGPLv3 or LGPLv2 | Qt5 - QtTool components
qt5-qttranslations | LGPLv2 with exceptions or GPLv3 with exceptions and GFDL | Qt5 - QtTranslations module
qt5-qtwayland | LGPLv3 | Qt5 - Wayland platform support and QtCompositor module
qt5-qtwebchannel | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - WebChannel component
qt5-qtwebsockets | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - WebSockets component
qt5-qtx11extras | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - X11 support library
qt5-qtxmlpatterns | LGPLv2 with exceptions or GPLv3 with exceptions | Qt5 - QtXmlPatterns component
radvd | BSD with advertising | A Router Advertisement daemon
raptor2 | GPLv2+ or LGPLv2+ or ASL 2.0 | RDF Parser Toolkit for Redland
rarian | LGPLv2+ | Documentation meta-data library
rasqal | LGPLv2+ or ASL 2.0 | RDF Query Library
re2c | Public Domain | Tool for generating C-based recognizers from regular expressions
rear | GPLv3 | Relax-and-Recover is a Linux disaster recovery and system migration tool
recode | GPLv2+ | Conversion between character sets and surfaces
redfish-finder | GPLv2 | Utility for parsing SMBIOS information and configuring canonical BMC access
redhat-lsb | GPLv2 | Implementation of Linux Standard Base specification
redhat-menus | GPL+ | Configuration and data files for the desktop menus
redhat-rpm-config | GPL+ | Red Hat specific rpm configuration files
redis | BSD and MIT | A persistent key-value database
redis | BSD and MIT | A persistent key-value database
redland | LGPLv2+ or ASL 2.0 | RDF Application Framework
relaxngDatatype | BSD | RELAX NG Datatype API
rest | LGPLv2 | A library for access to RESTful web services
resteasy | ASL 2.0 | Framework for RESTful Web services and Java applications
rhc | GPLv3 | Message dispatch agent for cloud-connected systems
rhythmbox | GPLv2+ with exceptions and GFDL | Music Management Application
rig | GPLv2 | Monitor a system for events and trigger specific actions
rpcsvc-proto | BSD and LGPLv2+ | RPC protocol definitions
rpm-mpi-hooks | MIT | RPM dependency generator hooks for MPI packages
rpm-ostree | LGPLv2+ | Hybrid image/package system
rpmdevtools | GPLv2+ and GPLv2 | RPM Development Tools
rpmlint | GPLv2 | Tool for checking common errors in RPM packages
rrdtool | GPLv2+ with exceptions | Round Robin Database Tool to store and display time-series data
rshim | GPLv2 | User-space driver for Mellanox BlueField SoC
rsyslog | (GPLv3+ and ASL 2.0) | Enhanced system logging and kernel message trapping daemon
rt-tests | GPLv2 | Programs that test various rt-features
rtkit | GPLv3+ and BSD | Realtime Policy and Watchdog Daemon
ruby | (Ruby or BSD) and Public Domain and MIT and CC0 and zlib and UCD | An interpreter of object-oriented scripting language
ruby | (Ruby or BSD) and Public Domain and MIT and CC0 and zlib and UCD | An interpreter of object-oriented scripting language
ruby | (Ruby or BSD) and Public Domain and MIT and CC0 and zlib and UCD | An interpreter of object-oriented scripting language
ruby | (Ruby or BSD) and Public Domain and MIT and CC0 and zlib and UCD | An interpreter of object-oriented scripting language
rubygem-abrt | MIT | ABRT support for Ruby
rubygem-abrt | MIT | ABRT support for Ruby
rubygem-abrt | MIT | ABRT support for Ruby
rubygem-abrt | MIT | ABRT support for Ruby
rubygem-bson | ASL 2.0 | Ruby Implementation of the BSON specification
rubygem-bson | ASL 2.0 | Ruby Implementation of the BSON specification
rubygem-bson | ASL 2.0 | Ruby implementation of the BSON specification
rubygem-bundler | MIT | Library and utilities to manage a Ruby application's gem dependencies
rubygem-diff-lcs | GPLv2+ or Artistic or MIT | Provide a list of changes between two sequenced collections
rubygem-mongo | ASL 2.0 | Ruby driver for MongoDB
rubygem-mongo | ASL 2.0 | Ruby driver for MongoDB
rubygem-mysql2 | MIT | A simple, fast Mysql library for Ruby, binding to libmysql
rubygem-mysql2 | MIT | A simple, fast Mysql library for Ruby, binding to libmysql
rubygem-mysql2 | MIT | A simple, fast Mysql library for Ruby, binding to libmysql
rubygem-mysql2 | MIT | A simple, fast Mysql library for Ruby, binding to libmysql
rubygem-pg | (BSD or Ruby) and PostgreSQL | A Ruby interface to the PostgreSQL RDBMS
rubygem-pg | (BSD or Ruby) and PostgreSQL | A Ruby interface to the PostgreSQL RDBMS
rubygem-pg | (BSD or Ruby) and PostgreSQL | A Ruby interface to the PostgreSQL RDBMS
rubygem-pg | (BSD or Ruby) and PostgreSQL | A Ruby interface to the PostgreSQL RDBMS
rubygem-rspec | MIT | Behaviour driven development (BDD) framework for Ruby
rubygem-rspec-core | MIT | Rspec-2 runner and formatters
rubygem-rspec-expectations | MIT | RSpec expectations (should and matchers)
rubygem-rspec-mocks | MIT | RSpec's 'test double' framework (mocks and stubs)
rubygem-rspec-support | MIT | Common functionality to Rspec series
runc | ASL 2.0 | CLI for running Open Containers
runc | ASL 2.0 | CLI for running Open Containers
runc | ASL 2.0 | CLI for running Open Containers
runc | ASL 2.0 | CLI for running Open Containers
runc | ASL 2.0 | CLI for running Open Containers
rust | (ASL 2.0 or MIT) and (BSD and MIT) | The Rust Programming Language
rust-srpm-macros | MIT | RPM macros for building Rust source packages
saab-fonts | GPLv2+ with exceptions | Free Punjabi Unicode OpenType Serif Font
sac | W3C | Java standard interface for CSS parser
samyak-fonts | GPLv3+ with exceptions | Free Indian truetype/opentype fonts
sane-backends | GPLv2+ and GPLv2+ with exceptions and Public Domain and IJG and LGPLv2+ and MIT | Scanner access software
sane-frontends | GPLv2+ and LGPLv2+ and GPLv2+ with exceptions | Graphical frontend to SANE
sassist | MIT | Dell SupportAssist log collector
sat4j | EPL-1.0 or LGPLv2 | A library of SAT solvers written in Java
satyr | GPLv2+ | Tools to create anonymous, machine-friendly problem reports
sbc | GPLv2 and LGPLv2+ | Sub Band Codec used by bluetooth A2DP
sbd | GPL-2.0-or-later | Storage-based death
sblim-cmpi-base | EPL-1.0 | SBLIM CMPI Base Providers
sblim-cmpi-devel | EPL | SBLIM CMPI Provider Development Support
sblim-gather | EPL | SBLIM Gatherer
sblim-indication_helper | EPL-1.0 | Toolkit for CMPI indication providers
sblim-sfcCommon | EPL | Common functions for SBLIM Small Footprint CIM Broker and CIM Client Library.
sblim-sfcb | EPL-1.0 | Small Footprint CIM Broker
sblim-sfcc | EPL-1.0 | Small Footprint CIM Client Library
sblim-wbemcli | EPL-1.0 | SBLIM WBEM Command Line Interface
scap-security-guide | BSD-3-Clause | Security guidance and baselines in SCAP formats
scap-workbench | GPLv3+ | Scanning, tailoring, editing and validation tool for SCAP content
scipy | BSD and Boost and Public Domain | Scientific Tools for Python
scipy | BSD and Boost and Public Domain | Scientific Tools for Python
scipy | BSD and Boost and Public Domain | Scientific Tools for Python
scipy | BSD and Boost and Public Domain | Scientific Tools for Python
scl-utils | GPLv2+ | Utilities for alternative packaging
scons | MIT | An Open Source software construction tool
scotch | CeCILL-C | Graph, mesh and hypergraph partitioning library
scrub | GPLv2+ | Disk scrubbing program
seabios | LGPLv3 | Open-source legacy BIOS implementation
seahorse | GPLv2+ and LGPLv2+ | A GNOME application for managing encryption keys
sendmail | Sendmail | A widely used Mail Transport Agent (MTA)
setroubleshoot | GPLv2+ | Helps troubleshoot SELinux problems
setroubleshoot-plugins | GPLv2+ | Analysis plugins for use with setroubleshoot
sevctl | ASL 2.0 | Administrative utilities for AMD SEV
sgabios | ASL 2.0 | Serial graphics BIOS option rom
sharutils | GPLv3+ and (GPLv3+ and BSD) and (LGPLv3+ or BSD) and LGPLv2+ and Public Domain and GFDL | The GNU shar utilities for packaging and unpackaging shell archives
shim-unsigned-aarch64 | BSD | First-stage UEFI bootloader
shim-unsigned-x64 | BSD | First-stage UEFI bootloader
si-units | BSD | International System of Units (JSR 363)
sil-abyssinica-fonts | OFL | SIL Abyssinica fonts
sil-nuosu-fonts | OFL | The Nuosu SIL Font
sil-padauk-fonts | OFL | A font for Burmese and the Myanmar script
sil-scheherazade-fonts | OFL | An Arabic script unicode font
sip | GPLv2 or GPLv3 and (GPLv3+ with exceptions) | SIP - Python/C++ Bindings Generator
sisu | EPL-1.0 and BSD | Eclipse dependency injection framework
sisu | EPL-1.0 and BSD | Eclipse dependency injection framework
skkdic | GPLv2+ | Dictionaries for SKK (Simple Kana-Kanji conversion program)
skopeo | ASL 2.0 | Inspect Docker images and repositories on registries
skopeo | ASL 2.0 | Inspect container images and repositories on registries
skopeo | ASL 2.0 | Inspect container images and repositories on registries
skopeo | ASL 2.0 | Inspect container images and repositories on registries
skopeo | ASL 2.0 | Inspect container images and repositories on registries
slapi-nis | GPLv3 | NIS Server and Schema Compatibility plugins for Directory Server
slf4j | MIT and ASL 2.0 | Simple Logging Facade for Java
slf4j | MIT and ASL 2.0 | Simple Logging Facade for Java
slf4j | MIT and ASL 2.0 | Simple Logging Facade for Java
slirp4netns | GPLv2 | slirp for network namespaces
slirp4netns | GPLv2 | slirp for network namespaces
slirp4netns | GPLv2 | slirp for network namespaces
slirp4netns | GPLv2 | slirp for network namespaces
slirp4netns | GPLv2 | slirp for network namespaces
smc-fonts | GPLv3+ with exceptions and GPLv2+ with exceptions and GPLv2+ and  GPLv2 and GPL+ | Open Type Fonts for Malayalam script
smc-meera-fonts | OFL | Open Type Fonts for Malayalam script
smc-rachana-fonts | OFL | Open Type Fonts for Malayalam script
socat | GPLv2 | Bidirectional data relay between two data channels ('netcat++')
socket_wrapper | BSD | A library passing all socket communications through Unix sockets
softhsm | BSD | Software version of a PKCS#11 Hardware Security Module
sombok | GPLv2+ or Artistic clarified | Unicode Text Segmentation Package
sound-theme-freedesktop | GPLv2+ and LGPLv2+ and CC-BY-SA and CC-BY | freedesktop.org sound theme
soundtouch | LGPLv2+ | Audio Processing library for changing Tempo, Pitch and Playback Rates
source-highlight | GPLv3+ | Produces a document with syntax highlighting
spamassassin | ASL 2.0 | Spam filter for email which can be invoked from mail delivery agents
sparsehash | BSD | Extremely memory-efficient C++ hash_map implementation
speech-dispatcher | GPLv2+ and GPLv2 | To provide a high-level device independent layer for speech synthesis
speex | BSD | A voice compression format (codec)
speexdsp | BSD | A voice compression format (DSP)
spice | LGPLv2+ | Implements the SPICE protocol
spice-client-win | GPLv2+ | Spice client MSI installers for Windows clients
spice-gtk | LGPLv2+ | A GTK+ widget for SPICE clients
spice-protocol | BSD and LGPLv2+ | Spice protocol header files
spice-qxl-wddm-dod | ASL 2.0 | A QXL display-only driver for Windows 10 virtual machines.
spice-streaming-agent | ASL 2.0 | SPICE streaming agent
spice-vdagent | GPLv3+ | Agent for Spice guests
spice-vdagent-win | GPLv2+ | Spice agent MSI installers for Windows guests
spirv-tools | ASL 2.0 | API and commands for processing SPIR-V modules
splix | GPLv2 | Driver for QPDL/SPL2 printers (Samsung and several Xerox printers)
squid | GPLv2+ and (LGPLv2+ and MIT and BSD and Public Domain) | The Squid proxy caching server
squid | GPLv2+ and (LGPLv2+ and MIT and BSD and Public Domain) | The Squid proxy caching server
sscg | GPLv3+ with exceptions | Simple SSL certificate generator
sshpass | GPLv2 | Non-interactive SSH authentication utility
stalld | GPLv2 | Daemon that finds starving tasks and gives them a temporary boost
startup-notification | LGPLv2 | Library for tracking application startup
stax-ex | CDDL-1.1 or GPLv2 | StAX API extensions
stix-fonts | OFL | Scientific and engineering fonts
stratis-cli | ASL 2.0 | Command-line tool for interacting with the Stratis daemon
stratisd | MPLv2.0 | Daemon that manages block devices to create filesystems
stress-ng | GPLv2+ | Stress test a computer system in various ways
subscription-manager-migration-data | CC0 | RHN Classic to RHSM migration data
subversion | ASL 2.0 | A Modern Concurrent Version Control System
subversion | ASL 2.0 | A Modern Concurrent Version Control System
suitesparse | (LGPLv2+ or BSD) and LGPLv2+ and GPLv2+ | A collection of sparse matrix libraries
supermin | GPLv2+ | Tool for creating supermin appliances
sushi | GPLv2+ with exceptions | A quick previewer for Nautilus
swig | GPLv3+ and BSD | Connects C/C++/Objective C to some high-level programming languages
swig | GPLv3+ and BSD | Connects C/C++/Objective C to some high-level programming languages
switcheroo-control | GPLv3 | D-Bus service to check the availability of dual-GPU
swtpm | BSD | TPM Emulator
sysstat | GPLv2+ | Collection of performance monitoring tools for Linux
system-config-printer | GPLv2+ | A printer administration tool
system-lsb | GPLv2 | Implementation of Linux Standard Base specification
system-rpm-config | GPL+ | Anolis OS specific rpm configuration files
systemtap | GPLv2+ | Programmable system-wide instrumentation system
taglib | LGPLv2 or MPLv1.1 | Audio Meta-Data Library
tagsoup | ASL 2.0 and (GPLv2+ or AFL) | A SAX-compliant HTML parser written in Java
tang | GPLv3+ | Network Presence Binding Daemon
targetcli | ASL 2.0 | An administration shell for storage targets
tbb | ASL 2.0 | The Threading Building Blocks library abstracts low-level threading details
tcpdump | BSD with advertising | A network traffic monitoring tool
tcsh | BSD | An enhanced version of csh, the C shell
teckit | LGPLv2+ or CPL | Conversion library and mapping compiler
telnet | BSD | The client program for the Telnet remote login protocol
tesseract | ASL 2.0 | Raw OCR Engine
tex-fonts-hebrew | GPL+ and LPPL | Culmus Hebrew fonts support for LaTeX
texi2html | GPLv2+ and OFSFDL and (CC-BY-SA or GPLv2) | A highly customizable texinfo to HTML and other formats translator
texlive | Artistic 2.0 and GPLv2 and GPLv2+ and LGPLv2+ and LPPL and MIT and Public Domain and UCD and Utopia | TeX formatting system
tftp | BSD | The client for the Trivial File Transfer Protocol (TFTP)
thai-scalable-fonts | GPLv2+ and Bitstream Vera | Thai TrueType fonts
thermald | GPLv2+ | Thermal Management daemon
thunderbird | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Thunderbird mail/newsgroup client
thunderbird | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Thunderbird mail/newsgroup client
thunderbird | MPLv1.1 or GPLv2+ or LGPLv2+ | Mozilla Thunderbird mail/newsgroup client
tibetan-machine-uni-fonts | GPLv3+ with exceptions | Tibetan Machine Uni font for Tibetan, Dzongkha and Ladakhi
tigervnc | GPLv2+ | A TigerVNC remote display system
tinycdb | Public Domain | Utility and library for manipulating constant databases
tinyxml2 | zlib | Simple, small and efficient C++ XML parser
tix | TCL | A set of extension widgets for Tk
tk | TCL | The graphical toolkit for the Tcl scripting language
tlog | GPLv2+ | Terminal I/O logger
tog-pegasus | MIT | OpenPegasus WBEM Services for Linux
tokyocabinet | LGPLv2+ | A modern implementation of a DBM
tomcatjss | LGPLv2+ | JSS Connector for Apache Tomcat
toolbox | ASL 2.0 | Script to launch privileged container with podman
toolbox | ASL 2.0 | Unprivileged development environment
toolbox | ASL 2.0 | Tool for containerized command line environments on Linux
toolbox | ASL 2.0 | Tool for containerized command line environments on Linux
torque | OpenPBS and TORQUEv1.1 | Tera-scale Open-source Resource and QUEue manager
totem | GPLv2+ with exceptions | Movie player for GNOME
totem-pl-parser | LGPLv2+ | Totem Playlist Parser library
tracer | GPLv2+ | Finds outdated running applications in your system
tracker | GPLv2+ | Desktop-neutral metadata database and search tool
tracker-miners | GPLv2+ and LGPLv2+ | Tracker miners and metadata extractors
transfig | MIT | Utility for converting FIG files (made by xfig) to other formats
ttmkfdir | LGPLv2+ | Utility to create fonts.scale files for truetype fonts
twolame | LGPLv2+ | Optimized MPEG Audio Layer 2 encoding library based on tooLAME
ucs-miscfixed-fonts | Public Domain | Selected set of bitmap fonts
ucx | BSD | UCX is a communication library implementing high-performance messaging
udftools | GPLv2+ | Linux UDF Filesystem userspace utilities
udica | GPLv3+ | A tool for generating SELinux security policies for containers
udica | GPLv3+ | A tool for generating SELinux security policies for containers
udica | GPLv3+ | A tool for generating SELinux security policies for containers
udica | GPLv3+ | A tool for generating SELinux security policies for containers
udisks2 | GPLv2+ | Disk Manager
uglify-js | BSD | JavaScript parser, mangler/compressor and beautifier toolkit
uid_wrapper | GPLv3+ | A wrapper for privilege separation
unbound | BSD | Validating, recursive, and caching DNS(SEC) resolver
unicode-ucd | MIT | Unicode Character Database
unit-api | BSD | JSR 363 - Units of Measurement API
univocity-parsers | ASL 2.0 | Collection of parsers for Java
unixODBC | GPLv2+ and LGPLv2+ | A complete ODBC driver manager for Linux
uom-lib | BSD | Java Unit of Measurement Libraries (JSR 363)
uom-parent | BSD | Units of Measurement Project Parent POM
uom-se | BSD | Unit Standard (JSR 363) implementation for Java SE 8 and above
uom-systems | BSD | Units of Measurement Systems (JSR 363)
upower | GPLv2+ | Power Management Service
urlview | GPLv2+ | URL extractor/launcher
urw-base35-fonts | AGPLv3 | Core Font Set containing 35 freely distributable fonts from (URW)++
usbguard | GPLv2+ | A tool for implementing USB device usage policy
usbguard | GPLv2+ | A tool for implementing USB device usage policy
usbmuxd | GPLv3+ or GPLv2+ | Daemon for communicating with Apple's iOS devices
usbredir | LGPLv2+ | USB network redirection protocol libraries
ustr | MIT or LGPLv2+ or BSD | String library, very low memory overhead, simple to import
utf8proc | Unicode and MIT | Library for processing UTF-8 encoded Unicode strings
utf8proc | Unicode and MIT | Library for processing UTF-8 encoded Unicode strings
uthash | BSD | A hash table for C structures
uuid | MIT | Universally Unique Identifier library
v4l-utils | GPLv2+ and GPLv2 | Utilities for video4linux and DVB devices
vala | LGPLv2+ and BSD | A modern programming language for GNOME
valgrind | GPLv2+ | Dynamic analysis tools to detect memory or thread bugs and profile
varnish | BSD | High-performance HTTP accelerator
varnish | BSD | High-performance HTTP accelerator
varnish-modules | BSD | A collection of modules ("vmods") extending Varnish VCL
velocity | ASL 2.0 | Java-based template engine
vinagre | GPLv2+ | VNC client for GNOME
vino | GPLv2+ | A remote desktop system for GNOME
virt-manager | GPLv2+ | Desktop tool for managing virtual machines via libvirt
virt-p2v | GPLv2+ | Convert a physical machine to run on KVM
virt-top | GPLv2+ | Utility like top(1) for displaying virtualization stats
virt-viewer | GPLv2+ | Virtual Machine Viewer
virtio-win | Red Hat Proprietary and BSD-3-Clause and Apache and GPLv2 | VirtIO para-virtualized drivers for Windows(R)
volume_key | GPLv2 and (MPLv1.1 or GPLv2 or LGPLv2) | An utility for manipulating storage encryption keys and passphrases
vorbis-tools | GPLv2 | The Vorbis General Audio Compression Codec tools
vsftpd | GPLv2 with exceptions | Very Secure Ftp Daemon
vte291 | LGPLv2+ | Terminal emulator library
vulkan-headers | ASL 2.0 | Vulkan Header files and API registry
vulkan-loader | ASL 2.0 | Vulkan ICD desktop loader
vulkan-tools | ASL 2.0 | Vulkan tools
vulkan-validation-layers | ASL 2.0 | Vulkan validation layers
wavpack | BSD | A completely open audiocodec
wayland | MIT | Wayland Compositor Infrastructure
wayland-protocols | MIT | Wayland protocols that adds functionality not available in the core protocol
web-assets | MIT | A simple framework for bits pushed to browsers
webkit2gtk3 | LGPLv2 | GTK Web content engine library
webkit2gtk3 | LGPLv2 | GTK Web content engine library
webrtc-audio-processing | BSD and MIT | Library for echo cancellation
weldr-client | ASL 2.0 | Command line utility to control osbuild-composer
wget | GPLv3+ | A utility for retrieving files using the HTTP or FTP protocols
whois | GPLv2+ | Improved WHOIS client
wireshark | GPL+ | Network traffic analyzer
woff2 | MIT | Web Open Font Format 2.0 library
wpebackend-fdo | BSD | A WPE backend designed for Linux desktop systems
wqy-microhei-fonts | ASL 2.0 or GPLv3 with exceptions | Compact Chinese fonts derived from Droid
wqy-unibit-fonts | GPLv2 with exceptions | WenQuanYi Unibit Bitmap Font
wsmancli | BSD | WS-Management-Command line Interface
xalan-j2 | ASL 2.0 and W3C | Java XSLT processor
xapian-core | GPLv2+ | The Xapian Probabilistic Information Retrieval Library
xcb-proto | MIT | XCB protocol descriptions
xcb-util | MIT | Convenience libraries sitting on top of libxcb
xcb-util-image | MIT | Port of Xlib's XImage and XShmImage functions on top of libxcb
xcb-util-keysyms | MIT | Standard X key constants and keycodes conversion on top of libxcb
xcb-util-renderutil | MIT | Convenience functions for the Render extension
xcb-util-wm | MIT | Client and window-manager helper library on top of libxcb
xdg-desktop-portal | LGPLv2+ | Portal frontend service to flatpak
xdg-desktop-portal-gnome | LGPLv2+ | Backend implementation for xdg-desktop-portal using GNOME
xdg-desktop-portal-gtk | LGPLv2+ | Backend implementation for xdg-desktop-portal using GTK+
xdg-user-dirs | GPLv2+ and MIT | Handles user special directories
xdg-user-dirs-gtk | GPL+ | Gnome integration of special directories
xdg-utils | MIT | Basic desktop integration functions
xdp-tools | GPLv2 | Utilities and example programs for use with XDP
xerces-j2 | ASL 2.0 and W3C | Java XML parser
xhtml1-dtds | W3C | XHTML 1.0 document type definitions
xinetd | xinetd | A secure replacement for inetd
xkeyboard-config | MIT | X Keyboard Extension configuration data
xml-commons-apis | ASL 2.0 and W3C and Public Domain | APIs for DOM, SAX, and JAXP
xml-commons-apis | ASL 2.0 and W3C and Public Domain | APIs for DOM, SAX, and JAXP
xml-commons-resolver | ASL 2.0 | Resolver subproject of xml-commons
xmlgraphics-commons | ASL 2.0 | XML Graphics Commons
xmlsec1 | MIT | Library providing support for "XML Signature" and "XML Encryption" standards
xmlstreambuffer | CDDL-1.0 or GPLv2 with exceptions | XML Stream Buffer
xmlto | GPLv2+ | A tool for converting XML files to various formats
xmltoman | GPLv2+ | Scripts for converting XML to roff or HTML
xorg-sgml-doctools | MIT | X.Org SGML documentation generation tools
xorg-x11-apps | MIT | X.Org X11 applications
xorg-x11-docs | MIT | X.Org X11 documentation
xorg-x11-drivers | MIT | X.Org X11 driver installation package
xorg-x11-drv-ati | MIT | Xorg X11 ati video driver
xorg-x11-drv-dummy | MIT | Xorg X11 dummy video driver
xorg-x11-drv-evdev | MIT | Xorg X11 evdev input driver
xorg-x11-drv-fbdev | MIT | Xorg X11 fbdev video driver
xorg-x11-drv-intel | MIT | Xorg X11 Intel video driver
xorg-x11-drv-libinput | MIT | Xorg X11 libinput input driver
xorg-x11-drv-nouveau | MIT | Xorg X11 nouveau video driver for NVIDIA graphics chipsets
xorg-x11-drv-qxl | MIT | Xorg X11 qxl video driver
xorg-x11-drv-v4l | MIT | Xorg X11 v4l video driver
xorg-x11-drv-vesa | MIT | Xorg X11 vesa video driver
xorg-x11-drv-vmware | MIT | Xorg X11 vmware video driver
xorg-x11-drv-wacom | GPLv2+ | Xorg X11 wacom input driver
xorg-x11-drv-wacom | GPLv2+ | Xorg X11 wacom input driver
xorg-x11-font-utils | MIT | X.Org X11 font utilities
xorg-x11-fonts | MIT and Lucida and Public Domain | X.Org X11 fonts
xorg-x11-proto-devel | MIT | X.Org X11 Protocol headers
xorg-x11-server | MIT | X.Org X11 X server
xorg-x11-server-Xwayland | MIT | Xwayland
xorg-x11-server-utils | MIT | X.Org X11 X server utilities
xorg-x11-util-macros | MIT | X.Org X11 Autotools macros
xorg-x11-utils | MIT | X.Org X11 X client utilities
xorg-x11-xauth | MIT | X.Org X11 X authority utilities
xorg-x11-xbitmaps | MIT | X.Org X11 application bitmaps
xorg-x11-xinit | MIT | X.Org X11 X Window System xinit startup scripts
xorg-x11-xkb-utils | MIT | X.Org X11 xkb utilities
xorg-x11-xtrans-devel | MIT | X.Org X11 developmental X transport library
xrestop | GPLv2+ | X Resource Monitor
xsane | GPLv2+ and LGPLv2+ | X Window System front-end for the SANE scanner interface
xsom | CDDL-1.1 or GPLv2 with exceptions | XML Schema Object Model (XSOM)
xterm | MIT | Terminal emulator for the X Window System
xz-java | Public Domain | Java implementation of XZ data compression
yajl | ISC | Yet Another JSON Library (YAJL)
yasm | BSD and (GPLv2+ or Artistic or LGPLv2+) and LGPLv2 | Modular Assembler
yelp | LGPLv2+ and ASL 2.0 and GPLv2+ | Help browser for the GNOME desktop
yelp-tools | GPLv2+ | Create, manage, and publish documentation for Yelp
yelp-xsl | LGPLv2+ and GPLv2+ | XSL stylesheets for the yelp help browser
yp-tools | GPLv2 | NIS (or YP) client programs
ypbind | GPLv2 | The NIS daemon which binds NIS clients to an NIS domain
ypserv | GPLv2 | The NIS (Network Information Service) server
zaf | LGPLv2+ | South Africa hyphenation rules
zenity | LGPLv2+ | Display dialog boxes from shell scripts
zziplib | LGPLv2+ or MPLv1.1 | Lightweight library to easily extract data from zip files

## 3. PowerTools
### 3.1 PowerTools 软件包清单
PowerTools 软件包库包含开发人员使用的附加软件包。该软件包库未包含在 ISO 镜像中，只以在线仓库形式提供，可按需启用。

下表列出了 Anolis OS 8.9 GA PowerTools 软件包库中的所有软件包及其许可证。

软件包 | 许可协议 | 功能简述
-------|----------|---------
alsa-sof-firmware | BSD | Firmware and topology files for Sound Open Firmware project
ant | ASL 2.0 | Java build tool
ant-contrib | ASL 2.0 and ASL 1.1 | Collection of tasks for Ant
antlr | ANTLR-PD | ANother Tool for Language Recognition
aopalliance | Public Domain | Java/J2EE AOP standards
apache-commons-beanutils | ASL 2.0 | Java utility methods for accessing and modifying the properties of arbitrary JavaBeans
apache-commons-cli | ASL 2.0 | Command Line Interface Library for Java
apache-commons-codec | ASL 2.0 | Implementations of common encoders and decoders
apache-commons-collections | ASL 2.0 | Provides new interfaces, implementations and utilities for Java Collections
apache-commons-compress | ASL 2.0 | Java API for working with compressed files and archivers
apache-commons-exec | ASL 2.0 | Java library to reliably execute external processes from within the JVM
apache-commons-io | ASL 2.0 | Utilities to assist with developing IO functionality
apache-commons-jxpath | ASL 2.0 | Simple XPath interpreter
apache-commons-lang | ASL 2.0 | Provides a host of helper utilities for the java.lang API
apache-commons-lang3 | ASL 2.0 | Provides a host of helper utilities for the java.lang API
apache-commons-logging | ASL 2.0 | Apache Commons Logging
apache-commons-net | ASL 2.0 | Internet protocol suite Java library
apache-commons-parent | ASL 2.0 | Apache Commons Parent Pom
apache-ivy | ASL 2.0 | Java-based dependency manager
apache-parent | ASL 2.0 | Parent POM file for Apache projects
apache-resource-bundles | ASL 2.0 | Apache Resource Bundles
aqute-bnd | ASL 2.0 | BND Tool
asio | Boost | A cross-platform C++ library for network programming
asio | Boost | A cross-platform C++ library for network programming
assertj-core | ASL 2.0 | Library of assertions similar to fest-assert
atinject | ASL 2.0 | Dependency injection specification for Java (JSR-330)
bcel | ASL 2.0 | Byte Code Engineering Library
beust-jcommander | ASL 2.0 | Java framework for parsing command line parameters
bsf | ASL 2.0 | Bean Scripting Framework
bsh | ASL 2.0 and BSD and Public Domain | Lightweight Scripting for Java
byaccj | Public Domain | Parser Generator with Java Extension
cal10n | MIT | Compiler assisted localization library (CAL10N)
cdi-api | ASL 2.0 | CDI API
cglib | ASL 2.0 and BSD | Code Generation Library for Java
dleyna-connector-dbus | LGPLv2 | D-Bus connector for dLeyna services
dleyna-core | LGPLv2 | Utilities for higher level dLeyna libraries
dleyna-renderer | LGPLv2 | Service for interacting with Digital Media Renderers
dleyna-server | LGPLv2 | Service for interacting with Digital Media Servers
dmidecode | GPLv2+ | Tool to analyse BIOS DMI data
dotnet5.0-build-reference-packages | MIT | Reference packages needed by the .NET 5.0 SDK build
easymock | ASL 2.0 | Easy mock objects
eog | GPLv2+ and GFDL | Eye of GNOME image viewer
exec-maven-plugin | ASL 2.0 | Exec Maven Plugin
felix-osgi-compendium | ASL 2.0 | Felix OSGi R4 Compendium Bundle
felix-osgi-core | ASL 2.0 | Felix OSGi R4 Core Bundle
felix-osgi-foundation | ASL 2.0 | Felix OSGi Foundation EE Bundle
felix-parent | ASL 2.0 | Parent POM file for Apache Felix Specs
felix-utils | ASL 2.0 | Utility classes for OSGi
flute | W3C and LGPLv2+ | Java CSS parser using SAC
forge-parent | ASL 2.0 | Sonatype Forge Parent Pom
fusesource-pom | ASL 2.0 | Parent POM for FuseSource Maven projects
gcc-toolset-10-dyninst | LGPLv2+ | An API for Run-time Code Generation
gcc-toolset-10-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-11-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-12-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-13 | GPLv2+ | Package that installs gcc-toolset-13
gcc-toolset-13-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
gcc-toolset-9-gdb | GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL | A GNU source-level debugger for C, C++, Fortran, Go and other languages
geronimo-annotation | ASL 2.0 | Java EE: Annotation API v1.3
geronimo-jms | ASL 2.0 | J2EE JMS v1.1 API
geronimo-jpa | ASL 2.0 | Java persistence API implementation
geronimo-parent-poms | ASL 2.0 | Parent POM files for geronimo-specs
gfbgraph | LGPLv2+ | GLib/GObject wrapper for the Facebook Graph API
glassfish-annotation-api | CDDL or GPLv2 with exceptions | Common Annotations API Specification (JSR 250)
glassfish-el | CDDL-1.1 or GPLv2 with exceptions | J2EE Expression Language Implementation
glassfish-jsp-api | (CDDL-1.1 or GPLv2 with exceptions) and ASL 2.0 | Glassfish J2EE JSP API specification
glassfish-legal | CDDL or GPLv2 with exceptions | Legal License for glassfish code
glassfish-master-pom | CDDL or GPLv2 with exceptions | Master POM for Glassfish Maven projects
glassfish-servlet-api | (CDDL or GPLv2 with exceptions) and ASL 2.0 | Java Servlet API
gnome-boxes | LGPLv2+ | A simple GNOME 3 application to access remote or virtual systems
gnome-color-manager | GPLv2+ | Color management tools for GNOME
gnome-online-miners | GPLv2+ and LGPLv2+ and MIT | Crawls through your online content
gnome-video-effects | GPLv2 | Collection of GStreamer video effects
gom | LGPLv2+ | GObject to SQLite object mapper library
google-guice | ASL 2.0 | Lightweight dependency injection framework for Java 5 and above
grilo-plugins | LGPLv2+ | Plugins for the Grilo framework
guava20 | ASL 2.0 and CC0 | Google Core Libraries for Java
gupnp-av | LGPLv2+ | A collection of helpers for building UPnP AV applications
gupnp-dlna | LGPLv2+ | A collection of helpers for building UPnP AV applications
hamcrest | BSD | Library of matchers for building test expressions
hawtjni | ASL 2.0 and EPL-1.0 and BSD | Code generator that produces the JNI code
httpcomponents-client | ASL 2.0 | HTTP agent implementation based on httpcomponents HttpCore
httpcomponents-core | ASL 2.0 | Set of low level Java HTTP transport components for HTTP services
httpcomponents-project | ASL 2.0 | Common POM file for HttpComponents
isorelax | MIT and ASL 1.1 | Public interfaces for RELAX Core
jakarta-commons-httpclient | ASL 2.0 and (ASL 2.0 or LGPLv2+) | Jakarta Commons HTTPClient implements the client side of HTTP standards
jakarta-oro | ASL 1.1 | Full regular expressions API
jansi | ASL 2.0 | Jansi is a java library for generating and interpreting ANSI escape sequences
jansi-native | ASL 2.0 | Jansi Native implements the JNI Libraries used by the Jansi project
java-1.8.0-openjdk-portable | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib | OpenJDK 8 Runtime Environment portable edition
java-11-openjdk-portable | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 11 Runtime Environment portable edition
java-17-openjdk-portable | ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA | OpenJDK 17 Runtime Environment portable edition
java_cup | MIT | LALR parser generator for Java
javacc | BSD | A parser/scanner generator for java
javacc-maven-plugin | ASL 2.0 | JavaCC Maven Plugin
javamail | CDDL-1.0 or GPLv2 with exceptions | Java Mail API
javapackages-tools | BSD | Macros and scripts for Java packaging support
javassist | MPLv1.1 or LGPLv2+ or ASL 2.0 | The Java Programming Assistant provides simple Java bytecode manipulation
jaxen | BSD and W3C | An XPath engine written in Java
jboss-interceptors-1.2-api | CDDL or GPLv2 with exceptions | Java EE Interceptors 1.2 API
jboss-parent | CC0 | JBoss Parent POM
jdepend | BSD | Java Design Quality Metrics
jdependency | ASL 2.0 | This project provides an API to analyse class dependencies
jdom | Saxpath | Java alternative to DOM and SAX
jdom2 | Saxpath | Java manipulation of XML made easy
jflex | BSD | Fast Scanner Generator
jline | BSD | JLine is a Java library for handling console input
jsch | BSD | Pure Java implementation of SSH2
jsoup | MIT | Java library for working with real-world HTML
jsr-305 | BSD and CC-BY | Correctness annotations for Java code
jtidy | zlib | HTML syntax checker and pretty printer
junit | EPL-1.0 | Java regression test package
jvnet-parent | ASL 2.0 | Java.net parent POM file
jzlib | BSD | Re-implementation of zlib in pure Java
libabigail | LGPLv3+ | Set of ABI analysis tools
libbase | LGPLv2 | JFree Base Services
libdlb | BSD-3-Clause and GPL-2.0 | intel dynamic load balancer
libdmapsharing | LGPLv2+ | A DMAP client and server library
libfonts | LGPLv2 and UCD | TrueType Font Layouting
libformula | LGPLv2 | Formula Parser
liblayout | LGPLv2+ and UCD | CSS based layouting framework
libloader | LGPLv2 | Resource Loading Framework
libmediaart | LGPLv2+ | Library for managing media art caches
librepository | LGPLv2 | Hierarchical repository abstraction layer
libserializer | LGPLv2+ | JFreeReport General Serialization Framework
log4j12 | ASL 2.0 | Java logging package
maven | ASL 2.0 and MIT | Java project management and project comprehension tool
maven-antrun-plugin | ASL 2.0 | Maven AntRun Plugin
maven-archiver | ASL 2.0 | Maven Archiver
maven-artifact-resolver | ASL 2.0 | Maven Artifact Resolution API
maven-artifact-transfer | ASL 2.0 | Apache Maven Artifact Transfer
maven-assembly-plugin | ASL 2.0 | Maven Assembly Plugin
maven-clean-plugin | ASL 2.0 | Maven Clean Plugin
maven-common-artifact-filters | ASL 2.0 | Maven Common Artifact Filters
maven-compiler-plugin | ASL 2.0 | Maven Compiler Plugin
maven-dependency-analyzer | ASL 2.0 | Maven dependency analyzer
maven-dependency-plugin | ASL 2.0 | Plugin to manipulate, copy and unpack local and remote artifacts
maven-dependency-tree | ASL 2.0 | Maven dependency tree artifact
maven-doxia | ASL 2.0 | Content generation framework
maven-doxia-sitetools | ASL 2.0 | Doxia content generation framework
maven-enforcer | ASL 2.0 | Maven Enforcer
maven-file-management | ASL 2.0 | Maven File Management API
maven-filtering | ASL 2.0 | Shared component providing resource filtering
maven-install-plugin | ASL 2.0 | Maven Install Plugin
maven-invoker | ASL 2.0 | Fires a maven build in a clean environment
maven-invoker-plugin | ASL 2.0 | Maven Invoker Plugin
maven-jar-plugin | ASL 2.0 | Maven JAR Plugin
maven-parent | ASL 2.0 | Apache Maven parent POM
maven-plugin-build-helper | MIT | Build Helper Maven Plugin
maven-plugin-bundle | ASL 2.0 | Maven Bundle Plugin
maven-plugin-testing | ASL 2.0 | Maven Plugin Testing
maven-plugin-tools | ASL 2.0 | Maven Plugin Tools
maven-plugins-pom | ASL 2.0 | Maven Plugins POM
maven-remote-resources-plugin | ASL 2.0 | Maven Remote Resources Plugin
maven-reporting-api | ASL 2.0 | API to manage report generation
maven-reporting-impl | ASL 2.0 | Abstract classes to manage report generation
maven-resolver | ASL 2.0 | Apache Maven Artifact Resolver library
maven-resources-plugin | ASL 2.0 | Maven Resources Plugin
maven-script-interpreter | ASL 2.0 | Maven Script Interpreter
maven-shade-plugin | ASL 2.0 | This plugin provides the capability to package the artifact in an uber-jar
maven-shared | ASL 2.0 | Maven Shared Components
maven-shared-incremental | ASL 2.0 | Maven Incremental Build support utilities
maven-shared-io | ASL 2.0 | API for I/O support like logging, download or file scanning
maven-shared-utils | ASL 2.0 | Maven shared utility classes
maven-source-plugin | ASL 2.0 | Plugin creating source JAR
maven-surefire | ASL 2.0 and CPL | Test framework project
maven-verifier | ASL 2.0 | Maven verifier
maven-wagon | ASL 2.0 | Tools to manage artifacts and deployment
maven2 | ASL 2.0 | Java project management and project comprehension tool
mockito | MIT | A Java mocking framework
modello | ASL 2.0 and BSD and MIT | Modello Data Model toolkit
mojo-parent | ASL 2.0 | Codehaus MOJO parent project pom file
munge-maven-plugin | CDDL-1.0 | Munge Maven Plugin
objectweb-asm | BSD | Java bytecode manipulation and analysis framework
objectweb-pom | ASL 2.0 | Objectweb POM
objenesis | ASL 2.0 | A library for instantiating Java objects
os-maven-plugin | ASL 2.0 | Maven plugin for generating platform-dependent properties
osgi-annotation | ASL 2.0 | Annotations for use in compiling OSGi bundles
osgi-compendium | ASL 2.0 | Interfaces and Classes for use in compiling OSGi bundles
osgi-core | ASL 2.0 | OSGi Core API
pentaho-libxml | LGPLv2 | Namespace aware SAX-Parser utility library
pentaho-reporting-flow-engine | LGPLv2+ | Pentaho Flow Reporting Engine
plexus-ant-factory | ASL 2.0 | Plexus Ant component factory
plexus-archiver | ASL 2.0 | Plexus Archiver Component
plexus-bsh-factory | MIT | Plexus Bsh component factory
plexus-build-api | ASL 2.0 | Plexus Build API
plexus-cipher | ASL 2.0 | Plexus Cipher: encryption/decryption Component
plexus-classworlds | ASL 2.0 and Plexus | Plexus Classworlds Classloader Framework
plexus-cli | ASL 2.0 | Command Line Interface facilitator for Plexus
plexus-compiler | MIT and ASL 2.0 | Compiler call initiators for Plexus
plexus-component-api | ASL 2.0 | Plexus Component API
plexus-component-factories-pom | ASL 2.0 | Plexus Component Factories POM
plexus-components-pom | ASL 2.0 | Plexus Components POM
plexus-containers | ASL 2.0 and MIT and xpp | Containers for Plexus
plexus-i18n | ASL 2.0 | Plexus I18N Component
plexus-interactivity | MIT | Plexus Interactivity Handler Component
plexus-interpolation | ASL 2.0 and ASL 1.1 and MIT | Plexus Interpolation API
plexus-io | ASL 2.0 | Plexus IO Components
plexus-languages | ASL 2.0 | Plexus Languages
plexus-pom | ASL 2.0 | Root Plexus Projects POM
plexus-resources | MIT | Plexus Resource Manager
plexus-sec-dispatcher | ASL 2.0 | Plexus Security Dispatcher Component
plexus-utils | ASL 1.1 and ASL 2.0 and xpp and BSD and Public Domain | Plexus Common Utilities
plexus-velocity | ASL 2.0 | Plexus Velocity Component
powermock | ASL 2.0 | A Java mocking framework
pytest | MIT | Simple powerful testing with Python
python-atomicwrites | MIT | Python Atomic file writes on POSIX
python-attrs | MIT | Python attributes without boilerplate
python-joblib | BSD | Lightweight pipelining: using Python functions as pipeline jobs
python-more-itertools | MIT | Python library for efficient use of itertools utility
python-packaging | BSD or ASL 2.0 | Core utilities for Python packages
python-pluggy | MIT | The plugin manager stripped of pytest specific details
python-py | MIT and Public Domain | Library with cross-python path, ini-parsing, io, code, log facilities
python-wcwidth | MIT | Measures number of Terminal column cells of wide-character codes
python3x-pyparsing | MIT | Python package with an object-oriented approach to text processing
qdox | ASL 2.0 | Extract class/interface/method definitions from sources
regexp | ASL 2.0 | Simple regular expressions API
rhythmbox | GPLv2+ with exceptions and GFDL | Music Management Application
rubygem-asciidoctor | MIT | A fast, open source AsciiDoc implementation in Ruby
sac | W3C | Java standard interface for CSS parser
sisu | EPL-1.0 and BSD | Eclipse dependency injection framework
sisu-mojos | EPL-1.0 | Sisu plugin for Apache Maven
slf4j | MIT and ASL 2.0 | Simple Logging Facade for Java
sonatype-oss-parent | ASL 2.0 | Sonatype OSS Parent
sonatype-plugins-parent | ASL 2.0 | Sonatype Plugins Parent POM
spec-version-maven-plugin | CDDL or GPLv2 with exceptions | Spec Version Maven Plugin
spice-parent | ASL 2.0 | Sonatype Spice Components
testng | ASL 2.0 | Java-based testing framework
totem | GPLv2+ with exceptions | Movie player for GNOME
umockdev | LGPLv2+ | Mock hardware devices
velocity | ASL 2.0 | Java-based template engine
weld-parent | ASL 2.0 | Parent POM for Weld
xalan-j2 | ASL 2.0 and W3C | Java XSLT processor
xbean | ASL 2.0 | Java plugin based web server
xerces-j2 | ASL 2.0 and W3C | Java XML parser
xml-commons-apis | ASL 2.0 and W3C and Public Domain | APIs for DOM, SAX, and JAXP
xml-commons-resolver | ASL 2.0 | Resolver subproject of xml-commons
xmlunit | BSD | Provides classes to do asserts on xml
xmvn | ASL 2.0 | Local Extensions for Apache Maven
xz-java | Public Domain | Java implementation of XZ data compression
<!-- endtoc -->
