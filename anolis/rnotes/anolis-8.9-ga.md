Anolis OS 8.9 发行声明
=====================

<!-- toc -->
## 1. 引言
龙蜥操作系统 Anolis OS 8 是 OpenAnolis 龙蜥社区发行的开源 Linux 发行版，支持多计算架构，提供稳定、高性能、安全、可靠的操作系统支持。本文档是最新的 Anolis OS 8.9 版本的发行声明，提供了该版本的交付物清单与获取版本的方式，并介绍了该版本中的新特性、功能改进和缺陷修复等发布详情，以及介绍了该版本的已知问题和其他发布详情信息。 Anolis OS 8.9 是 Anolis OS 8 发布的第五个小版本。

## 2. 交付物清单

Anolis OS 8.9 发布交付物包含 ISO 镜像、虚拟机镜像、容器镜像和 YUM 仓库。
该版本发布的所有交付物清单及下载链接，可以在[社区网站](https://mirrors.openanolis.cn/anolis/8/)中找到详细信息。

### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-8.9-x86_64-dvd.iso | x86_64 架构的基础安装 ISO, 约 16 GB
AnolisOS-8.9-x86_64-minimal.iso | x86_64 架构的精简安装 ISO, 约 2.5 GB
AnolisOS-8.9-x86_64-boot.iso | x86_64 架构的网络安装 ISO, 约 1.0 GB
AnolisOS-8.9-aarch64-dvd.iso | aarch64 架构的基础安装 ISO, 约 13 GB
AnolisOS-8.9-aarch64-minimal.iso | aarch64 架构的精简安装 ISO, 约 2.3 GB
AnolisOS-8.9-aarch64-boot.iso | aarch64 架构的网络安装 ISO, 约 942 MB
AnolisOS-8.9-loongarch64-dvd.iso | loongarch64 架构的基础安装 ISO, 约 8.6 GB
AnolisOS-8.9-loongarch64-minimal.iso | loongarch64 架构的精简安装 ISO, 约 1.8 GB
AnolisOS-8.9-loongarch64-boot.iso | loongarch64 架构的网络安装 ISO, 约 842 MB

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
AnolisOS-8.9-x86_64-ANCK.qcow2 | x86_64 架构 QEMU 虚拟机镜像
AnolisOS-8.9-x86_64-RHCK.qcow2 | x86_64 架构 QEMU 虚拟机镜像
AnolisOS-8.9-aarch64-ANCK.qcow2 | aarch64 架构 QEMU 虚拟机镜像
AnolisOS-8.9-aarch64-RHCK.qcow2 | aarch64 架构 QEMU 虚拟机镜像
AnolisOS-8.9-loongarch64.qcow2 | loongarch64 架构 QEMU 虚拟机镜像

### 2.3 容器镜像
名称 | 描述
-----|-----
AnolisOS-8.9-x86_64-docker.tar | x86_64 架构容器镜像
AnolisOS-8.9-aarch64-docker.tar | aarch64 架构容器镜像
AnolisOS-8.9-loongarch64-docker.tar | loongarch64 架构容器镜像

### 2.4 软件 YUM 仓库
名称 | 描述
-----|-----
BaseOS | BaseOS 软件包源，该源目的是提供安装基础的所有核心包。
AppStream | AppStream 软件包源，该源提供额外的多场景，多用途的用户态程序，数据库等。
PowerTools | PowerTools 软件包源，该源提供开发者需要的额外包。
Plus | Plus 软件包源，提供社区滚动内核以及相应的组件。
DDE | DDE 软件包源，提供 DDE 桌面环境以及相应的组件。
Extras | repo仓库包源，提供了各类衍生仓库的仓库包。
kernel-5.10 | 5.10 内核软件包源，提供 5.10 内核包以及相应的组件。
EAPO | EAPO 软件包源，提供社区孵化类软件，不默认开启，需要安装 anolis-epao-release 后使用。

## 3. 发布详情
### 3.1 概述
#### 3.1.1 亮点
- **新平台**：Anolis OS 8.9 全面支持海光四代 CPU 平台。
- **新特性**：海光安全特性支持，新增CSV/CSV2机密虚拟机启动/迁移、机密容器支持。新增CSV3安全虚拟机，支持热迁移功能。

#### 3.1.2 发行版整体支持
- Anolis OS 8.9 ANCK 镜像默认内核变更为 5.10.134-16.2 版本。
- Anolis OS 8.9 正式支持 GB 18030-2022 字符集标准，并匹配阿里巴巴普惠字体实现对应的展示。
- 虚拟机社区镜像支持 Legacy + UEFI 双启动。

#### 3.1.3 平台支持
- **全面支持海光四号处理器(749X、748X、34XX)。**
  - 包括全新 CPU 拓扑、x2APIC、微码加载、MCA RAS 功能、DDR5、SR-IOV、QoS、L3 Perf/PMU 以及温度监控等。
  - 新增 CSV3 安全虚拟机支持，包括支持热迁移功能，支持硬件保护虚拟机页表，硬件隔离虚拟机内存，修复主机 VMM 无法读写虚拟机内存问题。

- **更完善地支持 loongarch64 架构平台。**
  - 支持龙芯 3A5000、3A6000、3C5000、3D5000 等多个基于 loongarch 架构设计的 CPU。
  - 增加对上述 CPU 配套的 7A1000/7A2000 桥片，以及桥片中的各种外设 I/O 驱动和显示设备的支持。
  - 增加对 Loongarch 2K0500BMC 驱动的支持。
  - 在支持 Loongarch 基础架构的基础上，进一步增加对向量指令、虚拟化、二进制翻译等扩展功能的支持。
  - 增加对 perf、ftrace、kdump、uprobe、kprobe、kretprobes 等调测工具的支持。

### 3.2 L0 层软件（内核层）
#### 3.2.1 ANCK-5.10

对于全新安装的操作系统，无论是通过 ISO 镜像安装，还是启动 Anolis OS 8.9 虚拟机镜像，默认的内核版本是 5.10 版本，Anolis OS 8.9 默认搭载的内核版本是 `5.10.134-16.2.an8`, 可以在系统内执行下列命令查看对应的内核版本信息：
```bash
$ uname -r
5.10.134-16.2.an8.x86_64
```
- 增加对国产申威 (sw_64 6b) 架构的基础支持。
- 增加对海光四号 CPU 的全面支持。
- 增加对飞腾 S5000C 处理器的支持。
- 内核密码模块（Cryptographic Coprocessor）的国密支持。
- 使能 ANCK 5.10 kABI 机制。
- 支持 core scheduling。
- 在 arm64 中支持 eBPF trampoline 特性。
- 支持 mglru 特性。
- batch TLB flushing 支持。
- ARM64 内核态触发 RAS 事件增强。
- SMC-D loopback 特性。
- 支持页表绑核，提供页表跨 die 的统计。
- 代码多副本增强。
- kfence 增强。
- 提供 memcg THP控制接口。
- ACPU（Assess CPU）支持。
- 自研HT-aware-quota特性。
- 提供 Group Identity 2.0 细粒度优先级特性。
- Group Identity 2.0 CPU share 比例校准。
- 增加 Group Identity 2.0 force idled time 指标。
- 提供 cgroup v2 IO SLI。

### 3.3 L1 层（核心层）软件
#### 3.3.1 核心库
+ glibc 升级至 2.28-236 版本，支持GB 18030-2022字符集标准，增加海光支持，同时修复了CVE-2023-4911，CVE-2023-4806 等问题。[贡献团队：发布小组 SIG]

### 3.4 L2 层（系统层）软件
#### 3.4.1 系统工具
+ ethtool 升级至 6.6 版本，更好的支持了 CMIS 协议，并支持更新的光模块。[贡献团队：发布小组 SIG]

#### 3.4.2 系统库
+ **Intel SPR 特性更新。** 发布外设驱动 ，增强 QAT/DLB/IAA 等加速能力。[贡献团队：Intel Arch SIG]
  - intel-QAT20 升级至 intel-QAT20-L.0.9.4-00004.12.an8
  - kmod-intel-QAT20 升级至 kmod-intel-QAT20-5.10.134~16-L.0.9.4~00004~2.an8
  - intel-ipp-crypto-mb 升级至 intel-ipp-crypto-mb-1.0.6-3.an8
  - intel-accel-plugin 升级至 intel-accel-plugin-0.27-1.an8
  - kmod-intel_dlb2 升级至 kmod-intel_dlb2-5.10.134~16-8.2.0~1.an8
  - kmod-udma 升级至 kmod-udma-5.10.134~16-0.1.0~2.an8
+ **Inter EMR 特性支持。** [贡献团队：Intel Arch SIG]
  - mcelog 更新至 mcelog-194-1.an8
  - numatop 更新至 numatop-2.4-1.an8
  - accel-config 更新至 accel-config-3.5.3-1.an8

### 3.5 L3 层（应用层）软件
#### 3.5.1 应用工具
+ 引入  KeenTune 作为系统智能优化工具，可选择替代 tuned 使用。[贡献团队：系统运维 SIG]
+ qemu-kvm 更新至 6.2.0-41.0.1 版本，edk2 更新至 20220126gitbb1bba3d77 版本，支持海光 CSV3 热迁移特性，修复了 CVE-2023-3019 问题。[贡献团队：海光信息]

#### 3.5.2 应用库
+ libvirt 更新至 8.0.0-22 版本，删除 tpm-tis arch 验证，修复了 CVE-2023-2700 问题。

### 3.6 其他层
+ 集成自研 llvm 编译器 alibaba-cloud-compiler ，针对大规模云业务场景进行了优化。
+ 集成自研 plugsched，作为内核调度器子系统热升级的 SDK，它可以实现在不重启系统、应用的情况下动态替换调度器子系统。
+ 新增 dragonwell 17 版本，同时提供 dragonwell 8/11/17 三个版本替代默认的 Open JDK。
+ 新引入 gcc-toolset-13 工具链。

### 3.7 场景化组件
#### 3.7.1 桌面场景
+ **DDE**
  - DDE 桌面组件从 dde-2021.07.03 版本更新至 dde-2022.1.4 版本。[贡献团队：DDE SIG]

## 4. 已知问题
+ [Bug 8358](https://bugzilla.openanolis.cn/show_bug.cgi?id=8358) - DDE 桌面鼠标拖拽窗口标题栏至屏幕顶端释放鼠标后，窗口偶现没有最大化。
+ [Bug 8616](https://bugzilla.openanolis.cn/show_bug.cgi?id=8616) - DDE 桌面计算机属性版本与控制中心关于本机的小版本显示不一致。

## 5. 特别声明
Anolis OS 8 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第 2 版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 6. 致谢
感谢统信软件、龙芯中科、浪潮信息、中兴通讯、新华三、海光信息等（排名不分先后）各方对 Anolis OS 8.9 版本的大力支持。

## 7. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
<!-- endtoc -->
