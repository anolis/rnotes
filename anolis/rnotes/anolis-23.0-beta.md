Anolis OS 23.0 公测版 (Beta) 发行声明
=====================================

<!-- toc -->
## 1. 引言
龙蜥操作系统 Anolis OS 23 是 OpenAnolis 龙蜥社区基于操作系统分层分类理论，面向上游原生社区独立选型；原生打造全栈软硬件协同的下一代操作系统版本。支持多架构计算，并提供安全、稳定的操作系统支持。

当前 Anolis OS 23 最新的版本号为 Anolis OS 23.0 公测版 (Beta).

## 2. 交付物清单
{% hint style='tip' %}
该版本发布的所有交付物清单及下载链接，可以在[社区网站](https://openanolis.cn/download)中找到详细信息。
{% endhint %}

### 2.1 ISO 镜像
名称 | 描述
-----|-----
AnolisOS-23-beta-x86\_64-dvd.iso  | x86\_64 架构的基础安装 ISO, 约 5GB
AnolisOS-23-beta-x86\_64-boot.iso | x86\_64 架构的网络安装 ISO，约 0.6GB
AnolisOS-23-beta-aarch64-dvd.iso  | aarch64 架构的基础安装 ISO，约 5GB
AnolisOS-23-beta-aarch64-boot.iso | aarch64 架构的网络安装 ISO，约 0.6GB

### 2.2 虚拟机镜像
名称 | 描述
-----|-----
anolis-23-beta-x86\_64.vhd | x86\_64 架构 QEMU 虚拟机镜像 (vhd 格式)
anolis-23-beta-aarch64.vhd | aarch64 架构虚拟机镜像

{% hint style='info' %}
镜像缺省 sudo 用户为 `anuser`，对应登录密码是 `anolisos`.
{% endhint %}

### 2.3 容器镜像
名称 | 描述
-----|-----
anolis-23-beta-x86\_64-docker.tar | x86\_64 架构本地容器镜像
anolis-23-beta-aarch64-docker.tar | aarch64 架构本地容器镜像

### 2.4 软件 YUM 仓库
名称 | 描述
-----|-----
BaseOS    | BaseOS 软件包源，该源目的是提供安装基础的所有核心包。
AppStream | AppStream 软件包源，该源提供额外的多场景，多用途的用户态程序，数据库等。

## 3. 发布详情
### 3.1 发行版整体支持
Anolis OS 23.0 Beta 版本默认提供下列镜像介质：
+ ISO 镜像；
+ vhd 格式的虚拟机镜像，适用于 QEMU/KVM 虚拟化平台；
+ tar 压缩包格式的容器镜像，适用于运行不同引擎的容器场景。

### 3.2 平台支持
+ **体系结构支持**: Anolis OS 23.0 Beta 版本支持 x86\_64 架构、aarch64 架构。
+ **微架构支持**: Anolis OS 23.0 Beta 版本分别支持 x86\_64-v2 及 armv8-a 微架构，并提供对 armv9-a 的支持能力。
+ **CPU 芯片支持**: Anolis OS 23.0 Beta 版本提供对主流 CPU 芯片厂商的支持能力，详细支持情况如下：

厂商   | 架构    | CPU 型号
-------|---------|---------
Intel  | x86\_64 | 市售主流型号
AMD    | x86\_64 | 市售主流型号
海光   | x86\_64 | Hygon C86 7185 32-core Processor
飞腾   | aarch64 | Phytium FT2000+/64, Phytium S2500/64
兆芯   | aarch64 | Zhaoxin KH-37800D
平头哥 | aarch64 | YT710
鲲鹏   | aarch64 | Kunpeng-920

+ **虚拟化平台支持**: Anolis OS 23 Beta 提供对如下虚拟化平台的支持能力：
    + QEMU/KVM

### 3.3 版本高亮说明

1. 提供双内核机制，anck-5.10 作为默认内核，同时提供滚动更新内核版本为 anck-5.19；
2. 支持 x86\_64 和 aarch64 架构，以及倚天、Intel、海光、鲲鹏等芯片，适配 x86 及 arm64 主流服务器硬件；
3. 默认使能 cgroup v2；
4. 根文件系统模式使用 xfs；
5. 工具链选用 gcc12/glibc2.35/binutils2.38，并选择 x86-64-v2 作为默认微架构选型；
6. openssl 选用 3.0.7 版本，提供更多的新协议，新算法以及其他各个方面的优化支持；
7. 提供 dragonwell 8/11/17 版本的Java运行时组件，并作为默认的Java运行时组件；
8. 提供性能调优组件 keentune；
9. 提供运维组件 sysak/sysom/ssar；
10. 提供Intel SPR用户态工具；
11. 提供应用组件，nginx/httpd/mysql/mariadb/redis/php/nodejs/memcached/tomcat；
12. 提供云原生组件，containerd/kubernetes。

## 4. 已知问题
该部分描述当前公测版的已知问题，龙蜥社区正在积极解决问题。
1. sysak 部分命令报错。([BZ2547](https://bugzilla.openanolis.cn/show_bug.cgi?id=2547))
2. 安装 sysom 后，服务启动报错，需要手动修改selinux配置。（[BZ2534](https://bugzilla.openanolis.cn/show_bug.cgi?id=2534)）
3. 部分环境内核切换至 5.19 后出现启动失败。（[BZ2551](https://bugzilla.openanolis.cn/show_bug.cgi?id=2551)）
4. SPR 机器上启动后日志有报错。（[BZ2588](https://bugzilla.openanolis.cn/show_bug.cgi?id=2588)）
5. chrt 修改进程调度策略失败。（[BZ2664](https://bugzilla.openanolis.cn/show_bug.cgi?id=2664)）

## 5. 特别声明
Anolis OS 23 操作系统发行版不提供任何形式的书面或暗示的保证或担保。

该发行版作为木兰宽松许可证第2版发布，发行版中的各个软件包都带有自己的许可证，木兰宽松许可证的副本包含在分发媒介中。

使用过程请参照发行版各软件包许可证。

## 6. 致谢
忠心感谢参与和协助 OpenAnolis 龙蜥社区的所有成员，尤其是产品发布兴趣小组和测试兴趣小组是你们的辛勤付出，以及对开源的热爱才保障版本顺利发布，也为龙蜥操作系统 Anolis OS 23 更好地发展提供无限空间！

## 7. 反馈
+ [Bug 跟踪](https://bugzilla.openanolis.cn/)
+ [邮件列表讨论](http://lists.openanolis.cn/)
<!-- endtoc -->
