# 4.19.91-27
发布时间：2023-01-04  
发布链接：
- [anolis8](https://anas.openanolis.cn/errata/detail/ANSA-2023:0002)
- [anolis7](https://anas.openanolis.cn/errata/detail/ANSA-2023:0001)

## 内核更新
  - 版本更新至 4.19.91-27
  - 重要内核缺陷及安全漏洞(CVE)修复
  - 在namespace_unlock中使用synchronize_rcu_expedited加速rcu宽限期，使并发启动100个busybox容器的速度提升19%
  - 调整Trusted Platform Module驱动的缓冲区大小，避免上下文切换时因内存不足报错
  - 默认使能mq-deadline io调度器
  - 提升NVMe、megaraid_sas和mpt3sas三个驱动的稳定性
  - 全面支持Aero系列raid卡
  - 修复了飞腾处理器 SMMU 的硬件缺陷导致的问题

支持以下阿里云自研技术：
  - 支持动态开启Group Identity特性
  - 支持稀疏文件映射使用系统零页，减少启动虚拟机时的内存消耗
## CVE修复列表
- CVE-2021-33656
- CVE-2021-4037
- CVE-2021-4159
- CVE-2022-0001
- CVE-2022-0002
- CVE-2022-0494
- CVE-2022-1012
- CVE-2022-1048
- CVE-2022-1184
- CVE-2022-1198
- CVE-2022-1462
- CVE-2022-1679
- CVE-2022-1729
- CVE-2022-1734
- CVE-2022-21125
- CVE-2022-21166
- CVE-2022-2153
- CVE-2022-2318
- CVE-2022-24958
- CVE-2022-2503
- CVE-2022-25258
- CVE-2022-2586
- CVE-2022-2588
- CVE-2022-2602
- CVE-2022-26365
- CVE-2022-2639
- CVE-2022-26490
- CVE-2022-27223
- CVE-2022-28388
- CVE-2022-28389
- CVE-2022-28390
- CVE-2022-2978
- CVE-2022-30594
- CVE-2022-3176
- CVE-2022-3202
- CVE-2022-32250
- CVE-2022-3542
- CVE-2022-36879
- CVE-2022-36946
- CVE-2022-39188