# 5.10.134-13
发布时间: 2023-01-10  
发布链接: [anolis8](https://anas.openanolis.cn/errata/detail/ANSA-2023:0006)

## 内核更新
- 版本更新至 5.10.134-13
- 重要内核缺陷及安全漏洞(CVE)修复
- 支持用户态/dev/ioasid  
在ANCK 5.10-013以前的版本中，对于直接分配给用户态的设备，VFIO，vDPA等用户态直通框架需要使用自己的逻辑来隔离用户态发起的DMA（这些DMA请求往往不可信）。从ANCK 5.10-013版本开始，引入了/dev/ioasid，提供了统一的接口用于管理I/O页表，简化了VFIO，vDPA的实现。

- SWIOTLB机制性能优化  
在 ANCK 5.10-013以前的版本中，与外设通信使用的swiotlb机制在分配内存时仅使用单个锁；从 ANCK 5.10-013 版本开始，该锁被被拆分为了多个并允许用户配置；当前受益方主要为大规格（如CPU数量大于32）的机密虚拟机（AMD SEV/Intel TDX），对于Redis和Mysql，测试显示此改动最大可以获得8倍的IO性能增幅。

- virtio-net 打开 napi.tx 优化 TCP Small Queue 性能  
在3bedc5bca69d ('ck: Revert "virtio_net: enable napi_tx by default"') 中, 由于一些特殊场景下的高 si 导致了性能下降。但是这样导致 tcp small queue 不能正常工作, 所以我们重新打开这个功能。

- 支持AST2600 PCIe 2D VGA Driver  
在ANCK 5.10-013以前的版本中，不支持aspeed的ast2600显卡，在ANCK 5.10-013版本之后开始支持aspeed的ast2600显卡，在外接显示器时能够正常显示画面。

- 支持FT2500处理器  
此版本添加了对飞腾 FT2500 处理器的支持，ANCK 5.10-013 目前可以正常运行在 FT2500 机器上。更新内容主要包括添加了 FT2500 its 驱动、rtc 驱动、clock frequency 支持以及相关 bug 修复等。

- 支持动态开启Group identity特性  
在ANCK 5.10-013版本，group identity特性增加了全局sysctl开关，默认关闭，用以降低普通进程的调度开销，执行命令 "echo 1 > /proc/sys/kernel/sched_group_identity_enabled" 开启开关。

- arm64平台默认内核启动cmdline调整  
从5.10.134-013版本开始，arm64平台会添加以下参数到boot cmdline中以提升性能。
cgroup.memory=nokmem iommu.passthrough=1 iommu.strict=0
    - cgroup.memory=nokmem，cgroup.memory使能后会在slab管理的page分配和释放流程上增加额外的处理逻辑，对性能造成影响，关闭该功能可提升性能。详情请参考：https://openanolis.cn/sig/Cloud-Kernel/doc/721476463521955981
    - iommu.passthrough=1 iommu直通模式，如果不显式指定则以CONFIG_IOMMU_DEFAULT_PASSTHROUGH的配置来控制，可减少页表映射的转换，此参数对物理机有效。
    - iommu.strict=0 表示TLB invalidation使用lazy模式，即DMA unmap时，推迟对应TLB的失效动作以提升吞吐量，加快unmap速度，如果IOMMU driver不支持则会自动回退到strict=1的严格模式，即DMA unmap操作的同时使TLB无效。

- 添加 Compact Numa Aware (CNA) spinlock 功能支持  
从5.10.134-013版本开始，为qspinlock添加了numa感知功能。可以通过在boot cmdline中添加numa_spinlock=on或者numa_spinlock=auto开启此功能。
开启此功能后，可以使qspinlock在不同numa节点上的CPU竞争spinlock时尽量将锁交给同一numa节点的CPU，以减少跨numa的次数，从而提高性能。目前在sysbench以及leveldb的满足应用场景的benchmark中，能获得10%以上的性能提升效果。

- 丰富arm64的perf mem和perf c2c功能  
从5.10.134-013版本开始，丰富了arm64中的perf mem，perf c2c功能。在arm64平台，可以通过perf mem/perf c2c显示样本的数据来源，如L1 hit等；perf mem增加综合内存事件支持、综合指令事件支持和指令总延迟信息显示等；perf c2c 增加了节点信息定位等。

- fsck.xfs 支持日志恢复  
当宕机发生后，文件系统可能处于不一致状态，且日志没有恢复。在 xfsprogs-5.0.0-10.0.4 及以前版本，fsck.xfs 不支持日志恢复，可能导致系统重启后进入救援模式，需要系统管理员手动介入。
xfsprogs-5.0.0-10.0.5 开始支持日志恢复。系统管理员如需启用该能力，需要配置启动参数 fsck.mode=force 和 fsck.repair=yes。注意，目前该能力仅支持对系统盘生效。

- hugetext自适应按需大页  
从5.10.134-013版本开始，针对代码大页在x86平台下的缺陷（2M iTLB entries数量非常少），引入代码大页自适应处理功能，根据2M区域的PTE扫描热度，按照热点较高整合为大页的原则，控制代码大页的使用。简而言之，该功能主要是控制每个应用的代码大页使用数量，防止iTLB miss负优化。该特性主要针对JAVA类应用和代码段较大的应用（例如Oceanbase, Mysql）。

- 支持SGX动态内存管理  
在 ANCK 5.10 以前的版本中，不具备对 SGX动态内存管理 功能的支持；从 ANCK 5.10 版本开始，添加了对 SGX EDMM 功能的支持，因此新版本可以获得SGX动态内存管理的能力。

- 使能wireguard模块  
在ANCK 5.10-013以前的版本中，wireguard模块未被启用；从 ANCK 5.10 版本开始，将开启对wireguard模块的配置。WireGuard 是一种安全、高效且易用的IPSec替代品，它设计得非常通用且足够抽象，适用于大多数场景，并且很容易配置。

## CVE修复列表
- CVE-2021-4037
- CVE-2022-0171
- CVE-2022-1679
- CVE-2022-2585
- CVE-2022-2586
- CVE-2022-2588
- CVE-2022-2602
- CVE-2022-26373
- CVE-2022-2663
- CVE-2022-2905
- CVE-2022-2978
- CVE-2022-3028
- CVE-2022-3061
- CVE-2022-3169
- CVE-2022-3176
- CVE-2022-3435
- CVE-2022-3521
- CVE-2022-3524
- CVE-2022-3534
- CVE-2022-3535
- CVE-2022-3542
- CVE-2022-3545
- CVE-2022-3564
- CVE-2022-3565
- CVE-2022-3566
- CVE-2022-3567
- CVE-2022-3586
- CVE-2022-3594
- CVE-2022-3621
- CVE-2022-3623
- CVE-2022-3625
- CVE-2022-3628
- CVE-2022-3629
- CVE-2022-3633
- CVE-2022-3635
- CVE-2022-3646
- CVE-2022-3649
- CVE-2022-36946
- CVE-2022-39189
- CVE-2022-39190
- CVE-2022-39842
- CVE-2022-40307
- CVE-2022-40768
- CVE-2022-41222
- CVE-2022-41674
- CVE-2022-42719
- CVE-2022-42720
- CVE-2022-42721
- CVE-2022-42722
- CVE-2022-42895
- CVE-2022-42896
- CVE-2022-43750
- CVE-2022-4378
## 
# 5.10.134-13.1
发布时间: 2023-02-07  
发布链接: 请参考[yum源](https://mirrors.openanolis.cn/anolis/8.6/Experimental/)
## 内核更新
- 修复CVE: CVE-2022-4696